---
title: New Cwtch Nightly (v1.11.0-74-g0406)
description: "In this development log we take a look at the new Cwtch Nightly"
slug: cwtch-nightly-v.11-74
tags: [cwtch, cwtch-stable, nightly]
image: /img/devlog10_small.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
- name: Sarah Jamie Lewis
  title: Executive Director, Open Privacy Research Society
  image_url: /img/sarah.jpg
---

We are getting close to a 1.12 release. This week we are drawing attention to the latest Cwtch Nightly (2023-06-05-17-36-v1.11.0-74-g0406) that is now available for wider testing.

As a reminder, the Open Privacy Research Society have [also announced they are want to raise $60,000 in 2023](https://openprivacy.ca/discreet-log/38-march-2023/) to help move forward projects like Cwtch. Please help support projects like ours with a [one-off donations](https://openprivacy.ca/donate) or [recurring support via Patreon](https://www.patreon.com/openprivacy).

![](/img/devlog10.png)

<!--truncate-->

### New Nightly

There is a [new Nightly build](https://docs.cwtch.im/docs/contribute/testing#cwtch-nightlies) are available from our build server. The latest nightly we recommend testing is [2023-06-05-17-36-v1.11.0-74-g0406](https://build.openprivacy.ca/files/flwtch-2023-06-05-17-36-v1.11.0-74-g0406/).

This version has a large number of improvements and bug fixes including:

* A new Font Scaling setting
* Several networking and connection management improvements including automatic detection and response to network changes, and several bug fixes that impacted time-to-connection after a resetting Tor.
* Updated UI font styles
* Dependency updates, including a new base of Flutter 3.10.
* A fix for stuck file downloading notifications on Android
* A fix for missing profile images in certain edge cases on Android
* Japanese, Swedish, and Swahili translation options
* A new retry peer connection button for prompting Cwtch to prioritize specific connections
* [Tails support](/docs/platforms/tails)

In addition, this nightly also includes a number of performance improvements that should fix reported rendering issues on less powerful devices.

Please see the contribution documentation for advice on [submitting feedback](/docs/contribute/testing#submitting-feedback)

Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

