---
title: Availability Status and Profile Attributes
description: "Two new Cwtch features are now available to test in nightly: Availability Status and Profile Information."
slug: availability-status-profile-attributes
tags: [cwtch, cwtch-stable, nightly]
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
- name: Sarah Jamie Lewis
  title: Executive Director, Open Privacy Research Society
  image_url: /img/sarah.jpg
---

Two new Cwtch features are now available to test in nightly: [Availability Status](/docs/profiles/availability-status) and [Profile Information](/docs/profiles/profile-info).

Additionally, we have also published draft guidance on [running Cwtch on Tails](/docs/platforms/tails) that we would like volunteers to test and report back on.
 
The Open Privacy Research Society have [also announced they are want to raise $60,000 in 2023](https://openprivacy.ca/discreet-log/38-march-2023/) to help move forward projects like Cwtch. Please help support projects like
ours with a [one-off donations](https://openprivacy.ca/donate) or [recurring support via Patreon](https://www.patreon.com/openprivacy).

<!--truncate-->


## Availability Status

New in this nightly is the ability to notify your conversations that you are "Away" or "Busy".

<figure>

[![](/img/profiles/status-tooltip-busy-set.png)](/img/profiles/status-tooltip-busy-set.png)

<figcaption></figcaption>
</figure>

Read more: [Availability Status](/docs/profiles/availability-status)

## Profile Attributes

Also new is the ability to augment your profile with a few small pieces of **public** information.

<figure>

[![](/img/profiles/attributes-set.png)](/img/profiles/attributes-set.png)

<figcaption></figcaption>
</figure>

Read more: [Profile Information](/docs/profiles/profile-info)
  
## Downloading the Nightly

[Nightly builds](https://docs.cwtch.im/docs/contribute/testing#cwtch-nightlies) are available from our build server. Download links for **2023-04-05-18-28-v1.11.0-7-g0290** are available below.

* Windows: [https://build.openprivacy.ca/files/flwtch-win-2023-04-05-18-28-v1.11.0-7-g0290/](https://build.openprivacy.ca/files/flwtch-win-2023-04-05-18-28-v1.11.0-7-g0290/)
* Linux: [https://build.openprivacy.ca/files/flwtch-2023-04-05-18-27-v1.11.0-7-g0290/](https://build.openprivacy.ca/files/flwtch-2023-04-05-18-27-v1.11.0-7-g0290/)
* Mac: [https://build.openprivacy.ca/files/flwtch-macos-2023-04-05-14-27-v1.11.0-7-g0290/](https://build.openprivacy.ca/files/flwtch-macos-2023-04-05-14-27-v1.11.0-7-g0290/)
* Android: [https://build.openprivacy.ca/files/flwtch-2023-04-05-18-27-v1.11.0-7-g0290/](https://build.openprivacy.ca/files/flwtch-2023-04-05-18-27-v1.11.0-7-g0290/)

Please see the contribution documentation for advice on [submitting feedback](/docs/contribute/testing#submitting-feedback)

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

