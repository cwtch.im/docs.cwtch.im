---
title: Cwtch UI Platform Support
description: "This development log captures the current state of Cwtch platform support, and how we plan to make platform support decisions going forward are we move towards Cwtch Stable."
slug: cwtch-platform-support
tags: [cwtch, cwtch-stable,support]
image: /img/devlog4_small.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
- name: Sarah Jamie Lewis
  title: Executive Director, Open Privacy Research Society
  image_url: /img/sarah.jpg
---

One of the [tenets for Cwtch Stable is **Universal Availability and Cohesive Support**](https://docs.cwtch.im/blog/path-to-cwtch-stable#tenets-of-cwtch-stable):

> "People who use Cwtch understand that if Cwtch is available for a platform then that means all features will work as expected, that there are no surprise limitations, and any differences are well documented. People should not have to go out of their way to install Cwtch."

This development log seeks to capture the current state of Cwtch platform support, and how we plan to make platform support decisions going forward as we move towards Cwtch Stable.

The questions we aim to answer in this post are: 

- What systems do we currently support?
- How do we decide what systems are supported?
- How do we handle new OS versions?
- How does application support differ from library support?
- What blockers exist for systems we wish to support, but currently cannot e.g ios?

![](/img/devlog4.png)

<!--truncate-->

## Constraints on support

From CPU architecture, to app store policies, there are a large number of constraints that restrict what platforms Cwtch can target, and how usable Cwtch may be on those systems. 

In this section we will highlight the restrictions that we are aware of, and provide a summary of the major external forces that impact our ability to support Cwtch across various platforms.

### Limitations on general-purpose computing 

In order for Cwtch to work, and be useful, it needs the ability to launch and manage long-lived onion services (in addition to Tor connections to *other* onion services). 

On desktop platforms this is usually a given, but the ability to do that kind of activity on mobile operating systems is severely limited or, in many cases, **blocked entirely**. 

This is the core reason why Cwtch is not available on iOS, and the main reason why Android support often lags behind.

While we expect that [Arti](https://gitlab.torproject.org/tpo/core/arti) will improve the management of onion services and connections, there is no way around the need to have an active process managing such services. 

As Appstore restrictions are tightened, and mobile operating systems are likewise restricted, we expect that Cwtch on mobile will have to move to a light-client model, requiring the aid of a companion desktop application to be usable.

We encourage you to support mobile operating system vendors who understand the value of general purpose computing, and who don't place restrictions on what you can do with your own device.

### Constraints introduced by the Flutter SDK

The Cwtch UI is based on Flutter, and as such we have some hard boundaries driven by [platforms that are supported by the Flutter SDK](https://docs.flutter.dev/development/tools/sdk/release-notes/supported-platforms).

To summarize, as of writing this document those platforms are:

- Android API 16 and above (arm, arm64, and amd64)
- Debian-based Linux Distributions (64-bit only)
- macOS El Capitan (10.11) and above
- Windows 7 & above (64-bit only)

To put it plainly, without porting Cwtch UI to a different UI platform **we cannot support a 32-bit desktop version**.

### Constraints introduced by Appstore Policy   

As of writing, [Google is pushing applications to target API 31 or above](https://developer.android.com/google/play/requirements/target-sdk). This target API version is increased on a regular cadence and usually packaged with greater restrictions on what applications can do. To put it another way, even if our minimum theoretical supported Android version is 16, we are practically limited to a subset of tolerated functionality.

### CPU Architecture and Cwtch Bindings

We currently build the Cwtch UI and Cwtch Bindings for a wide variety of platform/architecture combinations (see the table below). Our ability to support a given architecture is driven primarily by the overlap of Go Compiler support, Flutter SDK support, and what architectures the underling operating system is available for.

It is worth noting that there is an explicit dependency between the Bindings and the UI. If we cannot build Cwtch Bindings for a given architecture (i.e. if the Go Compiler does not support a given architectures), then we also cannot offer the Cwtch UI for that architecture.

| Architecture  / Platform | Windows | Linux | macOS | Android |
|--------------------------|---------|-----|-------| -------------|
| arm                      |    ❌     | ❌   | ❌     | ✅️| 
| arm64                    |     ❌    |  🟡   |   ✅      | ✅️ | 
| x86-64  / amd64          | ✅       | ✅   | ✅️     | ✅️ |

"🟡" - indicates that support is possible, but not yet official e.g. arm64 linux (Raspberry Pi).

### Testing and official support

As a non-profit, and an open source software project, we are limited in the resources we have to invest. We rely on the [Cwtch Release Candidate Testers](https://docs.cwtch.im/docs/contribute/testing#join-the-cwtch-release-candidate-testers-group) to do much of the heavy lifting when it comes to Cwtch support on various platforms. This is especially true when it comes to Android variants where, even after testing across the spread of devices available to the Cwtch team, testers still encounter major issues.

We officially only perform full scale automated tests on Linux. With minimal platform regression tests on Windows, Android and OSX. Prior to Cwtch Stable we plan to have support for running automated regression tests across Linux, Windows and Android instances.

### End-of-life platforms

Operating Systems are never supported indefinitely. The Flutter SDK may allow support for Windows 7, but Microsoft no longer does. [Windows 7 fell out of support on January 14, 2020](https://www.microsoft.com/en-us/windows/end-of-support), Windows 8 followed early this month, on January 10th. 2023. Windows 10 will no longer be support after October 14, 2025.

Likewise, while the Flutter SDK official supports OSX versions back to El Capitan (version 10.11), the oldest OSX version currently supported by Apple is Big Sur (version 11). While it may be possible for us to build different versions of Cwtch targeting different OSX versions, we would be doing so against unsupported SDK versions - incurring not only a support cost, but a possible security one also.

The same fundamental restrictions also impact Linux based distributions. While Flutter supports Ubuntu 18.04, and the platform still receiving updates until April 2023, the Cwtch team does not, because of the outdated version of libc installed on the platform would require a distinct build process. [Cwtch currently requires libc 2.31+](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#linux-specific-considerations).

Android versions prior to Android 10 are no longer officially support, and the requirement to target the most recent versions of Android for inclusion on the Google Playstore mean that long term support for Android versions is driven almost entirely by Google. While Flutter technically has support for Android 16 and above (and we target that as a minimum SDK version), because we have to target the most recent SDK for inclusion on Google Playstore, we cannot make guarantees that these SDKs are fully backwards compatible. We encourage volunteers interested in Cwtch Android to join our [Cwtch Release Candidate Testers groups](https://docs.cwtch.im/docs/contribute/testing#join-the-cwtch-release-candidate-testers-group) to help us understand the limitations of Android support across different API versions.

## How we decide to officially support a platform

To help make decisions on what platforms we target for official builds, the Cwtch team have developed four key tenets:

1. **The target platform needs to be officially supported by our development tools** -  We do not have the resources to maintain forks of the Go compiler or the Flutter SDK that target other operating systems or architectures. The one exception to this rule are non-Debian Linux distributions which while not officially supported by Flutter, are unlikely to have major blockers to official support.
2. **The target operating system needs to be supported by the Vendor** -  We cannot support a platform that is no longer receiving security updates. Nor do we have the resources to maintain distinct build environments that target out-of-support operating systems. While Cwtch may run on these platforms without additional assistance, we will not schedule work to fix broken support on such platforms. (We may, however, accept Pull Requests from volunteers).
3. **The target platform must be backwards compatible with the most recent version in general use** - Even if a system is technically supported by our development tools, and still receives security updates from the vendor, we may still be unbale to officially support it if doing so requires maintaining a separate build environment (because SDK or APIs of dependent libraries are no longer backwards compatible). Like above, Cwtch *may* run on these platforms without additional assistance, but we will not schedule work to fix broken support on such platforms. (we may, however, accept Pull Requests from volunteers).
4. **People want to use Cwtch on that platform** - We will generally only consider new platform support if people ask us about it. If Cwtch isn't available for a platform you want to use it on, then please get in touch and ask us about it!

## Summary of official support

The table below represents our current understanding of Cwtch support across various operating systems and architectures (as of Cwtch 1.10 and January 2023). 

In many cases we are looking for testers to confirm that various functionality works. A version of this table will be [maintained as part of the Cwtch Handbook](/docs/getting-started/supported_platforms).

**Legend:**

- ✅: **Officially Supported**. Cwtch should work on these platforms without issue. Regressions are treated as high priority.
- 🟡: **Best Effort Support**. Cwtch should work on these platforms but there may be documented or unknown issues. Testing may be needed. Some features may require additional work. Volunteer effort is appreciated.
- ❌: **Not Supported**. Cwtch is unlikely to work on these systems. We will probably not accept bug reports for these systems.



| Platform                    | Official Cwtch Builds |  Source Support    | Notes                                                                                                                             |
|-----------------------------|-----------------------|--------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| Windows 11                  | ✅                     |   ✅    | 64-bit amd64 only.                                                                                                                |
| Windows 10                  |✅                   | ✅     | 64-bit amd64 only. Not officially supported, but official builds may work.                                                        |
| Windows 8 and below         | ❌                     | 🟡     | Not supported. Dedicated builds from source may work. Testing Needed.                                                             |
| OSX 10 and below            | ❌                    | 🟡     | 64-bit Only. Official builds have been reported to work on Catalina but not High Sierra                                           |
| OSX 11                      | ✅                     | ✅      | 64-bit Only. Official builds supports both arm64 and x86 architectures.                                                           |
| OSX 12                      | ✅                     | ✅      | 64-bit Only. Official builds supports both arm64 and x86 architectures.                                                           |
| OSX 13                      | ✅                     | ✅      | 64-bit Only.  Official builds supports both arm64 and x86 architectures.                                                          |
| Debian 11                   | ✅                     | ✅     | 64-bit amd64 Only.                                                                                                                |
| Debian 10                   | 🟡                     | ✅      | 64-bit amd64 Only.                                                                                                                |
| Debian 9 and below          | 🟡                    | ✅     | 64-bit amd64 Only. Builds from source should work, but official builds may be incompatible with installed dependencies.           |
| Ubuntu 22.04                | ✅                     | ✅      | 64-bit amd64 Only.                                                                                                                |
| Other Ubuntu                | 🟡                    | ✅     | 64-bit Only. Testing needed. Builds from source should work, but official builds may be incompatible with installed dependencies. | 
| CentOS                      | 🟡                    | 🟡     | Testing Needed.                                                                                                                   |
| Gentoo                      | 🟡                    | 🟡     | Testing Needed.                                                                                                                   |
| Arch                        | 🟡                    | 🟡     | Testing Needed.                                                                                                                   |
| Whonix                      | 🟡                     | 🟡    | [Known Issues. Specific changes to Cwtch are required for support. ](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/550)     |
| Raspian (arm64)             | 🟡                    | ✅  | Builds from source work.                                                                                                          |
| Other Linux Distributions   | 🟡                    | 🟡    | Testing Needed.                                                                                                                   |
| Android 9 and below         | 🟡                    | 🟡     | Official builds may work.                                                                                                         |
| Android 10                  | ✅                     | ✅     | Official SDK supprts arm, arm64, and amd64 architectures.                                                                         |
| Android 11                  | ✅                     | ✅      | Official SDK supprts arm, arm64, and amd64 architectures.                                                                         |
| Android 12                  | ✅                     | ✅      | Official SDK supprts arm, arm64, and amd64 architectures.                                                                         |
| Android 13                  | ✅                     | ✅      | Official SDK supprts arm, arm64, and amd64 architectures.                                                                         |
| LineageOS                   | 🟡                    |  🟡    | [Known Issues. Specific changes to Cwtch are required for support.](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/607)      |
| Other Android Distributions | 🟡                    | 🟡      | Testing Needed.                                                                                                                   |

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

