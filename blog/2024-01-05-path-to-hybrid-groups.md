---
title: "Path to Hybrid Groups"
description: "A look at how we plan on implementing the next generation of Cwtch multi-party messaging"
slug: path-to-hybrid-groups
tags: [cwtch, hybrid-groups]
image: /img/hybridgroups.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
- name: Sarah Jamie Lewis
  title: Executive Director, Open Privacy Research Society
  image_url: /img/sarah.jpg
---

Back in [September 2023 we released Cwtch 1.13](/blog/cwtch-1-13), the first version of Cwtch to be labelled as **stable**, 
and a major milestone in Cwtch development. 

With the Cwtch interface now stable, we are in a position to begin a new phase in Cwtch development: a Path towards
**Hybrid Groups**.

![](/img/hybridgroups.png)
 
<!--truncate-->

## The Problem with Cwtch Groups

One of the unique features of Cwtch is that [groups](/docs/groups/introduction) are dependent on [untrusted infrastructure](/security/components/cwtch/server).

Because of this, at their most basic, a Cwtch group is simply an agreement between a set of peers on a common
cryptographic key, and a common (set of) untrusted server(s).

This provides Cwtch Groups with very nice properties such as anonymity to anyone not in the group, but it does mean
that certain other nice properties like member flexibility, and credential rotation are difficult to achieve.

We want to allow people to make the right trade-off when it comes to their own risk models, i.e. to be able to trade
efficiency for trust when that decision makes sense.

To do that we need to introduce a new class of group into Cwtch, something we are calling **Hybrid Groups**.

## What Are Hybrid Groups?

The goal of hybrid groups is to balance the security properties of Cwtch peer-to-peer communication with the
properties of untrusted infrastructure. 

This is done by augmenting existing Cwtch Groups with an additional layer of peer-to-peer communication in order to provide
efficient participant management, key rotation, and other useful features.

### Levels of Hybrid Groups

In practice, we imagine there will be a few different levels of Hybrid Group, reflecting different trade-offs between inter-peer trust,
communication efficiency, and group security.

There are **Traditional Groups**, these have similar properties to the existing Cwtch Groups. Highly inefficient, but essentially 
require zero-trust on behalf of participants other than an expectation that the key is kept secret.

We plan to introduce **Managed Groups**: A new kind of group where all participants explicitly trust a given always-online peer (e.g. a bot) with group operations. These 
will be highly efficient, at the cost of that explicit trust (if that peer behaves maliciously then certain properties are broken). Managed groups will
be the first Cwtch groups to allow **Contractable** and **Expandable** groups, and more efficient **Key Rotation**.

To start with this *trusted peer* will take the form of an external bot (powered by [a cwtch bot framework](/developing/building-a-cwtch-app/building-an-echobot)) however we 
eventually plan to expose this capability as part of the Cwtch UI.

And finally a category of **Augmented Groups**: An extension of Managed Groups that places configurable restrictions of the trust given to 
the peer e.g. by requiring participants to take part in a meta-protocol that confirms certain actions before they are carried out (preventing
the trusted-peer from harming properties like **Participant Consistency**.

## Group Messaging Metadata

As with the rest of Cwtch, our ultimate goal is that no metadata (and specifically as part of this work, no group metadata e.g. membership, message timing) be
available to a party outside of the group.

Traditional Cwtch Groups take this to the extreme, and the expense of long syncing times, and a high possibility of disruption. Managed Groups
and Augmented groups will allow communities to make the right trade-offs allowing for greater resilience and faster syncing.

## A Rough Timeline (Q1: Week 0 - Week 10 2024)

- **Week 0** - Planning Q1 Cwtch Timeline (this devlog), minor bug fixes and other small UI-focused work originating from reports and feedback
from [Cwtch testers](/docs/contribute/testing).
- **Week 1** - Work begins on exposing **Enhanced Permissions** in the Cwtch library. These are essential to implementing many of the aspects
of the new group design, as well as improving other parts of contact management. (Expect more about this in a future devlog). Also, a formal model for Managed Groups will be created and documented. 
This will form the basis of the implementation.
- **Week 2** - At this point we should be able to begin designing the Managed Group Extension to Cwtch. This will use the Cwtch Event Hooks API
to respond to Peer events to manage groups. During this work, we also expect to migrate the legacy group code into it's own similar extension to make
best use of the APIs. 
- **Week 3** - Towards the end of January we expect to have a complete formal model of Managed Groups and to be able to start integrating the new extensions into the
Cwtch-UI. We also expect to be in the process of releasing a new 1.14 version of Cwtch that supports Enhanced Permissions.
- **Weeks 4 - Week 6** - February marks the 6th anniversary of the founding of [Open Privacy Research Society](https://openprivacy.ca), and our organizational year end. During this
time core members of the Cwtch team are often involved in administrative tasks that need to be done during this time, as such we are not planning to make too much progress on Cwtch during this time.
- **Weeks 7 - Week 10** - As we approach March, we will be formally integrating Managed Groups in Cwtch, and planning a Cwtch 1.15 release which will feature the new group type. During this time we will also be updating
Cwtch [Group Documentation](https://docs.cwtch.im/docs/category/groups) .

Once Managed Groups have been rolled out, we will assess what we have learned and proceed with similar steps for 
Augmented Groups in Q2 (more on that in a later devlog!).

## Stay up to date!

As always, we will be regularly updating this devlog [and other channels](https://fosstodon.org/@cwtch) as we continue to make progress towards
surveillance resistant infrastructure!

Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

