---
title: Cwtch Stable Roadmap Update
description: "Back in March we provided an update on several goals that we would have to hit on our way to Cwtch Stable, and the timelines to hit them. In this post we provide a new update on those goals"
slug: cwtch-stable-roadmap-update-june
tags: [cwtch, cwtch-stable, planning]
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
- name: Sarah Jamie Lewis
  title: Executive Director, Open Privacy Research Society
  image_url: /img/sarah.jpg
---

The next large step for the Cwtch project to take is a move from public **Beta** to **Stable** – marking a point at which we consider Cwtch to be secure and usable. We have been working hard towards that goal over the last few months.

This post [revisits the Cwtch Stable roadmap update](/blog/cwtch-stable-roadmap-update) we provided back in March, and provides an overview of the next steps on our journey towards Cwtch Stable.

![](/img/devlog1.png)
 
<!--truncate-->

## Update on the Cwtch Stable Roadmap

Back in March we extended and updated several goals from [our January roadmap](https://docs.cwtch.im/blog/path-to-cwtch-stable) that we would have to hit on our way to Cwtch Stable, and the timelines for achieving them. Now that we have reached target date of many of these goals, we can look back and see how work is progressing.

(✅ means complete, 🟡 means in-progress, 🕒 reprioritized)

- By **30th April 2023** the Cwtch team will have written the remaining outstanding documentation from the January roadmap including:
  - A Cwtch Release Process Document ✅ - [Release Process](https://docs.cwtch.im/developing/release/#official-releases)
  - A Cwtch Packaging Document ✅ - [Packaging Documentation](https://docs.cwtch.im/developing/release/)
  - Completion of documentation of existing Cwtch features, including relevant screenshots. 🟡 - new features are documented to the standards outlined in new [documentation style guide](/docs/contribute/documentation), and many older feature documentation features have been updated to that standard. Work is ongoing to refine the standard.
- By **30th April 2023** the Cwtch team will have also released developer-centric documentation including:
  - A guide to building Cwtch-apps using official libraries ✅ - [Building a Cwtch App](https://docs.cwtch.im/developing/category/building-a-cwtch-app)
  - Automatically generated API documentation for libCwtch 🕒  - this effort has been delayed pending other higher priority work. 
- By **30th June 2023** the Cwtch team will have released new Cwtch Beta releases (1.12+) featuring:
  - An implementation of [Conversation Search](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/129) 🟡 - currently in [active development](https://git.openprivacy.ca/cwtch.im/cwtch/pulls/518)
  - [Profile statuses](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/27) and other associated information ✅ - released in [Cwtch Beta 1.12](https://docs.cwtch.im/blog/cwtch-nightly-1-12)
  - An update to the network handling code to allow for [better Protocol Engine management](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/593) 🟡🕒 - new Network Management code was released in [Cwtch Beta 1.12](https://docs.cwtch.im/blog/cwtch-nightly-1-12). We now believe these changes will be complete in Cwtch Beta 1.13.
- By **31st July 2023** the Cwtch team will have completed several infrastructure upgrades including:
  - Extended reproducible builds to cover the Cwtch UI, or document where the blockers to achieving this exist. 🟡 - we have recently made a few updates to [Repliqate](https://git.openprivacy.ca/openprivacy/repliqate) to support this work, and expect to begin in-depth examination of build artifacts in the next couple of weeks.
  - Integration of automated fuzzing into the build pipeline for all Cwtch dependencies maintained by the Cwtch team 🕒 - after some initial explorations into new Go fuzzing tools we reached the conclusion that it would be better to replace this effort with other assurance work (see below).
  - New testing environments for F-droid, Whonix, Raspberry Pi and other [partially supported systems](/docs/getting-started/supported_platforms) 🟡 - we have already launched an environment for testing [Tails](/docs/platforms/tails). Other platforms are underway.
- By **31st August 2023** the Cwtch team will have a released Cwtch Stable Release Candidate:
  - At this point we expect that the Cwtch application and existing documentation will be robust and complete enough to be labeled as stable.
  - Along with this label comes a higher standard for how we consider all aspects of Cwtch development. The work we have done up to this point reflects a much stronger development pipeline, and an ongoing commitment to security.
  - **This does not mark an end to Cwtch development**, or new Cwtch features. But it does denote the point at which we consider Cwtch to be appropriate for wider use.


## Next Steps, Refinements, Additional Work

As you may have noticed above we have reprioritized some work after initial investigations forced us to reevaluate the expected cost/benefit trade-off. This has allowed us to move up timelines for tasks e.g. reproducible UI builds and testing environments. 

Other work has been reprioritized due to developer availability. Documentation work in particular has not progressed as fast as we would like.

However, [Cwtch Beta 1.12](https://docs.cwtch.im/blog/cwtch-nightly-1-12) featured many new features alongside improved performance, more robust packaging, and several fixes impacting experimental features like file sharing.

The work that we have done on reproducible and automatically generated bindings has considerably reduced the maintenance burden associated with updates and adding new features, and has allowed us to also tackle long standing issues related to Tor process managements and Cwtch startup.

We are still on track for releasing a Cwtch Stable release candidate in August 2023, with an official Cwtch Stable release expected shortly afterwards.

This is not all we have planned for the upcoming months. Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

## Get Involved

We have noticed an uptick in the number of people reaching out interested in contributing to Cwtch development. In order to help people get acclimated to our development flow we have created a new section on the main documentation site called [Developing Cwtch](/docs/contribute/developing) - there you will find a collection of useful links and information about how to get started with Cwtch development, what libraries and tools we use, how pull requests are validated and verified, and how to choose an issue to work on.

We also also updated our guides on [Translating Cwtch](/docs/contribute/translate) and [Testing Cwtch](/docs/contribute/testing).

If you are interested in getting started with Cwtch development then please check it out, and feel free to reach out to `team@cwtch.im` (or open an issue) with any questions. All types of contributions [are eligible for stickers](/docs/contribute/stickers).

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

