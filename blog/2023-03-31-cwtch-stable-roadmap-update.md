---
title: Cwtch Stable Roadmap Update
description: "Back in january we outlined several goals that we would have to hit on our way to Cwtch Stable, and the timelines to hit them. In this post we revisit those and announce some more"
slug: cwtch-stable-roadmap-update
tags: [cwtch, cwtch-stable, planning]
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
- name: Sarah Jamie Lewis
  title: Executive Director, Open Privacy Research Society
  image_url: /img/sarah.jpg
---

The next large step for the Cwtch project to take is a move from public **Beta** to **Stable** – marking a point at which we consider Cwtch to be secure and usable. We have been working hard towards that goal over the last few months.

This post [revisits the Cwtch Stable roadmap](/blog/path-to-cwtch-stable) we introduced at the start of the year, and provides an overview of the next steps on our journey towards Cwtch Stable.

![](/img/devlog1.png)
 
<!--truncate-->

## Update on the January Roadmap

Back in January we outlined several goals that we would have to hit on our way to Cwtch Stable, and the timelines for achieving them. Now that we have reached target date of the last of these goals, we can look back and see how we did:

(✅ means complete, 🟡 means in-progress, ❌ not started.)

- By **1st February 2023**, the Cwtch team will have reviewed all existing Cwtch issues in line with this document, and established a timeline for including them in upcoming releases (or specifically commit to not including them in upcoming releases). ✅
- By **1st February 2023**, the Cwtch team will have [finalized a feature set that defines Cwtch Stable](/blog/cwtch-stable-api-design) and established a timeline for including these features in upcoming Cwtch Beta releases. ✅
- By **1st February 2023**, the Cwtch team will have expanded the Cwtch Documentation website to include a section for:
  - [Security and Design Documents](/security/intro) ✅
  - Infrastructure and [Support](/docs/getting-started/supported_platforms) 🟡
  - in addition to a new development blog. ✅
- By **31st March 2023**, the Cwtch team will have created:
  - a [style guide for documentation](/docs/contribute/documentation), and ✅
  - have used it to ensure that all Cwtch features have consistent documentation available, 🟡
  - with at least one screenshot (where applicable). 🟡
- By **31st March 2023** the Cwtch team will have published: 
  - a Cwtch [Interface Specification Document](/blog/cwtch-stable-api-design) ✅
  - a Cwtch Release Process Document 🟡
  - a Cwtch [Support Plan document](/blog/cwtch-platform-support) ✅
  - a Cwtch Packaging Document 🟡
  - a document describing the [Reproducible Builds Process](/blog/cwtch-bindings-reproducible) ✅
  - These documents will be available on the newly expanded Cwtch Documentation website 🟡
- By **31st March 2023** the Cwtch team will have integrated automated UI tests into the build pipeline for the cwtch-ui repository. ✅
- By **31st March 2023** the Cwtch team will have integrated automated fuzzing into the build pipeline for all Cwtch dependencies maintained by the Cwtch team ❌
- By **31st March 2023** the Cwtch team will have committed to a date, timeline, and roadmap for launching Cwtch Stable ✅ (this post!)

While we didn't hit all of our goals, we did make progress on nearly all of them, and in addition also made progress in a few other key areas:

* [Cwtch Autobindings](/blog/autobindings) with [compile-time optional experiments](/blog/autobindings-ii)
* [Cwtch 1.11](/blog/cwtch-nightly-1-11) - with support for reproducible bindings, two new localizations (Slovak and Korean), in addition to a myriad of bug fixes and performance improvements.
* [Repliqate](https://git.openprivacy.ca/openprivacy/repliqate) - a tool for testing and confirming reproducible builds processes based on Qemu, and a Debian Cloud image.

## A Timeline for Cwtch Stable

Now for the big news, we plan on releasing a candidate Cwtch Stable release during **Summer 2023**. Here is our plan for getting there:

- By **30th April 2023** the Cwtch team will have written the remaining outstanding documentation from the January roadmap including:
  - A Cwtch Release Process Document
  - A Cwtch Packaging Document
  - Completion of documentation of existing Cwtch features, including relevant screenshots.
- By **30th April 2023** the Cwtch team will have also released developer-centric documentation including:
  - A guide to building Cwtch-apps using official libraries
  - Automatically generated API documentation for libCwtch
- By **30th June 2023** the Cwtch team will have released new Cwtch Beta releases (1.12+) featuring:
  - An implementation of [Conversation Search](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/129)
  - [Profile statuses](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/27) and other associated information
  - An update to the network handling code to allow for [better Protocol Engine management](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/593)
- By **31st July 2023** the Cwtch team will have completed several infrastructure upgrades including:
  - Extended reproducible builds to cover the Cwtch UI, or document where the blockers to achieving this exist.
  - Integration of automated fuzzing into the build pipeline for all Cwtch dependencies maintained by the Cwtch team
  - New testing environments for F-droid, Whonix, Raspberry Pi and other [partially supported systems](/docs/getting-started/supported_platforms)
- By **31st August 2023** the Cwtch team will have a released Cwtch Stable Release Candidate:
  - At this point we expect that the Cwtch application and existing documentation will be robust and complete enough to be labelled as stable.
  - Along with this label comes a higher standard for how we consider all aspects of Cwtch development. The work we have done up to this point reflects a much stronger development pipeline, and an ongoing commitment to security.
  - **This does not mark an end to Cwtch development**, or new Cwtch features. But it does denote the point at which we consider Cwtch to be appropriate for wider use.

This is not all we have planned for the upcoming months. Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

## Get Involved

We have noticed an uptick in the number of people reaching out interested in contributing to Cwtch development. In order to help people get acclimated to our development flow we have created a new section on the main documentation site called [Developing Cwtch](/docs/contribute/developing) - there you will find a collection of useful links and information about how to get started with Cwtch development, what libraries and tools we use, how pull requests are validated and verified, and how to choose an issue to work on.

We also also updated our guides on [Translating Cwtch](/docs/contribute/translate) and [Testing Cwtch](/docs/contribute/testing).

If you are interested in getting started with Cwtch development then please check it out, and feel free to reach out to `team@cwtch.im` (or open an issue) with any questions. All types of contributions [are eligible for stickers](/docs/contribute/stickers).

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

