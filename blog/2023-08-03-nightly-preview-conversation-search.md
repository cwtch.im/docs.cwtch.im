---
title: "Nightly Preview: Conversation Search"
description: "A new Cwtch Nightly contains a first cut of Conversation Search."
slug: cwtch-nightly-preview-conversation-search
tags: [cwtch, cwtch-stable, nightly, search, preview]
image: /img/devlog10_small.jpg
hide_table_of_contents: false
authors:
- name: Sarah Jamie Lewis
  title: Executive Director, Open Privacy Research Society
  image_url: /img/sarah.jpg
---

There is a [new Nightly build](https://docs.cwtch.im/docs/contribute/testing#cwtch-nightlies) are available from our build server. The latest nightly we recommend testing 
is [2023-08-02-20-24-v1.12.0-19-g75b7](https://build.openprivacy.ca/files/flwtch-2023-08-02-20-24-v1.12.0-19-g75b7/).

This nightly contains a first cut of Conversation Search, in addition to several bug fixes impacting effectiveness of the contact retry plugin when combined with a large contact list, and an unstable network
connection. Finally we have made a few tweaks to the font scaling based on feedback.

Please see the contribution documentation for advice on [submitting feedback](/docs/contribute/testing#submitting-feedback)

![](/img/search-nightly.png)
 
<!--truncate-->

## Stay up to date!

Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)



