---
title: Cwtch 1.13 Stable Release Candidate
description: Cwtch 1.13 (Stable Release Candidate)
slug: cwtch-1-13
tags:
  - cwtch
  - cwtch-stable
  - release
image: /img/picnic1.13.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
  - name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

[Cwtch 1.13 is now available for download](https://cwtch.im/download)!

Cwtch is a communication application (and associated libraries) that uses Tor v3 Onion Services to establish surveillance resistant channels between people. Cwtch has been designed to be
secure, private, and resilient.

Cwtch 1.13 is the culmination of the last few years of effort by the Cwtch team, and is the first release that meets our bar to be labelled a [Cwtch Stable](/blog/path-to-cwtch-stable) candidate.

While much more work remains, we are now very confident in the state of the Cwtch library, and the Cwtch UI. We are prepared to make certain commitments regarding peer-to-peer messaging, the UI,
and experimental interfaces. In this post we will chart the journey that got us to this point, highlight what is in this new release, and talk about our next steps.

![](/img/devlog14.png)

<!--truncate-->

## Cwtch Stable and Beyond!

Over five years ago, on the 28th June 2018, we published the first official announcement of Cwtch. Throughout 2019 we published various Alpha releases of Cwtch. The original plan was to release a Cwtch Beta in 2020.

Like so many other projects in 2020, Cwtch Beta was delayed, and towards the end of 2020 it became clear that our original approach to a cross-platform UI was not sustainable long term.

Finally, in June 2021 we launched Cwtch Beta 1.0. We have spent the years since refining beta, adding features, and responding to feedback.

We have now reached a pivotal moment in Cwtch, one that the team has been working towards for many years. We now believe that Cwtch has reached a point where people can use core features, and enable
experimental features, with a confidence that any risks are well understood and appropriately mitigated. As such we are dropping the "beta" label.

Some features, like automatically downloading and displaying images, will always carry some risk - as such these will always remain off-by-default in Cwtch.

However, if approporate precautions are taken (like never accepting conversations from untrusted entities) then these features can be turned on and used without additional considerable risk.

Further, we believe that the API presented by libCwtch has reached a point where its core design is unlikely to require changes - and as such are prepared to make additional committements to the stability
of that API going forward. Any new functionality will be provided by new interfaces, or otherwise be handled behind the scenes.

This is certainly not the end of Cwtch development. We have big plans for the future including the long-anticipated Hybrid Groups
implementation, a light client for restricted mobile operating systems, a return of the bulletin boards overlay, and much more.

We want to extend a huge thank you to everyone who helped Cwtch get this far. We could not have done it without you. If you
have helped in any way and would like to be listed in the contributor credits [please reach out](https://docs.cwtch.im/blog/cwtch-stable-call-for-credits).

## A Big Thank You

On a personal note, as Executive Director of Open Privacy, and lead of the Cwtch project. I want to take this opportunity to thank the Cwtch core team across time: Dan Ballard, Erinn Atwater and Marcia Díaz Agudelo - this work isn’t glamorous, and doesn’t pay well,
there is no profit to be made in decentralizing power. A lack of funding means we don't all work together any more, but you all still contribute so much to this project.

It takes a special kind of person to be willing to spend a significant fraction of their lives devoted to working on something for the benefit of other people.
Thank you for believing in this mission.

I also want to say thank you to all the people who tested Cwtch over the years and provided invaluable feedback, bug reports and critique.
You have made Cwtch what it is today, and I am sure you will be making it even better in the coming weeks, months, and years.

Last, but certainly not least, I want to extend a big thank you to all of the supporters of Open Privacy around the world - without your donations
and continued enthusiasm for the work that we do, none of this would be possible.

## Download Cwtch 1.13

You can download Cwtch from [https://cwtch.im/download](https://cwtch.im/download).

Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

Alternatively we also provide a [releases-only RSS feed](https://cwtch.im/releases/index.xml).

## In This Release

<figure>

[![](/img/picnic1.13.png)](/img/picnic1.13.png)

<figcaption>A screenshot of Cwtch 1.13</figcaption>
</figure>

A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Conversation Search** - Cwtch can now find messages based on their content.
  - **Appear Offline Mode** - in this mode Cwtch does not launch a listening service for inbound contacts, and allows a profile to be more selective in the contacts they connect to.
  - **Whonix Support** - new runtime flags make changes that allow Cwtch to [run on Whonix](https://docs.cwtch.im/docs/platforms/whonix)
  - **Save History Global Setting** - by default Cwtch deletes all messages on shutdown unless a conversation is otherwise configured. This change allows a user to change this default behaviour.
- **Bug Fixes  / Improvements:**
  - Based on Flutter 3.13.4
  - Updated Android Target to 33
  - Profile Status Menu now has many more options, including offline status, edit profile and enabling/disabling profile
  - File Sharing Bug Fixes
    - Manage shared files now supports re-enabling older file shares
  - Improvements towards [UI Reproducible Builds](https://docs.cwtch.im/blog/cwtch-ui-reproducible-builds-linux)
  - Server Info now propagates to the UI consistently
  - Prevent DBus Exceptions on platforms where it is unsupported
  - Packaged Emoji Font
  - Fixes to retry manager which have greater improved (re)connection efficacy
  - Allow deleting server info in Manage Servers
- **Accessibility / UX:**
  - Core translations for **Brazilian Portuguese**, **Danish** , **Dutch**, **French**, **German**, **Italian**, **Norwegian** , **Romanian** , **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Partial translations for **Korean** (37%), **Japanese** (27%), , **Luxembourgish** (20%), **Greek** (15%), **Uzbek** (10%), and **Portuguese** (5%)
  - Font Scaling improvements on several screens

## Reproducible Bindings

Cwtch 1.13 is based on libCwtch version `libCwtch-autobindings-2023-09-26-13-15-v0.0.10`.
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10)

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)
