# Image Previews

Built on the back of filesharing in Cwtch 1.3, image previews are keyed by the suggested filename’s extension (and no, we’re not interested in using MIME types or magic numbers) and advertised size. If enabled, the preview system will automatically download shared images to a configured downloads folder and display them as part of the message itself. (Due to limitations on Android, they’ll go to the app’s private storage cache, and give you the option to save them elsewhere later instead.) The file size limit is TBD but will obviously be much lower than the overall filesharing size limit, which is currently 10 gigabytes.

For now, we only support single-image messages, and any image editing/cropping will have to be done in a separate application. As we mention in the filesharing FAQ, image files also frequently contain significant hidden metadata, and you should only share them with people you trust.

## KnownRisks

## Other Applications and/or the OS Inferring Information from Images

Images must be stored somewhere, and for now we have chosen to store them unencrypted on the file system. We have done this for 2 reasons:

1. In order to support more powerful file sharing schemes like rehosting we require the ability to efficiently scan files and deliver chunks - doing this through an encrypted database layer would harm performance.
2. This information always has to transit the application boundary (either via display drivers, or storing and viewing the file in an external application) - there is nothing that Cwtch can do after that point in any case.

## Malicious Images  Crashing or otherwise Compromising Cwtch

Flutter uses Skia to render Images. While the underlying code is memory unsafe, it is [extensively fuzzed](https://github.com/google/skia/tree/main/fuzz) as part of regular development.

We also conduct our own fuzz testing of Cwtch components. In that analysis we found a single crash bug related to a malformed GIF file that caused the renderer to allocate a ridiculous amount of memory (and eventually be refused by the kernel). To prevent this from impacting Cwtch we have adopted the policy of always enabling a maximum `cacheWidth` and/or `cacheHeight` for Image widgets.

## Malicious Images Rendering Differently on Different Platforms, Potentially Exposing Metadata

Recently [a bug was found in Apple's png parser](https://www.da.vidbuchanan.co.uk/widgets/pngdiff/) which would cause an image to render differently on Apple devices as it would on non-Apple devices.

We conducted a few tests on our Mac builds and could not replicate this issue for Flutter (because all Flutter builds use Skia for rendering), however we will continue to include such cases in our testing corpus.

For now image previews will remain experimental and opt-in.

