---
sidebar_position: 6
---

# How to Leave a Group

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and the [Group Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) turned on.

:::

:::warning

This feature will result in **irreversible** deletion of key material. This **cannot be undone**.

:::

1. On the chat pane go to settings
2. Scroll down on the settings pane
3. Press on leave conversation
4. Confirm you want to leave

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Leave.mp4" />
    </video>
</div>