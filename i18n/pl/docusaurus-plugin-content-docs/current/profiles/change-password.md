---
sidebar_position: 3
---

# Changing Your Password

1. Press the pencil next to the profile you want to edit
2. Go to current password and input your current password
3. Go to new password and input your new password
4. Re enter your password
5. Click Save profile