---
sidebar_position: 1
---

# Custom Cwtch Themes

:::note

We are still finalizing the exact format of the theme files. Please consider this functonality experimental.

:::

Good news, now you can create your own Cwtch light and dark themes with a simple template! Follow these steps to create a new theme:

#### Downloading and installing the test theme

- Create a test folder, which includes: [theme.yml](/test_theme/theme.yml) (editable file), [test.png](/test_theme/test.png) (background image) and [Instructions.pdf](/test_theme/Instructions.pdf)

- Load the test theme through the settings pane. [Settings](https://docs.cwtch.im/docs/settings/) > Import theme , select the “test” folder and click on “select folder”

- Now you have the test theme installed and you can edit the .yml file

- Check the instructions.pdf file for more in depth instructions on how to edit the .yml file and what each part of the code changes!

#### Editing the .yml file

- Pick your colours using any web tool

- Pick a colour to be your accent colour and select all the other colours based on that one

- Open theme.yml

- Replace the part under "colours" where 0x`FFFFFF` colours appear with the HEX value of colours you chose (follow the instructions after each colour for better results!)

- Don't include the # on the HEX value

- Replace test.png for a transparent png you want to use as a background, if you don’t want a background image, disable theme images on settings>theme images

- Don't forget to save!

#### Testing your new theme

- Re-load the test theme through the settings pane. Settings> Import theme , select the “test” folder and click on “select folder”

- Repeat until you're satisfied with your new theme!

- If you're an advanced user, feel free to edit the .yml code beyond selecting colours!

As always, let us know if this is working for you, [we'd love to hear your feedback](https://docs.cwtch.im/docs/contribute/testing/#submitting-feedback)
