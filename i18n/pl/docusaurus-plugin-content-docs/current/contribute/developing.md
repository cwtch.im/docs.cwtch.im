---
sidebar_position: 1
---

# Developing Cwtch

This section documents some ways to get started with Cwtch Development.

## Cwtch Issues Tracking Process

All Cwtch issues are tracked from the [cwtch-ui git repository](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues), even if the bug/feature originates in an upstream library. This allows us to keep everything in one place.

Issues are generally divided into 4 distinct categories:

- **Unprocessed** - These are new issues that have not been discussed by the Cwtch team.
- [**Scheduled**](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=open&labels=195&milestone=0&assignee=0&poster=0) - These issues have been planned for an upcoming release. They are usually tagged with the release they are expected to be fixed in e.g. `cwtch-1.11`. A core Cwtch team member is likely working on the issue, or is expecting to work on the issue in the coming weeks.
- [**Desired**](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=open&labels=153&milestone=0&assignee=0&poster=0) - These are issues that we would like to fix but for some reason we are unable to schedule. This might be because the feature is large and requires a lot of effort, or because there is some blocker (e.g. a missing feature in Flutter or some other library) that prevents work on the feature.
- [**Help Wanted**](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=open&labels=136&milestone=0&assignee=0&poster=0) - These are generally small issues that we would like to fix but that have been designated low priority. These are ideal first issues for volunteers.

If you would like to work on an open bug/feature, please comment on the issue and a member of the Cwtch team will follow up with advice on where to go from there. This helps us keep track of who is working on what problems, and reduces the amount of duplicate work. We aim to answer most queries within 24 hours, feel free to "bump" an issue if it takes longer than that.

:::note

Due to an issue with our email provider, we are currently unable to consistently send email from our gitea instance. Please regularly check open issues / pull-requests for updates (or subscribe to the repository's RSS feeds)

:::

## Cwtch Pull-Request Process

All pull-requests must be reviewed and approved by a core Cwtch team member prior to merging. Sarah reviews all new and active pull requests multiple times a week.

### Build Bot

All Cwtch projects are set up with automated builds and testing. Every pull request is expected to be able to pass through these pipelines prior to being merged. If buildbot reports a failure then Sarah will work with you to determine the issue, and any necessary fixes.

Buildbot can fail for reasons beyond your control e.g. many of our integration tests rely setting up Tor connections, these can be brittle on occasion and result in timeouts and failures. Always confirm the root cause of a test failure before deciding what to do next.


## Useful Resources

- [Cwtch Ecosystem Overview](/security/components/ecosystem-overview) - a summary of active Cwtch repositories from the Cwtch Secure Development Handbook.
- [Contributing Documentation](/docs/contribute/documentation) - advice on contributing Cwtch documentation.
- [Contributing Testing](/docs/contribute/testing) - advice on contributing by testing Cwtch.
- [Contributing Translations](/docs/contribute/translate) - advice on contributing translations to Cwtch.


:::note

All contributions are [eligible for stickers](/docs/contribute/stickers)

:::