---
sidebar_position: 2
---

# Accepting/Denying New Conversations

1. Go to your profile
2. If someone added you a new name will appear and two options
   1. Click heart icon to accept this new connection
   2. Click the trash icon to block them

See also: [Setting: Block Unknown Connections](https://docs.cwtch.im/docs/settings/behaviour/block-unknown-connections)