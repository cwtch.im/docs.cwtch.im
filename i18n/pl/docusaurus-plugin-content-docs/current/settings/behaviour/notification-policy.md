---
sidebar_position: 2
---

# Notification policy

1. Go to settings
2. Scroll to behaviour
3. The notification policy controls the notification behaviour
4. Click on Default all to change the behaviour to opt in or to mute all notifications
5. Pick your preferred notification Policy