---
title: Enhanced Permissions
description: An introduction to the new fine-grained permissions API exposed in libCwtch
slug: enhanced-permissions
tags:
  - cwtch
  - hybrid-groups
  - acl
image: /img/enhancedpermissions.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
  - name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

[Last week](/blog/path-to-hybrid-groups), we mentioned that **enhanced permissions** are essential to implementing many of the aspects
of the new **hybrid groups** design.

In this post we will introduce the new enhanced permissions api in libCwtch, explain why it is needed, and how future releases of Cwtch will manage
conversation-level permissions.

![](/img/enhancedpermissions.png)

<!--truncate-->

## How do conversation-level permissions work in Cwtch right now?

Underneath the libCwtch API, all conversations maintain an access control list. For peer-to-peer conversations this list has a single entry, keyed by
the handle of the contact.

Currently, this access control list only maintains the `Blocked`-state of the contact. If a user blocks another, then this is where the flag is stored. While
there are other fields in this list, they have been unused, until now.

Additionally, each conversation also stored an `Accepted` flag, indicating if the user has approved a contact request. The `Accepted` flag has many different
effects including whether certain features (like file sharing) are accessible, or whether connection attempts with the contact are tried automatically.

## What Are Enhanced Permissions?

The current approach has several issues; the major issue being that profiles have minimal control over how they interact with contacts. Ideally, you should be able to
automatically attempt to connect with a given cwtch user, but not allow file shares, for example.

This is the initial driving use case behind Enhanced Permissions. Exposing this underlying ACL and augmenting it with additional flags like `ShareFiles`,
`RenderImages`, and `AutoConnect`.

## The API

Enhanced permissions will be available through a new set of low level libCwtch APIs i.e.:

```
	UpdateConversationAccessControlList(id int, acl model.AccessControlList) error
	EnhancedUpdateConversationAccessControlList(conversation int, serialized_acl string) error

	GetConversationAccessControlList(conversation int) (model.AccessControlList, error)
	EnhancedGetConversationAccessControlList(conversation int) (string, error)
```

As with other libCwtch APIs the `Enhanced` functions speak JSON and be suitable for integration with UIs and other clients that cannot speak the
Go model.

`AccessControlList` maintains it's current structure, while `AccessControl` itself has been augmented with the fields
described in the previous section.

Existing access-control APIs e.g. `AcceptConversation`, `BlockConversation` and `UnblockConversation` have been updated
internally to use the augmented `AccessControl` model, and will be able to be safely used by existing and future clients.

However, the `Accepted` field in the `Conversation` model is now considered deprecated, and will be removed in a future Cwtch version.

## How will Migration Work?

To simplify migration newer versions of Cwtch will detect if Enhanced Permissions have been enabled, and if they haven't will automatically turn on
all new flags for `Accepted` conversations. This will maintain behaviour between versions, and should minimize any potential confusion.

You will then be able to modify this ACL on a per-conversation level in the Cwtch UI.

## When will Enhanced Permissions be Available?

We are aiming to roll out a nightly with Enhanced Permissions in the coming weeks. We will then spend some time iterating
on the UI to ensure that the new permissions are understandable and usable.

## How does this fit into Hybrid Groups?

That is not the end of the story as far as Enhanced Permissions go. As I mentioned above for peer-to-peer conversations the
ACL has a single entry.

For Hybrid Groups, this list will have multiple entries, for each member of the group. This will allow group admins to
control who can post to a group, and what kinds of messages they can post.

How this list will be maintained, propagated, and authenticated by group members will be a topic of a future development log.

## Stay up to date!

As always, we will be regularly updating this devlog [and other channels](https://fosstodon.org/@cwtch) as we continue to make progress towards
surveillance resistant infrastructure!

Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)
