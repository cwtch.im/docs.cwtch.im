---
title: Cwtch-Aufruf Anerkennungen für Mitwirkende
description: "Da wir immer näher an einen Cwtch Stable Kandidaten herankommen, möchten wir diese Gelegenheit nutzen, um sicherzustellen, dass diejenigen, die im Laufe der Jahre an Cwtch mitgewirkt haben, die Möglichkeit haben, die Anerkennung zu erhalten."
slug: cwtch-stable-call-for-credits
tags:
  - cwtch
  - cwtch-stable
  - Mitwirkende
  - Community
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

Da wir immer näher an einen Cwtch Stable Kandidaten herankommen, möchten wir diese Gelegenheit nutzen, um sicherzustellen, dass diejenigen, die im Laufe der Jahre an Cwtch mitgewirkt haben, die Möglichkeit haben, die Anerkennung zu erhalten.

Wenn du in irgendeiner Weise am Entwicklungsprozess teilgenommen hast, z.B. beim Protokoll-Design, Code-Design, UI-Design, das Schreiben von Tests, Tests von Release-Kandidaten, Berichtsprobleme, Übersetzung der Anwendung oder der Dokumentation, die Förderung von metadatenresistenten Anwendungen oder irgendwelchen anderen sinnvollen Beitrag zum Cwtch-Ökosystem, dann möchten wir dir die Möglichkeit bieten, deinen Namen oder dein Handle sowohl im Quellcode-Repository als auch in der Anwendung selbst zu haben.

![](/img/devlog1.png)
 
<!--truncate-->

## Eine Geschichte von Cwtch Beiträgen und Anonmymität

In den ersten Tagen von Cwtch haben wir uns ausdrücklich dazu entschlossen, die Anerkennungen nirgends in der Applikation aufzunehmen und Beiträge anonym über eine Vielzahl von Kanälen zu akzeptieren, einschließlich Cwtch selbst.

Aufgrund der Art der Applikation und des Privatsphäre und Metadaten resistenten Raumes im Allgemeinen haben wir immer eine Richtlinie gehabt, die Beiträge basierend auf Leistung und nicht auf Identität bewertet. Dieser Ansatz bedeutet, dass wir zwar Mitwirkende haben, deren Identität in irgendeiner Weise bekannt ist, aber wir haben auch viele, die wir nur durch Schreibstil, Beitragstyp oder Cwtch Adresse kennen.

Wir verstehen, dass es viele Menschen diese Weise bevorzugen und keinen Wunsch haben, eine Identität zu haben, die mit dem Cwtch Projekt verbunden ist. Bei diesen Menschen bedanken wir uns sehr herzlich. Vielen Dank. Du hast Cwtch zu dem gemacht, was er ist. (Und wenn du jemals Cwtch Sticker möchtest - lass es uns bitte wissen!)

Es wäre jedoch nicht richtig von uns, Cwtch Stable ohne ein endgültiges Angebot an alle Mitwirkenden freizugeben. Wenn du möchtest, dass für deine Beiträge an Cwtch eine Anerkennung haben möchtest, dann sprich uns bitte an und teile uns mit, wie wir entsprechend die Anerkennung zuteil werden können.

## Kontaktiere uns

Du kannst per E-Mail um Anerkennung bitten (team@cwtch.im) oder über Cwtch (entweder öffentlich über die [Cwtch Release Candidate Tester Gruppen](https://docs.cwtch.im/docs/contribute/testing#join-the-cwtch-release-candidate-testers-group), oder privat in einer Nachricht an Sarah: `icyt7rvdsdci42h6si2ibtwucdmjrlcb2ezkecuagtquiiflbkxf2cqd`).

Du kannst auch [ein issue](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/new) erstellen.

Bitte gib in deiner Anfrage einen Namen oder ein Handle an und, wenn gewünscht, eine grobe Beschreibung des Beitrags an (z.B. Entwicklung, Design, Dokumentation, Übersetzung, Finanzierung). Jeder, der keine Beschreibung angibt, wird mit in einem allgemeinen Dankesbereich zusammengefasst.

Dies ist ein offenes Angebot. Wenn du zu irgendeinem Zeitpunkt deine Meinung änderst und möchtest, dass Anerkennung hinzugefügt (oder entfernt wird), dann lass es uns bitte wissen.

Ich möchte noch eine weitere Gelegenheit nutzen dir zu sagen, unabhängig davon ob du für deine Arbeit bei Cwtch öffentlich Anerkennung haben möchtest oder nicht: **Vielen Dank**.

## Bleib auf dem Laufenden!

Abonniere unseren [RSS-Feed](/blog/rss.xml), [Atom-Feed](/blog/atom.xml), oder [JSON-Feed](/blog/feed.json), um auf dem Laufenden zu bleiben und die aktuellsten Aspekte der Cwtch-Entwicklung zu erhalten.

## Hilf uns, weiter zu gehen!

Wir könnten nicht das tun, was wir tun ohne die wunderbare Unterstützung der Gemeinschaft, die wir erhalten, von [einmalige Spenden](https://openprivacy.ca/donate) bis [wiederkehrende Unterstützung über Patreon](https://www.patreon.com/openprivacy).

Wenn du sehen möchtest, dass wir uns bei einigen dieser Ziele schneller bewegen und in der Lage sind, bitte [spende](https://openprivacy.ca/donate). Wenn du zufällig bei einem Unternehmen bist, das mehr für die Community tun will und dies passt, erwäge bitte, einen Entwickler zu spenden oder zu sponsern.

Spenden in Höhe von **$5 oder mehr** berechtigen dazu, Aufkleber als Dankeschön zu erhalten!

Für weitere Informationen über die Spende an Open Privacy und das Einfordern eines Dankeschön Geschenkes [besuche bitte die Open Privacy Spenden-Seite](https://openprivacy.ca/donate/).

![Ein Foto von Cwtch-Aufklebern](/img/stickers-new.jpg)



