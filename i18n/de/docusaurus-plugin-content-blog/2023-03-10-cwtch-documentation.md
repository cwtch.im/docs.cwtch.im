---
title: Updates zur Cwtch-Dokumentation
description: "In diesem Entwicklungsprotokoll werden wir einige der wichtigsten Dokumentationsaktualisierungen der letzten Wochen hervorheben."
slug: cwtch-Dokumentation
tags:
  - cwtch
  - cwtch-stable
  - Dokumentation
  - Sicherheitshandbuch
image: /img/devlog9_small.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

Einer der wichtigsten Arbeitsströme auf dem Weg bis zum Cwtch Stable war die Verbesserung aller Aspekte der Cwtch Dokumentation. In diesem Entwicklungsprotokoll werden wir einige der wichtigsten Updates der letzten Wochen hervorheben.

![](/img/devlog9.png)
 
<!--truncate-->

## Cwtch Handbuch für sichere Entwicklung

Eines der frühesten Kompendien der Cwtch-Dokumentation war das Cwtch Handbuch für sichere Entwicklung. Dieses Handbuch bot einen Überblick über die verschiedenen Teile des Cwtch-Ökosystems, die bekannten Risiken und alle bestehenden Eindämmungen. Das Handbuch wurde als Leitfaden für Entwickler konzipiert, die Cwtch bauen oder erweitern und im Laufe der Jahre diente es auch als festes Zuhause für die Dokumentation von langjährigen Designentscheidungen.

Wir haben nun [das Handbuch zu dieser Dokumentations-Website portiert](/security/intro) und dabei Teile des Inhaltes aktualisiert. In den nächsten Monaten werden wir diesen Abschnitt um neue Abschnitte zu Fuzzing, Plugins und Client-Implementierung erweitern.

## Entwicklung Freiwilliger

Wir haben einen Aufschwung bei der Anzahl der Menschen festgestellt, die sich für die Entwicklung von Cwtch interessieren. Um den Leuten zu helfen, sich an unseren Entwicklungsfluss zu gewöhnen, haben wir einen neuen Abschnitt auf der Dokumentationsseite mit dem Namen [Cwtch Entwicklung ](/docs/contribute/developing) erstellt - dort findest du eine Sammlung nützlicher Links und Informationen, wie man mit der Cwtch Entwicklung beginnen kann. welche Bibliotheken und Werkzeuge wir verwenden, wie Pull-Requests validiert und überprüft werden und wie ein Problem ausgewählt wird, an dem gearbeitet werden soll.

Wir haben auch unsere Anleitungen zu [Cwtch übersetzen](/docs/contribute/translate) und [Cwtch testen](/docs/contribute/testing) aktualisiert.

Wenn du Interesse an der Entwicklung von Cwtch hast, dann schaue dir das an und wende dich an `team@cwtch. m` (oder öffne ein issue) mit allen Fragen. Alle Arten von Beiträgen [sind berechtigt für Aufkleber](/docs/contribute/stickers).

## Nächste Schritte

Wir haben noch mehr Arbeit an der Dokumentationsfront zu tun:

* Sicherstellung, dass alle Seiten [den neuen Dokumentationsstil-Leitfaden](/docs/contribute/documentation)implementieren und entsprechende Screenshots und Beschreibungen enthalten.
* Erweitern des Sicherheitshandbuchs, um Informationen über [reproduzierbare Builds zu liefern,](/blog/cwtch-bindings-reproducible), [die neue Cwtch Stable API](/blog/cwtch-stable-api-design) und kommende Verbesserungen rund um Fuzz Tests.
* Erstellung neuer Dokumentationsabschnitte auf der [libCwtch Autobindings API](/blog/autobindings) und Erstellung neuer Anwendungen auf Cwtch.

Da diese Änderungen vorgenommen wurden und diese Ziele erreicht wurden, werden wir sie hier veröffentlichen! Abonniere unseren [RSS-Feed](/blog/rss.xml), [Atom-Feed](/blog/atom.xml), oder [JSON-Feed](/blog/feed.json), um auf dem Laufenden zu bleiben und die aktuellsten Aspekte der Cwtch-Entwicklung zu erhalten.

## Hilf uns, weiter zu gehen!

Wir könnten nicht das tun, was wir tun ohne die wunderbare Unterstützung der Gemeinschaft, die wir erhalten, von [einmalige Spenden](https://openprivacy.ca/donate) bis [wiederkehrende Unterstützung über Patreon](https://www.patreon.com/openprivacy).

Wenn du sehen möchtest, dass wir uns bei einigen dieser Ziele schneller bewegen und in der Lage sind, bitte [spende](https://openprivacy.ca/donate). Wenn du zufällig bei einem Unternehmen bist, das mehr für die Community tun will und sich dies passt, erwäge bitte, einen Entwickler zu spenden oder zu sponsern.

Spenden in Höhe von **$5 oder mehr** können sich entscheiden, Aufkleber als Dankeschön zu erhalten!

Für weitere Informationen über die Spende an Open Privacy und das Einfordern eines Dankeschön Geschenkes [besuche bitte die Open Privacy Spenden-Seite](https://openprivacy.ca/donate/).

![Ein Foto von Cwtch-Aufklebern](/img/stickers-new.jpg)

