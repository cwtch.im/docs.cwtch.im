---
sidebar_position: 2
---

# Kernkonzepte

Diese Seite dokumentiert die Kernkonzepte, denen du als Cwtch App-Entwickler ziemlich häufig begegnen wirst.

## Cwtch Anwendungsverzeichnis

Oft als `$CWTCH_HOME`bezeichnet, ist das Cwtch Anwendungsverzeichnis der Ort, an dem Cwtch alle Informationen aus einer Cwtch Anwendung speichert.

## Profile

Cwtch Profile werden als verschlüsselte sqlite3 Datenbanken gespeichert. Du musst selten/nie direkt mit der Datenbank interagieren. Stattdessen bietet jede Bibliothek eine Reihe von Schnittstellen, um mit der Cwtch App zu interagieren, Profile zu erstellen, Profile zu verwalten und Konversationen zu führen.

## Der Eventbus

Unabhängig davon, welche Bibliothek du am Ende wählst, ist der Eventbus die einzige ständige Schnittstelle, an die du dich gewöhnen musst. Cwtch übernimmt alle asynchronen Aufgaben (z.B. das Empfangen einer Nachricht von einem Teilnehmer) automatisch, indem er eine Nachricht auf den EventBus legt. Die Anwendung kann bestimmte Arten von Nachrichten abonnieren, z.B. `NewMessageFromPeer` und einen Ereignishandler konfigurieren, um Code als Antwort auf eine solche Nachricht auszuführen.

Für ein Beispiel schau in das Echo Bot Tutorial.

## Einstellungen

Die meisten Cwtch-Einstellungen (mit Ausnahme von Experimenten) sind für nachgeschaltete grafische Benutzeroberflächen konzipiert, wie z.B. Themes / Spaltenlayouts - insbesondere das Cwtch UI. Als solche werden diese Einstellungen von Cwtch Bibliotheken überhaupt nicht verwendet und sind nur als bequemer Speicherplatz für die UI-Konfiguration gedacht.

### Experimentelle Funktionen

Bestimmte Cwtch-Funktionen sind [hinter den Experimenten](/docs/category/experiments) eingebunden. Diese Experimente müssen aktiviert sein, bevor die damit verbundenen Funktionen aktiviert werden. Verschiedene Bibliotheken können verschiedene Experimente enthüllen, und einige Bibliotheken unterstützen bestimmte Experimente möglicherweise überhaupt nicht.
