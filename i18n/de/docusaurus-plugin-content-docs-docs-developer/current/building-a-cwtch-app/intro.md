---
sidebar_position: 1
---

# Erste Schritte

## Auswahl einer Cwtch-Bibliothek

### Cwtch Go Lib

Die offizielle Cwtch-Bibliothek ist in Go geschrieben und kann unter [https://git.openprivacy.ca/cwtch.im/cwtch](https://git.openprivacy.ca/cwtch.im/cwtch) gefunden werden. Diese Bibliothek ermöglicht den Zugriff auf alle Cwtch-Funktionen.

### CwtchBot

Wir bieten auch ein spezialisiertes Cwtch Bot Framework in Go an, dass einen leichteren und maßgeschneiderten Ansatz für den Bau von Chat-Bots bietet. Für eine Einführung in das Erstellen von Chatbots mit dem CwtchBot Framework schau dir das [an und baue ein echobot Tutorial](/developing/building-a-cwtch-app/building-an-echobot#using-cwtchbot-go).

### Autobindings (C-bindings)

Die [offiziellen c-bindigs für Cwtch](https://git.openprivacy.ca/cwtch.im/autobindings) werden automatisch aus der Cwtch Go Library generiert. Die API ist im Vergleich zum direkten Zugriff auf die Cwtch Go Library beschränkt und ist explizit auf den Aufbau der Cwtch UI zugeschnitten.

### libCwtch-rs (Rust)

Eine experimentelle rust-fied Version von Cwtch Autobindings ist in [libCwtch-rs](https://crates.io/crates/libcwtch) verfügbar. Während wir in Zukunft Pläne haben, Rust bindings zu anzunehmen, hinkt die Unterstützung von Rust dem Rest des Ökosystems Cwtch hinterher.
