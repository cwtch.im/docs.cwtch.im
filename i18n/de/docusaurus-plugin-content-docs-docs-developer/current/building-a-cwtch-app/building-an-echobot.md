---
sidebar_position: 3
---

# Einen Cwtch Echobot erstellen

In diesem Tutorial werden wir durch den Aufbau einer einfachen Cwtch Echobot gehen. Ein Bot, der einfach mit der Nachricht antwortet, die an ihn gesendet wurde.

Zur Vollständigkeit, wir werden einen Echobot in verschiedenen Cwtch Frameworks erstellen, um ein Gefühl für die verschiedenen Funktionsebenen zu bekommen, die jede Bibliothek oder jedes Framework bietet.

## CwtchBot (Go) verwenden

:::info CwtchBot Framework

Dieses Tutorial verwendet das CwtchBot Framework.
:::

Beginne mit dem Erstellen eines neuen Go-Projekts und einer Datei `main.go`. In der `main` Funktion:

```go
package main

import (
    "cwtch.im/cwtch/event"
    "cwtch.im/cwtch/model"
    "cwtch.im/cwtch/model/attr"
    "cwtch.im/cwtch/model/constants"
    "fmt"
    "git.openprivacy.ca/sarah/cwtchbot"
    _ "github.com/mutecomm/go-sqlcipher/v4"
    "os/user"
    "path"
)

func main() {
    user, _ := user.Current()
    cwtchbot := bot.NewCwtchBot(path.Join(user.HomeDir, "/.echobot/"), "echobot")
    cwtchbot.Launch()

    // Set Some Profile Information
    cwtchbot.Peer.SetScopedZonedAttribute(attr.PublicScope, attr.ProfileZone, constants.Name, "echobot2")
    cwtchbot.Peer.SetScopedZonedAttribute(attr.PublicScope, attr.ProfileZone, constants.ProfileAttribute1, "A Cwtchbot Echobot")

    fmt.Printf("echobot address: %v\n", cwtchbot.Peer.GetOnion())

    for {
        message := cwtchbot.Queue.Next()
        cid, _ := cwtchbot.Peer.FetchConversationInfo(message.Data[event.RemotePeer])
        switch message.EventType {
        case event.NewMessageFromPeer:
            msg := cwtchbot.UnpackMessage(message.Data[event.Data])
            fmt.Printf("Message: %v\n", msg)
            reply := string(cwtchbot.PackMessage(msg.Overlay, msg.Data))
            cwtchbot.Peer.SendMessage(cid.ID, reply)
        case event.ContactCreated:
            fmt.Printf("Auto approving stranger %v %v\n", cid, message.Data[event.RemotePeer])
            // accept the stranger as a new contact
            cwtchbot.Peer.AcceptConversation(cid.ID)
            // Send Hello...
            reply := string(cwtchbot.PackMessage(model.OverlayChat, "Hello!"))
            cwtchbot.Peer.SendMessage(cid.ID, reply)
        }
    }
}
```


## Imp (Rost) verwenden

:::info Imp (Rost) Bot Framework

Dieses Tutorial verwendet das Imp Cwtch Bot Framework (Rust). Dieses Framework ist derzeit in Bearbeitung und das API-Design kann sich ändern. IMP basiert auch auf libcwtch-rs, die derzeit auf einer älteren pre-stable API-Version von Cwtch basiert. Wir planen die Aktualisierung der libcwtch-rs im Sommer 2023.
:::

```go
use std::borrow::BorrowMut;
use std::thread;
use chrono::{DateTime, FixedOffset};
use libcwtch;
use libcwtch::CwtchLib;
use libcwtch::structs::*;
use libcwtch::event::*;
use cwtch_imp::imp;
use cwtch_imp::behaviour::*;
use cwtch_imp::imp::Imp;

const BOT_HOME: &str = "~/.cwtch/bots/echobot";
const BOT_NAME: &str = "echobot";

struct Echobot {}

fn main() {
    let behaviour: Behaviour = BehaviourBuilder::new().name(BOT_NAME.to_string()).new_contact_policy(NewContactPolicy::Accept).build();
    let event_loop_handle = thread::spawn(move || {
        let mut echobot = Echobot {};
        let mut bot = Imp::spawn(behaviour,String::new(), BOT_HOME.to_string());
        bot.event_loop::<Echobot>(echobot.borrow_mut());
    });
    event_loop_handle.join().expect("Error running event loop");
}

impl imp::EventHandler for Echobot {
    fn on_new_message_from_contact(&self, cwtch: &dyn libcwtch::CwtchLib, profile: &Profile, conversation_id: ConversationID, handle: String, timestamp_received: DateTime<FixedOffset>, message: Message) {
        let response = Message {
            o: MessageType::TextMessage,
            d: message.d,
        };
        cwtch.send_message(&profile.profile_id, conversation_id, &response);
    }

    fn handle(&mut self, cwtch: &dyn CwtchLib, profile_opt: Option<&Profile>, event: &Event) {
        match event {
            Event::NewPeer { profile_id, tag, created, name, default_picture, picture, online, profile_data } => {
                println!(
                    "\n***** {} at {} *****\n",
                    name, profile_id.as_str()
                );
            }
            _ => (),
        };
    }
}
```
