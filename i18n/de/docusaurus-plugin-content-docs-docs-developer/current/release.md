---
sidebar_position: 1
---

# Freigabe und Pack Prozess

Cwtch-Builds werden automatisch über Drone erstellt. Um gebaut zu werden, müssen die Aufgaben von einem Projektteammitglied genehmigt werden.

## Automatisiertes Testen

Drone führt eine Reihe automatisierter Tests in verschiedenen Phasen der Release-Pipeline durch.

| Test-Suite                  | Repository        | Anmerkungen                                                                                                                      |
| --------------------------- | ----------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| Integrationstests           | cwtch.im/cwtch    | Eine vollständige Übung von Peer-to-Peer und Gruppen-Nachrichten                                                                 |
| Dateifreigabe Test          | cwtch.im/cwtch    | Prüft, ob Dateifreigabe und das Herunterladen von Bildern wie erwartet funktioniert                                              |
| Automatischer Download Test | cwtch.im/cwtch    | Tests die überprüfen, ob das automatische Herunterladen von Bildern (z.B. Profilbilder) wie erwartet funktioniert                |
| UI-Integrationstest         | cwtch.im/cwtch-ui | Eine Reihe von Gherkin Tests um verschiedene UI-Ströme wie Erstellen / Löschen von Profilen und Ändern von Einstellungen zu üben |

## Cwtch Autobindings

Drone erzeugt die folgenden Build-Artefakte für alle Cwtch-Autobinding-Builds.

| Erstelle Artefakt          | Plattform | Anmerkungen                                                 |
| -------------------------- | --------- | ----------------------------------------------------------- |
| android/cwtch-sources.jar  | Android   | gomobile Quellcode für die Android Cwtch-Bibliothek         |
| android/cwtch.aar          | Android   | Android Cwtch-Bibliothek. Unterstützt arm, arm64 und amd64. |
| linux/libCwtch.h           | Linux     | C-Header-Datei                                              |
| linux/libCwtch.so          | Linux     | x64 geteilte Bibliothek                                     |
| windows/libCwtch.h         | Windows   | C-Header-Datei                                              |
| windows/libCwtch.dll       | Windows   | x64 geteilte Bibliothek                                     |
| macos/libCwtch.arm64.dylib | MacOS     | Arm64 geteilte Bibliothek                                   |
| macos/libCwtch.x64.dylib   | MacOS     | x64 geteilte Bibliothek                                     |

## UI Nightly Builds

Wir stellen unveröffentlichte Versionen von Cwtch zum Testen als [Cwtch Nightlies](/docs/contribute/testing#cwtch-nightlies) zur Verfügung.

Jeder nächtliche Build-Ordner enthält eine Sammlung von Build-Artefakten z.B. (APK-Dateien für Android, Installer-ausführbare Dateien für Android) in einem komfortablen Ordner. Eine vollständige Liste der derzeit erzeugten Build Artefakte lautet wie folgt:

| Build-Artefakt              | Plattform | Anmerkungen                                                                                                |
| --------------------------- | --------- | ---------------------------------------------------------------------------------------------------------- |
| cwtch-VERSION.apk           | Android   | Unterstützt arm, arm64 und amd64. Kann über sideload geladen werden.                                       |
| cwtch-VERSION.aab           | Android   | Android App Bundle für Veröffentlichung in Appstores                                                       |
| Cwtch-VERSION.dmg           | MacOS     |                                                                                                            |
| cwtch-VERSION.tar.gz        | Linux     | Enthält den Code, Bibliotheken und Assets zusätzlich zu den Installations-Skripten für verschiedene Geräte |
| cwtch-VERSION.zip           | Windows   |                                                                                                            |
| cwtch-installer-VERSION.exe | Windows   | NSIS basierter Installationsassistent                                                                      |

Nächtliche Builds werden regelmäßig vom System entfernt

## Offizielle Releases

Das Cwtch Team trifft sich regelmäßig und erzielt einen Konsens auf der Grundlage von Test Feedback der nächtlichen Builds und der Projekt Roadmap.

Wenn die Entscheidung getroffen wird, eine Release-Version zu erstellen, wird eine nächtliche Version mit einem neuen git Tag gebaut, der die Release-Version widerspiegelt., `v.1.12.0`. Die Build-Artefakte werden dann auf die Cwtch-Release-Website in einen dedizierten Versionsordner kopiert.

### Reproduzierbare Builds

Wir verwenden [repliqate](https://git.openprivacy.ca/openprivacy/repliqate) um [reproduzierbare Build-Skripte für Cwtch](https://git.openprivacy.ca/cwtch.im/repliqate-scripts) bereitzustellen.

Wir aktualisieren das Repository `repliqate-scripts` mit Skripten für alle offiziellen Releases. Derzeit sind nur Cwtch-Bindings reproduzierbar