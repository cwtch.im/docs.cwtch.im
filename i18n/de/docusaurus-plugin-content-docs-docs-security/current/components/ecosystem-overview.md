---
sidebar_position: 1.5
---

# Komponenten Ecosystem Übersicht

Cwtch besteht aus mehreren kleineren Komponenten-Bibliotheken. Dieses Kapitel gibt einen kurzen Überblick über jede Komponente und wie sie sich auf das größere Cwtch-Ökosystem bezieht.

## [offene Privatsphäre/Konnektivität](https://git.openprivacy.ca/openprivacy/connectivity)

Zusammenfassung: Eine Bibliothek, die eine ACN (Anonymous Communication Network ) Netzwerkabstraktion zur Verfügung stellt.

Das Ziel der Konnektivität ist es, die zugrunde liegenden Bibliotheken/Software zu abstrahieren, die für die Kommunikation mit einer spezifischen ACN benötigt wird. Zurzeit unterstützen wir nur Tor und deshalb ist die Aufgabe der Verbindung folgendes:

* Start und Stopp des Tor Prozesses
* Bereitstellen der Konfiguration des Tor-Prozesses
* Erlaube unveränderte Verbindungen zu Endpunkten über den Tor-Prozess (z.B. Verbindung mit Onion-Diensten)
* Endpunkte über den Tor-Prozess zur Verfügung stellen (z.B. Host-Onion-Dienste)
* Statusaktualisierungen über den zugrunde liegenden Tor-Prozess bereitstellen

Für weitere Informationen siehe [Konnektivität](/security/components/connectivity/intro)

## [cwtch.im/tapir](https://git.openprivacy.ca/cwtch.im/tapir)

Zusammenfassung: Tapir ist eine kleine Bibliothek für das Erstellen von p2p Anwendungen über anonyme Kommunikationssysteme.

Das Ziel von Tapir ist es, **Anwendungen** über eine bestimmte ACN zu abstrahieren. Tapir unterstützt:

* Erstellen einer kryptographischen Identität (einschließlich kurzlebiger Identitäten)
* Wartung eines Verbindungspools von eingehenden und ausgehenden Verbindungen zu Diensten
* Behandlung verschiedener Anwendungsschichten, einschließlich kryptographischer Transkripte, [Authentifizierungs- und Autorisierungsprotokolle](/security/components/tapir/authentication_protocol)und [Token-basierte Dienste über PrivacyPass](https://openprivacy.ca/research/OPTR2019-01/),

Für weitere Informationen siehe [Tapir](/security/components/tapir/authentication_protocol)

## [cwtch.im/cwtch](https://git.openprivacy.ca/cwtch.im/cwtch)

Zusammenfassung: Cwtch ist die Hauptbibliothek für die Implementierung des Cwtch-Protokolls / System.

Das Ziel von Cwtch ist die Bereitstellung von Implementierungen für cwtch-spezifische Anwendungen wie z.B. Nachrichtensendung, Gruppen und Dateifreigabe (implementiert als Tapir Anwendungen), Schnittstellen zur Verwaltung und Speicherung von Cwtch Profilen, bietet neben der Verwaltung anderer Kernfunktionen auch einen Eventbus für Subsystem Splitting und das Erstellen von Plugins mit neuen Funktionen.

Die Cwtch Bibliothek ist auch für die Pflege kanonischer Modelldarstellungen für Formate und Overlays zuständig.


## [cwtch.im/libcwtch-go](https://git.openprivacy.ca/cwtch.im/libcwtch-go)

Zusammenfassung: libcwtch-go stellt C (auch für Android) Bindungen für Cwtch bereit für die Benutzung in UI Implementierungen.

Das Ziel von libcwtch-go ist es, die Lücke zwischen der Backend Cwtch Bibliothek und allen Frontend-Systemen zu überbrücken, die in einer anderen Sprache geschrieben werden können.

Die von libcwtch bereitgestellte API ist wesentlich eingeschränkter als die von Cwtch direkt bereitgestellte API, jede libcwtch API paketiert normalerweise mehrere Aufrufe an Cwtch.

libcwtch-go ist auch für die Verwaltung der UI-Einstellungen und des experimentellen Gateways zuständig. Es wird auch oft als Staging-Boden für experimentelle Funktionen und Code verwendet, die möglicherweise am Ende in Cwtch enden.

## [cwtch-ui](https://git.openprivacy.ca/cwtch.im/cwtch-ui)

Zusammenfassung: Eine flutterbasierte Oberfläche (UI) für Cwtch.

Cwtch UI verwendet libcwtch-go um ein vollständiges UI für Cwtch bereitzustellen, das es Leuten ermöglicht, Profile zu erstellen und zu verwalten, Kontakte und Gruppen hinzuzufügen, Nachrichten zu versenden, Dateien freizugeben (bald) und vieles mehr.

Das UI ist auch für die Verwaltung von Lokalisierung und Übersetzungen zuständig.

Für weitere Informationen siehe [Cwtch UI](/security/category/cwtch-ui)

## Zusätzliche Komponenten

Gelegentlich wird Open Privacy Teile von Cwtch in eigenständige Bibliotheken einbeziehen, die nicht Cwtch spezifisch sind. Diese werden hier kurz zusammengefasst:

### [openprivacy/log](https://git.openprivacy.ca/openprivacy/log)

Ein Open Privacy spezifisches Logging-Framework, das in allen Cwtch Paketen verwendet wird.