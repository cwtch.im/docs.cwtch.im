---
sidebar_position: 3
---


# Verbindung

Cwtch nutzt Tor Onion Services (v3) für die Kommunikation zwischen Knoten.

Wir stellen das [openprivacy/connectivity](https://git.openprivacy.ca/openprivacy/connectivity) Paket zur Verfügung, um den Tor-Daemon zu verwalten und Onion Dienste durch Tor einzurichten und abzubauen.

## Bekannte Risiken

### Privater Schlüssel dem Tor Prozess ausgesetzt

**Status: Teilweise gemildert** (Erfordert physischen Zugriff oder Privilegierte Eskalation zum ausnutzen)

Wir müssen den privaten Schlüssel eines Onion Dienstes an die Konnektivitätsbibliothek übergeben über die `Listen` Schnittstelle (und damit zum Tor Prozess). Dies ist einer der kritischsten Bereiche, der außerhalb unserer Kontrolle liegt. Jede Bindung an einen Rogue-Tor-Prozess oder ein Binary führt zur Kompromittierung des privaten Schlüssels von Onion führen.

### Eindämmung

Verbindungsversuche als Standard an den vom System bereitgestellten Tor-Prozess zu binden, *nur* wenn ihm ein Authentifizierungs-Token zur Verfügung gestellt wurde.

Andernfalls versucht die Konnektivität immer einen eigenen Tor-Prozess mit einem bekannten guten Binärpaket, das mit dem System gepackt wurde (außerhalb des Geltungsbereichs des Konnektivitäts-Paketes)

Langfristig hoffen wir, dass eine integrierte Bibliothek verfügbar sein wird und die direkte Verwaltung über eine In-Prozessoberfläche ermöglicht, um zu verhindern, dass der private Schlüssel die Prozessgrenze verlässt (oder andere alternative Pfade, die es uns erlauben, die volle Kontrolle über den privaten Schlüssel im Speicher zu erhalten.)

### Tor-Prozess-Management

**Status: Teilweise gemildert** (Erfordert physischen Zugriff oder Privilegierte Eskalation zum ausnutzen)

Viele Probleme können sich aus der Verwaltung eines separaten Prozesses ergeben, einschließlich der Notwendigkeit, neu zu starten, zu beenden und anderweitig eine angemessene Verwaltung zu gewährleisten.

Die [ACN](https://git.openprivacy.ca/openprivacy/connectivity/src/branch/master/acn.go) Schnittstelle bietet `Restart`, `Close` und `GetBootstrapStatus` Schnittstellen die es Anwendungen erlauben, den zugrunde liegenden Tor-Prozess zu verwalten. Zusätzlich kann die `SetStatusCallback` Methode verwendet werden, um eine Anwendung zu benachrichtigen, wenn der Status des Tor-Prozesses sich ändert.

Wenn jedoch genügend privilegierte Benutzer es wünschen, können sie diesen Mechanismus stören, und als solches ist der Tor-Prozess eine zerbrechlichere Komponente in der Interaktion als andere.

## Teststatus

Die aktuelle Konnektivität hat begrenzte Test Möglichkeiten keine davon wird während Pull-Requests oder Zusammenschlüssen ausgeführt. Es gibt keine Integrationstests.

Es ist anzumerken, dass die Konnektivität sowohl von Tapir als auch von Cwtch in ihren Integrationstests verwendet wird (und dies trotz fehlender Tests auf Paketebene ist systemweiten Prüfbedingungen ausgesetzt)

