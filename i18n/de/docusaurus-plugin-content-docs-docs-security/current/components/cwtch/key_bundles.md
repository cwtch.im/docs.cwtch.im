---
sidebar_position: 3
---

# Schlüsselbündel

Cwtch-Server identifizieren sich mit signierten Schlüsselbündeln. Diese Schlüsselbündel enthalten eine Liste der benötigten Schlüssel, um die Kommunikation der Cwtch-Gruppe sicher zu machen und Metadaten widerstandsfähig zu machen.

Zum Zeitpunkt des Schreibens wird erwartet, dass Schlüsselbündel 3 Schlüssel enthalten:

1. Ein öffentlicher Tor v3 Onion Service Public Key für das Token Board (ed25519)- verwendet um sich über Tor mit dem Dienst zu verbinden und Nachrichten zu empfangen.
2. Ein öffentlicher Schlüssel des Tor-v3-Onion Service für den Token Service (ed25519) - der verwendet wird, um Tokens zu erwerben, um über eine kleine Proof-Work-Übung auf den Dienst zu posten.
3. Ein Public Key für den Privacy-Pass - verwendet im Token-Akquisitionsprozess (ein ristretto Kurvenpunkt) . Siehe: [OPTR2019-01](https://openprivacy.ca/research/OPTR2019-01/)

Das Schlüsselbündel ist signiert und kann über den ersten v3-Onion Service Schlüssel verifiziert werden, so dass es an die onion-Adresse gebunden wird.

## Überprüfe Schlüsselbündel

Profile, die Server-Schlüsselpakete importieren, überprüfen sie anhand des folgenden "trust-on-first-use"-Algorithmus (TOFU):

1. Überprüfung der angehängten Signatur mit der v3-Onion Adresse des Servers. (Wenn dies fehlschlägt, wird der Importprozess gestoppt)
2. Überprüfung, ob jeder Schlüsseltyp existiert. (Wenn dies fehlschlägt, wird der Importprozess gestoppt)
3. Wenn das Profil zuvor das Server-Schlüsselbündel importiert hat, Sicherstellung, dass alle Schlüssel gleich sind. (Wenn dies fehlschlägt, wird der Importprozess gestoppt)
4. Speichern der Schlüssel im Kontakt-Eintrag des Servers.

In Zukunft wird dieser Algorithmus wahrscheinlich so geändert, dass neue öffentliche Schlüssel hinzugefügt werden können (z.B. um Tokens über eine Zcash Adresse zu erwerben.)

Technisch gesehen kann der Server in den Schritten (2) und (3() als bösartig angesehen werden, weil er ein gültiges Schlüsselbündel unterzeichnet hat, das nicht den Spezifikationen entspricht. Wenn Gruppen von "experimentell" in "stable" verschoben werden, führt eine solche Aktion dazu, dass eine Warnung an das Profil kommuniziert wird.