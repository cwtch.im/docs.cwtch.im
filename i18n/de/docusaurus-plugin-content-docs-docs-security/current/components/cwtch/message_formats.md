---
sidebar_position: 2
---

# Nachrichtenformate

## Peer-to-Peer Nachrichten

```
    PeerMessage {
        ID      string // A unique Message ID (primarily used for acknowledgments)
        Context string // A unique context identifier i.e. im.cwtch.chat
        Data    []byte // The context-dependent serialized data packet.
    }
```
### Kontext-Bezeichner

* `im.cwtch.raw` - Daten enthalten eine Klartext-Chat-Nachricht (siehe: [Overlays](../ui/overlays.md) für weitere Informationen)
* `im.cwtch.acknowledgement` - Daten sind leer und ID verweist auf eine zuvor gesendete Nachricht

* `im.cwtch.getVal` und `im.cwtch.retVal` - Wird für die Abfrage / Rückgabe spezifischer Informationen über einen Peer verwendet. Daten enthalten eine serialisierte `peerGetVal` und `peerRetVal` Struktur.

```
        peerGetVal struct {
                Scope string
                Path string
        }

        type peerRetVal struct {
            Val    string // Serialized path-dependent value
            Exists bool
        }
```

## Klartext / Entschlüsselte Gruppennachrichten

```
    type DecryptedGroupMessage struct {
        Text      string // plaintext of the message
        Onion     string // The Cwtch address of the sender
        Timestamp uint64 // A user specified timestamp
        // NOTE: SignedGroupID is now a misnomer, the only way this is signed is indirectly via the signed encrypted group messages
        // We now treat GroupID as binding to a server/key rather than an "owner" - additional validation logic (to e.g.
        // respect particular group constitutions) can be built on top of group messages, but the underlying groups are
        // now agnostic to those models.
        SignedGroupID      []byte 
        PreviousMessageSig []byte // A reference to a previous message
        Padding            []byte // random bytes of length = 1800 - len(Text)
    }
```
Entschlüsselte Gruppennachrichten enthalten zufällige Füllung zu einer festen Größe, die der Länge aller Felder mit fester Länge + 1800 entspricht. Dies stellt sicher, dass alle verschlüsselten Gruppen-Nachrichten gleich lang sind.

## Verschlüsselte Gruppennachrichten
```
    // EncryptedGroupMessage provides an encapsulation of the encrypted group message stored on the server
    type EncryptedGroupMessage struct {
        Ciphertext []byte
        Signature  []byte // Sign(groupID + group.GroupServer + base64(decrypted group message)) using the senders Cwtch key
    }
```
Die Berechnung der Signatur erfordert das Wissen über die Gruppen-Id der Nachricht, des Servers, mit dem die Gruppe verbunden ist und der entschlüsselten Gruppennachricht (und damit der Gruppenschlüssel). Es ist vom Absender der Nachricht (ed25519) signiert und kann mit ihrem öffentlichen Cwtch-Adressschlüssel verifiziert werden.
