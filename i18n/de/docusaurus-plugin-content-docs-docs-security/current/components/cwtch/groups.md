---
sidebar_position: 4
---

# Gruppen

Das Cwtch Risikomodell für Gruppen ist größtenteils in zwei unterschiedliche Profile aufgeteilt:

* Gruppen aus gegenseitig vertrauenswürdigen Teilnehmern, in denen Peers als ehrlich angenommen werden.
* Gruppen, die aus Fremden bestehen, in denen von Peers angenommen wird, dass sie potenziell bösartig sind.

Die meisten der in diesem Abschnitt beschriebenen Eindämmungen beziehen sich auf den letztgenannten Fall, aber wirken sich natürlich auch auf den Ersteren aus. Selbst wenn angenommen wird, dass ehrliche Peers später böswillig werden, gibt es Mechanismen, die solches Böse erkennen und verhindern können, dass es in der Zukunft passiert.

## Überblick über die Risiken: Schlüssel-Ableitung

Im Idealfall würden wir ein Protokoll wie OTR verwenden, die Einschränkungen, die uns im Moment daran hindern, sind:

* Offline-Nachrichten sind nicht garantiert, dass sie alle Peers erreichen, und als solche können alle Metadaten in Bezug auf Schlüsselmaterial verloren gehen. Wir benötigen einen Schlüsselableitungs-Prozess, der robust ist für fehlende Nachrichten oder unvollständige Übertragungen.

## Risiko: Schädlicher Peer- verrät Gruppenschlüssel und/oder Unterhaltung

**Status: Teilweise gemildert (aber unmöglich vollständig zu mildern)**

Ob mit vertrauenswürdigen kleineren Gruppen oder partiell öffentliche größeren Gruppen, es gibt *immer * die Möglichkeit, dass ein böswilliger Akteur Gruppe Nachrichten verraten kann.

Wir planen, es den Peers zu erleichtern, [Gruppen zu fork](#fork), den gleichen Schlüssel zu entschärfen, der zur Verschlüsselung vieler sensibler Informationen verwendet wird und ein gewisses Maß an Weiterleitungsgeheimhaltung für frühere Gruppengespräche bereitzustellen.

## Risiko: Aktive Angriffe von Gruppenmitgliedern

**Status: Teilweise gemildert**

Gruppenmitglieder, die Zugang zum Schlüsselmaterial der Gruppe haben, können sich mit einem Server oder anderen Gruppenmitgliedern verschwören, um die Konsistenz des Transkripts zu unterbrechen.

Während wir die Zensur angesichts dieser aktiven Absprachen nicht direkt verhindern können, so verfügen wir doch über eine Reihe von Mechanismen, die ehrlichen Gruppen-Mitgliedern das Vorhandensein einer Zensur offenbaren sollten.

### Eindämmung:

* Weil jede Nachricht vom öffentlichen Schlüssel der Peers signiert ist, es sollte nicht möglich sein (innerhalb der kryptographischen Annahmen der zugrunde liegenden Kryptographie) für ein Gruppenmitglied ein anderes nachzuahmen.
* Jede Nachricht enthält eine eindeutige Identifikation, die aus dem Inhalt abgeleitet wird und den vorherigen Hash der Nachrichten - wodurch es Kollaborateuren unmöglich gemacht wird, Nachrichten von nicht geheimen Mitgliedern einzufügen, ohne eine implizite Meldungs-Kette zu enthüllen (die wenn sie versuchen andere Nachrichten zu zensieren würde eine solche Zensur offenbaren)

Abschließend: Wir arbeiten aktiv daran, den Cwtch-Servern eine Nichtabstreitbarkeit hinzuzufügen, so dass dass sie selbst in dem, was sie effizient zensieren können, eingeschränkt sind.
