# Cwtch Server

Ziel des Cwtch-Protokolls ist es, die Gruppenkommunikation über **Nicht vertrauenswürdige Infrastruktur** zu aktivieren.

Im Gegensatz zu Relay basierten Schemata, bei denen die Gruppen einen Führer, einen Satz von Führern, oder ein vertrauenswürdiger Server von Drittanbietern zuweisen, um sicherzustellen, dass jedes Mitglied der Gruppe Nachrichten zeitnah senden und empfangen kann (selbst wenn Mitglieder offline sind) - hat die nicht vertrauenswürdige Infrastruktur das Ziel, diese Eigenschaften zu realisieren ohne Vertrauen anzunehmen.

Das ursprüngliche Cwtch-Papier hat eine Reihe von Eigenschaften definiert, die Cwtch-Server erwartbar zur Verfügung stellen sollten:

*  Cwtch Server kann von mehreren Gruppen oder nur einer verwendet werden.
* Ein Cwtch Server, ohne Zusammenarbeit mit einem Gruppenmitglied, sollte niemals die Identität der Teilnehmer innerhalb einer Gruppe erlernen.
* Ein Cwtch Server sollte niemals den Inhalt einer Kommunikation erlernen.
* Ein Cwtch-Server sollte niemals in der Lage sein, Nachrichten als einer bestimmten Gruppe zugehörig unterscheiden zu können.

Wir stellen hier fest, dass diese Eigenschaften ein Superset der Designziele der privaten Informationsabfrage sind.

## Bösartige Server

Wir erwarten das Vorhandensein bösartiger Entitäten innerhalb des Cwtch-Ökosystems.

Wir legen auch Wert auf Dezentralisierung und erlaubnisfreien Zugang zum Ökosystem und stützen daher keine Sicherheitsansprüche auf die folgenden Punkte:

* Jede Nicht-Kollusionsannahme zwischen einer Reihe von Cwtch-Servern
* Jedes von Dritten definierte Prüfverfahren

Die Peers selbst werden ermutigt, Cwtch-Server einzurichten und zu betreiben, wo sie effizientere Eigenschaften garantieren können, indem sie die Vertrauens- und Sicherheits Annahmen voraussetzen - standardmäßig ist das Protokoll jedoch so konzipiert, dass es auch ohne diese Annahmen sicher ist - auf Kosten der Effizienz, wo es nötig ist.

### Erkennbare Fehler

* Wenn ein Cwtch-Server eine bestimmte Nachricht nicht an eine Gruppe von Gruppenmitgliedern weiterleitet, wird es eine erkennbare Lücke im Nachrichtenbaum einiger Peers geben, die durch Peer-to-Peer Klatsch entdeckt werden kann.
* Ein Cwtch-Server kann keine Nachricht ohne das Schlüsselmaterial, das der Gruppe bekannt ist, ändern (jeder Versuch, dies für eine Teilmenge von Gruppenmitgliedern zu tun, führt dazu, dass es das gleiche Verhalten hat, wie wenn keine Nachricht weitergeleitet wird).
* Während ein Server Nachrichten duplizieren *kann*, haben diese keine Auswirkungen auf den Gruppen-Nachrichtenbaum (aufgrund der Verschlüsselung, Nonces und Nachrichtenidentitäten) - die Quelle der Vervielfältigung ist für einen Peer nicht bekannt.

## Effizienz

Zum Zeitpunkt des Schreibens dieser Seite ist nur ein Protokoll für das Erreichen der gewünschten Eigenschaften bekannt, naive PIR oder "der Server sendet alles, und die Peers sichten es".

Dies hat einen offensichtlichen Einfluss auf die Effizienz der Bandbreite, insbesondere für Peers mit mobilen Geräten, als solche entwickeln wir aktiv neue Protokolle, in denen die Datenschutz- und Effizienzgarantien auf unterschiedliche Weise ausgehandelt werden können.

Beim Zeitpunkt des Schreibens dieser Seite erlauben die Server sowohl einen vollständigen Download aller gespeicherten Nachrichten, und eine Anfrage, um Nachrichten von einer bestimmten Nachricht herunterzuladen.

Alle Peers, wenn sie einer Gruppe auf einem neuen Server beitreten, laden alle Nachrichten vom Server herunter und von dann an laden sie nur neue Nachrichten.

*Hinweis*: Dieses Verhalten erlaubt eine milde Form der Metadatenanalyse. Der Server kann neue Nachrichten für jedes verdächtige eindeutige Profil anzeigen und dann diese eindeutigen Signaturen benutzen um eindeutige Sitzungen im Laufe der Zeit zu verfolgen (über Anfragen für neue Nachrichten).

Dies wird durch 2 Verwirrungsfaktoren gemildert:

1. Profile können ihre Verbindungen jederzeit aktualisieren - was zu einer neuen Serversitzung führt.
2. Profile können jederzeit von einem Server "synchronisieren" - was zu einem neuen Aufruf führt, um alle Nachrichten herunterzuladen. Die am meisten verbreitete Benutzungsgrundlage für dieses Verhalten ist das Abrufen älterer Nachrichten aus einer Gruppe.

In Kombination setzen diese 2 Abschwächungen Grenzen auf das, was der Server herausfinden kann, aber wir können noch keine vollständige Metadatenresistenz bieten.

Für mögliche zukünftige Lösungen für dieses Problem siehe [Niwl](https://git.openprivacy.ca/openprivacy/niwl)

# Den Server wird vor böswilligen Peers schützen

Das Hauptrisiko für Server besteht in Form von Spam, der von Peers erzeugt wird. Im Prototyp von Cwtch wurde ein Spamschutzmechanismus eingeführt, der von anderen Peers verlangte, einen willkürlichen Nachweis der Arbeit mit einem server-spezifizierten Parameter durchzuführen.

Dies ist keine robuste Lösung in Anwesenheit eines bestimmten Gegners mit einer beträchtlichen Menge an Ressourcen und damit wird eines der wichtigsten externen Risiken für das Cwtch-System wird Zensur-Via-Ressourcen-Erschöpfung.

Wir haben eine mögliche Lösung dafür in [Token basierten Diensten](https://openprivacy.ca/research/OPTR2019-01/) skizziert, aber beachte, dass dies ebenfalls weiterentwickelt werden muss.
