---
sidebar_position: 1
---

# Paketformat

Alle Tapir-Pakete haben eine feste Länge (8192 Bytes) und die ersten 2 Bytes zeigen die tatsächliche Länge der Nachricht, `len` Bytes an Daten und der Rest mit null aufgefüllt:

    | len (2 bytes) | data (len bytes) | paddding (8190-len bytes)|

Einmal verschlüsselt, ist das gesamte 8192-Byte-Datenpaket mit [libsodium secretbox](https://libsodium.gitbook.io/doc/secret-key_cryptography/secretbox) verschlüsselt unter Verwendung der Standardstruktur ( beachte in diesem Fall die tatsächlich benutzbare Größe des Datenpakets ist 8190-14, um die Nonce in der Geheimbox zu unterbringen)

Informationen darüber, wie der geheime Schlüssel abgeleitet wird, findest du im [Authentifizierungsprotokoll](authentication_protocol.md)

 