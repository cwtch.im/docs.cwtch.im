---
sidebar_position: 1
---

# Cwtch technische Grundlagen

Diese Seite bietet einen kurzen technischen Überblick über das Cwtch-Protokoll.

## Ein Cwtch Profil

Benutzer können eines von mehreren Cwtch Profilen erstellen. Jedes Profil erzeugt ein zufälliges ed25519 Schlüsselpaar, welches kompatibel mit Tor ist.

Zusätzlich zum kryptographischen Material enthält ein Profil auch eine Liste von Kontakten (öffentliche Schlüssel für das Cwtch-Profil + zugeordnete Daten über dieses Profil wie Nickname und (optional) historische Nachrichten), eine Liste von Gruppen (mit kryptographischem Gruppenmaterial zusätzlich zu anderen zugehörigen Daten wie dem Gruppennickname und historischen Nachrichten).

## 2-Parteien-Konversionen: Peer to Peer

![](/img/BASE_3.png)

Damit 2 Parteien ein Peer-to-Peer-Gespräch führen können, müssen beide online sein, aber nur eine Partei muss über ihren Onion-Dienst erreichbar sein. Um der Klarheit willen kennzeichnen wir oft eine Partei als den "eingehenden Peer" (die Person, die den Onion-Dienst beherbergt) und die andere Partei den "ausgehenden Peer" (der sich mit dem Onion-Dienst verbindet).

Nach einer Verbindung beteiligen sich beide Seiten an einem -Authentifizierungsprotokoll von:

* Behauptet, dass jede Partei Zugriff auf den privaten Schlüssel hat, der mit ihrer öffentlichen Identität verbunden ist.
* Erzeugt einen kurzlebigen Sitzungs-Schlüssel, der zur Verschlüsselung aller weiteren Kommunikation während der Sitzung verwendet wird.

Dieser Austausch (detailliert im [-Authentifizierungsprotokoll](tapir/authentication_protocol.md)dokumentiert) ist *offline verweigerbar* d. h. es ist möglich für jede Seite Transkripte dieses Protokoll-Austausches zu fälschen und daher ist es unmöglich definitiv zu beweisen, dass der Austausch überhaupt stattgefunden hat.

Nach dem Authentifizierungsprotokoll können beide Parteien frei miteinander kommunizieren.

## Mehrparteien-Unterhaltungen: Gruppen und Kommunikation zwischen Peer to Server

**Hinweis: Metadaten resistente Kommunikation ist noch ein aktives Entwicklungsfeld und was hier dokumentiert ist wird sehr wahrscheinlich in der Zukunft sich ändern.**

Wenn eine Person eine Gruppendiskussion starten möchte, generiert sie einen zufälligen geheimen `Gruppenschlüssel`. Alle Gruppenkommunikation wird mit diesem Schlüssel verschlüsselt.

Zusammen mit dem `Gruppenschlüssel` entscheidet der Gruppenersteller sich auch für einen **Cwtch Server** als Host der Gruppe zu verwenden. Weitere Informationen darüber, wie Server sich selbst authentifizieren, findest du unter [Schlüsselbündel](cwtch/key_bundles.md).

Ein `Gruppen Identifikator` wird mit dem Gruppenschlüssel und dem Gruppenserver generiert und diese drei Elemente sind in einer Einladung verpackt, die an potenzielle Gruppenmitglieder gesendet werden kann (z.B. über existierende Peer-to-Peer-Verbindungen).

Um eine Nachricht an die Gruppe zu senden, verbindet sich ein Profil mit dem Server, der die Gruppe beherbergt (siehe unten), und verschlüsselt deine Nachricht mit dem `Gruppenschlüssel` und erzeugt eine kryptographische Signatur über die `Gruppen-Id`, `Gruppen Server` und die entschlüsselte Nachricht (siehe: [Nachrichtenformate](cwtch/message_formats.md) für weitere Informationen).

Um Nachrichten von der Gruppe zu erhalten, verbindet sich ein Profil mit dem Server, der die Gruppe beherbergt und lädt *alle* Nachrichten (seit seiner vorherigen Verbindung) herunter. Die Profile versuchen dann jede Nachricht mit dem  `Gruppenschlüssel` zu entschlüsseln und wenn dies erfolgreich war, dann die Signatur zu verifizieren (siehe [Cwtch Server](cwtch/server.md)  [Cwtch Gruppen](./cwtch/groups.md) für einen Überblick über Attacken und Abschwächungen).

### Server sind Peers

In vielerlei Hinsicht ist die Kommunikation mit einem Server identisch mit einer regulären Cwtch Gegenstelle, die gleichen Schritte oben werden durchgeführt, aber der Server fungiert immer als eingehender Peer, und der ausgehende Peer verwendet immer neu generierte **kurzlebige Schlüsselpaare** als "Langfristige Identität".

Somit unterscheidet sich die Peer zu Server Konversation nur in der *Art* der Nachrichten, die zwischen zwei Seiten gesendet werden, mit dem Server, der alle Nachrichten, die er erhält, weiterleitet und außerdem den Clients erlaubt den Server nach älteren Nachrichten abzufragen.

