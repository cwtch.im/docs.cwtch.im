# Nachrichten Overlays

[Angepasst von: Discreet Log #8: Anmerkungen zur Cwtch Chat API](https://openprivacy.ca/discreet-log/08-chatapi/)

**Hinweis: Dieser Abschnitt behandelt Overlay-Protokolle, die auf das Cwtch-Protokoll aufsetzen. Für Informationen über das Cwtch-Protokoll Nachrichten selbst schaue unter [Nachrichtenformate](../cwtch/message_formats.md)**

Wir sehen Cwtch als eine Plattform für die Bereitstellung einer authentifizierten Transportschicht für Anwendungen auf höherer Ebene. Es steht den Entwicklern frei, selbst zu entscheiden, welche Protokolle der Anwendungsschicht sie verwenden wollen, ob sie maßgeschneiderte binäre Nachrichtenformate wünschen oder einfach nur eine HTTP-Bibliothek aufsetzen und die Sache abhaken wollen. Cwtch kann neue Schlüsselpaare für Sie generieren (die zu Onion-Adressen werden; es sind keine DNS-Registrierungen erforderlich!) und du kannst mit REST sicher sein, dass alle Daten, die deine Anwendung vom (anonymen Kommunikations-) Netz empfängt, bereits authentifiziert wurden.

In unserem aktuellen Stack sind die Nachrichten in einen minimalen JSON-Rahmen verpackt, der einige kontextbezogene Informationen über den Nachrichtentyp hinzufügt. Und da es sich bei serialisierten JSON-Objekten nur um Wörterbücher handelt, können wir später bei Bedarf problemlos weitere Metadaten hinzufügen.


## Chat-Overlays, Listen und Bulletins

Das ursprüngliche Cwtch alpha zeigte "Overlays": verschiedene Arten, denselben Datenkanal zu interpretieren, abhängig von der Struktur der atomaren Daten selbst. Wir haben einfache Checklisten und BBS/Klassifizierungsanzeigen als Overlays eingefügt, die mit jedem Cwtch-Kontakt angeschaut und geteilt werden konnten, sei es mit einem einzelnen Peer oder einer Gruppe. Das Übertragungsformat sah wie folgt aus:

```
{o:1,d:"hey there!"}
{o:2,d:"bread",l:"groceries"}
{o:3,d:"garage sale",p:"[parent message signature]"}
```

Overlay-Feld `o` bestimmt, ob es ein Chat (1), Liste (2), oder Bulletin (3) Nachricht war. Das Datenfeld `d` ist überladen und Listen/Bulletins benötigen zusätzliche Informationen darüber, welcher Gruppe/Beitrag sie angehören. (Wir verwenden Nachrichtensignaturen anstelle von IDs, um Probleme wie Ordnungsprobleme und böswillig erstellte IDs zu vermeiden. Auf diese Weise teilt auch das Cwtch-Protokoll dem Frontend mit, welche Nachricht abgeholt wird)

## Datenstruktur

Die Implementierung von baumstrukturierten Daten auf einem sequentiellen Nachrichtenspeicher hat offensichtliche Leistungsnachteile. Betrachte z. B. die Nachrichtenansicht, die die neuesten Nachrichten zuerst lädt und nur so weit zurückgeht, dass sie das aktuelle Ansichtsfenster ausfüllt, im Vergleich zu einem (etwas pathologischen) Forum, in dem fast jede Nachricht ein Kind der allerersten Nachricht in der Historie ist, die Gigabytes an Daten enthalten kann. Wenn die Benutzeroberfläche nur Beiträge der obersten Ebene anzeigt, bis der Benutzer sie erweitert, müssen wir den gesamten Verlauf analysieren, bevor wir genügend Informationen erhalten, um überhaupt etwas anzuzeigen.

Ein weiteres Problem besteht darin, dass das Multiplexen all dieser Überlagerungen in einem Datenspeicher "Löcher" in den Daten erzeugt, die [lazy-loaded listviews](https://api.flutter.dev/flutter/widgets/ListView/ListView.builder.html) und Scrollbars verwirren. Die Anzahl der Nachrichten kann darauf hinweisen, dass es eine Menge weiterer Informationen gibt, die angezeigt werden können, wenn der Benutzer einfach scrollt, aber wenn sie tatsächlich abgerufen und geparst werden, könnten wir feststellen, dass nichts davon für das aktuelle Overlay relevant ist.

Keines dieser Probleme ist unüberwindbar, aber sie zeigen einen Fehler in unseren ursprünglichen Annahmen über die Art des kollaborativen Nachrichtenflusses und die Art und Weise, wie wir mit diesen Daten umgehen sollten.

# Overlay Typen

Wie oben erwähnt, werden Overlays in einem sehr einfachen JSON-Format mit der folgenden Struktur angegeben:
```
    type ChatMessage struct {
        O int    `json:"o"`
        D string `json:"d"`
    }
```
Wo O für `Overlay` mit den unten dokumentierten unterstützten Overlays steht:
```
    1: data is a chat string
    2: data is a list state/delta
    3: data is a bulletin state/delta
    100: contact suggestion; data is a peer onion address
    101: contact suggestion; data is a group invite string
```
## Chat-Nachrichten (Overlay 1)

Die einfachste Variante ist eine Chat-Nachricht, die einfach nur rohe, unverarbeitete Informationen über die Chat-Nachricht enthält.

```
{o:1,d:"got milk?"}
```

## Einladungen (Overlays 100 und 101)

Anstatt die Einladung als eingehende Kontaktanfrage auf Profilebene zu erhalten, werden neue Inline-Einladungen mit einem bestimmten Kontakt/einer bestimmten Gruppe geteilt, wo sie später eingesehen und/oder angenommen werden können, auch wenn sie zunächst (möglicherweise versehentlich) abgelehnt wurden.

Das Format ist für diese gleichermaßen einfach:

```
{o:100,d:"u4ypg7yyyrrvf2aceeclq5dgwtkirzletltbqofnb6km7u542qqk4jyd"}
{o:101,d:"torv3eyJHcm91cElEIjoiOWY3MWExYmFhNDkzNTAzMzAyZDFmODRhMzI2ODY2OWUiLCJHcm91cE5hbWUiOiI5ZjcxYTFiYWE0OTM1MDMzMDJkMWY4NGEzMjY4NjY5ZSIsIlNpZ25lZEdyb3VwSUQiOiJyVGY0dlJKRkQ2LzFDZjFwb2JQR0xHYzdMNXBKTGJTelBLRnRvc3lvWkx6R2ZUd2Jld0phWllLUWR5SGNqcnlmdXVRcjk3ckJ2RE9od0NpYndKbCtCZz09IiwiVGltZXN0YW1wIjowLCJTaGFyZWRLZXkiOiJmZVVVQS9OaEM3bHNzSE9lSm5zdDVjNFRBYThvMVJVOStPall2UzI1WUpJPSIsIlNlcnZlckhvc3QiOiJ1cjMzZWRid3ZiZXZjbHM1dWU2anBrb3ViZHB0Z2tnbDViZWR6ZnlhdTJpYmY1Mjc2bHlwNHVpZCJ9"}
```

Dies stellt eine Abkehr von unserem ursprünglichen "Overlays"-Denken hin zu einer stärker handlungsorientierten Darstellung dar. Das Chat-"Overlay" kann mitteilen, dass jemand etwas *getan* hat, selbst wenn es auf "ein Element zu einer Liste hinzugefügt" umschrieben wird, und die Listen und Bulletins und andere schön chaotische Daten können ihren Zustand vorberechnet und separat gespeichert haben.

## Listen / Bulletin Boards

**Hinweis: Wird voraussichtlich in Cwtch Beta 1.5 definiert werden**
