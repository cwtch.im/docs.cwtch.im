# Bildervorschau

Basierend auf dem Filesharing in Cwtch 1.3 werden die Bildvorschauen durch die Erweiterung des vorgeschlagenen Dateinamens (und nein, wir sind nicht daran interessiert, MIME-Typen oder magische Zahlen zu verwenden) und die angezeigte Größe bestimmt. Wenn diese Option aktiviert ist, lädt das Vorschausystem automatisch freigegebene Bilder in einen konfigurierten Download-Ordner herunter und zeigt sie als Teil der Nachricht selbst an. (Aufgrund von Beschränkungen auf Android werden sie im privaten Cache der App gespeichert und Sie haben die Möglichkeit, sie später an anderer Stelle zu speichern) Die Dateigrößenbeschränkung ist noch nicht festgelegt, wird aber deutlich niedriger sein als die allgemeine Größenbeschränkung für die gemeinsame Nutzung von Dateien, die derzeit bei 10 Gigabyte liegt.

Im Moment unterstützen wir nur Einzelbildnachrichten und jede Bildbearbeitung/jeder Bildausschnitt muss in einer separaten Anwendung erfolgen. Wie in den FAQ zum Filesharing erwähnt, enthalten Bilddateien häufig auch wichtige versteckte Metadaten und du solltest sie nur mit Personen teilen, denen du vertraust.

## Bekannte Risiken

## Andere Anwendungen und/oder das Betriebssystem, die Informationen aus Bildern ableiten

Bilder müssen irgendwo gespeichert werden und wir haben uns dafür entschieden, sie zunächst unverschlüsselt im Dateisystem zu speichern. Wir haben dies aus 2 Gründen gemacht:

1. Um leistungsfähigere Dateifreigabeverfahren wie Rehosting zu unterstützen, müssen wir in der Lage sein, Dateien effizient zu scannen und Chunks zu liefern - dies über eine verschlüsselte Datenbankschicht zu tun, würde die Leistung beeinträchtigen.
2. Diese Informationen müssen immer die Anwendungsgrenze passieren (entweder über Anzeigetreiber oder durch Speichern und Anzeigen der Datei in einer externen Anwendung) - es gibt nichts, was Cwtch nach diesem Punkt tun kann.

## Bösartige Bilder, die Cwtch zum Absturz bringen oder anderweitig kompromittieren

Flutter verwendet Skia, um Bilder zu rendern. Der zugrundeliegende Code ist zwar speicherunsicher, wird aber im Rahmen der regulären Entwicklung [ausführlich zerfusselt](https://github.com/google/skia/tree/main/fuzz).

Wir führen auch unsere eigenen Fuzz-Tests von Cwtch-Komponenten durch. Bei dieser Analyse haben wir einen einzigen Absturzfehler gefunden, der mit einer fehlerhaften GIF-Datei, die den Renderer dazu veranlasste, eine lächerliche Menge an Speicher zuzuweisen (und schließlich vom Kernel abgelehnt wurde). Um zu verhindern, dass sich dies auf Cwtch auswirkt, haben wir die Richtlinie eingeführt, immer eine maximale `cacheWidth` und/oder `cacheHeight` für Bild-Widgets.

## Bösartige Bilder werden auf verschiedenen Plattformen unterschiedlich gerendert, was zur Offenlegung von Metadaten führen kann

Vor kurzem wurde [ein Fehler in Apples png-Parser](https://www.da.vidbuchanan.co.uk/widgets/pngdiff/) gefunden, der dazu führte, dass ein Bild auf Apple-Geräten anders dargestellt wurde als auf Nicht-Apple-Geräten.

Wir haben einige Tests mit unseren Mac-Builds durchgeführt und konnten dieses Problem für Flutter nicht replizieren (da alle Flutter-Builds Skia für das Rendering verwenden), aber wir werden solche Fälle weiterhin in unseren Testkorpus aufnehmen.

Die Bildvorschau bleibt vorerst experimentell und auf freiwilliger Basis.

