# Eingabe


## Risiko: Abfangen von Cwtch-Inhalten oder Metadaten durch eine IME auf mobilen Geräten

**Status: Teilweise gemildert**

Jede Komponente, die die Möglichkeit hat, Daten zwischen einer Person und der Cwtch-App abzufangen, stellt ein potenzielles Sicherheitsrisiko.

Einer der wahrscheinlichsten Abfangjäger ist ein IME (Input Method Editor) eines Drittanbieters, der häufig um Zeichen zu erzeugen, die von ihrem Gerät nicht unterstützt werden.

Selbst gutartige und standardmäßige IME-Anwendungen können unbeabsichtigt Informationen über den Inhalt einer Person preisgeben, z. B. durch Cloud-Synchronisierung, Cloud-Übersetzung oder persönliche Wörterbücher.

Letztlich kann dieses Problem nicht von Cwtch allein gelöst werden, sondern stellt ein größeres Risiko dar, welches auf das gesamte mobile Ökosystem auswirkt.

Ein ähnliches Risiko besteht auf dem Desktop durch die Verwendung ähnlicher Eingabeanwendungen (zusätzlich zu den Software-Keyloggern), Wir sind jedoch der Ansicht, dass dies völlig außerhalb des Rahmens der Cwtch-Risikobewertung liegt (in Übereinstimmung mit anderen Angriffen auf die Sicherheit des zugrunde liegenden Betriebssystems selbst).

Dies wird in Cwtch 1.2 teilweise durch die Verwendung von `enableIMEPersonalizedLearning: false` entschärft. Siehe [diesen PR](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/142) für weitere Informationen.