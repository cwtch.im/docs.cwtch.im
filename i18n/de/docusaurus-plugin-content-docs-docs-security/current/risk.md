---
sidebar_position: 2
---

# Risiko-Modell

Es ist bekannt, dass Kommunikationsmetadaten von verschiedenen Gegnern ausgenutzt werden, um die Sicherheit von Systemen zu untergraben, Opfer zu verfolgen und groß angelegte Analysen sozialer Netzwerke durchzuführen, um die Massenüberwachung zu unterstützen. Metadaten-resistente Tools stecken noch in den Kinderschuhen und es fehlt an Forschung zur Konstruktion und Benutzererfahrung solcher Tools.

![](/img/4.png)

Cwtch wurde ursprünglich als Erweiterung des Metadaten-resistenten Protokolls Ricochet konzipiert, um die asynchrone Multi-Peer-Gruppenkommunikation durch die Verwendung einer verwerfbaren, nicht vertrauenswürdigen, anonymen Infrastruktur zu unterstützen.

Seitdem hat sich Cwtch zu einem eigenständigen Protokoll entwickelt. In diesem Abschnitt werden die verschiedenen bekannten Risiken beschrieben, die Cwtch zu mindern versucht, und es wird im Rest des Dokuments stark darauf verwiesen, wenn die verschiedenen Unterkomponenten der Cwtch-Architektur erörtert werden.

## Bedrohungsmodell

Es ist wichtig zu erkennen und zu verstehen, dass Metadaten in Kommunikationsprotokollen allgegenwärtig sind, es ist in der Tat notwendig, dass solche Protokolle effizient und in großem Umfang funktionieren. Informationen, die für die Unterstützung von Peers und Servern nützlich sind, sind jedoch auch für Gegner, die solche Informationen ausnutzen möchten, von großer Bedeutung.

Für unsere Problemstellung gehen wir davon aus, dass der Inhalt einer Kommunikation so verschlüsselt ist, dass ein Angreifer sie praktisch nicht knacken kann (siehe [tapir](/security/category/tapir) und [cwtch](security/category/cwtch) für Details zu der von uns verwendeten Verschlüsselung) und daher konzentrieren wir uns auf den Kontext der Kommunikationsmetadaten.

Wir bemühen uns, die folgenden Kommunikationskontexte zu schützen:

* Wer ist an einer Kommunikation beteiligt? Es kann möglich sein, Personen oder einfach Geräte- oder Netzwerkkennungen zu identifizieren. Beispiel: „An dieser Kommunikation sind Alice, eine Journalistin, und Bob, ein Regierungsangestellter, beteiligt.“.
* Wo sind die Gesprächsteilnehmer? Beispiel: „Während dieser Kommunikation war Alice in Frankreich und Bob in Kanada.“
* Wann hat ein Gespräch stattgefunden? Der Zeitpunkt und die Länge der Kommunikation können viel über die Art eines Anrufs verraten, z. B. „Bob, ein Regierungsangestellter, hat gestern Abend eine Stunde lang mit Alice telefoniert. Dies ist das erste Mal, dass sie kommuniziert haben.“ *Wie wurde das Gespräch vermittelt? Ob eine Konversation über eine verschlüsselte oder unverschlüsselte E-Mail stattgefunden hat, kann nützliche Informationen liefern. Beispiel: „Alice hat gestern eine verschlüsselte E-Mail an Bob gesendet, während sie normalerweise nur Klartext-E-Mails aneinander senden.“
* Worum geht es in dem Gespräch? Selbst wenn der Inhalt der Kommunikation verschlüsselt ist, ist es manchmal möglich, einen wahrscheinlichen Kontext eines Gesprächs abzuleiten, ohne genau zu wissen, was gesagt wird, z. „Eine Person hat zum Abendessen in einem Pizzaladen angerufen“ oder „Jemand hat um 3 Uhr morgens eine bekannte Selbstmord-Hotline angerufen.“

Über einzelne Konversationen hinaus versuchen wir auch, uns gegen Kontextkorrelationsangriffe zu verteidigen, wobei mehrere Konversationen analysiert werden, um Informationen auf höherer Ebene abzuleiten:

* Beziehungen: Entdeckung sozialer Beziehungen zwischen zwei Entitäten durch Analyse der Häufigkeit und Länge ihrer Kommunikation über einen bestimmten Zeitraum. z.B. Carol und Eve telefonieren jeden Tag mehrere Stunden am Stück.
* Cliquen: Entdeckung sozialer Beziehungen zwischen einer Gruppe von Einheiten, die alle miteinander interagieren. z.B. Alice, Bob und Eve kommunizieren alle miteinander.
* Lose verbundene Cliquen und Brücken-Individuen: Entdeckung von Gruppen, die über Vermittler miteinander kommunizieren, indem Kommunikationsketten analysiert werden (z. B. jedes Mal, wenn Alice mit Bob spricht, spricht sie fast unmittelbar danach mit Carol; Bob und Carol kommunizieren nie.)
* Muster des Lebens: Entdecken, welche Kommunikation zyklisch und vorhersehbar ist. z.B. Alice ruft Eve jeden Montagabend etwa eine Stunde lang an.


### Aktive Angriffe

#### Falschdarstellungsangriffe

Cwtch bietet keine globale Anzeigenamenregistrierung und daher sind Personen, die Cwtch verwenden, anfälliger für Angriffe, die auf falscher Darstellung beruhen, d. h. Personen, die vorgeben, andere Personen zu sein:

Ein grundlegender Ablauf eines dieser Angriffe sieht wie folgt aus, obwohl auch andere Abläufe existieren:

- Alice hat einen Freund namens Bob und einen anderen namens Eve
- Eve findet heraus, dass Alice einen Freund namens Bob hat
- Eve erstellt Tausende neuer Konten, um eines zu finden, das ein ähnliches Bild / einen ähnlichen öffentlichen Schlüssel wie Bob hat (wird nicht identisch sein, könnte aber jemanden für ein paar Minuten täuschen)
- Eve nennt dieses neue Konto "Eve Neues Konto" und fügt Alice als Freundin hinzu.
- Eve ändert dann ihren Namen auf "Eve Neues Konto" in "Bob"
- Alice sendet Nachrichten, die für „Bob“ bestimmt sind, an Eves gefälschtes Bob-Konto

Da es bei Angriffen mit falscher Darstellung an sich um Vertrauen und Verifizierung geht, besteht die einzige absolute Möglichkeit, sie zu verhindern, darin, dass Benutzer den öffentlichen Schlüssel absolut validieren. Das ist offensichtlich nicht ideal und wird in vielen Fällen einfach *nicht passieren*.

Daher möchten wir einige Hinweise zur Benutzererfahrung in der [ui](/security/category/cwtch-ui) bereitstellen, um Menschen bei der Entscheidung zu helfen, Konten zu vertrauen und/oder Konten zu unterscheiden die möglicherweise versuchen, sich als andere Benutzer auszugeben.

## Eine Anmerkung zu physischen Angriffen

Cwtch betrachtet Angriffe, die physischen Zugriff (oder gleichwertigen Zugriff) auf den Computer des Benutzers erfordern, nicht als praktisch verteidigbar. Im Interesse einer guten Sicherheitstechnik werden wir jedoch in diesem Dokument immer noch auf Angriffe oder Bedingungen verweisen, die ein solches Privileg erfordern und darauf hinweisen, wo von uns eingerichtete Minderungsmaßnahmen fehlschlagen.