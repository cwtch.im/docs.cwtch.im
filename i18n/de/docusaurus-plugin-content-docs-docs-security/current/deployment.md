# Veröffentlichung


## Risiko: Binärdateien werden auf der Website durch böswillige Versionen ersetzt

**Status: Teilweise gemildert**

Während dieser Prozess jetzt größtenteils automatisiert ist, sollte diese Automatisierung jemals kompromittiert sein, dann gibt es in unserem aktuellen Prozess nichts, was dies erkennen würde.

Wir benötigen:

* Reproduzierbare Builds - derzeit verwenden wir öffentliche Docker-Container für alle Builds, was jedem erlauben sollte, verteilte Builds mit denen aus dem Quellcode zu vergleichen.
 * Signierte Veröffentlichungen - Open Privacy verwaltet noch keinen öffentlichen Datensatz von Mitarbeitern öffentlichen Schlüsseln. Dies ist wahrscheinlich eine Notwendigkeit um veröffentlichte Builds zu signieren und eine Audit-Kette zu erstellen, die von der Organisation unterstützt wird. Dieser Prozess muss per Definition manuell sein.
  
