---
sidebar_position: 1
---

# Cwtch Sicherheitshandbuch

Willkommen im Cwtch Sicherheitshandbuch! Der Zweck dieses Handbuchs ist es, eine Anleitung für die verschiedenen Komponenten des Cwtch Ökosystems zu liefern um die bekannten Risiken und Abschwächungen zu dokumentieren und Diskussionen über Verbesserungen und Aktualisierungen für Cwtch sichere Entwicklungsprozesse zu ermöglichen.

![](https://docs.openprivacy.ca/cwtch-security-handbook/2.png)


## Was ist Cwtch?

Cwtch (/kʊtʃ/ - ein walisisches Wort, das in etwa mit "eine Umarmung, die einen sicheren Ort schafft" übersetzt werden kann) ist ein dezentralisiertes, datenschutzfreundliches Mehrparteien-Nachrichtenprotokoll, das zum Aufbau metadatenresistenter Anwendungen verwendet werden kann.

* **Dezentralisiert und offen**: Es gibt keinen „Cwtch-Dienst“ oder „Cwtch-Netzwerk“. Die Cwtch Teilnehmer können ihre eigenen sicheren Räume hosten oder ihre Infrastruktur anderen anbieten, die auf der Suche nach einem sicheren Raum sind. Das Cwtch-Protokoll ist offen und jeder kann Bots, Dienste und Benutzeroberflächen erstellen und in Cwtch integrieren und interagieren.
* **Datenschutz wahrend**: Alle Kommunikation in Cwtch ist Ende-zu-Ende verschlüsselt und findet über Tor v3 onion Dienste statt.
* **Metadaten resistent**: Cwtch wurde so konzipiert, dass ohne deine ausdrückliche Zustimmung keine Informationen ausgetauscht oder zugänglich sind einschließlich On-the-wire Nachrichten und Protokoll-Metadaten.


## Eine (kurze) Geschichte des metadatenresistenten Chats

In den letzten Jahren ist das öffentliche Bewusstsein für die Notwendigkeit und Vorteile von Ende-zu-Ende-verschlüsselten Lösungen mit Anwendungen wie [Signal](https://signalapp.org), [Whatsapp](https://whatsapp.com) und [Wire](https://wire.org) gestiegen. Diese bieten Benutzern jetzt sichere Kommunikation.

Diese Werkzeuge benötigen jedoch verschiedene Ebenen von Metadaten, um zu funktionieren und viele dieser Metadaten können verwendet werden, um Details darüber zu erfahren, wie und warum eine Person ein Kommunikationstool nutzt. [[rottermanner2015privacy]](https://www.researchgate.net/profile/Peter_Kieseberg/publication/299984940_Privacy_and_data_protection_in_smartphone_messengers/links/5a1a9c29a6fdcc50adeb1335/Privacy-and-data-protection-in-smartphone-messengers.pdf).

Ein Werkzeug, das versucht hat, Metadaten zu reduzieren, ist [Ricochet](https://ricochet.im) welches 2014 zum ersten Mal veröffentlicht wurde. Ricochet nutzte die Onion-Dienste von Tor v2, um eine sichere Ende-zu-Ende-verschlüsselte Kommunikation zu gewährleisten und die Metadaten der Kommunikation zu schützen.

Es gab keine zentralisierten Server, die beim Routen von Ricochet Unterhaltungen helfen. Keine andere als die an einem Gespräch beteiligten Parteien konnte wissen, dass ein solches Gespräch stattfindet.

Ricochet war nicht ohne Einschränkungen, es gab weder eine Multidevice Unterstützung noch einen Mechanismus zur Unterstützung der Gruppenkommunikation oder eine Unterstützung zum Senden von Nachrichten, während ein Kontakt offline ist.

Dies machte die Annahme von Ricochet zu einer schwierigen Angelegenheit. selbst diejenigen in Umgebungen, die am besten von Metadatenresistenz profitieren würden, wissen nicht, dass es [[ermoshina2017can]](https://www.academia.edu/download/53192589/ermoshina-12.pdf) gibt [[renaud2014doesn]](https://eprints.gla.ac.uk/116203/1/116203.pdf).

Darüber hinaus ist jede Lösung für dezentralisierte, metadatenresistente Kommunikation mit [grundlegenden Problemen](https://code.briarproject.org/briar/briar/-/wikis/Fundamental-Problems) konfrontiert, wenn es um Effizienz, Datenschutz und Gruppensicherheit geht (wie von [Konsens und Konsistenz der Transkription](https://code.briarproject.org/briar/briar/-/wikis/Fundamental-Problems) definiert).

Moderne Alternativen zu Ricochet sind [Briar](https://briarproject.org), [Zbay](https://www.zbay.app/) und [Ricochet Refresh](https://www.ricochetrefresh.net/) - Jedes Tool versucht, für eine andere Reihe von Kompromissen zu optimieren, z. Briar möchte es Menschen ermöglichen, [auch dann zu kommunizieren, wenn die zugrunde liegende Netzwerkinfrastruktur ausgefallen ist](https://briarproject.org/how-it-works/), und bietet gleichzeitig Schutz vor Metadatenüberwachung.

<hr />

Das Cwtch-Projekt begann 2017 als ein Erweiterungsprotokoll für Ricochet, das Gruppengespräche über nicht vertrauenswürdige Server, um dezentrale, metadatenresistente Anwendungen zu ermöglichen (wie gemeinsame Listen und Bulletin Board)

Eine Alpha-Version von Cwtch wurde [im Februar 2019 gestartet](https://openprivacy.ca/blog/2019/02/14/cwtch-alpha/) und seitdem hat das Cwtch-Team ( betrieben von der [Open Privacy Research Society](https://openprivacy.ca)) Forschung und Entwicklung zu Cwtch und den zugrunde liegenden Protokollen und Bibliotheken und Problembereichen durchgeführt.





