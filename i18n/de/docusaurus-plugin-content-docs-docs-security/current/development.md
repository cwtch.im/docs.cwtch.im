# Entwicklung

Der Hauptprozess gegen böswillige Akteure bei der Entwicklung von Cwtch ist die Offenheit des Prozesses.

Um diese Offenheit zu verstärken, werden automatisierte buidls, Tests und Paketierungen als Teil der Repositories definiert - Verbesserung der Robustheit der Codebasis in jeder Phase.

![](https://docs.openprivacy.ca/cwtch-security-handbook/1.png)

Während die einzelnen Tests nicht perfekt sind und alle Prozesse Lücken haben, sollten wir verpflichtet sein, es so einfach wie möglich machen, etwas zu Cwtch beizutragen und gleichzeitig Pipelines und Prozesse so erstellen, dass Fehler (unabsichtlich oder bösartig) so früh wie möglich abgefangen werden.

### Risiko: Entwickler liefert bösartigen Code direkt aus

**Status: Gemildert**

`trunk` ist derzeit gesperrt und nur 3 Open Privacy Mitarbeiter haben die Berechtigung um es zu überschreiben und darüber hinaus die Verantwortung für die Überwachung der Änderungen.

Darüber hinaus löst jeder neue Pull-Request und Merge automatisierte Builds & Tests aus, die E-Mails und Audit Protokolle auslösen.

Der Code ist auch Open Source und von jedem überprüfbar.

### Risiko: Code Regressionen

**Status: Teilweise gemildert** (Siehe einzelne Projekteinträge in diesem Handbuch für weitere Informationen)

Unsere automatisierten Pipelines haben die Möglichkeit, Regressionen zu erfassen, wenn dieses Verhalten erkennbar ist.

Die größte Herausforderung besteht darin, festzulegen, wie solche Regressionen für die [UI](/security/category/cwtch-ui) erkannt werden - wo das Verhalten nicht so streng definiert ist wie für die einzelnen Bibliotheken.
 