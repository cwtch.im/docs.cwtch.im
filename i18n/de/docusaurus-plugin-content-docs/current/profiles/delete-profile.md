---
sidebar_position: 6
---

# Ein Profil löschen

:::warning Warnung

Diese Funktion wird das Schlüsselmaterial **unwiderruflich** löschen. Dies **kann nicht rückgängig gemacht werden**.

:::

1. Drücke den Stift neben dem Profil, das du bearbeiten möchtest
2. Scrolle nach unten zum Ende des Bildschirms
3. Löschen drücken
4. Drücke wirklich Profil löschen