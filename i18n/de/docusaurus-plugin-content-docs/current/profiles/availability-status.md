---
sidebar_position: 14
---

# Verfügbarkeitsstatus einstellen

:::warning Warnung Neues Feature

Neu in [Cwtch 1.12](/blog/cwtch-nightly-1-12)

Diese Funktionalität kann unvollständig und/oder gefährlich sein, wenn sie falsch benutzt wird. Bitte hilf uns bei der Überprüfung und dem Test.
:::

Klicke im [-Konversationsfenster](https://docs.cwtch.im/docs/category/conversations) auf das Status-Symbol neben deinem Profilbild.

<figure>

[![](/img/profiles/status-tooltip.png)](/img/profiles/status-tooltip.png)

<figcaption></figcaption>
</figure>

Ein Dropdown-Menü wird angezeigt mit verschiedenen Optionen wie Verfügbar, Abwesend und Beschäftigt

<figure>

[![](/img/profiles/status-tooltip-busy.png)](/img/profiles/status-tooltip-busy.png)

<figcaption></figcaption>
</figure>

Wenn du Abwesend oder Beschäftigt als Status auswählst, wird sich die Grenzlinie deines Profilbildes ändern, um den Status zu reflektieren
<figure>

[![](/img/profiles/status-tooltip-busy-set.png)](/img/profiles/status-tooltip-busy-set.png)

<figcaption></figcaption>
</figure>

Kontakte sehen diese Änderung in ihrem Konversationsfenster.

<figure>

[![](/img/profiles/status-busy.png)](/img/profiles/status-busy.png)

<figcaption></figcaption>
</figure>




