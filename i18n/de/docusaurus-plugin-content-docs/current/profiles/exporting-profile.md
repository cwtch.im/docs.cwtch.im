---
sidebar_position: 10
---

# Sicherung oder Export eines Profils

Auf der Profil-Verwaltungsseite:

1. Klicke auf den Stift neben dem Profil, das du bearbeiten möchtest
2. Scrolle nach unten zum Ende des Bildschirms
3. Wähle "Export Profil"
4. Wähle einen Ort und einen Dateinamen
5. Bestätigen

Nach der Bestätigung wird Cwtch eine Kopie des Profils an der angegebenen Stelle ablegen. Diese Datei wird auf die gleiche Stufe verschlüsselt wie das Profil. Siehe [Eine Notiz über passwortgeschützte (verschlüsselte) Profile](/docs/profiles/create-a-profile#a-note-on-password-protected-encrypted-profiles) für weitere Informationen zu verschlüsselten Profilen.

Diese Datei kann in eine andere Cwtch-Instanz auf jedem Gerät [importiert](/docs/profiles/importing-a-profile) werden.