---
sidebar_position: 1
---

# Eine Einführung in die Cwtch Profile

Mit Cwtch kannst Du eines von mehrere **Profile** erstellen. Jedes Profil erzeugt ein zufälliges ed25519 Schlüsselpaar, welches kompatibel mit dem Tor-Netzwerk ist.

Dies ist der Identifikator, den Du Personen geben kannst und den diese verwenden können, um dich über Cwtch zu kontaktieren.

Cwtch ermöglicht das Erstellen und Verwalten mehrerer separater Profile. Jedes Profil ist mit einem anderen Schlüsselpaar verknüpft, welches einen anderen Onion-Dienst startet.

## Profile verwalten

Beim Start startet Cwtch den Bildschirm "Profile verwalten". Von diesem Bildschirm aus kannst Du:

- [Neues Profil erstellen](https://docs.cwtch.im/docs/profiles/create-a-profile)
- [Existierende verschlüsselte Profile freischalten](https://docs.cwtch.im/docs/profiles/unlock-profile)
- Geladene Profile verwalten
    - [Ändern des Anzeigennamens eines Profils](https://docs.cwtch.im/docs/profiles/change-name/)
    - [Ändern des Passwortes eines Profils](https://docs.cwtch.im/docs/profiles/change-password/)
    - [Löschen eines Profils](https://docs.cwtch.im/docs/profiles/delete-profile)
    - [Ändern eines Profilbildes](https://docs.cwtch.im/docs/profiles/change-profile-image/)