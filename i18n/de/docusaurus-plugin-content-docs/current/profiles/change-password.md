---
sidebar_position: 3
---

# Passwort ändern

1. Drücke den Stift neben dem Profil, das du bearbeiten möchtest
2. Gehe zum aktuellen Passwort und gib dein aktuelles Passwort ein
3. Gehe zu neuem Passwort und gib dein neues Passwort ein
4. Passwort erneut eingeben
5. Profil speichern klicken