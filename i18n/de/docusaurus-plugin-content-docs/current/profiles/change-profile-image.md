---
sidebar_position: 4
---

# Dein Profilbild ändern

:::caution Vorsicht

Diese Funktion erfordert, dass [Experimente aktiviert sind](https://docs.cwtch.im/docs/settings/introduction#experiments) und sowohl die  [Dateifreigabe](https://docs.cwtch.im/docs/settings/experiments/file-sharing) als auch [Bild-Vorschau und Profilbilder](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) aktiviert sind.

:::

1. Klicke auf den Stift neben dem Profil, das du bearbeiten möchtest
2. Drücke auf den rosa Bleistift über deinem Profilfoto
3. Wähle ein Bild von Deinem Gerät
4. Scrolle nach unten und klicke auf Profil speichern