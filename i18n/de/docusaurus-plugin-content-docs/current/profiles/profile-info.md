---
sidebar_position: 15
---

# Profilattribute einstellen

:::warning Warnung Neues Feature

Neu in [Cwtch 1.12](/blog/cwtch-nightly-1-12)

Diese Funktionalität kann unvollständig und/oder gefährlich sein, wenn sie falsch benutzt wird. Bitte hilf uns bei der Überprüfung und dem Test.
:::

Im [Profilverwaltungsfenster](/docs/profiles/introduction#manage-profiles) befinden sich drei freiformatige Textfelder unterhalb deines Profilbilds.

<figure>

[![](/img/profiles/attributes-empty.png)](/img/profiles/attributes-empty.png)

<figcaption></figcaption>
</figure>

Du kannst diese Felder mit allen Informationen ausfüllen, die du potenzielle Kontakte wissen möchtest. **Diese Informationen sind öffentlich** - lege hier keine Informationen an, die du nicht mit allen teilen möchtest.

<figure>

[![](/img/profiles/attributes-set.png)](/img/profiles/attributes-set.png)

<figcaption></figcaption>
</figure>

Kontakte können diese Informationen in den [Konversationseinstellungen](/docs/chat/conversation-settings) einsehen.

<figure>

[![](/img/profiles/attributes-contact.png)](/img/profiles/attributes-contact.png)

<figcaption></figcaption>
</figure>




