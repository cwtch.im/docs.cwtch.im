---
sidebar_position: 3
---

# Dateifreigabe

Diese Einstellung aktiviert Cwtch [Dateisharing-Funktionalität](/docs/chat/share-file). Dies zeigt die Option "Datei teilen" im Konversationsbereich an und erlaubt dir Dateien von Konversationen herunterzuladen.

Optional kannst du [Bildvorschau und Profilbilder](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) aktivieren, um Bilddateien automatisch herunterzuladen, Bilder im Konversationsfenster anzuzeigen und die Funktion [Profilbilder](/docs/profiles/change-profile-image) aktivieren;

:::info Info

Diese Dokumentationsseite ist ein Muster. Du kannst helfen, indem [du es mit vergrößerst](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::