---
sidebar_position: 1
---

# experimentelle Gruppen Funktion

Ermöglicht Cwtch [eine Verbindung zu nicht vertrauenswürdigen Server](/docs/servers/introduction) und verwendet diese für [private, asynchrone Gruppen](/docs/groups/introduction).

## Zum Einschalten

1. Zu den Einstellungen
2. Experimente aktivieren
3. Gruppen-Experiment aktivieren


