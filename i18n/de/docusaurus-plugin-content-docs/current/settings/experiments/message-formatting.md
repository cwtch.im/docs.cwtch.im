---
sidebar_position: 6
---

# Nachrichtenformatierung

Wenn aktiviert, ändert diese experimentelle Funktion die Konversationsbox um [Nachrichtenformatierung](/docs/chat/message-formatting) UX hinzuzufügen.

Diese experimentelle Funktion ist nun standardmäßig aktiviert.