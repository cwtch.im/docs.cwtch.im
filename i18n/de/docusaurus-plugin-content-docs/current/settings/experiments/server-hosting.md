---
sidebar_position: 2
---

# Server Hosting

**Server-Hosting ist derzeit eine experimentelle Funktion in Cwtch, es ist standardmäßig nicht aktiviert.**

1. Zu den Einstellungen
2. Experimente aktivieren
3. Server-Hosting-Experiment aktivieren
4. Du wirst wahrscheinlich auch das Gruppenexperiment aktivieren wollen, wenn du an einer Gruppe teilnehmen möchtest, die auf deinem Server gehostet wird