---
sidebar_position: 4
---

# Bildvorschau und Profilbilder

:::caution Vorsicht

Dieses experimentelle Funktion erfordert das  [File Sharing](https://docs.cwtch.im/docs/settings/experiments/file-sharing) aktiviert ist.

:::

Wenn aktiviert, wird Cwtch die Bilddateien automatisch herunterladen, Bildvorschau im Konversationsfenster anzeigen und [Profilbilder](/docs/profiles/change-profile-image) aktivieren;

Auf dem Desktop, durch das Aktivieren dieser experimentellen Funktion wird der Zugriff auf eine zusätzliche Einstellung `"`Downloadordner` erlaubt, die geändert werden kann, um Cwtch zu sagen, wo (automatisch) Bilder heruntergeladen werden sollen.