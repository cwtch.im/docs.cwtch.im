---
sidebar_position: 4
---

# Streamer/Präsentationsmodus

Streamer/Präsentationsmodus macht die App optisch privater. In diesem Modus wird Cwtch keine Hilfsinformationen wie Cwtch-Adressen und andere sensible Informationen auf den Hauptbildschirmen anzeigen.

Dies ist nützlich, wenn Du Screenshots machst oder Cwtch auf andere Art und Weise öffentlich anzeigst.

1. Drücke das Einstellungssymbol
2. "Streamer-Modus" auf "An"
3. Überprüfe, ob es funktioniert, indem Du Dein Profil oder Deine Kontaktliste ansiehst