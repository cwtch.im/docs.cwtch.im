---
sidebar_position: 2
---

# Helles/Dunkles Design und Theme-Aufteilung

1. Drücke das Einstellungssymbol
2. Du kannst ein helles oder dunkles Thema wählen, indem Du den Schalter „Verwende helles Theme“ einschaltest
3. Wähle mit dem Dropdown-Menü „Farb-Theme“ ein Theme aus, das Dir gefällt
4.
   1. Cwtch: lila Tönung
   2. Geist: Graue Tönung
   3. Meerjungfrau: Türkise und violette Tönung
   4. Mitternacht: Schwarze und graue Tönung
   5. Neon 1: lila und rosa Tönung
   6. Neon 2: lila und türkise Tönung
   7. Kürbis: lila und orange Tönung
   8. Hexe: Grüne und rosa Tönung
   9. Vampir: Violette und rote Tönung