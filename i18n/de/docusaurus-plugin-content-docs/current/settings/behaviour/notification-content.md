---
sidebar_position: 3
---

# Benachrichtigungsinhalt

1. Zu den Einstellungen
2. Zum Verhalten scrollen
3. Der Benachrichtigungsinhalt steuert den Inhalt von Benachrichtigungen
   1. Einfaches Event: nur "Neue Nachricht"
   2. Konversationsinformation: "Neue Nachricht von XXXXX"
