---
sidebar_position: 2
---

# Benachrichtigungsrichtlinie

1. Zu den Einstellungen
2. Zum Verhalten scrollen
3. Die Benachrichtigungsrichtlinie steuert das Benachrichtigungsverhalten
4. Auf Voreinstellung klicken, um das Verhalten zum Einschalten oder Stummschalten aller Benachrichtigungen zu ändern
5. Wähle deine bevorzugte Benachrichtigungsrichtlinie