---
sidebar_position: 1
---

# Unbekannte Verbindungen blockieren

Standardmäßig interpretiert Cwtch Verbindungen von unbekannten Cwtch-Adressen als [Kontaktanfragen](https://docs.cwtch.im/docs/chat/accept-deny-new-conversation). Du kannst dieses Verhalten durch die Blockierung unbekannter Verbindungen ändern.

Wenn aktiviert, schließt Cwtch automatisch alle Verbindungen von Cwtch-Adressen, die Du nicht zur Konversationsliste hinzugefügt hast. Dadurch wird verhindert, dass Personen, die Deine Cwtch-Adresse haben, sich mit Dir in Verbindung setzen, es sei denn, Du fügst diese hinzu.

Zum Aktivieren:

1. Zu den Einstellungen
2. Blockieren unbekannter Kontakte aktivieren