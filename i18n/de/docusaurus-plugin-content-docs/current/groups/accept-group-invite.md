---
sidebar_position: 5
---

# Eine Gruppeneinladung annehmen

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Gruppen Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) eingeschaltet ist.

:::

1. Wenn ein Freund dir eine Gruppenanfrage sendet
2. Wähle ob du dieser Gruppe beitreten möchtest oder nicht
3. Die Gruppe ist jetzt in deiner Kontaktliste

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_acceptinvite.mp4" />
    </video>
</div>