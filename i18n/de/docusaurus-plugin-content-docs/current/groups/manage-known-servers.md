---
sidebar_position: 7
---

# Server verwalten

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Gruppen Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) eingeschaltet ist.

:::

Cwtch Gruppen werden von nicht vertrauenswürdigen Servern gehostet. Wenn du die Server sehen möchtest, über die du kennst, ihren Status und die auf ihnen gehosteten Gruppen:

1. In deinem Kontakt-Bereich
2. Zum Server verwalten Symbol gehen

## Import lokal gehosteter Server

1. Um einen lokal gehosteten Server zu importieren klicke auf lokalen Server auswählen
2. Wähle den gewünschten Server aus

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Manage.mp4" />
    </video>
</div>