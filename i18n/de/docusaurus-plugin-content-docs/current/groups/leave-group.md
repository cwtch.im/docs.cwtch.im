---
sidebar_position: 6
---

# Wie man eine Gruppe verlässt?

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Gruppen Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) eingeschaltet ist.

:::

:::warning Warnung

Diese Funktion wird das Schlüsselmaterial **unwiderruflich** löschen. Dieses **kann nicht rückgängig gemacht werden**.

:::

1. Gehen Sie im Chat-Fenster zu den Einstellungen
2. Im Einstellungsfenster nach unten scrollen
3. Drücke auf Konversation verlassen
4. Bestätige, dass du verlassen möchtest

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Leave.mp4" />
    </video>
</div>