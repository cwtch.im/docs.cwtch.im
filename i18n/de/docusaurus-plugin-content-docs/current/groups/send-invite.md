---
sidebar_position: 4
---

# Einladungen an eine Gruppe senden

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Gruppen Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) eingeschaltet ist.

:::

1. Gehe zu einem Chat mit einem Kontakt
2. Klicke auf das Einladungs-Symbol
3. Wähle die Gruppe aus, in die du einladen möchtest
4. Klicke Einladen
5. Du hast eine Einladung gesendet

<div>
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Invite.mp4" />
    </video>
</div>