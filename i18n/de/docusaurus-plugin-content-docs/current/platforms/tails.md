---
sidebar_position: 1
---

# Cwtch in Tails laufen lassen

:::warning Warnung Neues Feature

Neu in [Cwtch 1.12](/blog/cwtch-nightly-1-12)

Diese Funktionalität kann unvollständig und/oder gefährlich sein, wenn sie falsch benutzt wird. Bitte hilf uns bei der Überprüfung und dem Test.
:::

Die folgenden Schritte erfordern, dass Tails mit einem [Administrationspasswort gestartet wurde](https://tails.boum.org/doc/first_steps/welcome_screen/administration_password/).

Tails verwendet [Onion Grater](https://gitlab.tails.boum.org/tails/tails/-/blob/master/config/chroot_local-includes/usr/local/lib/onion-grater#L3) um den Zugriff auf den Kontrollport zu schützen. Wir haben eine Onion Gratar Konfiguration [`cwtch-tails.yml` ](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/linux/cwtch-tails.yml) paketiert und ein Skript (`install-tails.sh`) mit dem Cwtch unter Linux eingerichtet.

Der Tails-spezifische Teil des Skripts wird unten reproduziert:

        # Tails needs to be have been setup up with an Administration account
        # 
        # Make Auth Cookie Readable
        sudo chmod o+r /var/run/tor/control.authcookie
        # Copy Onion Grater Config
        sudo cp cwtch.yml /etc/onion-grater.d/cwtch.yml
        # Restart Onion Grater so the Config Takes effect
        sudo systemctl restart onion-grater.service

Beim Start sollte Cwtch auf Tails die `CWTCH_TAILS=true` Umgebungsvariable übergeben werden, um Cwtch automatisch für die Ausführung in einer Tails-ähnlichen Umgebung zu konfigurieren:

`exec env CWTCH_TAILS=true LD_LIBRARY_PATH=~/.local/lib/cwtch/:~/.local/lib/cwtch/Tor ~/.local/lib/cwtch/cwtch`

:::info Installationsort

Der obige Befehl und die unten stehende Onion Gratar Konfiguration gehen davon aus, dass Cwtch in `~/.local/lib/cwtch/cwtch` installiert wurde - wenn Cwtch irgendwo anders installiert wurde (oder wenn Du es direkt aus dem Download-Ordner laufen lässt) musst du die Befehle anpassen.

:::

## Onion Grater Konfiguration

Die Onion Gratar Konfiguration [`cwtch-tails.yml` ](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/linux/cwtch-tails.yml) wird unten reproduziert. Wie erwähnt, kann diese Konfiguration wahrscheinlich noch viel weiter eingeschränkt werden. 

        ---
        # TODO: This can likely be restricted even further, especially in regards to the ADD_ONION pattern
    
        - apparmor-profiles:
            - '/home/amnesia/.local/lib/cwtch/cwtch'
          users:
    
            - 'amnesia'
          commands:
            AUTHCHALLENGE:
    
              - 'SAFECOOKIE .*'
            SETEVENTS:
    
              - 'CIRC WARN ERR'
              - 'CIRC ORCONN INFO NOTICE WARN ERR HS_DESC HS_DESC_CONTENT'
            GETINFO:
    
              - '.*'
            GETCONF:
    
              - 'DisableNetwork'
            SETCONF:
    
              - 'DisableNetwork.*'
            ADD_ONION:
    
              - '.*'
            DEL_ONION:
    
              - '.+'
            HSFETCH:
    
              - '.+'
          events:
            CIRC:
              suppress: true
            ORCONN:
              suppress: true
            INFO:
              suppress: true
            NOTICE:
              suppress: true
            WARN:
              suppress: true
            ERR:
              suppress: true
            HS_DESC:
              response:
    
            - pattern:     '650 HS_DESC CREATED (\S+) (\S+) (\S+) \S+ (.+)'
              replacement: '650 HS_DESC CREATED {} {} {} redacted {}'
    
            - pattern:     '650 HS_DESC UPLOAD (\S+) (\S+) .*'
              replacement: '650 HS_DESC UPLOAD {} {} redacted redacted'
    
            - pattern:     '650 HS_DESC UPLOADED (\S+) (\S+) .+'
              replacement: '650 HS_DESC UPLOADED {} {} redacted'
    
            - pattern:     '650 HS_DESC REQUESTED (\S+) NO_AUTH'
              replacement: '650 HS_DESC REQUESTED {} NO_AUTH'
    
            - pattern:     '650 HS_DESC REQUESTED (\S+) NO_AUTH \S+ \S+'
              replacement: '650 HS_DESC REQUESTED {} NO_AUTH redacted redacted'
    
            - pattern:     '650 HS_DESC RECEIVED (\S+) NO_AUTH \S+ \S+'
              replacement: '650 HS_DESC RECEIVED {} NO_AUTH redacted redacted'
    
            - pattern:     '.*'
              replacement: ''
            HS_DESC_CONTENT:
              suppress: true

## Dauerhaft

Standardmäßig erstellt Cwtch `$HOME/.cwtch` und speichert dort alle verschlüsselten Profile und Einstellungsdateien. Um Profil/Konversationen in Cwtch auf Tails zu speichern, musst du diesen Ordner in einem nicht flüchtigen Zuhause sichern.

Siehe Tails Dokumentation zum Einrichten von [Dauerhaftem Speicher](https://tails.boum.org/doc/persistent_storage/)
