---
sidebar_position: 5
---

# Tor

Cwtch verwendet [Tor](https://www.torproject.org/) um Routing und Verbindungen bereitzustellen. Die Verwendung von Tor-versteckten Diensten für das Hosten von Profilen und nebenbei generierte "ephemerale" Verbindungen, wenn eine Verbindung aufgebaut wird, bietet eine starke Anonymität für Benutzer von Cwtch.

## Tor-Übersicht

Da wir Cwtch eine zusätzliche Netzwerk-Ebene hinzufügen, bieten wir eine Übersicht, um den Tor-Netzwerk-Status zu sehen und Änderungen vorzunehmen. Um darauf zuzugreifen

1. Klicke auf das Tor-Symbol in der Profilliste ![tor icon](/img/Tor_icon.png)
2. Den Tor Netzwerkstatus anzeigen

```
Tor-Status: Online
Torversion: 0.4.6.9
```

### Reset Tor

Das Tor-Netzwerk selbst kann gelegentlich veraltete Verbindungen haben, die nicht sofort von ihm oder Cwtch erkannt werden (wir versuchen dies immer weiter zu verbessern). Manchmal kann ein Benutzer Kontakte oder Gruppen finden, die offline erscheinen, auch wenn diese meinen, dass sie online sein sollten. Wenn du alle Netzwerkverbindungen in Cwtch neu starten möchtest, bieten wir einen Mechanismus zum Tor Neustart von innerhalb der App an. Der **Reset** Button startet Tor aus der Cwtch App heraus neu.


### Cache-Tor-Konsens

Standardmäßig starten wir jedesmal einen neuen Tor-Prozess, wenn die App bootet, und es erfordert das Herunterladen eines Tor-Netzwerk-Zustandes, bevor dieser gestartet werden kann. Dieser Prozess ist nicht so schnell. Wenn du den Cwtch-Start beschleunigen möchtest, kannst du den Tor-Cache-Konsens aktivieren, um zukünftige Starts zu beschleunigen. Wenn du in ein Start Problem kommst, wo die Daten veraltet oder korrupt sind und Cwtch meldet, dass es nicht starten kann, dann deaktiviere diese Einstellung und **reset** Tor erneut, dann sollte es funktionieren.

### Erweiterte Tor Einstellungen

Wir bieten dir auch die Möglichkeit an, in diesem Abschnitt eine erweiterte Tor-Konfiguration bereitzustellen, indem wir dir erlauben:

- Gib einen benutzerdefinierten SOCKS-Port an, um eine Verbindung zu einem bestehenden Tor herzustellen
- Gib einen benutzerdefinierten Kontroll-Port an, um eine Verbindung zu einem bestehenden Tor herzustellen
- und gib weitere Optionen an, indem du benutzerdefinierte `torrc` Optionen eingibst

