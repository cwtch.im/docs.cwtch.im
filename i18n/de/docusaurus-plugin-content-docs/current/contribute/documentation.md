---
sidebar_position: 6
---

# Stilregeln der Dokumentation

Dieser Abschnitt dokumentiert die erwartete Struktur und Qualität der Cwtch-Dokumentation.

## Screenshots und Übertragung von Zeichen

Die meisten Cwtch-Dokumentationen sollte mindestens einen Screenshot oder ein animiertes Bild enthalten. Screenshots der Cwtch-Anwendung sollten sich auf die in der Dokumentation beschriebene Funktion konzentrieren.

Um die Konsistenz zwischen Screenshots zu gewährleisten, schlagen wir vor, dass das betreffende Profil besonderen, konstanten und Rollen dienen sollte.

- **Alice** - wird verwendet, um das primäre Profil zu repräsentieren.
- **Bob** - der primäre Kontakt, nützlich bei der Darstellung von Peer-to-Peer-Funktionen
- **Carol** - ein sekundärer Kontakt, nützlich bei der Darstellung von Gruppenfunktionen
- **Mallory** - stellt einen böswilligen Partner dar (welcher verwendet wird, um die Blockierfunktionalität zu demonstrieren)

## Dialog und Inhalt

Wo Screenshots und Demonstrationen Dialog, Gespräche und/oder Bilder zeigen, halte bitte die Gespräche kurz zu einem lässigen Thema. Beispiele dafür sind:

- Organisieren eines Picknicks
- Teilen von Fotos aus einem Urlaub
- Senden des Dokuments zur Prüfung

## Experimente

Alle Funktionen, die auf die Aktivierung eines Experiments angewiesen sind, sollten dies alles oben auf der Seite hervorheben z. B.:

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Beispiel Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) eingeschaltet ist.
:::

## Risiko

Wenn eine Funktion zur Zerstörung des Schlüsselmaterials oder zur dauerhaften Löschung des Status führen könnte dann sollten diese auch oben in der Dokumentation aufgerufen werden, z. B.:


:::warning Warnung

Diese Funktion wird das Schlüsselmaterial **unwiderruflich** löschen. Dieses **kann nicht rückgängig gemacht werden**.

:::