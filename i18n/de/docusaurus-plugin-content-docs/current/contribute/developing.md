---
sidebar_position: 1
---

# Entwicklung von Cwtch

Dieser Abschnitt dokumentiert einige Möglichkeiten, um mit der Cwtch-Entwicklung zu beginnen.

## Cwtch-Fehlerverfolgungsprozess

Alle Cwtch-Probleme werden aus dem [cwtch-ui git Repository](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues)verfolgt, auch wenn der Fehler / das Feature in einer Upstream-Bibliothek entsteht. Dies erlaubt uns, alles an einem Ort zu halten.

Probleme sind in 4 verschiedene Kategorien unterteilt:

- **Unbearbeitete** - Dies sind neue Probleme, die vom Cwtch Team nicht diskutiert wurden.
- [**Geplant**](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=open&labels=195&milestone=0&assignee=0&poster=0) - Diese Probleme wurden für eine kommende Veröffentlichung eingeplant. Normalerweise werden sie mit der Release Version getaggt, in der sie voraussichtlich in `cwtch-1.11` behoben werden. Ein Kern-Cwtch-Team-Mitglied arbeitet wahrscheinlich an diesem Problem oder wird in den nächsten Wochen an diesem Thema arbeiten.
- [**Gewünscht**](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=open&labels=153&milestone=0&assignee=0&poster=0) - Dies sind Probleme, die wir gerne beheben würden, aber aus irgendeinem Grund können wir diese nicht planen. Dies könnte daran liegen, dass die Funktion groß ist und viel Aufwand erfordert, oder weil es einen Blocker gibt (z.B. Eine fehlende Funktion in Flutter oder einer anderen Bibliothek), die die Arbeit an der Funktion verhindert.
- [**Hilfe gesucht**](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=open&labels=136&milestone=0&assignee=0&poster=0) - Dies sind in der Regel kleine Probleme, die wir gerne beheben würden, die aber als geringe Priorität eingestuft wurden. Dies sind ideale erste Probleme für Freiwillige.

Wenn du an einem offenen Fehler/Feature arbeiten möchtest, bitte kommentiere das Problem und ein Mitglied des Cwtch Teams wird dir Ratschläge geben, wohin du von dort gehen solltest. Dies hilft uns, den Überblick zu behalten, wer an welchen Problemen arbeitet, und reduziert den Umfang der doppelten Arbeit. Wir sind bestrebt, die meisten Anfragen innerhalb von 24 Stunden zu beantworten, fühle dich frei, an ein Problem zu "erinnern", wenn es länger als diese Zeit dauert.

:::note Hinweis

Aufgrund eines Problems mit unserem E-Mail-Provider sind wir derzeit nicht in der Lage, E-Mails von unserer gitea-Instanz zu versenden. Bitte überprüfe regelmäßig offene Probleme / Pull-Requests auf Updates (oder abonniere die RSS-Feeds des Projektarchivs)

:::

## Cwtch Pull-Request Prozess

Alle Pull-Requests müssen von einem Kernmitglied des Cwtch Teams überprüft und genehmigt werden, bevor das Zusammenführen erfolgt. Sarah prüft alle neuen und aktiven Pull-Anfragen mehrmals in der Woche.

### Bot bauen

Alle Cwtch Projekte werden mit automatisierten Builds und Tests eingerichtet. Es wird erwartet, dass jeder Pull-Request diese Pipelines durchlaufen kann, bevor er zusammengeführt wird. Wenn buildbot einen Fehler meldet, dann wird Sarah mit dir zusammenarbeiten, um das Problem und alle notwendigen Korrekturen zu ermitteln.

Der Buildbot kann aus Gründen, die außerhalb deiner Kontrolle liegen, fehlschlagen, z.B. viele unserer Integrationstests sind auf das Einrichten von Tor-Verbindungen angewiesen, diese können gelegentlich spröde sein und zu Timeouts und Fehlern führen. Bestätige immer die Hauptursache für einen Testfehler, bevor du entscheidest, was als nächstes zu tun ist.


## Nützliche Ressourcen

- [Cwtch Ecosystem Übersicht](/security/components/ecosystem-overview) - eine Zusammenfassung der aktiven Cwtch Repositories aus dem Cwtch Secure Development Handbuch.
- [Beitragen zur Dokumentation](/docs/contribute/documentation) - Ratschläge zum Beitragen zur Cwtch-Dokumentation.
- [Beitragen zum Testen](/docs/contribute/testing) - Ratschläge zum Beitragen durch das Testen von Cwtch.
- [Beitragen zu Übersetzungen](/docs/contribute/translate) - Ratschläge zum Beitragen von Übersetzungen in Cwtch.


:::note Hinweis

Alle Beiträge sind [berechtigt für Aufkleber](/docs/contribute/stickers)

:::