---
sidebar_position: 10
---

# Aufkleber

Alle Beiträge sind für Aufkleber berechtigt. Wenn du zu Bugs, Features, Testen oder Sprache beiträgst oder in der Vergangenheit einen wesentlichen Beitrag geleistet hast, schreibe bitte eine E-Mail an erinn@openprivacy. mit Details und einer Adresse für uns, damit wir die Aufkleber dir schicken können.

![Ein Foto von Cwtch-Aufklebern](/img/stickers-new.jpg)

