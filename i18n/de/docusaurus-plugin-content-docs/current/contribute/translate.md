---
sidebar_position: 2
---

# Cwtch übersetzen

Wenn du Übersetzungen zu Cwtch beitragen möchtest, entweder zur App oder zu diesem Handbuch, kommt hier wie dies möglich ist.

## Übersetzungen zur Cwtch-Anwendung beitragen

Es gibt zwei Möglichkeiten, zu Cwtch Anwendungen beizutragen.

### Trete unserem Lokalise-Team bei

Wir verwenden [Lokalise](https://lokalise.com) für die Verwaltung von Übersetzungen für die Cwtch-Anwendung.

1. Registriere dich für ein Lokalise-Konto
2. Sende eine E-Mail an [team@cwtch.im](mailto:team@cwtch.im) mit der Sprache, die du übersetzen möchtest und einer E-Mail-Adresse, mit der wir dich in unser Lokalise-Team einladen können.

### Direkt über Git

Für neue Übersetzungen, kannst du eine Kopie von [https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/intl_en.arb](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/intl_en.arb) erstellen und übersetzen - Du kannst dann entweder [Pull-Requests einreichen oder direkt](/docs/contribute/developing#cwtch-pull-request-process) Aktualisierungen an uns senden (team@cwtch.im) und wir werden sie zusammenführen.

Um zu existierenden Übersetzungen hinzuzufügen, kannst du Pull-Requests direkt auf jede Datei in [https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/) einstellen und wir werden sie überprüfen und zusammenführen.

## Cwtch-Benutzerhandbuch

Dieses Handbuch wird über [Crowdin](https://crowdin.com) übersetzt.

Um unserem Crowdin-Projekt beizutreten:

1. Registrieren dich für ein Konto bei [Crowdin](https://crowdin.com).
2. Treten dem [cwtch-users-handbook Projekt](https://crowdin.com/project/cwtch-users-handbook) bei.

Wir bündeln Änderungen an der Dokumentation in Batches und synchronisieren sie regelmäßig mit dem Crowdin-Projekt.

:::note Hinweis

Alle Beiträge sind [berechtigt für Aufkleber](/docs/contribute/stickers)

:::