---
sidebar_position: 3
---

# Wie man einen Server bearbeitet

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) eingeschaltet ist.

:::

1. Zum Server-Symbol gehen
2. Wähle den Server aus, den du bearbeiten möchtest
3. Drücke das Stift-Symbol
4. Ändere die Beschreibung oder den Server aktivieren oder deaktivieren
5. Klicke auf "Server speichern"

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/server_edit.mp4" />
    </video>
</div>