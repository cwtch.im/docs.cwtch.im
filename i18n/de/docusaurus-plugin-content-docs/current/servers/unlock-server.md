---
sidebar_position: 6
---

# Wie man einen Server entsperrt

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) eingeschaltet ist.

:::

Wenn du deinen Server mit einem Passwort geschützt hast, muss er bei jedem Neustart der Anwendung entsperrt werden.

1. Zum Server-Symbol gehen
2. Klicken Sie auf das rosa Entsperr-Symbol
3. Gib das Kennwort deines Servers ein
4. Drücke Entsperren