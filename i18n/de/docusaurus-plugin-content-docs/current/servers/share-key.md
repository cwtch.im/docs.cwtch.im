---
sidebar_position: 5
---

# Wie du dein Server-Schlüsselpaket teilst

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) eingeschaltet ist.

:::

Dein Server-Schlüsselbündel ist das Datenpaket, das eine Cwtch-App benötigt, um einen Server nutzen zu können. Wenn du nur andere Cwtch-Benutzer auf deinen Server aufmerksam machen möchtest, kannst du dies mit ihnen teilen. Dann haben sie die Möglichkeit, eigene Gruppen auf dem Server zu erstellen.

1. Zum Server-Symbol gehen
2. Wähle den gewünschten Server aus
3. Benutze das Kopier-Adressen-Symbol um die Server-Schlüssel zu kopieren
4. Teile die Schlüssel nicht mit Leuten, denen du nicht vertraust

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Keys.mp4" />
    </video>
</div>