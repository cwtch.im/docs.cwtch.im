---
sidebar_position: 4
---

# Wie man einen Server löscht

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) eingeschaltet ist.

:::


1. Zum Server-Symbol gehen
2. Wähle den Server aus, den du löschen möchtest
3. Drücke das Stift-Symbol
4. Scrolle nach unten und gib dein Passwort ein
5. Drücke Löschen
6. Drücke wirklich Löschen
7. Dein Server ist gelöscht

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Delete.mp4" />
    </video>
</div>