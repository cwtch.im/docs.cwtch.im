---
sidebar_position: 8
---

# Einen Kontakt entsperren

1. Wähle den Kontakt in Ihrer Konversationsliste aus. Gesperrte Kontakte werden an das Ende der Liste verschoben.
2. Gehe zu den Konversationseinstellungen
3. Scrolle nach unten zum Kontakt sperren
4. Verschiebe den Schalter auf Kontakt entsperren


:::info

Diese Dokumentationsseite ist ein Muster. Du kannst helfen, indem [du es mit vergrößerst](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::