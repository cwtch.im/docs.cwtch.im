---
sidebar_position: 1.5
---

# Neues Gespräch beginnen

1. Ein Profil auswählen
2. Klicke auf den Hinzufügen Button
3. Wähle 'Kontakt hinzufügen'
4. Eine Cwtch-Adresse einfügen
5. Der Kontakt wird zu deiner Kontaktliste hinzugefügt

:::info

Diese Dokumentationsseite ist ein Muster. Du kannst helfen, indem [du es mit vergrößerst](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::
