---
sidebar_position: 6
---

# Eine Datei teilen

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Datei Teilen Experiment](https://docs.cwtch.im/docs/settings/experiments/file-sharing) eingeschaltet ist.

Optional kannst du [Bilder und Profilbilder Vorschau](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) aktivieren, um geteilte Bilder als Vorschau im Konversationsfenster zu sehen.

:::

In einer Konversation

1. Klicke auf das Anhang-Symbol
2. Finde die Datei, die du senden möchtest
3. Bestätige, dass du es senden möchtest

## Wie funktioniert das Teilen von Dateien mit Gruppen? Sind meine Dateien irgendwo auf einem Server gespeichert?

Dateien werden über onion-to-onion Cwtch Verbindungen direkt von der Person verschickt, die die Datei dem Empfänger anbietet. Das ursprüngliche Angebot zum Senden einer Datei wird als Standard Cwtch Konversation/Overlay-Nachricht veröffentlicht. Für Gruppen bedeutet dies, dass das ursprüngliche Angebot (welches den Dateinamen, die Größe, Hash und eine Nonce beinhaltet) auf den Gruppenserver gestellt wird aber dann verbindet sich jeder Empfänger zu dir, um den eigentlichen Dateiinhalt zu erhalten.

## Muss ich somit online sein, um eine Datei zu senden?

Ja. Wenn die Person, die die Datei anbietet, offline geht, musst du warten, bis sie online kommt, um die Übertragung fortzusetzen. Das zugrunde liegende Protokoll teilt die Dateien in einzelne, überprüfbare Chunks, so dass Sie in einer zukünftigen Version eine Datei "rehosten" können, die in einer Gruppe veröffentlicht wird und sogar Download von mehreren Hosts auf einmal (eine Art von Bittorrent) möglich sein wird.

## Warum tauchen neue Kontakte in meiner Liste auf?

Dies ist darauf zurückzuführen, wie Cwtch derzeit Verbindungen von unbekannten Adressen behandelt. Da eine Datei in eine Gruppe geschrieben wird, führt dies dazu, dass Gruppenmitglieder sich direkt mit dir verbinden, einige dieser Mitglieder befinden sich möglicherweise noch nicht in deiner Kontaktliste, so dass deren Downloadverbindung zu dir als Kontaktanfrage in deiner Liste erscheint.

## Was heißt "SHA512"?

SHA512 ist ein [kryptographischer Hash](https://en.wikipedia.org/wiki/Cryptographic_hash_function) der verwendet werden kann, um zu überprüfen, dass die heruntergeladene Datei eine korrekte Kopie der angebotenen Datei ist. Cwtch macht diese Verifizierung automatisch, aber du bist herzlich eingeladen, es selbst auszuprobieren! Beachte, dass wir auch ein zufälliges nonce in Datei-Angeboten einschließen, so dass die Leute dich nicht einfach nach einem zufälligen Hash fragen können, den du haben kannst, oder Dateien aus Unterhaltungen, von denen sie nicht selber dabei sind.

## Gibt es eine Begrenzung der Dateigröße?

Das aktuelle Limit beträgt 10 Gigabytes pro Datei.

## Was sind diese .manifest Dateien?

Die .manifest-Dateien werden beim Herunterladen der Datei verwendet, um zu überprüfen, ob einzelne Chunks korrekt empfangen werden, und um die Fortsetzung der unterbrochenen Übertragung zu unterstützen. Sie enthalten auch die Informationen aus dem ursprünglichen Datei-Angebot. Du kannst diese sicher löschen, sobald der Download abgeschlossen ist. Auf Android werden die Manifeste im Cache der App gespeichert und können über die Systemeinstellungen gelöscht werden.

## Was ist mit den Datei-Metadaten?

Wir schicken den Namen der Datei als Vorschlag und um zu helfen, die Datei von anderen Dateiangeboten zu unterscheiden. Vor dem Absenden des Angebots wird der gesamte Pfad abgeschnitten. Du solltest dich vor versteckten Metadaten hüten, die in der Datei selbst gespeichert sein könnten, was je nach Dateiformat variiert. Zum Beispiel können Bilder Geo-Lokationen und Informationen über die Kamera enthalten, die sie aufgenommen haben und PDF-Dateien sind dafür berüchtigt, versteckte Informationen wie den Namen des Autors oder die Maschine, auf der sie erstellt wurden, zu enthalten. Im Allgemeinen solltest du nur Dateien mit Leuten senden und empfangen, denen du vertraust.

## Kann ich Dateien automatisch herunterladen?

Wenn das [Bild-Vorschau und Profilbilder Experiment](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) aktiviert ist, wird Cwtch automatisch Bilder von akzeptierten Unterhaltungen herunterladen.
