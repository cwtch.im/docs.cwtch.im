---
sidebar_position: 3
---

# Teilen von Cwtch Adressen

Es gibt viele Möglichkeiten, eine Cwtch Adresse zu teilen.

## Teilen deiner Cwtch Adresse

1. Zu deinem Profil gehen
2. Klicke auf das Kopiere-Adresse-Symbol

Du kannst diese Adresse nun teilen. Personen mit dieser Adresse können dich als Cwtch Kontakt hinzufügen.

Für Informationen zum Blockieren von Verbindungen von Personen, die du nicht kennst, lies bitte [Einstellungen: Unbekannte Verbindungen blockieren](/docs/settings/behaviour/block-unknown-connections)


# Teilen der Cwtch Adresse von Freunden

Innerhalb von Cwtch gibt es einen weiteren Mechanismus zum Austausch von Cwtch-Adressen.

:::info

Diese Dokumentationsseite ist ein Muster. Du kannst helfen, indem [du es mit vergrößerst](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::