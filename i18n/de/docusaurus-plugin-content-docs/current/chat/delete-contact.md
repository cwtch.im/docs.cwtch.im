---
sidebar_position: 10
---

# Entfernen einer Konversation

:::warning Warnung

Diese Funktion wird **unwiderruflich** löschen. Dieses **kann nicht rückgängig gemacht werden**.

:::

- In einem Chat mit Kontakt, gehe in die Konversationseinstellungen oben rechts <span class="icon">![](/img/peer_settings-24px.svg)</span>
- Scrolle zum **Verlasse diese Konversation** und drücken darauf.
- Sie wirst aufgefordert zu bestätigen, wenn du die Unterhaltung verlassen möchtest. Diese Aktion kann nicht rückgängig gemacht werden.


:::info

Diese Dokumentationsseite ist ein Muster. Du kannst helfen, indem [du es mit vergrößerst](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::
