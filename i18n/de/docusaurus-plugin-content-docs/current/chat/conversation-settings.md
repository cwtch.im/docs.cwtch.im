---
sidebar_position: 5
---

# Konversationseinstellungen aufrufen

Klicke in einem Konversationsfenster auf das Einstellungssymbol in der oberen Leiste.

<figure>

[![](/img/conversations/settings.png)](/img/conversations/settings.png)

<figcaption></figcaption>
</figure>

Diese Aktion öffnet einen neuen Bildschirm, auf dem du den Kontakt ansehen und verwalten kannst.

<figure>

[![](/img/conversations/settings-full.png)](/img/conversations/settings-full.png)

<figcaption></figcaption>
</figure>