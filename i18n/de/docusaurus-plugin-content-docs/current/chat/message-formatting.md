---
sidebar_position: 4.5
---

# Nachrichtenformatierung

:::caution Experimentelle Funktionen erforderlich

Diese Funktion erfordert, dass [Experimente aktiviert](https://docs.cwtch.im/docs/settings/introduction#experiments) und das [Nachrichten Formatierung Experiment](https://docs.cwtch.im/docs/settings/experiments/message-formatting) eingeschaltet ist.

Optional kannst du [Klickbare Links](https://docs.cwtch.im/docs/settings/experiments/clickable-links) aktivieren, um URLs in Nachrichten in Cwtch anklickbar zu machen.

:::

Cwtch unterstützt zur Zeit folgende Formatierungsmarkierungen für Nachrichten:

* `**bold**` welches **fett** rendern wird
* `*italic*` welches *kursiv* rendern wird
* `code` welches `code` rendern wird
* `^superscript^` das <sup>superscript</sup> rendern wird
* `_subscript_` welches <sub>subscript</sub> rendern wird
* `~~strikthrough~~` welches <del>strikethrough</del> rendern wird
