---
sidebar_position: 5
---

# Auf eine Nachricht antworten

1. Wähle eine Nachricht aus, auf die du antworten möchtest
2. Nachricht nach rechts ziehen
3. Eine Antwort auf die zitierte Nachricht schreiben
4. Senden anklicken