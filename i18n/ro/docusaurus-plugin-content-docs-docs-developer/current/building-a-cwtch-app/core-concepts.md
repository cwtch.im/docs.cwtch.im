---
sidebar_position: 2
---

# Core Concepts

This page documents the core concepts that you, as a Cwtch App Developer, will encounter fairly frequently.

## Cwtch Home Directory

Often referred to as `$CWTCH_HOME`, the Cwtch application home directory is the location where Cwtch stores all information from a Cwtch application.

## Profiles

Cwtch profiles are saved as encrypted sqlite3 databases. You will rarely/never have to interact directly with the database. Instead each library provides a set of interfaces to interact with the Cwtch App, create profiles, manage profiles, and engage in conversations.

## The Event Bus

Regardless of which library you end up choosing, the one constant interface you will have to get used to is the EventBus. Cwtch handles all asynchronous tasks (e.g. receiving a message from a peer) automatically, eventually placing a message on the EventBus. Application can subscribe to certain kinds of messages e.g. `NewMessageFromPeer` and setup an event handler to run code in response to such a message.

For an example see the Echo Bot tutorial.

## Settings

Most Cwtch settings (with the exception of experiments) are designed for downstream graphical user interfaces e.g. themes / column layouts - in particular the Cwtch UI. As such these settings are not used at all by Cwtch libraries, and are only intended as a convenient storage place for UI configuration.

### Experiments

Certain Cwtch features are [gated behind experiments](/docs/category/experiments). These experiments need to be enabled before functionality related to them will activate. Different libraries may expose different experiments, and some libraries may not support certain experiments at all.
