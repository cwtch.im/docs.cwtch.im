---
sidebar_position: 10
---

# Backup or Exporting a Profile

On the Profile Management Screen:

1. Select the pencil next to the profile you want to edit
2. Scroll down to the bottom of the screen
3. Select "Export Profile"
4. Choose a location, and a file name
5. Confirm

Once confirmed, Cwtch will place a copy of the profile at the given location. This file is encrypted to the same level that the profile is. See [A note on Password Protected (Encrypted) Profiles](/docs/profiles/create-a-profile#a-note-on-password-protected-encrypted-profiles) for more information on encrypted profiles.

This file can be [imported](/docs/profiles/importing-a-profile) into another instance of Cwtch on any device.