---
sidebar_position: 1
---

# Running Cwtch on Whonix

:::warning New Feature

New in [Cwtch 1.13](/blog/cwtch-1-13)

This functionality may be incomplete and/or dangerous if misused. Please help us to review, and test.
:::

## Configure the Whonix-Gateway

The following steps should be done in the Whonix-Gateway.

### Onion Grater

Whonix uses [Onion Grater](https://www.whonix.org/wiki/Onion-grater) to guard access to the control port. We have packaged an onion-grater configuration [`cwtch-whonix.yml` ](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/linux/cwtch-whonix.yml) which is present in the root directory of the tarball.

This file needs to be placed in `/usr/share/doc/onion-grater-merger/examples/40_cwtch.yml`.

Whonix has also packaged the profile [`40_cwtch.yml`_](https://github.com/Whonix/onion-grater/blob/master/usr/share/doc/onion-grater-merger/examples/40_cwtch.yml), therefore you can use either one.

Enable the Cwtch onion-grater profile:
```shell
sudo onion-grater-add 40_cwtch
```

## Configure the Whonix-Workstation

The following steps should be done in the Whonix-Workstation.

### Linux Firewall

The Whonix-Workstation Firewall needs to have the possible Cwtch binding port open. Follow the [upstream firewall guide](https://www.whonix.org/wiki/Whonix-Workstation_Firewall#Open_an_Incoming_Port).

Use the following configuration in the file `/usr/local/etc/whonix_firewall.d/50_user.conf`:
```shell
EXTERNAL_OPEN_PORTS+=" $(seq 15000 15378) "
```

### Running Cwtch

When launching, Cwtch on Whonix, some environment variables must be set:
- `CWTCH_TAILS=true` - automatically configure Cwtch for running in a Whonix-like environment.
- `CWTCH_RESTRICT_PORTS=true` - forces connectivity to bind to a subset of ports `15000-15378`, easier to manage on the firewall configuration.
- `CWTCH_BIND_EXTERNAL_WHONIX=true` - forces connectivity to bind to external interfaces (only supported/recommended on for Whonix-based setups where the machine (Workstation) which is running Cwtch is firewalled, where the only source of network is through the Gateway.

```shell
cd ~/.local/lib/cwtch
env LD_LIBRARY_PATH=~/.local/lib/cwtch/:~/.local/lib/cwtch/Tor CWTCH_TAILS=true CWTCH_RESTRICT_PORTS=true CWTCH_BIND_EXTERNAL_WHONIX=true ~/.local/lib/cwtch/cwtch
```

:::info Install Location

The above command, and the below onion grater configuration assume that Cwtch was installed in `~/.local/lib/cwtch/cwtch` - if Cwtch was installed somewhere else (or if you are running directly from the download folder) then you will need to adjust the commands.

:::

# Removing Cwtch from Whonix

## Remove configuration from the Whonix-Gateway

The following steps should be done in the Whonix-Gateway.

Disable the Cwtch onion-grater profile:
```shell
sudo onion-grater-remove cwtch-whonix
```

## Remove configuration from the Whonix-Workstation

The following steps should be done in the Whonix-Workstation.

Delete the Cwtch application directory `$HOME/.local/lib/cwtch` and Cwtch data directory `$HOME/.cwtch`.

The firewall ports should also be closed manually by removing the configuration added above and reloading the firewall.
