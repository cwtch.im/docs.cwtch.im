---
sidebar_position: 2
---

# Translating Cwtch

If you would like to contribute translations to Cwtch the application or this handbook here is how

## Contributing Translations to the Cwtch Application

There are two ways to contribute to Cwtch applications.

### Join our Lokalise Team

We use [Lokalise](https://lokalise.com) for managing translations for the Cwtch application.

1. Sign up for a Lokalise account
2. Email [team@cwtch.im](mailto:team@cwtch.im) with the language you are interested in translating and an email we can use to invite you to our Lokalise team.

### Directly via Git

For new translations, you can make a copy of [https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/intl_en.arb](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/intl_en.arb) and begin translating - you can then either [submit pull requests or directly](/docs/contribute/developing#cwtch-pull-request-process) send updates to us (team@cwtch.im) and we will merge them in.

For adding to existing translations you can make pull requests directly on any file in [https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/) and we will review and merge them in.

## Cwtch User's Handbook

This handbook is translated through [Crowdin](https://crowdin.com).

To join our Crowdin project:

1. Sign up for an account on [Crowdin](https://crowdin.com).
2. Join the [cwtch-users-handbook project](https://crowdin.com/project/cwtch-users-handbook).

We bundle up changes to the documentation in batches and sync them with the Crowdin project on a regular basis.

:::note

All contributions are [eligible for stickers](/docs/contribute/stickers)

:::