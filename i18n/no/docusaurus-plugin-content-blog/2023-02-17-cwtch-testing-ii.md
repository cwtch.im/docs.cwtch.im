---
title: Notes on Cwtch UI Testing (II)
description: "In this development log we provide more updates on automated UI integration testing!"
slug: cwtch-testing-ii
tags:
  - cwtch
  - cwtch-stable
  - support
  - testing
image: /img/devlog7_small.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

In this development log, we investigate some text-based UI bugs encountered by [Fuzzbot](https://docs.cwtch.im/docs/contribute/testing#running-fuzzbot), add more [automated UI tests](/blog/cwtch-testing-i) to the pipeline, and announce a new release of the Cwtchbot library.

![](/img/devlog7.png)

<!--truncate-->


## Constraining Cwtch UI Fields

Fuzzbot identified a few bugs relating to UI layout and text clipping. Certain strings would violate the bounds of their containers and overlap with other UI elements. While this doesn't pose a safety issue, it is unsightly.

<figure>

[![](/img/dl7-before.png)](/img/dl7-before.png)

<figcaption>Screenshot demonstrating how certain strings would violate the bounds of their containers.</figcaption>
</figure>

These cases were fixed by parenting impacted elements in a `Container` with `clip: hardEdge` and `decoration:BoxDecoration()` (note that both of these are required as Container widgets in Flutter cannot set clipping logic without an associated decoration).

<figure>

[![](/img/dl7-after.png)](/img/dl7-after.png)

<figcaption>Now these clipped strings are tightly constrained to their container bounds.</figcaption>
</figure>

These fixes are available in the [latest Cwtch Nightly](/docs/contribute/testing#cwtch-nightlies), and will be officially released in Cwtch 1.11.

## More Automated UI Tests

We have added two new sets of automated UI tests to our pipeline:

- *02: Global Settings* - these tests check that certain global settings like languages, theme, unknown contacts blocking, and streamer mode work as expected. ([PR: 628](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/628))
- *04: Profile Management* - these tests check that creating, unlocking, and deleting a profile work as expected.   ([PR: 632](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/632))

## New Release of Cwtchbot

[Cwtchbot](https://git.openprivacy.ca/sarah/cwtchbot) has been updated to use the latest Cwtch 0.18.10 API.

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

