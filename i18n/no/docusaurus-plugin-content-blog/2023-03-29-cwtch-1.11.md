---
title: Cwtch Beta 1.11
description: "Cwtch Beta 1.11 is now available for download"
slug: cwtch-nightly-1-11
tags:
  - cwtch
  - cwtch-stable
  - release
image: /img/devlog12_small.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

[Cwtch 1.11 is now available for download](https://cwtch.im/download)!

Cwtch 1.11 is the culmination of the last few months of effort by the Cwtch team, and includes many foundational changes that pave the way for [Cwtch Stable](/blog/path-to-cwtch-stable) including new [reproducible](https://docs.cwtch.im/blog/cwtch-bindings-reproducible) and [automatically generated](https://docs.cwtch.im/blog/autobindings) bindings, as well as support for two new languages (Slovak and Korean), in addition to several performance improvements and bug fixes.

![](/img/devlog12.png)
 
<!--truncate-->

## In This Release

<figure>

[![](/img/picnic.png)](/img/picnic.png)

<figcaption>A screenshot of Cwtch 1.11</figcaption>
</figure>

A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Based on new Reproducible Cwtch Stable Autobuilds** - this is the first release of cwtch based on [reproducible Cwtch bindings](https://docs.cwtch.im/blog/cwtch-bindings-reproducible) in addition to our new [automatically generated](https://docs.cwtch.im/blog/autobindings)
  - **Two New Supported Localizations**: **Slovak** and **Korean**
- **Bug Fixes  / Improvements:**
  - When preserving a message draft, quoted messages are now also saved
  - Layout issues caused by pathological unicode are now prevented
  - Improved performance of message row rendering
  - Clickable Links: Links in replies are now selectable
  - Clickable Links: Fixed error when highlighting certain URIs
  - File Downloading: Fixes for file downloading and exporting on 32bit Android devices
  - Server Hosting: Fixes for several layout issues
  - Build pipeline now runs automated UI tests
  - Fix issues caused by scrollbar controller overriding
  - Initial support for the Blodeuwedd Assistant (currently compile-time disabled)
  - Cwtch Library:
    - [New Stable Cwtch Peer API](/blog/cwtch-stable-api-design)
    - Ported File Downloading and Image Previews experiments into Cwtch
- **Accessibility / UX:**
  - Full translations for **Brazilian Portuguese**, **Dutch**, **French**, **German**, **Italian**, **Russian**, **Polish**, **Spanish**, **Turkish**, and **Welsh**
  - Core translations for **Danish** (75%), **Norwegian** (76%), and  **Romanian** (75%)
  - Partial translations for **Luxembourgish** (22%), **Greek** (16%), and **Portuguese** (6%)



## Reproducible Bindings

Cwtch 1.11 is based on libCwtch version `2023-03-16-15-07-v0.0.3-1-g50c853a`. The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.3-1-g50c853a](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.3-1-g50c853a)

## Download the New Version

You can download Cwtch from [https://cwtch.im/download](https://cwtch.im/download).

Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

Alternatively we also provide a [releases-only RSS feed](https://cwtch.im/releases/index.xml).

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

