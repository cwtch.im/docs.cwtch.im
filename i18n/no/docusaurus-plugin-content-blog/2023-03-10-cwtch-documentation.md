---
title: Updates to Cwtch Documentation
description: "In this development log we will highlight some of the major documentation updates over the last few weeks."
slug: cwtch-documentation
tags:
  - cwtch
  - cwtch-stable
  - documentation
  - security-handbook
image: /img/devlog9_small.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

One of the main streams of work in the lead up to Cwtch Stable has been improving all aspects of Cwtch Documentation. In this development log we will highlight some of the major updates over the last few weeks.

![](/img/devlog9.png)
 
<!--truncate-->

## Cwtch Secure Development Handbook

One of the earliest compendiums of Cwtch documentation was the Cwtch Secure Development Handbook. This handbook provided an overview of the various parts of the Cwtch ecosystem, the known risks, and any existing mitigations. The handbook was designed to serve as a guide to developers who were building or extending Cwtch, and over the years it also served as a permanent home for documenting long-standing design decisions.

We have [now ported the the handbook to this documentation site](/security/intro), along with updating some of the contents. Over the next few months we will be expanding this section to include new sections on fuzzing, plugins, and client implementation.

## Volunteer Development

We have noticed an uptick in the number of people reaching out interested in contributing to Cwtch development. In order to help people get acclimated to our development flow we have created a new section on the main documentation site called [Developing Cwtch](/docs/contribute/developing) - there you will find a collection of useful links and information about how to get started with Cwtch development, what libraries and tools we use, how pull requests are validated and verified, and how to choose an issue to work on.

We also also updated our guides on [Translating Cwtch](/docs/contribute/translate) and [Testing Cwtch](/docs/contribute/testing).

If you are interested in getting started with Cwtch development then please check it out, and feel free to reach out to `team@cwtch.im` (or open an issue) with any questions. All types of contributions [are eligible for stickers](/docs/contribute/stickers).

## Next Steps

We still have more work to do on the documentation front:

* Ensuring all pages [implement the new documentation style guide](/docs/contribute/documentation), and include appropriate screenshots and descriptions.
* Expanding the security handbook to provide information on [reproducible builds](/blog/cwtch-bindings-reproducible), [the new Cwtch Stable API](/blog/cwtch-stable-api-design) and upcoming improvements around fuzz testing.
* Creating new documentation sections on the [libCwtch autobindings API](/blog/autobindings) and building applications on top of Cwtch.

As these changes are made, and these goals met we will be posting about them here! Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

