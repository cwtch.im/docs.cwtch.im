---
sidebar_position: 6
---

# Documentation Style Guide

This section documents the expected structure and quality of Cwtch documentation.

## Screenshots and Cast of Characters

Most Cwtch documentation should feature at least one screenshot or animated image. Screenshots of the Cwtch application should be focused on the feature being described by the documentation.

To ensure consistency between screenshots we suggest that the profile involved should serve particular, constant, roles.

- **Alice** - used to represent the primary profile.
- **Bob** - the primary contact, useful when demonstrating peer-to-peer features
- **Carol** - a secondary contact, useful when demonstrating group features
- **Mallory** - representing a malicious peer (to be used when demonstrating blocking functionality)

## Dialogue and Content

Where screenshots and demonstrations show dialogue, conversations, and/or images please keep the conversations short, on a casual topic. Examples include:

- Organizing a picnic
- Sharing photos from a vacation
- Sending a document for review

## Experiments

All features that rely on an experiment being enabled should all this out prominently at the top of the page e.g.:

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and the [Example Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) turned on.
:::

## Risks

If a feature might result in destruction of key material or permanent deletion of state, then these should also be called out at the top of the documentation e.g.:


:::warning

This feature will result in **irreversible** deletion of key material. This **cannot be undone**.

:::