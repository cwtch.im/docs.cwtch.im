---
sidebar_position: 1.5
---

# Starting a New Conversation

1. Select a Profile
2. Click on the Add button
3. Chose 'Add Contact'
4. Paste a Cwtch Address
5. The contact will be added to your contacts list

:::info

This documentation page is a stub. You can help by [expanding it](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::
