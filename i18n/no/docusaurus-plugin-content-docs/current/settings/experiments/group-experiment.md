---
sidebar_position: 1
---

# Groups Experiment

Enables Cwtch to [connect to untrusted servers](/docs/servers/introduction) and use them to [host private, asynchronous, groups](/docs/groups/introduction).

## To Turn On

1. Go to Settings
2. Enable Experiments
3. Enable the Group experiment


