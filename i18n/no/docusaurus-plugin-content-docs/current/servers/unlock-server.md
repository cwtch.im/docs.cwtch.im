---
sidebar_position: 6
---

# How to Unlock a server

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and the [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) turned on.

:::

If you protected your server with a password, it will need to be unlocked each time you restart the application.

1. Go to the server Icon
2. Click on the pink unlock icon
3. Input the password of your server
4. Press Unlock