---
sidebar_position: 1
---

# An Introduction to Cwtch Groups

:::caution Experiments Required

This feature requires [Experiments Enabled](/docs/settings/introduction#experiments) and the [Group Experiment](/docs/settings/experiments/group-experiment) turned on.

:::

**Note: Metadata Resistant Group Communication is still an active research area and what is documented here will likely change in the future.**

By default, Cwtch only supports peer-to-peer, online, chat. In order to support multi-party conversations, and offline delivery, an (untrusted) third-party is required. We call these entities ["servers"](/docs/servers/introduction)

These servers can be set up by anyone and are intended to be always online. Most importantly, all communication with a server is designed such that the server learns as little information as possible about the contents or metadata.

In many respects communication with a server is identical to communication with a regular Cwtch peer, all the same steps are taken however the server always acts as the inbound peer, and the outbound peer always uses newly generated **ephemeral keypair** - so that each server session is disconnected.

As such, peer-server conversations only differ in the *kinds* of messages that are sent between the two parties, with the server storing all messages that it receives and thus allowing any client to query for older messages.

The risk model associated with servers is more complicated that peer-to-peer communication, as such we currently require people who want to use servers within Cwtch to [opt-in to the Group Chat experiment](/docs/settings/experiments/group-experiment) in order to add, manage and create groups on untrusted servers.

## How Groups Work Under the Hood

When a person wants to start a group conversation they first randomly generate a secret `Group Key`. All group communication will be encrypted using this key.

Along with the `Group Key`, the group creator also decides on a **Cwtch Server** to use as the host of the group. For more information on how Servers authenticate themselves see [key bundles](https://docs.openprivacy.ca/cwtch-security-handbook/key_bundles.html).

A `Group Identifier` is generated using the group key and the group server and these three elements are packaged up into an invite that can be sent to potential group members (e.g. over existing peer-to-peer connections).

To send a message to the group, a profile connects to the server hosting the group (see below), and encrypts their message using the `Group Key` and generates a cryptographic signature over the `Group Id`, `Group Server` and the decrypted message (see: [wire formats](https://docs.openprivacy.ca/cwtch-security-handbook/message_formats.html) for more information).

To receive message from the group, a profile connected to the server hosting the group and downloads *all* messages (since their previous connection). Profiles then attempt to decrypt each message using the `Group Key` and if successful attempt to verify the signature (see [Cwtch Servers](https://docs.openprivacy.ca/cwtch-security-handbook/server.html)  [Cwtch Groups](https://docs.openprivacy.ca/cwtch-security-handbook/groups.html) for an overview of attacks and mitigations).


