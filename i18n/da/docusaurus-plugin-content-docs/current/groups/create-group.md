---
sidebar_position: 3
---

# Creating a New Group

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and the [Group Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) turned on.

:::

1. On your contacts pane
2. Press on the plus action button
3. Press on Create Group
4. Name your Group
5. Press on Create

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Create.mp4" />
    </video>
</div>