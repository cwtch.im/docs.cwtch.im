---
sidebar_position: 3
---

# Notification Content

1. Go to settings
2. Scroll to behaviour
3. The notification content controls the contents of notifications
   1. Plain Event: "New Message" only
   2. Conversation Information: "New Message from XXXXX"
