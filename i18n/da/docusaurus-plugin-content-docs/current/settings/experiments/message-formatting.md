---
sidebar_position: 6
---

# Message Formatting

When enabled, this experiment changes the conversation compose box to add [message formatting](/docs/chat/message-formatting) UX.

This experiment is now enabled by default.