---
sidebar_position: 10
---

# Stickers

All contributions are eligible for stickers. If you are contributing to bug, feature, testing, or language, or have contributed significantly in the past then please email erinn@openprivacy.ca with details and an address for us to mail stickers to.

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

