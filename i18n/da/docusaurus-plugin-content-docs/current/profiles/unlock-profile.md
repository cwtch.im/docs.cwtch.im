---
sidebar_position: 5
---

# Unlocking Encrypted Profiles

When you restart Cwtch, if you used a [password](/docs/profiles/change-password/) to protect your profile, it will not be loaded by default, and you will need to unlock it.

1. Press the pink unlock icon <img src="/img/lock_open-24px.webp" className="ui-button" alt="unlock icon" />
2. Input your password
3. Click “unlock your profile”

See also: [Cwtch Security Handbook: Profile Encryption & Storage](https://docs.openprivacy.ca/cwtch-security-handbook/profile_encryption_and_storage.html)
