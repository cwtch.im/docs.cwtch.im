---
sidebar_position: 1
---

# An Introduction to Cwtch Profiles

With Cwtch you can create one of more **Profiles**. Each profile generates a random ed25519 key pair compatible with the Tor Network.

This is the identifier that you can give out to people and that they can use to contact you via Cwtch.

Cwtch allows you to create and manage multiple, separate profiles. Each profile is associated with a different key pair which launches a different onion service.

## Manage Profiles

On start up Cwtch will launch the Manage Profiles screen. From this screen you can:

- [Create a New Profile](https://docs.cwtch.im/docs/profiles/create-a-profile)
- [Unlock Existing Encrypted Profiles](https://docs.cwtch.im/docs/profiles/unlock-profile)
- Manage Loaded Profiles
    - [Changing The Display Name of a Profile](https://docs.cwtch.im/docs/profiles/change-name/)
    - [Changing the Password of a Profile](https://docs.cwtch.im/docs/profiles/change-password/)
    - [Deleting a Profile](https://docs.cwtch.im/docs/profiles/delete-profile)
    - [Changing a Profile Image](https://docs.cwtch.im/docs/profiles/change-profile-image/)