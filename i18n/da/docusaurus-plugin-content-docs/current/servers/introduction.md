---
sidebar_position: 1
---

# Servers Introduction

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and the [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) turned on.

:::

Cwtch contact to contact chat is fully peer to peer, which means if one peer is offline, you cannot chat, and there is no mechanism for multiple people to chat.

To support group chat (and offline delivery) we have created untrusted Cwtch servers which can host messages for a group. The messages are encrypted with the group key, and fetch via ephemeral onions, so the server has no way to know what messages for what groups it might be holding, or who is accessing it.

Currently running servers in the Cwtch app is only supported on the Desktop version as mobile devices' internet conection and environment is too unstable and unsuitable to running a server.
