---
title: Path to Cwtch Stable
description: "The post outlines the general principles that are guiding the development of Cwtch Stable, the obstacles that prevent a stable Cwtch release, and closes with an overview the next steps and a timeline to tackle them."
slug: path-to-cwtch-stable
tags:
  - cwtch
  - cwtch-stable
  - planning
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

As of December 2022 we have released 10 versions of Cwtch Beta since the [initial launch, 18 months ago, in June 2021](https://openprivacy.ca/discreet-log/10-cwtch-beta-and-beyond/).

There is a consensus among the team that the next large step for the Cwtch project to take is a move from public **Beta** to **Stable** – marking a point at which we consider Cwtch to be secure and usable.

This post outlines the general principles that are guiding the development of Cwtch Stable, the obstacles that prevent a stable Cwtch release, and closes with an overview of the next steps and our timeline for tackling them.

![](/img/devlog1.png)

<!--truncate-->

### Tenets of Cwtch Stable

It is important to state that Cwtch Stable **does not mean an end to Cwtch development**. Rather, it establishes a baseline at which point Cwtch is considered to be a fully supported project. The Cwtch Team have set the following tenets that guide our decision-making and priorities:

1. **Consistent Interface** – each new Cwtch release should be accompanied by consistent releases to all support libraries. This requires a stable and documented API so that we can be clear when upgrading a library will result in breaking change for downstream projects.  We should not, as a general rule, have to make breaking changes to this API interface in order to support new experimental features.
2. **Universal Availability and Cohesive Support** –  people who use Cwtch understand that if Cwtch is available for a platform then that means all features will work as expected, that there are no surprise limitations, and any differences are well documented. People should not have to go out of their way to install Cwtch.
3. **Reproducible Builds** – Cwtch builds should be trivially reproducible, including the ability to reproduce all bundled assets. Reproducibility should not rely on containerization, but all containers used in our build process should be reproducible.
4. **Proven Security** – we can demonstrate that Cwtch provides first class security through well documented design, testing, and audit procedures. We should be able to do this for Cwtch in addition to all functional dependencies.

### Known Problems

To begin, let's outline the current state of Cwtch and lay out the issues that stand in the way of Cwtch Stable.

1. **Lack of a Stable API for future feature development** – while the core Cwtch API has remained fairly unchanged in recent releases we understand that the addition of new features e.g. cohesive group support likely requires new API hooks that allow safe manipulation of Cwtch Profile (transactional semantics and post-event hooks). Before we can even consider a stable release we need to define what this API should look like, and implement it. (Tenet 1)
2. **Special functionality in libCwtch-go** – our C-API bridge (libCwtch-go) currently implements a lot of special functionality in support for both experimental features (e.g. profile images) and UI settings. This special behaviour makes it difficult to track feature responsibility. This behaviour must either be pushed back into the main Cwtch library, or defined to be the responsibility of a downstream application e.g. Cwtch UI. (Tenet 1)
3. **libCwtch-rs partial support**  - we currently do not officially consider [libCwtch-rs](https://lib.rs/crates/libcwtch) when updating libCwtch-go as part of our release schedule. Before we can consider a Cwtch Stable release we should have multiple beta releases where libCwtch-rs has full support for any and all new Cwtch features. (Tenet 1, Tenet 2)
4. **Lack of Reproducible Pipelines** - while the vast majority of our build pipeline is automated, containerized, and reproducible, there remain bundled assets that cannot be trivially constructed, and assets that have non-reproducible elements (e.g. build-time injected via git tags, and go binaries including build user information). (Tenet 3)
5. **Lack of up to date, and translated, Security Documentation** – the [Cwtch security handbook](https://docs.openprivacy.ca/cwtch-security-handbook/) is currently isolated from the rest of our documentation and doesn’t benefit from cross-linking, or translations. (Tenet 4)
6. **No Automated UI Tests** – we put a lot of work into [building out a testing framework for the UI](https://openprivacy.ca/discreet-log/23-cucumber-testing/), but it currently sits mostly unused, and unexercised in our build pipelines. We should revisit that work. (Tenet 4)
7. **Code Signing Provider** – our previous code signing certificate provider had support issues, and we have not yet decided on a replacement. ( Tenet 4)
8. **Second-class Android Support**  - while we have put [a lot of effort behind Android support](https://openprivacy.ca/discreet-log/27-android-improvements/) across the Beta timeline, it still clearly suffers from additional issues that desktop editions do not. In order to consider Cwtch stable we must resolve all major bugs impacting Android usability. (Tenet 2)
9. **Lack of Fuzzing** – while [Fuzzbot](https://openprivacy.ca/discreet-log/07-fuzzbot/) sets a standard high above most other secure communication applications, we can and should do better. Fuzzbot currently only targets user-endpoint messages, which are the most likely to result in real-world risk, but we should strive to have the same coverage for internal events at both the network level, the internal Cwtch App level, and the event bus level. (Tenet 4)
10. **Lack of Formal Release Acceptance Process** – currently the features and experiments that get included in each release are determined in an ad-hoc consensus. This occasionally means that some features are left unsupported on certain platforms, and bugs occasionally arise in platforms (Android in particular) due to “unrelated” changes. In order for Cwtch to be declared stable, a formal acceptance process must ensure that new changes do not break existing features, and that they work across all platforms. (Tenet2, Tenet 4)
11. **Inconsistent Cwtch Information Discovery** – our current documentation is split between docs.cwtch.im, cwtch.im and docs.openprivacy.ca, in additional to blogs on Discreet Log. This makes it difficult for people to learn about Cwtch, and also means that our own explanations often must link across multiple different sites. (Tenet 2)
12. **Incomplete Documentation** – docs.cwtch.im was very well received. However, it still suffers from incomplete sections, missing links, and an overall lack of screenshots. What screenshots there are lack consistency in sizing, style, and feel. (Tenet 2)

### Plan of Action

Outside of the problems that have standalone solutions (e.g. find a new code signing provider, or fix all Android issues), there are a number of higher level activities that need to be completed before we can be confident in a Cwtch Stable release:

1. **Define, Publish, and Implement a Cwtch Interface Specification Documentation** – this should include examples of how new (experimental) behaviour might be implemented from finer-grained composition. Must include moving all special functionality out of libCwtch-go. Should be followed up by implementing the proposed design. (Tenet 1, Tenet 4)
2. **Define, Publish, and Implement a Cwtch Release Process** – this document should outline the criteria for publishing a new release, the difference between major and minor versions, how features are tested, how regressions are caught before release, and who is responsible for different parts of the process. (Tenet 2)
3. **Define, Publish, and Implement a Cwtch Support Document**  - including answers to the questions: what systems do we support, how do we decide what systems are supported, how do we handle new OS versions, and how does application support differ from library support. This should also include a list of blockers for systems we wish to support, but currently cannot e.g ios. (Tenet 2)
4. **Define, Publish, and Implement a Cwtch Packaging Document**  - as a supplement to the Support document we need to define what packaging we support, in addition to what app stores and managers for which we provide official releases. ( Tenet 2)
5. **Define, Publish, and Implement a Reproducible Builds Document** – this should cover not only Cwtch binaries, but also Docker containers, and included assets (e.g. Tor binaries). Followed up by implementing the plan into our build pipeline. ( Tenet 3)
6. **Expand the Cwtch Documentation Site** – to include the Security Handbook, development blogs, design documentation, and support plans. This should be our only publishing platform, outside of a landing page, and downloads on cwtch.im. This expansion should include a style guide for documentation and screenshots to ensure that we maintain consistent language and visuals when talking about a feature (e.g. we should use the same profile image style, theme, profile names, message style etc.) (Tenet 1, Tenet 2, Tenet 3, Tenet 4)
7. **Expand our Automated Testing to include UI and Fuzzing**  - integrate UI automated tests into our build pipeline. Expand our fuzzing to include the event bus, and PeerApp packets. Finally, integrate automated fuzzing into the build pipeline, so that all new features are fuzzed to the same level. (Tenet 4)
8. **Re-evaluate all Issues across all Cwtch related repositories** – issues are either bugs that need to be fixed before stable (i.e. they are in service of one of the Tenets), new feature ideas that should be scheduled around stable work (i.e. they don’t align with a specific Tenet), or support requests for systems that need input from the Support and Packaging Plans.
9. **Define a Stable Feature Set** – there are still a few features which do not exist in Cwtch Beta which would be required for a stable release, such as chat search. Following on from the Cwtch Interface Specification Document, the team should decide what features Cwtch Stable will target, and these features should be prioritized for inclusion in Cwtch 1.11, Cwtch 1.12 and any future Beta releases. (Tenet 1)

### Goals and Timelines

With all of that laid out, we are now ready to introduce a timeline for resolving some of these problems, and moving us towards a state where we can launch Cwtch Stable:

1. By **1st February 2023**, the Cwtch team will have reviewed all existing Cwtch issues in line with this document, and established a timeline for including them in upcoming releases (or specifically commit to not including them in upcoming releases).
2. By **1st February 2023**, the Cwtch team will have finalized a feature set that defines Cwtch Stable and established a timeline for including these features in upcoming Cwtch Beta releases.
3. By **1st February 2023**, the Cwtch team will have expanded the Cwtch Documentation website to include a section for Security, Design Documents, Infrastructure and Support, in addition to a new development blog.
4. By **31st March 2023**, the Cwtch team will have created a style guide for documentation and have used it to ensure that all Cwtch features have consistent documentation available, with at least one screenshot (where applicable).
5. By **31st March 2023** the Cwtch team will have published a Cwtch Interface Specification Document,  a Cwtch Release Process Document, a Cwtch Support Plan document, a Cwtch Packaging Document, and a document describing the Reproducible Builds Process. These documents will be available on the newly expanded Cwtch Documentation website.
6. By **31st March 2023** the Cwtch team will have integrated automated UI tests into the build pipeline for the cwtch-ui repository.
7. By **31st March 2023** the Cwtch team will have integrated automated fuzzing into the build pipeline for all Cwtch dependencies maintained by the Cwtch team.
8. By **31st March 2023** the Cwtch team will have committed to a date, timeline, and roadmap for launching Cwtch Stable.

As these documents are written, and these goals met we will be posting them here! Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, Cwtch development.

### Help us get there!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

