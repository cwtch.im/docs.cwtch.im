---
sidebar_position: 1
---

# Change Language

[Thanks to the help of volunteers](https://docs.cwtch.im/docs/contribute/translate), the Cwtch app has been translated to many languages.

To change the language Cwtch uses:

1. Open Settings
2. The top setting is "Language", you can use the drop down to select the language that you wish to use.