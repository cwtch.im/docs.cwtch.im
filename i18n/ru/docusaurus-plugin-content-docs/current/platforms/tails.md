---
sidebar_position: 1
---

# Running Cwtch on Tails

:::warning New Feature

New in [Cwtch 1.12](/blog/cwtch-nightly-1-12)

This functionality may be incomplete and/or dangerous if misused. Please help us to review, and test.
:::

The following steps require that Tails has been launched with an [Administration Password](https://tails.boum.org/doc/first_steps/welcome_screen/administration_password/).

Tails uses [Onion Grater](https://gitlab.tails.boum.org/tails/tails/-/blob/master/config/chroot_local-includes/usr/local/lib/onion-grater#L3) to guard access to the control port. We have packaged an oniongrater configuration [`cwtch-tails.yml` ](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/linux/cwtch-tails.yml) and setup script (`install-tails.sh`) with Cwtch on Linux.

The tails-specific part of the script is reproduced below:

        # Tails needs to be have been setup up with an Administration account
        # 
        # Make Auth Cookie Readable
        sudo chmod o+r /var/run/tor/control.authcookie
        # Copy Onion Grater Config
        sudo cp cwtch.yml /etc/onion-grater.d/cwtch.yml
        # Restart Onion Grater so the Config Takes effect
        sudo systemctl restart onion-grater.service

When launching, Cwtch on Tails should be passed the `CWTCH_TAILS=true` environment variable to automatically configure Cwtch for running in a Tails-like environment:

`exec env CWTCH_TAILS=true LD_LIBRARY_PATH=~/.local/lib/cwtch/:~/.local/lib/cwtch/Tor ~/.local/lib/cwtch/cwtch`

:::info Install Location

The above command, and the below onion grater configuration assume that Cwtch was installed in `~/.local/lib/cwtch/cwtch` - if Cwtch was installed somewhere else (or if you are running directly from the download folder) then you will need to adjust the commands.

:::

## Onion Grater Configuration

The oniongrater configuration [`cwtch-tails.yml` ](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/linux/cwtch-tails.yml) is reproduced below. As noted this configuration is can likely be restricted much further. 

        ---
        # TODO: This can likely be restricted even further, especially in regards to the ADD_ONION pattern
    
        - apparmor-profiles:
            - '/home/amnesia/.local/lib/cwtch/cwtch'
          users:
    
            - 'amnesia'
          commands:
            AUTHCHALLENGE:
    
              - 'SAFECOOKIE .*'
            SETEVENTS:
    
              - 'CIRC WARN ERR'
              - 'CIRC ORCONN INFO NOTICE WARN ERR HS_DESC HS_DESC_CONTENT'
            GETINFO:
    
              - '.*'
            GETCONF:
    
              - 'DisableNetwork'
            SETCONF:
    
              - 'DisableNetwork.*'
            ADD_ONION:
    
              - '.*'
            DEL_ONION:
    
              - '.+'
            HSFETCH:
    
              - '.+'
          events:
            CIRC:
              suppress: true
            ORCONN:
              suppress: true
            INFO:
              suppress: true
            NOTICE:
              suppress: true
            WARN:
              suppress: true
            ERR:
              suppress: true
            HS_DESC:
              response:
    
            - pattern:     '650 HS_DESC CREATED (\S+) (\S+) (\S+) \S+ (.+)'
              replacement: '650 HS_DESC CREATED {} {} {} redacted {}'
    
            - pattern:     '650 HS_DESC UPLOAD (\S+) (\S+) .*'
              replacement: '650 HS_DESC UPLOAD {} {} redacted redacted'
    
            - pattern:     '650 HS_DESC UPLOADED (\S+) (\S+) .+'
              replacement: '650 HS_DESC UPLOADED {} {} redacted'
    
            - pattern:     '650 HS_DESC REQUESTED (\S+) NO_AUTH'
              replacement: '650 HS_DESC REQUESTED {} NO_AUTH'
    
            - pattern:     '650 HS_DESC REQUESTED (\S+) NO_AUTH \S+ \S+'
              replacement: '650 HS_DESC REQUESTED {} NO_AUTH redacted redacted'
    
            - pattern:     '650 HS_DESC RECEIVED (\S+) NO_AUTH \S+ \S+'
              replacement: '650 HS_DESC RECEIVED {} NO_AUTH redacted redacted'
    
            - pattern:     '.*'
              replacement: ''
            HS_DESC_CONTENT:
              suppress: true

## Persistence

By default, Cwtch creates `$HOME/.cwtch` and saves all encrypted profiles and settings files there. In order to save any profiles/conversations in Cwtch on Tails you will have to backup this folder to a non-volatile home.

See the Tails documentation for setting up [persistent storage](https://tails.boum.org/doc/persistent_storage/)
