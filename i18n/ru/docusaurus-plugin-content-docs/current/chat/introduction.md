---
sidebar_position: 1
---

# An Introduction to Cwtch P2P Chat

Cwtch uses Tor v3 Onion Services to establish anonymous, peer-to-peer connections between Profiles.

## How P2P Chat Works Under the Hood

In order to chat with your friends in a peer-to-peer conversation both must be online.

After a successful connection both parties engage in an **authentication protocol** which:

* Asserts that each party has access to the private key associated with their public identity.
* Generates an ephemeral session key used to encrypt all further communication during the session.

This exchange (documented in further detail in [authentication protocol](https://docs.openprivacy.ca/cwtch-security-handbook/authentication_protocol.html)) is *offline deniable* i.e. it is possible for any party to forge transcripts of this protocol exchange after the fact, and as such - after the fact - it is impossible to definitely prove that the exchange happened at all.

Once the authentication process is successful then both you and your friend can communicate away assured that no one else can learn anything about the contents or the metadata of your conversation.
