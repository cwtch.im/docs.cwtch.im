---
sidebar_position: 10
---

# Removing a Conversation

:::warning

This feature will result in **irreversible** deletion. This **cannot be undone**.

:::

- In a chat with a contact, go to the conversation settings on the top right <span class="icon">![](/img/peer_settings-24px.svg)</span>
- Scroll to the **leave this conversation** button, and press it.
- You will be prompted to confirm if you want to leave the conversation. This action cannot be undone.


:::info

This documentation page is a stub. You can help by [expanding it](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::
