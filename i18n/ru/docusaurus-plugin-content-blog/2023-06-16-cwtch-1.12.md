---
title: Cwtch Beta 1.12
description: "Cwtch Beta 1.12 is now available for download"
slug: cwtch-nightly-1-12
tags:
  - cwtch
  - cwtch-stable
  - release
image: /img/devlog1_small.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

[Cwtch 1.12 is now available for download](https://cwtch.im/download)!

Cwtch 1.12 is the culmination of the last few months of effort by the Cwtch team, and includes many foundational changes that pave the way for [Cwtch Stable](/blog/path-to-cwtch-stable) including new features like [profile attributes](https://docs.cwtch.im/docs/profiles/profile-info), support for new platforms like [Tails](https://docs.cwtch.im/docs/platforms/tails), and multiple improvements to performance and stability.

![](/img/devlog1.png)
 
<!--truncate-->

## In This Release

<figure>

[![](/img/picnic1.12.png)](/img/picnic1.12.png)

<figcaption>A screenshot of Cwtch 1.12</figcaption>
</figure>

A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Profile Attributes** - profiles can now be augmented with [additional public information](https://docs.cwtch.im/docs/profiles/profile-info)
  - **Availability Status** - you can now notify contacts that you [are **away** or **busy**](https://docs.cwtch.im/docs/profiles/availability-status)
  - **Five New Supported Localizations**: **Japanese**, **Korean**, **Slovak**, **Swahili** and **Swedish**
  - **Support for Tails** - adds an [OnionGrater](https://docs.cwtch.im/docs/platforms/tails) configuration and a new `CWTCH_TAILS` environment variable that enables special Tor behaviour.
- **Bug Fixes  / Improvements:**
  - Based on Flutter 3.10
  - Inter is now the main UI font
  - New Font Scaling setting
  - New Network Management code to better manage Tor on unstable networks
  - File Sharing Experiment Fixes
    - Fix performance issues for file bubble
    - Allow restarting of file shares that have timed out
    - Fix NPE in FileBubble caused by deleting the underlying file
    - Move from RetVal to UpdateConversationAttributes to minimze UI thread issues
  - Updates to Linux install scripts to support more distributions
  - Add a Retry Peer connection to prioritize connection attempts for certain conversations
  - Updates to `_FlDartProject` to allow custom setting of Flutter asset paths
- **Accessibility / UX:**
  - Full translations for **Brazilian Portuguese**, **Dutch**, **French**, **German**, **Italian**, **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Core translations for **Danish** (75%), **Norwegian** (76%), and  **Romanian** (75%)
  - Partial translations for **Japanese** (29%), **Korean** (23%), **Luxembourgish** (22%), **Greek** (16%), and **Portuguese** (6%)

## Reproducible Bindings

Cwtch 1.12 is based on libCwtch version `libCwtch-autobindings-2023-06-13-10-50-v0.0.5`. The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.5](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.5)

## Download the New Version

You can download Cwtch from [https://cwtch.im/download](https://cwtch.im/download).

Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

Alternatively we also provide a [releases-only RSS feed](https://cwtch.im/releases/index.xml).

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

