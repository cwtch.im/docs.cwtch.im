---
sidebar_position: 1
---

# Getting Started

## Choosing A Cwtch Library

### Cwtch Go Lib

The official Cwtch library is written in Go and can be found at [https://git.openprivacy.ca/cwtch.im/cwtch](https://git.openprivacy.ca/cwtch.im/cwtch). This library allows access to all Cwtch functionality.

### CwtchBot

We also provide a specialized Cwtch Bot framework in Go that provides a more lightweight and tailored approach to building chat bots. For an introduction to building chatbots with the CwtchBot framework check out the [building an echobot tutorial](/developing/building-a-cwtch-app/building-an-echobot#using-cwtchbot-go).

### Autobindings (C-bindings)

The [official c-bindings for Cwtch](https://git.openprivacy.ca/cwtch.im/autobindings) are automatically generated from the Cwtch Go Library. The API is limited compared to accessing the Cwtch Go Library directly, and is explicitly tailored towards building the Cwtch UI.

### libCwtch-rs (Rust)

An experimental rust-fied version of Cwtch Autobindings is available in [libCwtch-rs](https://crates.io/crates/libcwtch). While we have plans to officially adopt rust bindings in the future, right now Rust support lags behind the rest of the Cwtch ecosystem.
