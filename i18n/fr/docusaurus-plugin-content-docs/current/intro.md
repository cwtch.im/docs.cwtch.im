---
sidebar_position: 1
---

# What is Cwtch?

Cwtch (/kʊtʃ/ - a Welsh word roughly translating to “a hug that creates a safe place”) is a decentralized, privacy-preserving, metadata resistant messaging app.

* **Decentralized and Open**: There is no “Cwtch service” or “Cwtch network”. Participants in Cwtch can host their own safe spaces, or lend their infrastructure to others seeking a safe space. The Cwtch protocol is open, and anyone is free to build bots, services and user interfaces and integrate and interact with Cwtch.
* **Privacy Preserving**: All communication in Cwtch is end-to-end encrypted and takes place over Tor v3 onion services.
* **Metadata Resistant**: Cwtch has been designed such that no information is exchanged or available to anyone without their explicit consent, including on-the-wire messages and protocol metadata.


# Security, Encryption and Safety

For a more in depth look at the security, privacy, and underlying encryption technology used in Cwtch, please consult our [Security Handbook](https://docs.openprivacy.ca/cwtch-security-handbook/)

# Getting Started

You can download the latest version of Cwtch from [https://cwtch.im/download/](https://cwtch.im/download/)

