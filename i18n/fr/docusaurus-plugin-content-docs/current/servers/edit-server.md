---
sidebar_position: 3
---

# How to edit a server

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and the [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) turned on.

:::

1. Go to the server icon
2. Select the server you want to edit
3. Press the pencil icon
4. Change the description/ or enable or disable the server
5. Click on save server

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/server_edit.mp4" />
    </video>
</div>