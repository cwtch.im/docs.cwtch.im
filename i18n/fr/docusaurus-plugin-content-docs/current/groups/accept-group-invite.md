---
sidebar_position: 5
---

# Accepting a Group Invite

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and the [Group Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) turned on.

:::

1. If a friend sends you a group request
2. Choose if you want or not to join this group
3. Now the group is on your contact list

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_acceptinvite.mp4" />
    </video>
</div>