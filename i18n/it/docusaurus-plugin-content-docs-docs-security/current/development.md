# Sviluppo

Il principale rimedio per contrastare attori malintenzionati nello sviluppo di Cwtch è il fatto che il processo sia pubblico e trasparente.

Per corroborare questa trasparenza, build, test e impacchettamento automatizzati sono definiti come parte dei repository, migliorando la robustezza della base di codice in ogni fase.

![](https://docs.openprivacy.ca/cwtch-security-handbook/1.png)

Sebbene i test individuali non siano perfetti e tutti i processi presentino lacune, dovremmo impegnarci a rendere il contributo a Cwtch il più semplice possibile costruendo al contempo pipeline e processi che rilevano errori (siano essi non intenzionali o dannosi) il prima possibile.

### Rischio: Uno sviluppatore inserisce direttamente codice dannoso

**Stato: Mitigato**

`trunk` è attualmente chiuso e solo 3 membri dello staff di Open Privacy hanno il permesso di sovrascriverlo, oltre alla responsabilità di monitorare le modifiche.

Inoltre, ogni nuova richiesta di pull e merge attiva build automatizzate & test che a loro volta generano e-mail e log di audit.

Il codice è anche open source e controllabile da chiunque.

### Rischio: Regressioni di codice

**Status: Parzialmente mitigato** (Per ulteriori informazioni, vedere le voci dei singoli progetti in questo manuale)

Le nostre pipeline automatizzate hanno la capacità di rilevare regressioni quando tale comportamento è rilevabile.

La sfida maggiore è definire come vengono rilevate tali regressioni per l'[interfaccia utente](/security/category/cwtch-ui), dove il comportamento non è definito così rigorosamente come lo è per le singole librerie.
 