---
sidebar_position: 1
---

# Manuale della sicurezza di Cwtch

Benvenuto nel manuale di sviluppo sicuro di Cwtch! Lo scopo di questo manuale è di fornire una guida ai vari componenti dell'ecosistema Cwtch, documentare i rischi e le mitigazioni noti e invitare una discussione sui miglioramenti e gli aggiornamenti ai processi di sviluppo sicuro di Cwtch.

![](https://docs.openprivacy.ca/cwtch-security-handbook/2.png)


## Che cos’è Cwtch?

Cwtch (/kʊtʃ/ - una parola gallese che si traduce approssimativamente in "un abbraccio che crea un luogo sicuro") è un protocollo di messaggistica multilaterale decentralizzato, che preserva la privacy e che può essere utilizzato per creare applicazioni resistenti ai metadati.

* **Decentralizzato e trasparente**: Non c'è nessun “servizio Cwtch” o “rete Cwtch”. Tramite Cwtch i partecipanti possono ospitare i loro propri spazi sicuri, o prestare la loro infrastruttura ad altri che cercano uno spazio sicuro. Il protocollo Cwtch è aperto, e chiunque può creare liberamente bot, servizi e interfacce utente, e integrarsi a Cwtch e interagire con esso.
* **Tutela della privacy**: tutte le comunicazioni in Cwtch sono crittografate end-to-end e avvengono tramite i servizi onion di Tor v3.
* **Resistente ai metadati**: il protocollo Cwtch è stato progettato in modo tale che nessuna informazione venga scambiata o resa disponibile a chicchessia senza il suo esplicito consenso, inclusi i messaggi in transito e i metadati del protocollo.


## Una (breve) storia della chat resistente ai metadati

Negli ultimi anni, la consapevolezza del pubblico sulla necessità e sui vantaggi di soluzioni crittografate end-to-end è aumentata, col risultato che applicazioni come [Signal](https://signalapp.org), [Whatsapp](https://whatsapp.com) e [Wire](https://wire.org) ora forniscono agli utenti comunicazioni sicure.

Tuttavia, questi strumenti richiedono vari livelli di esposizione dei metadati per funzionare, e molti di questi metadati possono essere utilizzati per ottenere dettagli su come e perché una persona utilizzi un certo strumento per comunicare. [[rottermanner2015privacy]](https://www.researchgate.net/profile/Peter_Kieseberg/publication/299984940_Privacy_and_data_protection_in_smartphone_messengers/links/5a1a9c29a6fdcc50adeb1335/Privacy-and-data-protection-in-smartphone-messengers.pdf).

Uno strumento che ha cercato effettivamente di ridurre i metadati è [Ricochet](https://ricochet.im), rilasciato per la prima volta nel 2014. Ricochet ha utilizzato i servizi onion di Tor v2 per fornire una comunicazione crittografata end-to-end sicura, e per proteggere i metadati delle comunicazioni.

Non c'erano server centralizzati che aiutassero nel routing delle conversazioni di Ricochet. Nessuno al di fuori delle parti coinvolte in una conversazione aveva modo sapere che tale conversazione avesse luogo.

Ricochet non era senza limitazioni; non c'era supporto multi-dispositivo, né era presente un meccanismo per supportare la comunicazione di gruppo o perché un utente potesse inviare messaggi ad un contatto offline.

Ciò ha reso l'adozione di Ricochet una proposta difficile; e anche coloro che si trovano in ambienti che sarebbero meglio serviti dalla resistenza ai metadati non erano al corrente della sua esistenza [[ermoshina2017can]](https://www.academia.edu/download/53192589/ermoshina-12.pdf) [[renaud2014non]](https://eprints.gla.ac.uk/116203/1/116203.pdf).

Inoltre, qualsiasi soluzione per una comunicazione decentralizzata e resistente ai metadati deve in generale affrontare [problemi fondamentali](https://code.briarproject.org/briar/briar/-/wikis/Fundamental-Problems) in termini di efficienza, privacy e sicurezza di gruppo (come definito dalla sezione [consenso e coerenza della trascrizione](https://code.briarproject.org/briar/briar/-/wikis/Fundamental-Problems)).

Le alternative moderne a Ricochet includono [Briar](https://briarproject.org), [Zbay](https://www.zbay.app/) e [Ricochet Refresh](https://www.ricochetrefresh.net/): ognuno di questi strumenti cerca di ottimizzare al netto di una serie diversa di compromessi, ad esempio Briar cerca di consentire alle persone di comunicare [anche quando l'infrastruttura di rete sottostante non funziona](https://briarproject.org/how-it-works/) fornendo allo stesso tempo resistenza alla sorveglianza dei metadati.

<hr />

Il progetto Cwtch è iniziato nel 2017 come protocollo di estensione per Ricochet che fornisse conversazioni di gruppo tramite server non attendibili, con in mente l'obiettivo di abilitare applicazioni decentralizzate e resistenti ai metadati (come elenchi condivisi e bacheca)

Una versione alpha di Cwtch è stata [lanciata nel febbraio 2019](https://openprivacy.ca/blog/2019/02/14/cwtch-alpha/) e da allora il team di Cwtch (gestito dalla [Società di ricerca Open Privacy](https://openprivacy.ca)) ha condotto ricerca e sviluppo su Cwtch e su sottostanti protocolli e librerie e ambiti dei problemi.





