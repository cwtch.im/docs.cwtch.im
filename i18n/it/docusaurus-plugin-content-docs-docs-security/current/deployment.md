# Distribuzione


## Rischio: I binari sono sostituiti sul sito web con binari dannosi

**Stato: Parzialmente mitigato**

Sebbene questo processo sia ora per lo più automatizzato, qualora tale automazione dovesse mai essere compromessa, non c'è nulla nel nostro processo attuale che possa rilevarlo.

Abbiamo bisogno di:

* Build riproducibili: attualmente utilizziamo contenitori docker pubblici per tutte le build che dovrebbero consentire a chiunque di confrontare le build distribuite con quelle create dal sorgente.
 * Versioni firmate: Open Privacy non mantiene ancora un registro pubblico delle chiavi pubbliche dello staff. Questa è probabilmente una necessità per firmare le build rilasciate e creare una catena di audit supportata dall'organizzazione. Questo processo deve essere manuale per definizione.
  
