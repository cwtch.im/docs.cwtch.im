---
sidebar_position: 2
---

# Protocollo di autenticazione

Ogni peer, data una connessione aperta `$C$`:
```
$$
I = \mathrm{InitializeIdentity()} \\
I_e = \mathrm{InitializeEphemeralIdentity()}  \\
I,I_e \rightarrow C \\
P,P_e \leftarrow C \\
k = \mathrm{KDF}({P_e}^{i} + {P}^{i_e} + {P_e}^{i_e}) \\
c = \mathrm{E}(k, transcript.Commit()) \\
c \rightarrow C \\
c_p \leftarrow C\\
\mathrm{D}(k, c_p) \stackrel{?}{=} transcript.LatestCommit()
$$
```
Quanto sopra rappresenta solo uno schizzo di protocollo, in realtà ci sono alcuni dettagli di implementazione che vale la pena sottolineare:

La chiave `($k$)`, una volta derivata dalla funzione di derivazione della chiave `($\mathrm{KDF}$)`, è impostata *sulla connessione*, il che significa che l'app di autenticazione non esegue esplicitamente la cifrazione o decifrazione.

La concatenazione delle parti dello scambio 3DH (Diffie-Hellman triplo) è rigorosamente ordinata:

* DH dell'identità a lungo termine della connessione in uscita attraverso la chiave effimera della connessione in entrata.
* DH dell'identità a lungo termine della connessione in entrata attraverso la chiave effimera della connessione in uscita.
* DH delle due identità effimere delle connessioni in entrata e in uscita.

Questo ordine rigoroso assicura che entrambi i lati della connessione derivino la *stessa* chiave di sessione.

### Proprietà Crittografiche

Durante una sessione online tutti i messaggi cifrati con la chiave di sessione possono essere autenticati dai peer come provenienti da propri peer (o perlomeno, qualcuno in possesso della chiave segreta dei propri peer in quanto correlata al loro indirizzo onion).

Una volta terminata la sessione, una trascrizione contenente la chiave pubblica effimera e quella a lungo termine, una chiave di sessione derivata e tutti i messaggi cifrati nella sessione non può essere dimostrata autentica, il che significa che questo protocollo fornisce la ripudiazione di messaggio & partecipante (negabile fuori rete), in aggiunta all'impossibilità di linkare nuovi messaggi (negabile fuori rete), nel caso in cui qualcuno sia soddisfatto che un singolo messaggio nella trascrizione debba aver avuto origine da un peer, [???] e non c'è quindi modo di collegare nessun altro messaggio alla sessione.

Intuizione per quanto sopra: l'unico materiale crittografico relativo alla trascrizione è la chiave di sessione derivata - se la chiave di sessione viene resa pubblica, può essere utilizzata per fabbricare nuovi messaggi nella trascrizione, e di conseguenza qualsiasi file di trascrizione preso separatamente è soggetto a falsificazione e quindi non può essere utilizzato per collegare crittograficamente un peer a una conversazione.

