---
sidebar_position: 1
---

# Formato dei pacchetti

Tutti i pacchetti di tapir sono di lunghezza fissa (8192 byte) con i primi 2 byte indicati la lunghezza effettiva del messaggio, ovvero `len bytes` di dati, e il resto riempito con zero:

    | len (2 bytes) | data (len bytes) | paddding (8190-len bytes)|

Una volta cifrato, l'intero pacchetto di dati da 8192 byte è crittografato utilizzando la struttura standard di [libsodium secretbox](https://libsodium.gitbook.io/doc/secret-key_cryptography/secretbox) (si noti in questo caso che la dimensione effettiva utilizzabile del pacchetto di dati è 8190-14 per ospitare il nonce incluso nella secretbox)

Per informazioni su come viene derivata la chiave segreta, si veda il [protocollo di autenticazione](authentication_protocol.md)

 