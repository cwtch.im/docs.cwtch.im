---
sidebar_position: 3
---

# Pacchetti di chiavi

I server Cwtch si identificano tra loro attraverso pacchetti di chiavi firmati. Questi pacchetti di chiavi contengono una lista di chiavi necessarie per rendere la comunicazione di gruppo Cwtch sicura e i metadati resistenti.

Al momento della scrittura di questo testo, i pacchetti di chiavi devono normalmente contenere 3 chiavi:

1. Una chiave pubblica di servizio onion Tor v3 per il Token Board (ed25519) - utilizzata per connettersi al servizio tramite Tor per postare e ricevere messaggi.
2. Una chiave pubblica di servizio onion di Tor v3 per il Token Service (ed25519) - utilizzata per acquisire token per pubblicare sul servizio tramite un piccolo esercizio di prova di lavoro (proof-of-work).
3. Una chiave pubblica di Privacy Pass - utilizzata nel processo di acquisizione token (un punto su una curva Ristretto). Vedi: [OPTR2019-01](https://openprivacy.ca/research/OPTR2019-01/)

Il pacchetto di chiavi è firmato e può essere verificato tramite la prima chiave di servizio onion v3, vincolandolo quindi a quel particolare indirizzo onion.

## Verifica dei pacchetti di chiavi

I profili che importano pacchetti di chiavi di server li verificano utilizzando il seguente algoritmo trust-on-first-use (TOFU, ovvero "fiducia al primo utilizzo"):

1. Verifica della firma allegata usando l'indirizzo onion v3 del server. (Se questo non funziona, il processo di importazione viene interrotto)
2. Verifica che tutti i tipi di chiave esistano. (Se questo non funziona, il processo di importazione viene interrotto)
3. Se il profilo ha importato il pacchetto di chiavi del server in precedenza, controllo che tutte le chiavi siano uguali. (Se questo non funziona, il processo di importazione viene interrotto)
4. Salvataggio delle chiavi nella voce di contatto dei server.

In futuro questo algoritmo sarà probabilmente modificato per consentire l'aggiunta di nuove chiavi pubbliche (ad esempio per consentire che i token siano acquisiti tramite un indirizzo Zcash).

Tecnicamente, in caso di fallimento ai passaggi (2) e (3) il server può essere considerato maligno, avendo firmato un pacchetto di chiavi valido che non è conforme alle specifiche. Quando i gruppi verranno spostati da "sperimentali" a "stabili" ciò comporterà che venga comunicato un avviso di attenzione al profilo.