---
sidebar_position: 2
---

# Formato dei messaggi

## Messaggi peer-to-peer

```
    PeerMessage {
        ID      string // A unique Message ID (primarily used for acknowledgments)
        Context string // A unique context identifier i.e. im.cwtch.chat
        Data    []byte // The context-dependent serialized data packet.
    }
```
### Identificatori di contesto

* `im.cwtch.raw` - I dati contengono un messaggio di chat in formato testo semplice (vedi gli [overlay](../ui/overlays.md) per maggiori informazioni)
* `im.cwtch.acknowledgement` - Non ci sono dati e l'ID fa riferimento a un messaggio precedentemente inviato

* `im.cwtch.getVal` e `im.cwtch.retVal` - Utilizzati per richiedere / restituire informazioni specifiche su un peer. I dati contengono rispettivamente una struttura `peerGetVal` e `peerRetVal` serializzata.

```
        peerGetVal struct {
                Scope string
                Path string
        }

        type peerRetVal struct {
                Val string // Valore serializzato dipendente dal path
                Exists bool
        }
```

## Messaggi di gruppo in formato testo semplice / decifrati

```
    type DecryptedGroupMessage struct {
        Text      string // testo semplice del messaggio
        Onion     string // l'indirizzo Cwtch del mittente 
        Timestamp uint64 // un timestamp specificato dall'utente 
        // NOTA: SignedGroupID ora è un nome improprio, l'unico modo in cui questo viene firmato è indirettamente tramite i messaggi di gruppo crittografati firmati
        // Ora trattiamo il GroupID come vincolante a un server/chiave piuttosto che a un "proprietario" - una logica di convalida aggiuntiva (ad esempio per
        // rispettare particolari costituzioni di gruppo) può essere costruita in aggiunta ai messaggi di gruppo, ma i gruppi sottostant sono
        // ora agnostici rispetto a quei modelli.
        SignedGroupID      []byte 
        PreviousMessageSig []byte // Un riferimento a un precedente messaggio
        Padding            []byte // byte casuali di lunghezza = 1800 - len(Text)
    }
```
DecryptedGroupMessage contiene una padding casuale a una dimensione fissa che è uguale alla lunghezza di tutti i campi di lunghezza fissa + 1800. Questo assicura che tutti i messaggi di gruppo cifrati siano di uguale lunghezza.

## Messaggi di gruppo cifrati
```
    // EncryptedGroupMessage fornisce una incapsulazione del messaggio di gruppo cifrato memorizzato sul server
    type EncryptedGroupMessage struct {
        Ciphertext []byte
        Signature  []byte // Firma(groupID + group.GroupServer + base64(messaggio di gruppo decifrato)) usando le chiavi Cwtch dei mittenti
    }
```
Per calcolare la firma è necessario conoscere il groupID del messaggio, il server al quale il gruppo è associatoe il messaggio di gruppo decifrato (e quindi la chiave di gruppo). È firmato (ed25519) dal mittente del messaggio, e può essere verificato utilizzando la chiave di indirizzo Cwtch pubblica.
