---
sidebar_position: 4
---

# Gruppi

Nella maggior parte dei casi, il modello di rischio Cwtch per i gruppi è suddiviso in due profili distinti:

* Gruppi composti da partecipanti che hanno reciproca fiducia, in cui si assume che i peer siano onesti.
* Gruppi costituiti da sconosciuti, in cui si presume che i peer possano essere potenzialmente maligni.

La maggior parte delle mitigazioni descritte in questa sezione si riferisce a quest'ultimo caso, ma naturalmente interessa anche il primo. Anche nel caso in cui presunti onesti peer in seguito si rivelino maligni, ci sono meccanismi in grado di rilevarne il comportamento maligno e impedire che si ripeta in futuro.

## Panoramica dei rischi: derivazione chiave

Idealmente utilizzeremmo un protocollo come OTR, le limitazioni che ci impediscono di farlo al momento sono:

* Non si può garantire che i messaggi offline raggiungano tutti i peer, e di conseguenza gli eventuali metadati legati al materiale della chiave potrebbero andare perduti. Abbiamo bisogno di un processo di derivazione della chiave che sia resistente a messaggi mancanti o a trasmissione incompleta.

## Rischio: Peer malintenzionato lascia trapelare la chiave di gruppo e/o la conversazione

**Stato: Parzialmente mitigato (ma impossibile da mitigare completamente)**

Sia che si tratti di piccoli gruppi fidati che di gruppi più grandi parzialmente pubblici c'è *sempre* la possibilità che un utente malintenzionato faccia trapelare i messaggi del gruppo.

Abbiamo intenzione di rendere più facile per i peer il [fork](#fork) dei gruppi per mitigare il fatto che la stessa chiave venga utilizzata per crittografare parecchie informazioni sensibili e fornire un minimo livello di "forward secrecy" per le conversazioni di gruppo passate.

## Rischio: Attacchi attivi da membri del gruppo

**Stato: Parzialmente mitigato**

Membri del gruppo, che hanno accesso al materiale della chiave del gruppo, possono cospirare con un server o altri membri del gruppo per interrompere la coerenza della trascrizione.

Mentre non possiamo impedire direttamente che avvenga censura, dato questo tipo di collusione attiva, disponiamo di una serie di meccanismi che dovrebbero rivelare la presenza di censura ai membri onesti del gruppo.

### Mitigazioni:

* Poiché ogni messaggio è firmato dalla chiave pubblica dei peer, non dovrebbe essere possibile (nell'ambito delle assunzioni crittografiche della crittografia sottostante) che un membro del gruppo possa imitarne un altro.
* Ogni messaggio contiene un esclusivo elemento di identificazione derivato dai contenuti e dall'hash del messaggio precedente, rendendo impossibile per i collaboratori malintenzionati includere messaggi di membri non collusi senza rivelare una catena di messaggi implicita (che, se stessero tentando di censurare altri messaggi, rivelerebbe tale censura)

Infine: stiamo lavorando attivamente per aggiungere il non ripudio ai server Cwtch in modo che essi stessi siano limitati in ciò che possono censurare in modo efficiente.
