# Server Cwtch

L'obiettivo del protocollo Cwtch è quello di abilitare la comunicazione di gruppo attraverso **Infrastruttura non attendibile**.

A differenza degli schemi basati su relay in cui i gruppi assegnano un leader, un insieme di leader, o un server di terze parti fidato per garantire che ogni membro del gruppo  possa inviare e ricevere messaggi in modo tempestivo (anche se i membri sono offline) - un'infrastruttura non affidabile ha l'obiettivo di realizzare tali proprietà senza l'assunzione di fiducia.

Il documento seminale di Cwtch ha definito una serie di proprietà che ci si aspetta i server Cwtch forniscano:

*  Il server Cwtch può essere utilizzato da più gruppi o solo da uno.
* Un server Cwtch, non dovrebbe mai essere in grado di apprendere l'identità dei partecipanti a un gruppo senza la collaborazione di un membro del gruppo.
* Un server Cwtch non dovrebbe mai essere in grado di apprendere il contenuto di alcuna comunicazione.
* Un server Cwtch non dovrebbe mai essere in grado di distinguere il gruppo di appartenenza dei messaggi.

Notiamo qui che queste proprietà sono un sovrainsieme degli obiettivi di progettazione delle "Private Information Retrieval structures" (strutture di recupero di informazioni private).

## Server Maligni

Ci aspettiamo la presenza di entità maligne all'interno dell'ecosistema Cwtch.

Diamo inoltre la priorità alla decentralizzazione e all'ingresso senza permesso nell'ecosistema e quindi non facciamo alcuna dichiarazione di sicurezza basata su:

* Qualsiasi ipotesi di non collusione tra un insieme di server Cwtch
* Eventuali processi di verifica definiti da terze parti

I peer sono incoraggiati a creare e gestire essi stessi server Cwtch dove possono garantire proprietà più efficienti rilassando le ipotesi di fiducia e sicurezza - tuttavia a priori progettiamo il protocollo per essere sicuro senza queste ipotesi - sacrificando l'efficienza se necessario.

### Errori Rilevabili

* Se un server Cwtch non riesce a trasmettere un messaggio specifico a un sottoinsieme di membri del gruppo, ci sarà una lacuna rilevabile nell'albero dei messaggi di alcuni peer che può essere scoperta attraverso il gossip peer-to-peer.
* Un server Cwtch non può modificare alcun messaggio senza il materiale chiave noto al gruppo (qualsiasi tentativo di farlo per un sottoinsieme di membri del gruppo comporterà un comportamento identico al mancato inoltro di un messaggio).
* Mentre un server *può* duplicare i messaggi, questi non avranno alcun impatto sull'albero dei messaggi di gruppo (grazie a cifratura, nonces e identità del messaggio) - la sorgente della duplicazione non è nota a un peer.

## Efficienza

Al momento dalla scrittura, solo 1 protocollo è noto per il raggiungimento delle proprietà desiderate, il semplice Private Information Retrieval (PIR, ovvero 'recupero di informazioni private'), o "il server invia tutto, e i peer lo esaminano".

Questo ha un ovvio impatto sull'efficienza della larghezza di banda, soprattutto per i peer che utilizzano dispositivi mobili, e di conseguenza stiamo sviluppando attivamente nuovi protocolli in cui le garanzie di privacy ed efficienza possano essere negoziate in modi diversi.

Al momento della scrittura, i server consentono sia il download completo di tutti i messaggi memorizzati, sia la richiesta di scaricare i messaggi a partire da un dato messaggio specificato.

Tutti i peer quando entrano a far parte di un gruppo su un nuovo server scaricano per la prima volta tutti i messaggi dal server, e da quel momento in poi scaricano solo nuovi messaggi.

*Nota*: Questo comportamento consente una lieve forma di analisi dei metadati. Il server può inviare[???] nuovi messaggi per ciascun profilo univoco sospetto e quindi utilizzare queste firme di messaggio univoche per tenere traccia delle sessioni univoche nel tempo (tramite richieste di nuovi messaggi).

Ciò è mitigato da due fattori di confusione:

1. I profili possono aggiornare le loro connessioni in qualsiasi momento - il che risulta in una nuova sessione sul server.
2. I profili possono "risincronizzare" da un server in qualsiasi momento - con conseguente nuova richiesta di download di tutti i messaggi. Lo scenario più comune per questo comportamento è il recupero di messaggi più vecchi da un gruppo.

In combinazione, queste 2 mitigazioni pongono limiti su ciò che il server è in grado di dedurre, tuttavia non possiamo ancora fornire piena resistenza ai metadati.

Per le possibili soluzioni future a questo problema si veda [Niwl](https://git.openprivacy.ca/openprivacy/niwl)

# Proteggere il server da peer maligni

Il rischio principale per i server si presenta sotto forma di spam generato dai peer. Nel prototipo di Cwtch era stato messo in atto un meccanismo di spamguard che richiede ai peer di condurre alcune arbitrarie "prove di lavoro" (proof of work) dato un parametro specificato dal server.

Questa non è una soluzione robusta in presenza di un avversario determinato e dotato di una quantità significativa di risorse, e quindi uno dei principali rischi esterni al sistema Cwtch diventa la censura dovuta all’esaurimento delle risorse.

Abbiamo delineato una potenziale soluzione a questo problema in [servizi basati su token](https://openprivacy.ca/research/OPTR2019-01/) ma si noti che questo richiede anche ulteriore sviluppo.
