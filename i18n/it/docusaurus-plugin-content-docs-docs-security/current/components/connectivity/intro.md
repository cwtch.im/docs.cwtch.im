---
sidebar_position: 3
---


# Connettività

Cwtch fa uso di servizi onion di Tor (v3) per tutte le comunicazioni tra nodi.

Forniamo il pacchetto [openprivacy/connettività](https://git.openprivacy.ca/openprivacy/connectivity) per gestire il demone Tor e impostare e distruggere i servizi onion tramite Tor.

## Rischi Noti

### Esposizione della chiave privata al processo Tor

**Stato: Mitigazione parziale** (Per lo sfruttamento sono necessari l'accesso fisico o l'escalation dei privilegi)

Dobbiamo passare alla libreria di connettività (e quindi al processo Tor ) la chiave privata di qualsiasi servizio onion che desideriamo configurare, tramite l'interfaccia `Listen` . Questa è una delle aree più critiche che rimangono fuori dal nostro controllo. Qualsiasi associazione a un processo o a un binario Tor maligno comporterà la compromissione della chiave privata del servizio onion.

### Mitigazioni

La connettività tenta per default di collegarsi al processo Tor fornito dal sistema, *solo* quando è stato reso disponibile con un token di autenticazione.

Altrimenti la connettività tenta sempre di lanciare un proprio processo Tor utilizzando un file binario di nota buona qualità impacchettato internamente al sistema (al di fuori dell'ambito di applicazione del pacchetto connettività)

A lungo termine speriamo che diventi disponibile una libreria integrata che consenta la gestione diretta attraverso un'interfaccia in-process per impedire che la chiave privata esca dal confine del processo (o altre alternative che ci consentano di mantenere il pieno controllo sulla chiave privata in memoria.)

### Gestione processi Tor

**Stato: Mitigazione parziale** (Per lo sfruttamento sono necessari l'accesso fisico o l'escalation dei privilegi)

Molti problemi possono sorgere dalla gestione di un processo separato, inclusa la necessità di riavviare, uscire o in generale garantire una gestione adeguata.

L'interfaccia [ACN](https://git.openprivacy.ca/openprivacy/connectivity/src/branch/master/acn.go) fornisce le interfacce `Riavvia`, `Chiudi` e `GetBootstrapStatus` per consentire alle applicazioni di gestire il processo Tor sottostante. In aggiunta, il metodo `SetStatusCallback` può essere utilizzato per consentire ad un'applicazione di essere notificata quando lo stato del processo Tor cambia.

Tuttavia, nel caso in cui utenti con privilegi sufficienti desiderino interferire con questo meccanismo, [......], e come tale il processo Tor è una componente con cui interagire più fragile delle altre.

## Stato dei test

La connettività attuale ha limitata capacità di test unitari e nessuno di questi viene eseguito durante richieste di pull o merge. Non ci sono test di integrazione.

Vale la pena notare che la connettività è utilizzata sia da Tapir che da Cwtch nei loro test di integrazione (e quindi nonostante la mancanza di test a livello di pacchetto, è esposta alle condizioni di test a livello di sistema)

