# Overlay dei messaggi

[Adattato da: Discreet Log #8: Note sull'API della chat di Cwtch](https://openprivacy.ca/discreet-log/08-chatapi/)

**Nota: questa sezione tratta i protocolli overlay che esistono in aggiunta al protocollo Cwtch. Per informazioni sui messaggi del protocollo Cwtch si prega di consultare [Formati messaggio](../cwtch/message_formats.md)**

Immaginiamo Cwtch come una piattaforma che fornisca un livello di trasporto autenticato ad applicazioni di livello superiore. Gli sviluppatori sono liberi di fare le proprie scelte su quali protocolli a livello di applicazione utilizzare, che desiderino formati di messaggi binari su misura o semplicemente vogliano incollarci sopra una libreria HTTP e accontentarsi. Cwtch può generare pe te nuove coppie di chiavi (che diventano indirizzi onion; nessuna necessità di registrazioni DNS!) ed è CERTO che tutti i dati che la tua applicazione riceve dalla rete (di comunicazione anonima) sono già stati autenticati.

Per il nostro stack attuale, i messaggi sono racchiusi in un frame JSON minimo che aggiunge alcune informazioni contestuali sul tipo di messaggio. E poiché gli oggetti JSON serializzati sono solo dizionari, possiamo facilmente aggiungere più metadati in seguito se necessario.


## Overlay, liste e bollettini di chat

L'originale versione alfa di Cwtch aveva un demo degli "overlay": diversi modi di interpretare lo stesso canale di dati, a seconda della struttura dei dati atomici stessi. Abbiamo incluso semplici liste di controllo e annunci [BBS/classificati???] come overlay che potrebbero essere visualizzati e condivisi con qualsiasi contatto Cwtch, che sia un singolo peer o un gruppo. Il formato del filo era come segue:

```
{o:1,d:"hey there!"}
{o:2,d:"bread",l:"groceries"}
{o:3,d:"garage sale",p:"[parent message signature]"}
```

Il campo overlay `o` determinava se si trattasse di un messaggio di chat (1), un elenco (2) o un bollettino (3). Il campo di dati `d` è soggetto a overload, e liste/bollettini hanno bisogno di ulteriori informazioni sul gruppo/post a cui appartengono. (Utilizziamo le firme dei messaggi al posto degli ID per evitare problemi nell'ordinare i messaggi e con ID creati in modo dannoso. Questo è anche il modo in cui il protocollo Cwtch comunica al front-end quale messaggio viene riconosciuto.)

## Struttura dati

L'implementazione di dati strutturati ad albero su un archivio di messaggi sequenziale comporta evidenti svantaggi in termini di prestazioni. Ad esempio, si consideri la visualizzazione dei messaggi, che carica prima i messaggi più recenti e cerca all'indietro solo quanto basta per recuperare abbastanza messaggi da riempire la finestra corrente, paragonata ad un forum (abbastanza patologico) in cui quasi ogni messaggio è figlio del il primissimo messaggio della storia, che potrebbe essere avvenuto gigabyte e gigabyte di dati fa. Se l'interfaccia utente visualizza solo i post di primo livello fino a quando l'utente li espande, dobbiamo analizzare l'intera cronologia prima di ottenere abbastanza informazioni per visualizzare qualsiasi cosa.

Un altro problema è che la multiplazione di tutti questi overlay in un unico archivio di dati crea "buchi" nei dati che confondono le [visualizzazioni a elenco caricate col metodo lazy](https://api.flutter.dev/flutter/widgets/ListView/ListView.builder.html) e le barre di scorrimento. Il conteggio dei messaggi può indicare che ci sono molte più informazioni da visualizzare se l'utente si limita a scorrere, ma quando vengono effettivamente recuperate e analizzate potrebbe essere il caso che nessuna di queste sia rilevante per l'overlay corrente.

Nessuno di questi problemi è insormontabile, ma dimostrano una falla nelle nostre ipotesi iniziali circa la natura dei flussi di messaggi in collaborazione e come dovremmo gestire tali dati.

# Tipi di overlay

Come indicato sopra, gli overlay sono specificati in un formato JSON molto semplice con la seguente struttura:
```
    type ChatMessage struct {
        O int    `json:"o"`
        D string `json:"d"`
    }
```
Dove O sta per `Overlay`, e gli overlay attualmente supportati sono documentati come segue:
```
    1: i dati sono una stringa di chat
    2: i dati sono in stato di lista / hanno codifica delta
    3: i dati sono in stato di bollettino / hanno codifica delta
    100: suggerimento di contatto; i dati sono l'indirizzo onion di un peer
    101: suggerimento di contatto; i dati sono una stringa di invito a un gruppo
```
## Messaggi di chat (Overlay 1)

Il più semplice overlay è un messaggio di chat che contiene semplicemente informazioni grezze, non elaborate.

```
{o:1,d:"hai preso il latte?"}
```

## Inviti (Overlay 100 e 101)

Invece di ricevere l'invito come richiesta di contatto in entrata a livello di profilo, nuovi inviti vengono condivisi con un particolare contatto/gruppo in linea, dove possono essere visualizzati e/o accettati in seguito, anche se inizialmente sono stati rifiutati (potenzialmente per sbaglio).

Il formato del wire per questi è altrettanto semplice:

```
{o:100,d:"u4ypg7yyyrrvf2aceeclq5dgwtkirzletltbqofnb6km7u542qqk4jyd"}
{o:101,d:"torv3eyJHcm91cElEIjoiOWY3MWExYmFhNDkzNTAzMzAyZDFmODRhMzI2ODY2OWUiLCJHcm91cE5hbWUiOiI5ZjcxYTFiYWE0OTM1MDMzMDJkMWY4NGEzMjY4NjY5ZSIsIlNpZ25lZEdyb3VwSUQiOiJyVGY0dlJKRkQ2LzFDZjFwb2JQR0xHYzdMNXBKTGJTelBLRnRvc3lvWkx6R2ZUd2Jld0phWllLUWR5SGNqcnlmdXVRcjk3ckJ2RE9od0NpYndKbCtCZz09IiwiVGltZXN0YW1wIjowLCJTaGFyZWRLZXkiOiJmZVVVQS9OaEM3bHNzSE9lSm5zdDVjNFRBYThvMVJVOStPall2UzI1WUpJPSIsIlNlcnZlckhvc3QiOiJ1cjMzZWRid3ZiZXZjbHM1dWU2anBrb3ViZHB0Z2tnbDViZWR6ZnlhdTJpYmY1Mjc2bHlwNHVpZCJ9"}
```

Ciò rappresenta un allontanamento dal nostro pensiero originale sugli "overlay" verso una rappresentazione più orientata all'azione. L'"overlay" della chat può comunicare che qualcuno *abbia fatto* qualcosa, anche se alla fine si traduce in "un elemento è stato aggiunto a un elenco", e gli elenchi, i bollettini e altri meravigliosamente caotici tipi di dati possono avere il loro stato precalcolato e archiviato separatamente.

## Liste / Bollettini

**Nota: si prevede vengano definiti in Cwtch Beta 1.5**
