# Anteprime immagini

Basate sulla condivisione di file in Cwtch 1.3, le anteprime immagini sono legate all'estensione suggerita dal nome del file (e no, non siamo interessati a utilizzare tipi MIME o numeri magici) e alle dimensioni riportate. Se abilitate, il sistema di anteprima scaricherà automaticamente le immagini condivise in una cartella di download configurata e le mostrerà come parte del messaggio stesso. (A causa delle limitazioni su Android, andranno invece in pratica alla cache di archiviazione privata dell'applicazione, e vi sarà data la possibilità di salvarle altrove in seguito.) Il limite di dimensione file è ancora da determinarsi, ma sarà ovviamente molto più basso del limite dimensionale complessivo della condivisione file, che è attualmente 10 gigabyte.

Per ora, supportiamo solo messaggi con singola immagine, e qualsiasi modifica/ritaglio di immagine dovrà essere fatta in un'applicazione separata. Come menzionato nelle domande frequenti sulla condivisione di file, anche i file di immagine contengono spesso metadati nascosti significativi e bisognerebbe condividerli solo con persone fidate.

## Rischi Noti

## Altre applicazioni e/o il sistema operativo che ricavano informazioni dalle immagini

Le immagini devono essere archiviate da qualche parte e per ora abbiamo scelto di archiviarle nel sistema di file non crittografate. Questo per 2 ragioni:

1. Per supportare schemi di condivisione di file più sofisticati come il rehosting, abbiamo bisogno della capacità di scansionare i file in modo efficiente e consegnarne blocchi: farlo attraverso un livello di database crittografato danneggerebbe le prestazioni.
2. Queste informazioni devono in ogni caso sempre transitare all'esterno dell'applicazione (tramite driver video o archiviando e visualizzando il file in un'applicazione esterna): non c'è nulla che Cwtch possa fare a quel punto.

## Immagini dannose che bloccano o compromettono in altri modi Cwtch

Flutter utilizza Skia per il rendering delle immagini. Mentre il codice sottostante non è sicuro a livello di memoria, è [ampiamente sottoposto a fuzzing](https://github.com/google/skia/tree/main/fuzz) come parte del normale sviluppo.

Eseguiamo anche i nostri propri test di fuzzing dei componenti Cwtch. In tale analisi abbiamo trovato un singolo bug di crash relativo a un file GIF malformato che faceva sì che il renderer allocasse una quantità ridicola di memoria (e alla fine venisse rifiutato dal kernel). Per evitare che ciò influisca su Cwtch, abbiamo adottato la politica di abilitare sempre un massimo di `cacheWidth` e/o `cacheHeight` per i widget immagine.

## Immagini dannose il cui rendering risulta diverso su piattaforme diverse, esponendo potenzialmente i metadati

Recentemente [è stato trovato un bug nel parser png di Apple](https://www.da.vidbuchanan.co.uk/widgets/pngdiff/) che causerebbe un rendering di immagine diverso su dispositivi Apple rispetto a dispositivi non Apple.

Abbiamo condotto alcuni test sulle nostre build Mac e non è stato possibile replicare questo problema per Flutter (perché tutte le build Flutter usano Skia per il rendering), tuttavia continueremo a includere tali casi nel nostro corpus di testing.

Per ora le anteprime immagini rimarranno sperimentali e opzionali.

