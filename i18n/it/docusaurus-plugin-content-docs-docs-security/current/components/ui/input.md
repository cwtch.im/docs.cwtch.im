# Input


## Rischio: intercettazione di contenuti o metadati di Cwtch tramite un IME su dispositivi mobili

**Stato: Parzialmente mitigato**

Qualsiasi componente che possa in teoria intercettare i dati tra una persona e l'applicazione Cwtch è un potenziale rischio di sicurezza.

Uno dei più probabili intercettatori è un IME di terze parti (Input Method Editor) comunemente usato dalle persone per generare caratteri non supportati nativamente dal loro dispositivo.

Anche le app IME benigne e di serie possono far trapelare involontariamente informazioni sul contenuto di un messaggio personale, ad esempio tramite sincronizzazione cloud, traduzione cloud o dizionari personali.

In definitiva, questo problema non può essere risolto solo da Cwtch e rappresenta un rischio più ampio che incide sull’intero ecosistema mobile.

Un rischio simile esiste sul desktop attraverso l'uso di simili applicazioni di input (oltre ai software keylogger), tuttavia lo consideriamo completamente al di fuori dell'ambito della valutazione del rischio di Cwtch (in linea con altri attacchi direttamente alla sicurezza del sistema operativo sottostante).

Questo è parzialmente mitigato in Cwtch 1.2 attraverso l'uso di `enableIMEPersonalizedLearning: false`. Si veda [questa PR](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/142) per maggiori informazioni.