---
sidebar_position: 1
---

# Fondamenti Tecnici di Cwtch

Questa pagina presenta una breve panoramica tecnica del protocollo Cwtch.

## Un profilo Cwtch

Gli utenti possono creare uno o più profili Cwtch. Ogni profilo genera una coppia casuale di chiavi ed25519 compatibile con Tor.

Oltre al materiale crittografico, un profilo contiene anche un elenco di contatti (altre chiavi pubbliche di profili Cwtch + dati associati a quei profili come nickname e (facoltativamente) la sotria dei messaggi), un elenco di gruppi (contenente il materiale crittografico del gruppo oltre ad altri dati associati, come il nickname del gruppo e la storia dei messaggi).

## Conversazioni a 2 parti: Peer to Peer

![](/img/BASE_3.png)

Affinché 2 parti intraprendano una conversazione peer-to-peer entrambe devono essere online, ma solo una deve essere raggiungibile tramite il proprio servizio onion. Per motivi di chiarezza spesso etichettiamo una parte come "peer in entrata" (quello che ospita il servizio onion) e l'altra parte come "peer in uscita" (quello che si collega al servizio onion).

Dopo la connessione, entrambe le parti effettuano un protocollo di autenticazione che:

* Si assicura che ogni parte abbia accesso alla chiave privata associata alla propria identità pubblica.
* Genera una chiave di sessione effimera usata per cifrare tutte le comunicazioni successive durante la sessione.

Questo scambio (documentato in maggior dettaglio nella sezione sul [protocollo di autenticazione](tapir/authentication_protocol.md)) è *negabile offline*, ovvero è possibile per qualsiasi parte falsificare le trascrizioni di questo protocollo di scambio dopo il fatto e si conseguenza, dopo il fatto, è impossibile dimostrare con certezza che lo scambio sia avvenuto.

Una volta eseguito il protocollo di autenticazione, le due parti possono scambiarsi messaggi liberamente tra loro.

## Conversazioni a più parti: comunicazione di gruppo e comunicazione Peer to Server

**Nota: la comunicazione di gruppo resistente ai metadati è ancora un'area di ricerca attiva e ciò che è documentato qui probabilmente cambierà in futuro.**

Quando una persona vuole iniziare una conversazione di gruppo, in primo luogo genera casualmente una `Chiave di gruppo` segreta. Tutte le comunicazioni del gruppo verranno cifrate utilizzando questa chiave.

Insieme alla `Chiave di gruppo`, chi ha creato il gruppo decide anche il **Server Cwtch** da utilizzare come host del gruppo. Per ulteriori informazioni su come i server si autenticano a vicenda si veda la sezione sui [pacchetti di chiavi](cwtch/key_bundles.md).

Un `Identificatore di gruppo` è generato utilizzando la chiave di gruppo e il server di gruppo e questi tre elementi sono raggruppati in un invito che può essere inviato ai potenziali membri del gruppo (per esempio tramite connessioni peer-to-peer esistenti).

Per inviare un messaggio al gruppo, un profilo si collega al server che ospita il gruppo (vedasi sotto), e crittografa il proprio messaggio con la `Chiave di gruppo` e genera una firma crittografica sotto l'`Identificatore di gruppo`, il `Server di gruppo` e il messaggio decriptato (si veda [formati di trasmissione](cwtch/message_formats.md) per ulteriori informazioni).

Per ricevere un messaggio dal gruppo, un profilo deve essere collegato al server che ospita il gruppo e scaricare *tutti* i messaggi (dall'ultima connessione precedente). I profili tentano quindi di decifrare ogni messaggio utilizzando la `Chiave di gruppo` e in caso di successo cercano di verificare la firma (si veda [Server di Cwtch](cwtch/server.md), [Gruppi di Cwtch](./cwtch/groups.md) per una panoramica degli attacchi e delle mitigazioni).

### I server sono peer

Per molti aspetti la comunicazione con un server è identica alla comunicazione con un normale peer Cwtch; tutti i passaggi descritti sopra vengono eseguiti allo stesso modo, tuttavia il server agisce sempre come peer in entrata e il peer in uscita utilizza sempre **coppia di chiavi effimere** appena generata come la propria "identità a lungo termine".

Di conseguenza, le conversazioni peer-server differiscono solo nei *tipi* di messaggi inviati tra le due parti, con il server che inoltra tutti i messaggi che riceve e consente anche a qualsiasi client di sottoporre query dei messaggi più vecchi.

