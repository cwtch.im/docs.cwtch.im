---
sidebar_position: 1.5
---

# Panoramica dell'ecosistema dei componenti

Cwtch è composto da diverse librerie di componenti più piccole. Questo capitolo fornirà una breve panoramica di ciascun componente e di come si collega al più ampio ecosistema Cwtch.

## [openprivacy/connettività](https://git.openprivacy.ca/openprivacy/connectivity)

Sommario: una libreria che fornisce un'astrazione di rete ACN (Anonymous Communication Network).

L'obiettivo della connettività è quello di astrarre le librerie/software sottostanti necessari per comunicare con una specifica ACN. Al momento supportiamo solo Tor e quindi il compito della connettività è di:

* Avviare e interrompere il processo Tor
* Fornire la configurazione al processo Tor
* Consentire connessioni grezze agli endpoint tramite il processo Tor (ad esempio connessione ai servizi onion)
* Fornire l'hosting degli endpoint tramite il processo Tor (ad esempio hosting di servizi onion)
* Fornire aggiornamenti di stato sul processo Tor sottostante

Per ulteriori informazioni si veda [connettività](/security/components/connectivity/intro)

## [cwtch.im/tapir](https://git.openprivacy.ca/cwtch.im/tapir)

Sommario: Tapir è una piccola libreria per costruire applicazioni p2p su sistemi di comunicazione anonimi.

L'obiettivo di Tapir è quello di astrarre **applicazioni** su una particolare ACN. Tapir supporta:

* Creazione di un'identità crittografica (comprese identità effimere)
* Mantenimento di un pool di connessioni in entrata e in uscita ai servizi
* Gestione di vari livelli di applicazioni, tra cui trascrizioni crittografiche, [protocolli di autenticazione e di autorizzazione](/security/components/tapir/authentication_protocol) e [servizi basati su token tramite PrivacyPass](https://openprivacy.ca/research/OPTR2019-01/)

Per ulteriori informazioni si veda [tapir](/security/components/tapir/authentication_protocol)

## [cwtch.im/cwtch](https://git.openprivacy.ca/cwtch.im/cwtch)

Sommario: Cwtch è la libreria principale per implementare il protocollo / sistema Cwtch.

L'obiettivo di Cwtch è fornire implementazioni per applicazioni specifiche di Cwtch, ad esempio invio di messaggi, gruppi e condivisione di file (implementati come applicazioni Tapir), fornire interfacce per la gestione e l'archiviazione dei profili Cwtch, fornire un bus di eventi per la suddivisione dei sottosistemi e la creazione di plug-in con nuove funzionalità, oltre a gestire altre funzionalità di base.

La libreria Cwtch è anche responsabile del mantenimento delle rappresentazioni dei modelli canonici per i formati wire e gli overlay.


## [cwtch.im/libcwtch-go](https://git.openprivacy.ca/cwtch.im/libcwtch-go)

Riepilogo: libcwtch-go fornisce binding di C (incluso Android) per Cwtch per l'uso nelle implementazioni di interfaccia utente.

L'obiettivo di libcwtch-go è colmare il divario tra la libreria backend Cwtch e qualsiasi sistema frontend che possa essere scritto in un linguaggio diverso.

L'API fornita da libcwtch è molto più ristretta di quella fornita direttamente da Cwtch, ogni API libcwtch tipicamente impacchetta diverse chiamate a Cwtch.

libcwtch-go è anche responsabile della gestione delle impostazioni dell'interfaccia utente e del gating sperimentale. È anche spesso usata come terreno di staging per funzioni sperimentali e codice che potrebbe eventualmente finire adessere parte di Cwtch.

## [cwtch-ui](https://git.openprivacy.ca/cwtch.im/cwtch-ui)

Sommario: un'interfaccia utente per Cwtch basata su flutter.

Cwtch utilizza libcwtch-go per fornire una interfaccia utente completa per Cwtch, consentendo alle persone di creare e gestire profili, aggiungere contatti e gruppi, mandare messaggi ad altre persone, condividere file (prossimamente) e altro ancora.

L'interfaccia utente è inoltre responsabile della gestione della localizzazione e delle traduzioni.

Per ulteriori informazioni si veda [Interfaccia utente Cwtch](/security/category/cwtch-ui)

## Componenti ausiliari

Occasionalmente, Open Privacy estrae parti di Cwtch come librerie autonome che non sono specifiche di Cwtch. Queste sono brevemente riassunte qui:

### [openprivacy/log](https://git.openprivacy.ca/openprivacy/log)

Un framework di logging specifico per Open Privacy che viene utilizzato in tutti i pacchetti Cwtch.