---
sidebar_position: 2
---

# Modello di rischio

È noto che i metadati delle comunicazioni vengono sfruttati da vari avversari con lo scopo di minare la sicurezza dei sistemi, rintracciare le vittime, e condurre analisi dei social network su larga scala per perpetrare operazioni di sorveglianza di massa. Gli strumenti resistenti ai metadati sono nella loro infanzia e la ricerca sulla costruzione e sull'esperienza utente di tali strumenti è carente.

![](/img/4.png)

Cwtch è stato originariamente concepito come un'estensione del protocollo resistente ai metadati Ricochet per supportare comunicazioni di gruppo asincrone e multi-peer attraverso l'uso di un'infrastruttura anonima, non affidabile e potenzialmente usa e getta.

Da allora, Cwtch si è evoluto in un protocollo a sé stante, questa sezione delineerà i vari rischi noti che Cwtch tenta di mitigare e sarà ampiamente citata nel resto del documento quando si discuteranno i vari sottocomponenti dell'architettura di Cwtch.

## Modello delle minacce

È importante riconoscere e comprendere che i metadati sono onnipresenti nei protocolli di comunicazione, e sono effettivamente necessari perché tali protocolli funzionino in modo efficiente e su larga scala. Tuttavia, le informazioni utili a facilitare peer e server sono anche molto importanti per gli avversari che desiderano sfruttarle.

Per la definizione del nostro problema, assumeremo che il contenuto di una comunicazione sia crittografato in un modo che un avversario non possa essere in grado praticamente di violare (vedi [tapir](/security/category/tapir) e [cwtch](security/category/cwtch) per i dettagli su la crittografia che utilizziamo), e quindi ci concentreremo sul contesto dei metadati di comunicazione.

Cerchiamo di proteggere i seguenti contesti di comunicazione:

* Chi è parte di una comunicazione? Può essere possibile identificare persone o semplicemente dispositivi o identificatori di rete. Ad esempio, “questa comunicazione coinvolge Alice, giornalista, e Bob, dipendente del governo”.
* Dove sono i partecipanti alla conversazione? Ad esempio, “durante questa comunicazione Alice era in Francia e Bob era in Canada.”
* Quando ha avuto luogo una conversazione? La tempistica e la lunghezza della comunicazione possono rivelare parecchio sulla natura di una chiamata, ad esempio, “Bob, dipendente del governo, ha parlato al telefono con Alice per un’ora ieri sera. Questa è la prima volta che comunicano“. *Come è stata mediata la conversazione? Il fatto che una conversazione abbia avuto luogo attraverso un'email cifrata o non cifrata può fornire informazioni preziose. Ad esempio, “Alice ha inviato una e-mail crittografata a Bob ieri, mentre di solito si scambiano solo messaggi di testo in chiaro”.
* Di cosa tratta la conversazione? Anche se il contenuto della comunicazione è crittografato, a volte è possibile ricavare un probabile contesto di una conversazione senza sapere esattamente cosa viene detto, ad esempio "una persona ha chiamato una pizzeria all'ora di cena" o "qualcuno ha chiamato il numero di una nota hotline per suicidi alle 3 del mattino".

Al di là di conversazioni singole, cerchiamo anche di difenderci dagli attacchi di correlazione di contesto, dove più conversazioni vengono analizzate assieme per ricavare informazioni di livello superiore:

* Relazioni: scoperta delle relazioni sociali tra una coppia di entità analizzando la frequenza e la durata delle loro comunicazioni in un periodo di tempo. Ad esempio Carol ed Eva si chiamano ogni singolo giorno per più ore alla volta.
* Cricche: scoperta delle relazioni sociali tra un gruppo di entità che interagiscono tra loro. Ad esempio Alice, Bob ed Eva comunicano tuttɜ tra di loro.
* Cricche vagamente connesse e individui che fanno da ponte: scoperta di gruppi che comunicano tra loro attraverso intermediari analizzando le catene di comunicazione (ad esempio, ogni volta che Alice parla con Bob, parla con Carol quasi immediatamente dopo; Bob e Carol non comunicano mai).
* Modello di vita: scoperta di quali comunicazioni siano cicliche e prevedibili. Ad esempio Alice chiama Eva ogni lunedì sera per circa un'ora.


### Attacchi attivi

#### Attacchi di falsa dichiarazione

Cwtch non fornisce un registro globale dei nomi visualizzati e di conseguenza le persone che utilizzano Cwtch sono più vulnerabili agli attacchi basati su false dichiarazioni, ovvero persone che fingono di essere altre persone:

Un flusso base di un atacco di questo tipo è il seguente (sebbene esistano anche altri flussi):

- Alice ha come amici Bob ed Eva
- Eva scopre che Alice ha un amico di nome Bob
- Eva crea migliaia di nuovi account per forgiarne uno che abbia un'immagine / chiave pubblica simile a quello di Bob (non sarà identico, ma potrebbe ingannare qualcuno per qualche minuto)
- Eva chiama questo nuovo account "Nuovo account di Eva" e aggiunge Alice agli amici.
- Eva cambia poi il suo nome su "Eva New Account" a "Bob"
- Alice invia messaggi destinati a "Bob" al falso account Bob di Eva

Poiché gli attacchi di falsa dichiarazione hanno a che fare intrinsecamente con la fiducia e la verifica, l'unico modo assoluto per prevenirli è che gli utenti convalidino la chiave pubblica in maniera assoluta. Questo ovviamente non è ideale e in molti casi semplicemente *non accadrà*.

Pertanto, miriamo a fornire alcuni suggerimenti sull'esperienza utente nella sezione sull'[interfaccia utente](/security/category/cwtch-ui) per guidare le persone nel decidere se fidarsi degli account e/o distinguere gli account che potrebbero tentare di presentarsi come altri utenti.

## Una nota sugli attacchi fisici

Cwtch non considera gli attacchi che richiedono l'accesso fisico (o equivalente) alla macchina degli utenti come difendibili in pratica. Tuttavia, nell'interesse della buona ingegneria della sicurezza, in questo documento faremo comunque riferimento agli attacchi o alle condizioni che richiedono tale privilegio e indicheremo dove le eventuali misure di mitigazione da noi messe in atto falliranno.