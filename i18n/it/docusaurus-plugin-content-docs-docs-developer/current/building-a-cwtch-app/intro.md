---
sidebar_position: 1
---

# Come iniziare

## Scegliere una libreria di Cwtch

### Cwtch Go Lib

La libreria ufficiale di Cwtch è scritta in Go e si trova su [https://git.openprivacy.ca/cwtch.im/cwtch](https://git.openprivacy.ca/cwtch.im/cwtch). Questa libreria consente l'accesso a tutte le funzionalità Cwtch.

### CwtchBot

Forniamo anche un framework specializzato per bot di Cwtch, in Go, che fornisce un approccio più agile e su misura per costruire bot di chat. Per un'introduzione alla costruzione di chatbots con il framework CwtchBot si veda il tutorial su come [implementare un echobot](/developing/building-a-cwtch-app/building-an-echobot#using-cwtchbot-go).

### Autobinding (binding di C)

I [binding di C ufficiali per Cwtch](https://git.openprivacy.ca/cwtch.im/autobindings) vengono generati automaticamente dalla libreria Go di Cwtch. L'API è limitata rispetto all'accesso diretto alla libreria Go di Cwtch, ed è esplicitamente adattata specificatamente alla costruzione dell'interfaccia utente di Cwtch.

### libCwtch-rs (Rust)

Una versione sperimentale tradotta in Rust degli autobinding di Cwtch è disponibile in [libCwtch-rs](https://crates.io/crates/libcwtch). Mentre abbiamo in programma di adottare ufficialmente binding di Rust in futuro, in questo momento il supporto a Rust è ancora indietro nel resto dell'ecosistema Cwtch.
