---
sidebar_position: 2
---

# Concetti di base

Questa pagina documenta i concetti fondamentali che chiunque si trovi a sviluppare un'app di Cwtch incontrerà abbastanza frequentemente.

## Directory home di Cwtch

Spesso indicata come `$CWTCH_HOME`, la directory home dell'applicazione Cwtch è la posizione in cui Cwtch archivia tutte le informazioni relative a un'applicazione Cwtch.

## Profili

I profili Cwtch vengono salvati come database sqlite3 crittografati. Raramente/mai si dovrà interagire direttamente con il database. Ogni libreria fornisce invece una serie di interfacce per interagire con l'applicazione Cwtch, creare profili, gestire profili e avviare conversazioni.

## Il bus eventi

Indipendentemente da quale libreria si finisce per scegliere, l'unica interfaccia costante a cui ci si dovrà abituare è l'EventBus. Cwtch gestisce automaticamente tutte le attività asincrone (ad es. ricevere un messaggio da un peer) inserendo un messaggio su EventBus. L'applicazione può iscriversi a certi tipi di messaggi, ad es. `NewMessageFromPeer`, e impostare un gestore di eventi per eseguire un certo codice in risposta a tale messaggio.

Per un esempio vedere il tutorial Echo Bot.

## Impostazioni

La maggior parte delle impostazioni Cwtch (ad eccezione degli esperimenti) sono progettate per avere a valle interfacce utente grafiche, ad esempio temi / layout a colonne - in particolare l'interfaccia utente Cwtch. In quanto tali, queste impostazioni non sono utilizzate dalle librerie Cwtch, e sono intese solo come un comodo luogo di archiviazione per la configurazione dell'interfaccia utente.

### Esperimenti

Alcune caratteristiche di Cwtch sono [limitate a specifici esperimenti](/docs/category/experiments). Questi esperimenti devono essere abilitati prima che la funzionalità ad essi correlata si attivi. Diverse librerie possono esporre esperimenti diversi, e alcune librerie potrebbero non supportare del tutto certi esperimenti.
