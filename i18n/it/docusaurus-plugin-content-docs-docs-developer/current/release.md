---
sidebar_position: 1
---

# Processo di packaging e rilascio

Le build di Cwtch sono costruiti automaticamente tramite Drone. Per poter prendere parte al build, i diversi step devono essere approvati da un membro del team del progetto.

## Test automatizzati

Drone effettua una serie di test automatizzati in varie fasi della pipeline di rilascio.

| Test Suite                                   | Repository        | Note                                                                                                                                                           |
| -------------------------------------------- | ----------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Test di integrazione                         | cwtch.im/cwtch    | Un esercizio completo di messaggi peer-to-peer e di gruppo                                                                                                     |
| Test di condivisione file                    | cwtch.im/cwtch    | Verifica che la condivisione di file e il download di immagini funzionino come previsto                                                                        |
| Test di download automatizzato               | cwtch.im/cwtch    | Verifica che il download automatico di immagini (ad esepio immagini del profilo) funzioni come previsto                                                        |
| Test di integrazione dell'interfaccia utente | cwtch.im/cwtch-ui | Una suite di test di Gherkin per esercitare varie procedure dell'interfaccia utente come la creazione/eliminazione di profili e la modifica delle impostazioni |

## Autobindings di Cwtch

Drone produce i seguenti artefatti di build per tutte le build di Cwtch con autobinding:

| Artefatto                  | Piattaforma | Note                                                                      |
| -------------------------- | ----------- | ------------------------------------------------------------------------- |
| android/cwtch-sources.jar  | Android     | codice sorgente della libreria per Android di Cwtch, derivato da gomobile |
| android/cwtch.aar          | Android     | Libreria di Cwtch per Android. Supporta arm, arm64 e amd64.               |
| linux/libCwtch.h           | Linux       | File header di C                                                          |
| linux/libCwtch.h           | Linux       | libreria x64 condivisa                                                    |
| windows/libCwtch.h         | Windows     | file header di C                                                          |
| windows/libCwtch.dll       | Windows     | libreria x64 condivisa                                                    |
| macos/libCwtch.arm64.dylib | MacOS       | libreria arm64 condivisa                                                  |
| macos/libCwtch.x64.dylib   | MacOS       | libreria x64 condivisa                                                    |

## Versioni di sviluppo ("nightly" build) dell'interfaccia utente

Versioni di Cwtch non ufficialmente rilasciate sono rese disponibili a scopo di test come [Cwtch Nightlies](/docs/contribute/testing#cwtch-nightlies).

Ogni cartella di nightly build contiene una serie di artefatti (ad esempio file APK per Android, eseguibili di installazione per Android) raccolti convenientemente in una singola sottocartella. Un elenco completo di artefatti di build attualmente prodotti è il seguente:

| Artefatto                   | Piattaforma | Note                                                                                                      |
| --------------------------- | ----------- | --------------------------------------------------------------------------------------------------------- |
| cwtch-VERSION.apk           | Android     | Supporta arm, arm64 e amd64. Caricabile attraverso sideloading.                                           |
| cwtch-VERSION.aab           | Android     | Pacchetto applicazione Android per la pubblicazione in appstore                                           |
| Cwtch-VERSION.dmg           | MacOS       |                                                                                                           |
| cwtch-VERSION.tar.gz        | Linux       | Contiene il codice, le librerie e le risorse, oltre che agli script di installazione per vari dispositivi |
| cwtch-VERSION.zip           | Windows     |                                                                                                           |
| cwtch-installer-VERSION.exe | Windows     | Assistente di installazione basato su NSIS                                                                |

Le build nightly vengono regolarmente eliminati dal sistema

## Versioni ufficiali

Il team di Cwtch si riunisce regolarmente e raggiunge un comune accordo sulla base del feedback dai test delle versioni nightly delle tabelle di marcia del progetto.

Quando si decide di creare un build di rilascio, viene costruita una versione nightly con un nuovo git tag che riflette la versione di rilascio ad esempio `v.1.12.0`. Gli artefatti di build vengono poi copiati sul sito web delle versioni di Cwtch in una cartella versionata dedicata.

### Build Riproducibili

Usiamo [repliqate](https://git.openprivacy.ca/openprivacy/repliqate) per fornire [script di build riproducibili per Cwtch](https://git.openprivacy.ca/cwtch.im/repliqate-scripts).

Aggiorniamo il repository `repliqate-scripts` con script per tutte le versioni ufficiali. Attualmente solo i binding di Cwtch sono riproducibili