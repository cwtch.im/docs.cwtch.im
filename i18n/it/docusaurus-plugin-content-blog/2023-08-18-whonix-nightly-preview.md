---
title: "Anteprima versione nightly: Supporto Whonix, salvare la cronologia di default, bug fix"
description: "Una nuova Cwtch Nightly contiene un primo tentativo di supporto per Whonix, Salva Cronologia come impostazione di default, bug fix"
slug: cwtch-nightly-anteprima-whonix-salva-cronologia
tags:
  - cwtch
  - cwtch-stabile
  - nightly
  - whonix
  - anteprima
image: /img/devlog10_small.png
hide_table_of_contents: false
authors:
  - 
    name: Sarah Jamie Lewis
    title: Direttore Esecutivo, Società di ricerca Open Privacy
    image_url: /img/sarah.jpg
---

C'è una [nuova build Nightly](https://docs.cwtch.im/docs/contribute/testing#cwtch-nightlies) disponibile sul nostro server di build. La nightly più recente che consigliamo di testare è [2023-08-22-23-27-v1.12.0-25-ge019f](https://build.openprivacy.ca/files/flwtch-2023-08-22-23-27-v1.12.0-25-ge019f/).

Questo nightly contiene una prima versione del [supporto per Whonix](https://docs.cwtch.im/docs/platforms/whonix), una nuova impostazione globale per la gestione del modo in cui viene conservata la cronologia delle conversazioni, oltre a diverse correzioni di bug segnalati nella nightly precedente.

Si prega di consultare la documentazione del contributo per consigli su come [inviare feedback](/docs/contribute/testing#submitting-feedback)

![](/img/devlog10.png)
 
<!--truncate-->

## Stai al passo con gli aggiornamenti!

Iscriviti al nostro [feed RSS](/blog/rss.xml), [feed Atom](/blog/atom.xml), o [feed JSON](/blog/feed.json) per rimanere al passo, e avere aggiornamenti sullo sviluppo di Cwtch.

## Aiutaci a fare di più!

Non potremmo fare quello che facciamo senza tutto il meraviglioso supporto della comunità che riceviamo, da [donazioni una tantum](https://openprivacy.ca/donate) a [supporto ricorrente tramite Patreon](https://www.patreon.com/openprivacy).

Se vuoi vederci muovere più velocemente su alcuni di questi obiettivi e ne hai possibilità, una [donazione](https://openprivacy.ca/donate) è benvenuta. Se sei parte di una azienda o società che vuole fare di più per la comunità e si trova allineata con i nostri principi, ti preghiamo di considerare una donazione o di sponsorizzare uno sviluppatore.

Donazioni di **5$ o più** danno la possibilità di ricevere adesivi come regalo di ringraziamento!

Per ulteriori informazioni sulla donazione a Open Privacy e per richiedere un regalo di ringraziamento [si prega di visitare la pagina di donazione a Open Privacy](https://openprivacy.ca/donate/).

![Una foto degli adesivi di Cwtch](/img/stickers-new.jpg)



