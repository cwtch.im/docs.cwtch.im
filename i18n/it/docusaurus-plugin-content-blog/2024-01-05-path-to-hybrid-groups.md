---
title: Verso i Gruppi ibridi
description: Uno sguardo a come progettiamo di implementare la prossima generazione di messaggistica multilaterale di Cwtch
slug: verso-gruppi-ibridi
tags:
  - cwtch
  - gruppi-ibridi
image: /img/hybridgroups.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
  - name: Sarah Jamie Lewis
    title: Direttore Esecutivo, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

In [settembre 2023 abbiamo rilasciato Cwtch 1.13](/blog/cwtch-1-13), la prima versione di Cwtch etichettata come **stabile** e un'importante pietra miliare nello sviluppo di Cwtch.

Con l'interfaccia Cwtch ora stabile, siamo nella posizione di iniziare una nuova fase nello sviluppo di Cwtch: un percorso verso i **Gruppi ibridi**.

![](/img/hybridgroups.png)

<!--truncate-->

## Il problema con i gruppi di Cwtch

Una delle caratteristiche uniche di Cwtch è che i [gruppi](/docs/groups/introduction) dipendono da [infrastruttura non attendibile](/security/components/cwtch/server).

A causa di questo, fondamentalmente un gruppo Cwtch è semplicemente un accordo tra un insieme di peer su una chiave crittografica comune, e un (insieme di) server non attendibili.

Ciò fornisce ai gruppi di Cwtch proprietà molto interessanti come l'anonimato per chiunque non faccia parte del gruppo, ma significa che alcune altre proprietà interessanti come la flessibilità dei membri e la rotazione delle credenziali sono difficili da ottenere.

Vogliamo consentire alle persone di trovare il giusto compromesso relativamente ai propri modelli di rischio, ovvero essere in grado di barattare l’efficienza con la fiducia quando la decisione ha senso.

Per fare questo, dobbiamo introdurre una nuova classe di gruppi in Cwtch, qualcosa che abbiamo deciso di chiamare **Gruppi ibridi**.

## Cosa sono i Gruppi Ibridi?

L'obiettivo dei gruppi ibridi è quello di bilanciare le proprietà di sicurezza della comunicazione peer-to-peer di Cwtch con le proprietà di un'infrastruttura non affidabile.

Ciò viene fatto equipaggiando i gruppi Cwtch esistenti con un ulteriore livello di comunicazione peer-to-peer al fine di fornire una gestione efficiente dei partecipanti, rotazione delle chiavi e altre funzionalità utili.

### Livelli dei Gruppi ibridi

In pratica, immaginiamo che ci saranno alcuni livelli diversi di Gruppi ibridi, che riflettano diversi compromessi tra fiducia tra peer, efficienza della comunicazione e sicurezza di gruppo.

Ci sono innanzitutto **Gruppi tradizionali**, questi hanno proprietà simili a quelle dei gruppi Cwtch esistenti. Altamente inefficienti, ma essenzialmente richiedono zero fiducia per conto dei partecipanti con eccezione dell'aspettativa che la chiave sia mantenuta segreta.

Abbiamo intenzione di introdurre poi \*\*Gruppi amministrati \*\*: un nuovo tipo di gruppo in cui tutti i partecipanti si affidano esplicitamente a un dato peer sempre online (ad esempio un bot) per le operazioni di gruppo. Questi saranno molto efficienti, al prezzo di questa fiducia esplicita (se il peer che amministra si comporta maliziosamente allora alcune proprietà vengono a cadere). I gruppi amministrati saranno i primi gruppi Cwtch a consentire gruppi **Contrattabili** ed **Espandibili** e una **Rotazione delle chiavi** più efficiente.

To start with this _trusted peer_ will take the form of an external bot (powered by [a cwtch bot framework](/developing/building-a-cwtch-app/building-an-echobot)) however we
eventually plan to expose this capability as part of the Cwtch UI.

E infine una categoria di **Gruppi potenziati**: un'estensione dei Gruppi amministrati che pone restrizioni configurabili sulla fiducia accordata al peer amministratore, ad esempio richiedendo ai partecipanti di prendere parte a un metaprotocollo che conferma determinate azioni prima che vengano eseguite (impedendo al peer fidato di danneggiare proprietà come la **Coerenza dei partecipanti**).

## Metadati di messaggi di gruppo

Come per il resto di Cwtch, il nostro obiettivo finale è che nessun metadato (e nello specifico, come parte di questo lavoro, nessun metadato di gruppo, ad esempio appartenenza, tempistica dei messaggi) sia disponibile a una parte esterna al gruppo.

I gruppi Cwtch tradizionali prendono tutto ciò estremamente alla lettera, a scapito di lunghi tempi di sincronizzazione e di un'elevata possibilità di interruzioni. I Gruppi amministrati e i Gruppi potenziati permetteranno alle comunità di fare i giusti compromessi, consentendo una maggiore resilienza e una sincronizzazione più veloce.

## Una cronologia approssimativa (Trimestre 1: settimana 0 - settimana 10 2024)

- **Settimana 0** - Pianificazione della cronologia di Cwtch del primo trimestre (questo devlog), correzioni di bug minori e altri piccoli lavori incentrati sull'interfaccia utente derivanti da rapporti e feedback dei [tester di Cwtch](/docs/contribute/testing).
- **Settimana 1** - Iniziano i lavori per esporre le **Autorizzazioni avanzate** nella libreria Cwtch. Queste sono essenziali per implementare molti degli aspetti del nuovo design di gruppo, nonché per migliorare altre parti della gestione dei contatti. (Aspettatevi di più su questo in un futuro devlog). Inoltre, verrà creato e documentato un modello formale per i Gruppi amministrati.
  Questo sarà la base dell'implementazione.
- **Settimana 2** - A questo punto dovremmo essere in grado di iniziare a progettare l'estensione di Cwtch ai Gruppi amministrati. Questa utilizzerà l'API "Event Hooks" di Cwtch per rispondere agli eventi "Peer" per gestire i gruppi. Durante questo lavoro, prevediamo anche di migrare il codice legacy dei gruppi in una sua propria simile estensione per utilizzare al meglio le API.
- **Settimana 3** - Verso la fine di gennaio ci aspettiamo di avere un modello formale completo di Gruppi amministrati e di poter iniziare ad integrare le nuove estensioni nell'interfaccia utente di Cwtch. Prevediamo inoltre di essere in procinto di rilasciare una nuova versione 1.14 di Cwtch che supporti le Autorizzazioni avanzate.
- **Settimana 4 - Settimana 6** - Febbraio segna il sesto anniversario della fondazione della società di ricerca [Open Privacy](https://openprivacy.ca) e la fine del nostro anno organizzativo. In queste settimane i membri principali del team di Cwtch sono spesso coinvolti in compiti amministrativi che devono essere svolti durante questo periodo, pertanto non prevediamo di fare troppi progressi su Cwitch.
- **Settimana 7 - Settimana 10** - Mentre ci avviciniamo a marzo, integreremo formalmente a Cwtch i Gruppi amministrati, e pianificheremo una versione di Cwtch 1.15 che presenterà il nuovo tipo di gruppo. Durante questo periodo aggiorneremo anche la [Documentazione sui gruppi](https://docs.cwtch.im/docs/category/groups) di Cwtch.

Una volta implementati i Gruppi amministrati, valuteremo ciò che abbiamo imparato e procederemo con passaggi simili per i Gruppi potenziati nel secondo trimestre (ne parleremo più avanti in un successivo devlog!).

## Stai al passo con gli aggiornamenti!

Come sempre, aggiorneremo regolarmente questo devlog [e altri canali](https://fosstodon.org/@cwtch) mentre continueremo a fare progressi verso un'infrastruttura resistente alla sorveglianza!

Iscrivetevi al nostro [feed RSS](/blog/rss.xml), [feed Atom](/blog/atom.xml) o [feed JSON](/blog/feed.json) per seguirci in tempo reale e ricevere le ultime novità su tutti gli aspetti dello sviluppo di Cwtch.

## Aiutaci a fare di più!

Non potremmo fare ciò che facciamo senza tutto il meraviglioso supporto che riceviamo dalla comunità, dalle [donazioni una tantum](https://openprivacy.ca/donate) al [supporto ricorrente tramite Patreon](https://www.patreon.com/openprivacy).

Se volete vederci muoverci più velocemente su alcuni di questi obiettivi e ne avete la possibilità, una [donazione](https://openprivacy.ca/donate) è benvenuta. Se siete parte di una azienda o società che vuole fare di più per la comunità e si trova allineata con i nostri principi, vi preghiamo di considerare una donazione o di sponsorizzare uno sviluppatore.

Donazioni di **5$ o più** danno la possibilità di ricevere adesivi come regalo di ringraziamento!

Per ulteriori informazioni sulla donazione a Open Privacy e per richiedere un regalo di ringraziamento si prega di [visitare la pagina di donazione a Open Privacy](https://openprivacy.ca/donate/).

![Un'immagine degli adesivi di Cwtch](/img/stickers-new.jpg)
