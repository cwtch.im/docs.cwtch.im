---
title: Richiesta di crediti per la collaborazione a Cwtch
description: "Mentre ci avviciniamo sempre più vicino ad una versione di Cwtch stabile, vorremmo cogliere l'occasione per garantire che coloro che hanno contribuito a Cwtch nel corso degli anni abbiano l'opzione di essere accreditati."
slug: cwtch-stabile-accrediti
tags:
  - cwtch
  - cwtch-stabile
  - collaboratori
  - comunità
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
  - 
    name: Sarah Jamie Lewis
    title: Direttore Esecutivo, Società di ricerca Open Privacy
    image_url: /img/sarah.jpg
---

Mentre ci avviciniamo sempre più ad una versione di Cwtch stabile, vorremmo cogliere l'occasione per garantire che coloro che hanno contribuito a Cwtch nel corso degli anni abbiano l'opzione di essere accreditati.

Se avete partecipato in qualsiasi modo al processo di sviluppo, ad es. alla progettazione del protocollo, alla scrittura del codice, alla progettazione dell'interfaccia utente, alla scrittura dei test, al test delle versioni candidate al rilascio, alla segnalazione dei problemi, alla traduzione dell'applicazione o della documentazione, alla promozione di applicazioni resistenti ai metadati o con qualsiasi altro contributo significativo all'ecosistema Cwtch, vogliamo offrirti la possibilità di avere il vostro nome o nickname accreditato sia nel repository del codice sorgente che nell'applicazione stessa.

![](/img/devlog1.png)
 
<!--truncate-->

## Una storia di contributi a Cwtch e anonimato

Agli albori di Cwtch abbiamo preso la decisione esplicita di non includere crediti da nessuna parte nell'applicazione, e di accettare contributi in modo anonimo attraverso una varietà di canali, tra cui Cwtch stessa.

A causa della natura dell'applicazione e dello spazio privato e resistente ai metadati in generale, abbiamo sempre avuto la politica di valutare i contributi in base al merito, e non all'identità. Questo approccio significa che, mentre abbiamo contributori la cui identità è conosciuta in qualche modo, ne abbiamo molti altri che conosciamo solo attraverso lo stile di scrittura, il tipo di contributo, o l'indirizzo cwtch.

Comprendiamo che molte persone preferiscono questo approccio, e non hanno alcun desiderio di avere alcuna identità legata al progetto Cwtch. A queste persone offriamo la nostra profonda gratitudine. Grazie. Avete reso Cwtch quello che è ora. (E se mai volete degli adesivi di Cwtch - fatecelo sapere!)

Tuttavia, non sarebbe giusto da parte nostra rilasciare una versione stabile di Cwtch senza almeno dare un'ultima possibilità a tutti i contributori. Se volete avere crediti per i vostri contributi a Cwtch, contattateci e fateci sapere in che modo vorreste che avvenga.

## Come contattarci

È possibile richiedere crediti via e-mail (team@cwtch.im), o via Cwtch (pubblicamente tramite i [gruppi Cwtch dei tester delle versioni candidate al rilascio](https://docs.cwtch.im/docs/contribute/testing#join-the-cwtch-release-candidate-testers-group), o privatamente in un messaggio a Sarah: `icyt7rvdsdci42h6si2ibtwucdmjrlcb2ezkecuagtquiiflbkxf2cqd`).

È anche possibile [aprire una issue](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/new).

Come parte della richiesta, si prega di fornire un nome o un nickname e, se lo si desidera, una breve descrizione del contributo (ad esempio, sviluppo, progettazione, documentazione, traduzione, finanziamento). Chiunque non fornisca una descrizione sarà parte di una sezione di ringraziamento generale.

Questa è un'offerta aperta. Se in qualsiasi momento cambiate idea e desiderate che vengano aggiunti (o rimossi) dei crediti, fatecelo sapere.

Voglio cogliere di nuovo l'occasione per dire, indipendentemente dal fatto che desideriate ricevere crediti pubblicamente per il vostro lavoro su Cwtch: **grazie mille**.

## State al passo con gli aggiornamenti!

Iscrivetevi al nostro [feed RSS](/blog/rss.xml), [feed Atom](/blog/atom.xml), o [feed JSON](/blog/feed.json) per rimanere al passo, e avere aggiornamenti sullo sviluppo di Cwtch.

## Aiutateci a fare di più!

Non potremmo fare quello che facciamo senza tutto il meraviglioso supporto della comunità che riceviamo, da [donazioni una tantum](https://openprivacy.ca/donate) a [supporto ricorrente tramite Patreon](https://www.patreon.com/openprivacy).

Se volete vederci muovere più velocemente su alcuni di questi obiettivi e ne avete la possibilità, una [donazione](https://openprivacy.ca/donate) è benvenuta. Se siete parte di una azienda o società che vuole fare di più per la comunità e si trova allineata con i nostri principi, vi preghiamo di considerare una donazione o di sponsorizzare uno sviluppatore.

Donazioni di **$5 o più** danno la possibilità di ricevere adesivi come regalo di ringraziamento!

Per ulteriori informazioni sulla donazione a Open Privacy e per richiedere un regalo di ringraziamento [si prega di visitare la pagina di donazione a Open Privacy](https://openprivacy.ca/donate/).

![Una foto degli adesivi di Cwtch](/img/stickers-new.jpg)



