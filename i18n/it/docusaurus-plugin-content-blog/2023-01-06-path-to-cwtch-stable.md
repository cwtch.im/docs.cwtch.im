---
title: Percorso verso Cwtch Stable
description: "Il post delinea i principi generali che stanno guidando lo sviluppo di Cwtch Stabile, gli ostacoli che ne impediscono il rilascio, e chiude con una panoramica sui prossimi passi e un calendario per affrontarli."
slug: path-to-cwtch-stable
tags:
  - cwtch
  - cwtch-stabile
  - pianificazione
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
  - 
    name: Sarah Jamie Lewis
    title: Direttore Esecutivo, Open Privacy Research Society
    image_url: /img/Sarah.jpg
---

Abbiamo rilasciato 10 versioni di Cwtch Beta dal [lancio iniziale, 18 mesi fa, nel giugno 2021](https://openprivacy.ca/discreet-log/10-cwtch-beta-and-beyond/) a dicembre 2022.

C'è consenso tra i membri del team sul fatto che il prossimo grande passo per il progetto Cwtch sia passare dalla versione pubblica **Beta** a quella **Stabile** – marcando un punto in cui riteniamo il codice Cwtch sicuro e utilizzabile.

Il post delinea i principi generali che stanno guidando lo sviluppo di Cwtch Stabile, gli ostacoli che ne impediscono il rilascio, e chiude con una panoramica sui prossimi passi e un calendario per affrontarli.

![](/img/devlog1.png)

<!--truncate-->

### Principi di Cwtch Stabile

È importante affermare che Cwtch Stabile **non significa la fine dello sviluppo di Cwtch**. Stabilisce piuttosto una base di riferimento a partire da cui si può considerare Cwtch un progetto pienamente sostenuto. Il team di Cwtch ha stabilito i seguenti principi che guidano le nostre decisioni e le nostre priorità:

1. **Interfaccia coerente** – ogni nuova versione di Cwtch rilasciata dovrebbe essere accompagnata da rilasci coerenti di tutte le librerie di supporto. Questo richiede un'API stabile e documentata in modo che possiamo stabilire chiaramente quando l'aggiornamento di una libreria porterà a cambiamenti sostanziali e incompatibili con precedenti versioni per progetti da essa dipendenti.  Come regola generale, non dovremmo avere bisogno di apportare modifiche sostanziali a questa interfaccia API per supportare nuove funzionalità sperimentali.
2. **Disponibilità universale e supporto coeso** – le persone che usano Cwtch capiscono che se Cwtch è disponibile per una piattaforma, questo significa che tutte le funzionalità funzioneranno come previsto, che non ci sono limitazioni a sorpresa, e le eventuali differenze sono ben documentate. Le persone non dovrebbero dover sudare sette camicie per installare Cwtch.
3. **Build riproducibili** – le build di Cwtch dovrebbero essere riproducibili in modo triviale, compresa la possibilità di riprodurre tutte le risorse raggruppate. La riproducibilità non dovrebbe basarsi sulla containerizzazione, ma tutti i container utilizzati nel nostro processo di build dovrebbero essere riproducibili.
4. **Sicurezza comprovata** – possiamo dimostrare che Cwtch fornisce sicurezza di prima classe attraverso procedure di progettazione, test e audit ben documentate. Dovremmo essere in grado di farlo per Cwtch oltre a tutte le dipendenze funzionali.

### Problemi Noti

Per cominciare, descriviamo lo stato attuale di Cwtch e delineiamo i problemi che ostacolano il raggiungimento di Cwtch Stabile.

1. **Mancanza di un'API stabile per lo sviluppo futuro delle funzionalità** – mentre l'API di base di Cwtch è rimasta relativamente invariata nelle recenti versioni, siamo consapevoli che l'aggiunta di nuove funzionalità, e.g. supporto coesivo per i gruppi, probabilmente richiede nuovi hook API che consentono la manipolazione sicura del profilo Cwtch (semantica transazionale e hook post-evento). Prima di poter anche solo prendere in considerazione una versione stabile, abbiamo bisogno di definire come dovrebbe apparire questa API e implementarla. (Principio 1)
2. **Funzionalità speciali in libCwtch-go** – il nostro ponte C-API (libCwtch-go) attualmente implementa molte funzionalità speciali a supporto sia delle funzionalità sperimentali (ad esempio le immagini del profilo) che delle impostazioni dell'interfaccia utente. Questa comportamento speciale rende difficile tenere traccia della responsabilità delle funzionalità. Deve essere o riportato nella libreria principale di Cwtch o definito come responsabilità di un'applicazione a valle, ad esempio l'interfaccia utente Cwtch. (Principio 1)
3. **supporto parziale di libCwtch-rs**  - al momento quando aggiorniamo libCwtch-go non consideriamo ufficialmente [libCwtch-rs](https://lib.rs/crates/libcwtch) come parte del nostro programma di rilascio. Prima di poter prendere in considerazione il rilascio di una versione Stabile di Cwtch dovremmo avere diverse versioni beta in cui libCwtch-rs ha pieno supporto per tutte le nuove funzionalità Cwtch. (Principio 1, Principio 2)
4. **Mancanza di pipeline riproducibili** - mentre la stragrande maggioranza delle nostre pipeline per il build è automatizzata, containerizzata, e riproducibile, rimangono risorse raggruppate che non possono essere impacchettate trivialmente e risorse che hanno elementi non riproducibili (ad esempio build-time iniettato tramite tag git e file binari che includono informazioni sull'utente del build). (Principio 3)
5. **Mancanza di Documentazione di sicurezza aggiornata e tradotta** – il [Manuale di sicurezza di Cwtch](https://docs.openprivacy.ca/cwtch-security-handbook/) è attualmente isolato dal resto della nostra documentazione e non beneficia del cross-linking, o delle traduzioni. (Principio 4)
6. **Mancanza di test automatizzati per l'interfaccia utente** – abbiamo messo molto lavoro nella [costruzione di un framework di test per l'interfaccia utente](https://openprivacy.ca/discreet-log/23-cucumber-testing/), ma attualmente risulta per lo più inutilizzato, e non esercitato dalle nostre build pipeline. Dovremmo rivedere questo lavoro. (Principio 4)
7. **Fornitore di firma del codice** – il nostro precedente fornitore di certificati di firma del codice ha avuto problemi di supporto, e non abbiamo ancora deciso un sostituto. (Principio 4)
8. **Supporto Android di seconda classe**  - anche se abbiamo dedicato [molti sforzi al supporto di Android](https://openprivacy.ca/discreet-log/27-android-improvements/) lungo tutta la sequenza temporale della versione Beta, è ancora chiaramente affetto da problemi aggiuntivi rispetto alle edizioni desktop. Al fine di considerare Cwtch stabile, dobbiamo risolvere tutti i principali bug che influenzano l'usabilità di Android. (Principio 2)
9. **Mancanza di Fuzzing** – anche se [Fuzzbot](https://openprivacy.ca/discreet-log/07-fuzzbot/) rappresenta uno standard piuttosto elevato rispetto alla maggior parte delle altre applicazioni di comunicazione sicure, possiamo e dobbiamo fare di meglio. Fuzzbot attualmente prende di mira solo i messaggi con l'utente come endpoint, che hanno maggiori probabilità di comportare rischi nel mondo reale, ma dovremmo sforzarci di avere la stessa copertura per gli eventi interni sia a livello di rete, sia a livello dell'applicazione Cwtch interna, sia a livello di bus degli eventi. (Principio 4)
10. **Mancanza di un processo formale di accettazione della versione**: attualmente le funzionalità e gli esperimenti che vengono inclusi in ciascuna versione sono basati su un consenso ad hoc. Questo significa occasionalmente che alcune caratteristiche rimangono non supportate su alcune piattaforme, e occasionalmente si manifestano dei bug sulle piattaforme (Android in particolare) a causa di cambiamenti "non correlati”. Affinché si possa dichiarare Cwtch stabile, un processo formale di accettazione deve garantire che le nuove modifiche non compromettano le caratteristiche esistenti, e che funzionino su tutte le piattaforme. (Principio 2, Principio 4)
11. **Fonti di informazione su Cwtch incoerenti**: la nostra documentazione attuale è suddivisa tra docs.cwtch.im, cwtch.im e docs.openprivacy.ca, oltre ai blog su Discreet Log. Ciò rende difficile per le persone trovare informazioni su Cwtch, e significa anche che le nostre spiegazioni spesso devono avere link a siti diversi. (Principio 2)
12. **Documentazione Incompleta** – docs.cwtch.im è stato accolto molto bene. Tuttavia, soffre ancora di sezioni incomplete, collegamenti mancanti e una mancanza generale di screenshot. Gli screenshot presenti mancano di coerenza nel dimensionamento, stile e feeling. (Principio 2)

### Piano d’azione

Al di fuori dei problemi che hanno soluzioni autonome (ad es. trovare un nuovo provider di firma del codice, o risolvere tutti i problemi Android), ci sono una serie di attività di livello superiore che devono essere completate prima di poter avere confidenza in una versione di Cwtch stabile:

1. **Definire, pubblicare e implementare una documentazione relativa alle specifiche dell'interfaccia Cwtch** – questa dovrebbe includere esempi di come un nuovo comportamento (sperimentale) potrebbe essere implementato da una composizione più dettagliata. Deve includere lo spostamento di tutte le funzionalità speciali fuori da libCwtch-go. Dovrebbe essere seguito dall’implementazione del design proposto. (Principio 1, Principio 4)
2. **Definire, pubblicare e implementare un processo di rilascio di Cwtch** – questo documento dovrebbe delineare i criteri per la pubblicazione di una nuova versione, la differenza tra versioni principali e secondarie, come vengono testate le funzionalità, come vengono rilevate le regressioni prima del rilascio, e chi è responsabile delle diverse parti del processo. (Principio 2)
3. **Definire, pubblicare e implementare un documento di supporto a Cwtch**  - comprensivo di risposte alle domande: quali sistemi supportiamo, come decidiamo quali sistemi sono supportati, come gestiamo nuove versioni del sistema operativo e come il supporto all'applicazione differisce dal supporto alle librerie. Questo dovrebbe anche includere, per i sistemi che desideriamo supportare ma attualmente non possiamo, per esempio iOS, un elenco delle ragioni bloccanti. (Principio 2)
4. **Definire, Pubblicare, e implementare un documento di packaging di Cwtch**  - come supplemento al documento di supporto dobbiamo definire quale packaging supportiamo, oltre agli  app store e gestori di app per i quali forniamo versioni ufficiali. (Principio 2)
5. **Definire, pubblicare e implementare un documento di compilazione riproducibile** – questo dovrebbe coprire non solo i file binari di Cwtch, ma anche i container Docker, e le risorse incluse (ad esempio i file binari di Tor). Seguito dall'implementazione del piano nella nostra build pipeline. (Principio 3)
6. **Espandere il sito web di documentazione di Cwtch** – in modo da includere il Manuale di sicurezza, i blog di sviluppo, la documentazione di progettazione e i piani di supporto. Questo dovrebbe essere la nostra unica piattaforma di pubblicazione, al di fuori di una landing page, e dei download su cwtch.im. Questa espansione dovrebbe includere una guida di stile per la documentazione e gli screenshot, per garantire che manteniamo linguaggio e visualizzazioni coerenti quando si parla di una funzione (ad esempio dovremmo usare lo stesso stile dell'immagine del profilo, tema, nomi del profilo, stile del messaggio, ecc.) (Principio 1, Principio 2, Principio 3, Principio 4)
7. **Espansione del nostro Testing automatico in modo da includere UI e Fuzzing**  - integrazione dei test automatizzati dell'interfaccia utente nella nostra build pipeline. Espansione del nostro fuzzing in modo da includere il bus eventi, e i pacchetti PeerApp. Infine, integrazionde del fuzzing automatizzato nella build pipeline, in modo che tutte le nuove funzionalità beneficino del fuzzing allo stesso livello. (Principio 4)
8. **Rivalutare tutti le issue aperte in tutti i repository correlati a Cwtch** – le issue sono o bug che devono essere risolti prima di poter avere una versione stabile (e sono quindi al servizio di uno dei Principi), o nuove idee per funzionalità che dovrebbero essere programmate al di fuori del lavoro per una versione stabile (e quindi non si allineano con un Principio specifico), oppure sono richieste di supporto per i sistemi che necessitano di input dai Piani di supporto e packaging.
9. **Definire un set di funzionalità stabili** – ci sono ancora alcune funzionalità che non esistono in Cwtch Beta ma che sono necessarie per il rilascio di una versione stabile, come la funzionalità di ricerca in chat. Seguendo il documento di specifica dell'interfaccia di Cwtch, il team dovrebbe decidere su quali funzionalità concentrarsi come obiettivo per Cwtch Stabile, e queste funzionalità dovrebbero essere prioritarie per l'inclusione in Cwtch 1.11, Cwtch 1.12 e tutte le altre future versioni Beta. (Principio 1)

### Obiettivi e Tempistiche

Detto tutto ciò, ora siamo pronti a introdurre una sequenza temporale per risolvere alcuni di questi problemi e spostarci verso uno stato in cui il rilascio di Cwtch Stabile diventa possibile:

1. Entro l'**1 Febbraio 2023**, il team di Cwtch avrà esaminato tutti i correnti problemi di Cwtch in linea con questo documento, e stabilito una linea temporale per includerli nelle prossime versioni (o specificamente deliberatamente escluderli dalle prossime versioni).
2. Entro l'**1 Febbraio 2023**, il team di Cwtch avrà finalizzato un set di funzionalità che definisce Cwtch Stable e stabilito una linea temporale per includere queste funzionalità nelle prossime versioni di Cwtch Beta.
3. Entro l'**1 febbraio 2023**, il team di Cwtch avrà ampliato il sito web di documentazione di Cwtch in modo da includere una sezioni per Sicurezza, Documenti di progettazione, Infrastruttura e supporto, oltre a un nuovo blog sullo sviluppo.
4. Entro il **31 Marzo 2023**, il team di Cwtch avrà creato una guida di stile per la documentazione e l'avrà utilizzata per garantire che tutte le funzioni Cwtch dispongano di una documentazione coerente, con almeno uno screenshot (dove rilevante).
5. Entro il **31 marzo 2023** il team di Cwtch avrà pubblicato un documento sulle specifiche dell'interfaccia di Cwtch, un documento sul processo di rilascio di Cwtch, un documento sul piano di supporto Cwtch, un documento sul packaging di Cwtch e un documento che descrive il processo di build riproducibile. Questi documenti saranno disponibili su una versione aggiornata del sito web della documentazione di Cwtch.
6. Entro il **31 marzo 2023** il team di Cwtch avrà integrato i test automatizzati dell'interfaccia utente nella build pipeline per il repository di cwtch-ui.
7. Entro il **31 marzo 2023** il team di Cwtch avrà integrato il fuzzing automatico nella build pipeline per tutte le dipendenze Cwtch gestite dal team di Cwtch.
8. Entro il **31 marzo 2023** il team di Cwtch avrà fissato una data, e stabilito una sequenza temporale e una tabella di marcia per il lancio di Cwtch Stabile.

Mano a mano che questi documenti verranno scritti, e questi obiettivi raggiunti, li pubblicheremo qui! Iscriviti al nostro [feed RSS](/blog/rss.xml), [feed Atom](/blog/atom.xml), o [feed JSON](/blog/feed.json) per rimanere al passo, e avere aggiornamenti sullo sviluppo di Cwtch.

### Aiutaci a raggiungere l'obiettivo!

Non potremmo fare quello che facciamo senza tutto il meraviglioso supporto della comunità che riceviamo, da [donazioni una tantum](https://openprivacy.ca/donate) a [supporto ricorrente tramite Patreon](https://www.patreon.com/openprivacy).

Se vuoi vederci muoverci più velocemente su alcuni di questi obiettivi e ne hai possibilità, una [donazione](https://openprivacy.ca/donate) è benvenuta. Se sei parte di una azienda o società che vuole fare di più per la comunità e si trova allineata con i nostri principi, ti preghiamo di considerare una donazione o di sponsorizzare uno sviluppatore.

Donazioni di **$5 o più** danno la possibilità di ricevere adesivi come regalo di ringraziamento!

Per ulteriori informazioni sulla donazione a Open Privacy e per richiedere un regalo di ringraziamento [si prega di visitare la pagina di donazione a Open Privacy](https://openprivacy.ca/donate/).

![Una foto di adesivi di Cwtch](/img/stickers-new.jpg)

