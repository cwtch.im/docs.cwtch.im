---
title: "Anteprima Nightly: Ricerca nelle conversazioni"
description: "Una nuova Nightly di Cwtch contiene una prima versione della ricerca nelle conversazioni."
slug: cwtch-nightly-anteprima-ricerac-conversazioni
tags:
  - cwtch
  - cwtch-stabile
  - nightly
  - ricerca
  - anteprima
image: /img/devlog10_small.jpg
hide_table_of_contents: false
authors:
  - 
    name: Sarah Jamie Lewis
    title: Direttore Esecutivo, Società di ricerca Open Privacy
    image_url: /img/sarah.jpg
---

C'è una [nuova build Nightly](https://docs.cwtch.im/docs/contribute/testing#cwtch-nightlies) disponibile sul nostro server di build. La nightly più recente che consigliamo di testare è [2023-08-02-20-24-v1.12.0-19-g75b7](https://build.openprivacy.ca/files/flwtch-2023-08-02-20-24-v1.12.0-19-g75b7/).

Questa nightly contiene una prima versione della Ricerca nelle conversazioni, oltre a diverse correzioni di bug che incidono sull'efficacia del plug-in di "riprova contatto" quando combinato con un elenco di contatti di grandi dimensioni e una connessione di rete instabile. Finalmente abbiamo inoltre apportato alcune modifiche alla scala del carattere in base al feedback.

Si prega di consultare la documentazione annessa per consigli su come [inviare feedback](/docs/contribute/testing#submitting-feedback)

![](/img/search-nightly.png)
 
<!--truncate-->

## State al passo con gli aggiornamenti!

Iscrivetevi al nostro [feed RSS](/blog/rss.xml), [feed Atom](/blog/atom.xml), o [feed JSON](/blog/feed.json) per rimanere al passo, e avere aggiornamenti sullo sviluppo di Cwtch.

## Aiutateci a fare di più!

Non potremmo fare quello che facciamo senza tutto il meraviglioso supporto che riceviamo dalla comunità, da [donazioni una tantum](https://openprivacy.ca/donate) a [supporto ricorrente tramite Patreon](https://www.patreon.com/openprivacy).

Se volete vederci muovere più velocemente su alcuni di questi obiettivi e ne avete la possibilità, una [donazione](https://openprivacy.ca/donate) è benvenuta. Se siete parte di una azienda o società che vuole fare di più per la comunità e si trova allineata con i nostri principi, vi preghiamo di considerare una donazione o di sponsorizzare uno sviluppatore.

Donazioni di **5$ o più** danno la possibilità di ricevere adesivi come regalo di ringraziamento!

Per ulteriori informazioni sulla donazione e per richiedere un regalo di ringraziamento [si prega di visitare la pagina di donazione a Open Privacy](https://openprivacy.ca/donate/).

![Una foto degli adesivi di Cwtch](/img/stickers-new.jpg)



