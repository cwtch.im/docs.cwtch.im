---
title: Aggiornamento di settembre della roadmap di Cwtch Stabile
description: "Nel mese di luglio abbiamo fornito un aggiornamento su diversi obiettivi che avremmo dovuto raggiungere nel nostro percorso verso Cwtch Stabile, e le tempistiche per colpirli. In questo post forniamo un nuovo aggiornamento su questi obiettivi"
slug: cwtch-stabile-roadmap-aggiornamento-sett
tags:
  - cwtch
  - cwtch-stabile
  - pianificazione
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
  - 
    name: Sarah Jamie Lewis
    title: Direttore Esecutivo, Società di ricerca Open Privacy
    image_url: /img/sarah.jpg
---

Il prossimo grande passo per il progetto Cwtch è un passaggio dalla versione pubblica **Beta** a quella **Stabile** – segnando un punto in cui consideriamo il codice Cwtch sicuro e utilizzabile. Abbiamo lavorato sodo per raggiungere tale obiettivo nell’ultimo anno.

Oggi, mentre ci avviciniamo al rilascio di Cwtch Stabile vorremmo fornire un altro aggiornamento sul lavoro in corso, e gli ostacoli rimanenti prima di poter certificare una release stabile di Cwtch. Abbiamo anche una nuova versione nightly da testare!

![](/img/devlog1.png)
 
<!--truncate-->

## Ostacoli alla versione stabile e tempistiche

Nel mese di gennaio avevamo fissato l'ambizioso obiettivo di lanciare una versione stabile di Cwtch nell'estate del 2023. Avevamo pianificato di finire tutto il lavoro prima della fine di agosto. La stragrande maggioranza di questo lavoro è stata completata - ciò che rimane è incluso nel progetto [Ostacoli alla versione stabile](https://git.openprivacy.ca/cwtch.im/cwtch-ui/projects/15), che tiene traccia dello stato attuale del lavoro che abbiamo contrassegnato come critico per una release stabile di Cwtch.

Nonostante vi sia un gran numero di questioni rimanenti, molti dei lavori in sospeso sono interconnessi, si basano sulle stesse implementazioni o sono strettamente legati assieme.

 In sintesi, le ultime questioni aperte sono:

 - La possibilità di ripulire o eliminare la cronologia delle conversazioni di gruppo. (Per ragioni storiche, la conservazione della cronologia di gruppo era considerata necessaria ma non è più così. Abbiamo in programma di attivare questa funzione nelle prossime settimane)
 - Gestione adeguata di configurazioni di sistema meno comuni. La versione corrente Cwtch emette eccezioni non fatali se alcuni servizi non sono disponibili ad esempio dbus. Questo è dovuto a precedente codice di terze parti per la gestione della rete e delle notifiche.
 - Un'ultima passata all'interfaccia utente. Abbiamo progetti per modi migliori di rendere disponibili determinate informazioni e funzionalità. Vorremmo implementarli prima del rilascio di una versione stabile.

Per questo motivo abbiamo fissato come obiettivo di avere un versione candidata al rilascio di Cwitch Stabile entro il **30 settembre 2023**.

## Una nuova nightly

C'è una [nuova versione nightly di Cwtch pronta da testare (2023-09-06-21-25-v1.12.0-33-g05b1)](https://build.openprivacy.ca/files/flwtch-2023-09-06-21-25-v1.12.0-33-g05b1/). Questa versione contiene alcunei bug fix relativi alla gestione della condivisione dei file, oltre a un miglioramento significativo del codice di gestione della connessione.

Inoltre, grazie a chi testa [e collabora](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/715) su base volontaria, le istruzioni di installazione e le configurazioni per Whonix impacchettate sono state molto migliorate. Vedasi [Far girare Cwtch su Whonix](https://docs.cwtch.im/docs/platforms/whonix) per maggiori informazioni.

## Partecipa

La carenza di membri dello staff e volontari ha leggermente esteso le nostre stime originali. In particolare, siamo rallentati dal lavoro di revisione di nuovo codice. Questo è il motivo per cui vorremmo incoraggiare le persone a provare le ultime versioni nightly e segnalare eventuali bug/problemi/miglioramenti possibili.

Per aiutare le persone ad abituarsi alle nostre procedure di sviluppo, abbiamo creato una nuova sezione sul sito principale della documentazione chiamata [Sviluppare Cwtch](/docs/contribute/developing) - lì si trova una raccolta di link e informazioni utili su come iniziare con lo sviluppo di Cwtch, quali librerie e strumenti utilizziamo, come vengono convalidate e verificate le richieste di pull e come scegliere un problema su cui lavorare.

Abbiamo anche aggiornato le nostre guide su [Tradurre Cwtch](/docs/contribute/translate) e [Testare Cwtch](/docs/contribute/testing).

In caso di interesse a iniziare con lo sviluppo di Cwtch, dai un occhio a questi materiali, e non esitare a contattare `team@cwtch.im` (o aprire una nuova issue) con eventuali domande. Tutti i contributi consentono di [ottenere degli adesivi](/docs/contribute/stickers).

## Aiutaci a fare di più!

Non potremmo fare quello che facciamo senza tutto il meraviglioso supporto della comunità che riceviamo, da [donazioni una tantum](https://openprivacy.ca/donate) a [supporto ricorrente tramite Patreon](https://www.patreon.com/openprivacy).

Se vuoi vederci muovere più velocemente su alcuni di questi obiettivi e ne hai possibilità, una [donazione](https://openprivacy.ca/donate) è benvenuta. Se sei parte di una azienda o società che vuole fare di più per la comunità e si trova allineata con i nostri principi, ti preghiamo di considerare una donazione o di sponsorizzare uno sviluppatore.

Donazioni di **$5 o più** danno la possibilità di ricevere adesivi come regalo di ringraziamento!

Per ulteriori informazioni sulla donazione a Open Privacy e per richiedere un regalo di ringraziamento [si prega di visitare la pagina di donazione a Open Privacy](https://openprivacy.ca/donate/).

![Una foto degli adesivi di Cwtch](/img/stickers-new.jpg)

