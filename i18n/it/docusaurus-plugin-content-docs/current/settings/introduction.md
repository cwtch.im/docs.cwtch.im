---
sidebar_position: 1
---

# Un'introduzione alle impostazioni dell'app di Cwtch

## Aspetto

Queste sono impostazioni che influenzano l'aspetto di Cwtch, inclusi temi e localizzazione.

## Comportamento

Queste impostazioni influenzano il modo in cui Cwtch risponde a determinati eventi, ad esempio notifiche di nuovi messaggi o richieste da indirizzi pubblici sconosciuti.

## Esperimenti

Ci sono molte funzionalitá in Cwtch che sono desiderabili ma la cui implementazione richiede metadati aggiuntivi, o un certo rischio, oltre il minimo che Cwtch richiede per le operazioni di base.

Di conseguenza, sotto **Esperimenti** troverete un certo numero di impostazioni **opzionali** che, quando abilitate, forniscono funzionalità aggiuntive come conversazioni di gruppo, condivisione di file o formattazione dei messaggi.

Rifletti attentamente sui nuovi rischi che potrebbero essere coinvolti quando abiliti queste funzionalitá, e chiediti se sei a tuo agio o meno nel momento in cui acconsenti a correre quei rischi. Per molti i vantaggi della condivisione di file, le anteprime dell'immagine e la chat di gruppo superano di gran lunga i potenziali danni - ma per tener conto e rispettare altre esigenze richiediamo a tutti di dare un consenso esplicito a queste funzionalità.

Puoi ritirare il tuo consensoin qualsiasi momento, tutte le funzionalità sono implementate localmente all'interno dell'app Cwtch.

