---
sidebar_position: 3
---

# Contenuto notifica

1. Vai alle impostazioni
2. Scorri fino a "comportamento"
3. Il contenuto notifica controlla il contenuto delle notifiche
   1. Evento semplice: solo "Nuovo messaggio"
   2. Informazioni sulla conversazione: "Nuovo messaggio da XXXXX"
