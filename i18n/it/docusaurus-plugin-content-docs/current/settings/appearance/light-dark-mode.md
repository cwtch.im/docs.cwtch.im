---
sidebar_position: 2
---

# Modalità chiara/scura e scomposizione dei temi

1. Clicca sull'icona impostazione
2. Puoi scegliere un tema chiaro o scuro interagendo con l'interruttore "usa temi chiari"
3. Utilizzando il menu a tendina “tema colori”, scegli un tema che ti piace
4.
   1. Cwtch: toni di viola
   2. Fantasma: toni di grigio
   3. Sirena: toni di turchese e viola
   4. Mezzanotte: toni neri e grigi
   5. Neon 1: toni viola e rosa
   6. Neon 2: toni viola e turchese
   7. Zucca: toni viola e arancione
   8. Strega: toni verdi e rosa
   9. Vampiro: toni viola e rossi