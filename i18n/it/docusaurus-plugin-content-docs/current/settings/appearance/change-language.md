---
sidebar_position: 1
---

# Cambia lingua

[Grazie all'aiuto dei volontari](https://docs.cwtch.im/docs/contribute/translate), l'app di Cwtch è stata tradotta in molte lingue.

Per cambiare la lingua che Cwtch utilizza:

1. Apri le Impostazioni
2. L'impostazione in cima è "Lingua", è possibile utilizzare il menu a tendina per selezionare la lingua che si desidera utilizzare.