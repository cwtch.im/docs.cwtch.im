---
sidebar_position: 4
---

# Modalità Streamer/Presentazione

La modalità Streamer/Presentazione rende l'applicazione visivamente più privata. In questa modalità, Cwtch non visualizzerà sulle schermate principali alcuna informazione ausiliaria come gli indirizzi Cwtch e altre informazioni sensibili.

Questo è utile quando si catturano screenshot o in altro modo si mostra Cwtch più "pubblicamente".

1. Clicca sull'icona impostazione
2. Imposta la "Modalità Streamer" su On
3. Controlla che funzioni guardando il tuo profilo la tua lista di contatti