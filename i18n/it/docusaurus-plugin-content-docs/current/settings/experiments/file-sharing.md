---
sidebar_position: 3
---

# Condivisione file

Questa impostazione abilita la [funzionalità di condivisione dei file](/docs/chat/share-file) di Cwtch. Questa rivela l'opzione "Condividi file" nel pannello conversazione, e consente di scaricare file dalle conversazioni.

Facoltativamente, è possibile abilitare le [Anteprime immagine e immagini del profilo](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) per scaricare automaticamente i file immagine, visualizzare le anteprime immagine nella finestra di conversazione e abilitare la funzione [Immagini del Profilo](/docs/profiles/change-profile-image);

:::info

Questa pagina di documentazione è uno stub. Puoi aiutare [espandendola](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::