---
sidebar_position: 4
---

# Anteprime immagine e immagini del profilo

:::attenzione

Questo esperimento richiede l'esperimento [Condivisione file](https://docs.cwtch.im/docs/settings/experiments/file-sharing) abilitato.

:::

Se abilitato, Cwtch scaricherà automaticamente i file di immagine, visualizzerà le anteprime immagine nella finestra di conversazione e attiverà la funzione [Immagini del profilo](/docs/profiles/change-profile-image);

Su Desktop, abilitando questo esperimento, sarà possibile accedere a una ulteriore impostazione `Download Folder` che può essere modificata per dire a Cwtch dove scaricare (automaticamente) le immagini.