---
sidebar_position: 1
---

# Esperimento Gruppi

Consente a Cwtch di [connettersi a server non attendibili](/docs/servers/introduction) e di usarli per ospitare [gruppi privati, asincroni](/docs/groups/introduction).

## Per attivare

1. Vai su "Impostazioni"
2. Abilita "Esperimenti"
3. Abilita l'Esperimento Gruppi


