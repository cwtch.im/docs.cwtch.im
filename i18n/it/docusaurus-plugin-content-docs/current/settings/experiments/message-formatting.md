---
sidebar_position: 6
---

# Formattazione messaggio

Se abilitato, questo esperimento cambia la casella di composizione della conversazione per aggiungere [formattazione del messaggio](/docs/chat/message-formatting) alla UX.

Questo esperimento è ora abilitato di default.