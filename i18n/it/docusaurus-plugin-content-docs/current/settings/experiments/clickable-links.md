---
sidebar_position: 5
---

# Esperimento Link cliccabili

:::pericolo

Questa funzione, se abilitata, presenta un rischio di **deanonimizzazione**.

**Non** aprire URL provenienti da persone di cui non ti fidi. I link inviati tramite Cwtch vengono aperti tramite il browser **predefinito** del sistema. La maggior parte dei browser non può fornire anonimato.

:::

# Abilita l'Esperimento Link cliccabili

I link cliccabili **non sono** abilitati come impostazione predefinita. Per consentire a Cwtch di aprire i link nei messaggi:

1. Vai su "Impostazioni"
2. Abilita "Esperimenti"
3. Abilita l'Esperimento `Link cliccabili`

![](/img/clickable_links_experiment.png)

## Rischi

Avere link cliccabili nei messaggi é molto utile, tuttavia ci sono rischi di cui devi essere consapevole se scegli di abilitare questa funzione.

Per evitare l'attivazione accidentale, dopo aver cliccato su un link in un messaggio, Cwtch aprirà prima un prompt aggiuntivo con due opzioni:

![](/img/clickable_links.png)

1. Copia l'URL negli appunti
2. Apri l'URL nel **web browser predefinito**

È possibile utilizzare il pulsante indietro sul dispositivo, o cliccare fuori da questo prompt per evitare di selezionare una delle due opzioni.

**Cwtch non può proteggerti se apri link maligni**.

L'URL viene aperto nel **web browser predefinito** che probabilmente, come minimo, esporrá il tuo indirizzo IP al server che ospita l'URL. Le pagine web possono anche utilizzare altre vulnerabilità del browser per raccogliere altre informazioni, o attaccare il tuo computer con ulteriori exploit.
