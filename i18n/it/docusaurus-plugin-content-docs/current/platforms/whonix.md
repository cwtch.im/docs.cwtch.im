---
sidebar_position: 1
---

# Far girare Cwtch su Whonix

:::warning New Feature

New in [Cwtch 1.13](/blog/cwtch-1-13)

Questa funzionalità può essere incompleta e/o pericolosa in caso di uso improprio. Aiutaci a rivederla, e a testarla.
:::

## Configurare il Whonix-Gateway

Sul Whonix-Gateway devono essere effettuate le seguenti operazioni.

### Onion Grater

Whonix utilizza [Onion Grater](https://www.whonix.org/wiki/Onion-grater) per sorvegliare l'accesso alla porta di controllo. Abbiamo impacchettato una configurazione onion grater [`cwtch-whonix.yml` ](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/linux/cwtch-whonix.yml) che è presente nella directory root del tarball.

Questo file deve essere inserito in `/usr/share/doc/onion-grater-merger/examples/40_cwtch.yml`.

Whonix ha anche impacchettato il profilo [`40_cwtch.yml`](https://github.com/Whonix/onion-grater/blob/master/usr/share/doc/onion-grater-merger/examples/40_cwtch.yml), quindi è possibile utilizzare uno qualsiasi dei due.

Abilitare il profilo onion grater di Cwtch:
```shell
sudo onion-grater-add 40_cwtch
```

## Configurare la Whonix-Workstation

Sulla Whonix-Workstation devono essere effettuate le seguenti operazioni.

### Firewall di Linux

Il Firewall della Whonix-Workstation deve avere la porta di potenziale associazione a Cwtch aperta. Seguire la [guida del firewall upstream](https://www.whonix.org/wiki/Whonix-Workstation_Firewall#Open_an_Incoming_Port).

Utilizzare la seguente configurazione nel file `/usr/local/etc/whonix_firewall.d/50_user.conf`:
```shell
EXTERNAL_OPEN_PORTS+=" $(seq 15000 15378) "
```

### Esecuzione di Cwtch

Quando si avvia Cwtch su Whonix, alcune variabili di ambiente devono essere impostate:
- `CWTCH_TAILS=true` - configura automaticamente Cwtch per essere eseguito in un ambiente simile a Whonix.
- `CWTCH_RESTRICT_PORTS=true` - forza la connettività ad un sottoinsieme di porte `15000-15378`, più facile da gestire nella configurazione del firewall.
- `CWTCH_BIND_EXTERNAL_WHONIX=true` - forza la connettività a legarsi a interfacce esterne (opzione supportata/consigliata solo per configurazioni Whonix dove la macchina (Workstation) che esegue Cwtch si trova dietro un firewall, e dove l'unica fonte di rete è attraverso il Gateway).

```shell
cd ~/.local/lib/cwtch
env LD_LIBRARY_PATH=~/.local/lib/cwtch/:~/.local/lib/cwtch/Tor CWTCH_TAILS=true CWTCH_RESTRICT_PORTS=true CWTCH_BIND_EXTERNAL_WHONIX=true ~/.local/lib/cwtch/cwtch
```

:::info Luogo di installazione

Il comando sopra e la configurazione onion grater sotto presuppongono che Cwtch sia stato installato in `~/.local/lib/cwtch/cwtch` - se Cwtch è stato installato da qualche altra parte (o se lo si sta eseguendo direttamente dalla cartella di download) sarà quindi necessario modificare i comandi in accordo.

:::

# Rimozione di Cwtch da Whonix

## Rimuovere la configurazione dal Whonix-Gateway

Sul Whonix-Gateway devono essere effettuate le seguenti operazioni.

Disabilitare il profilo onion grater di Cwtch:
```shell
sudo onion-grater-remove cwtch-whonix
```

## Rimuovere la configurazione dalla Whonix-Workstation

Sulla Whonix-Workstation devono essere effettuate le seguenti operazioni.

Eliminare la directory dell'applicazione Cwtch `$HOME/.local/lib/cwtch` e la directory dati di Cwtch `$HOME/.cwtch`.

Le porte del firewall devono anche essere chiuse manualmente rimuovendo la configurazione aggiunta sopra e ricaricando il firewall.
