---
sidebar_position: 1
---

# Far girare Cwtch su Tails

:::warning Nuova Funzionalità

Novità in [Cwtch 1.12](/blog/cwtch-nightly-1-12)

Questa funzionalità può essere incompleta e/o pericolosa in caso di uso improprio. Aiutaci a rivederla, e a testarla.
:::

I seguenti passaggi richiedono che Tails sia stato lanciato con una [Password di amministrazione](https://tails.boum.org/doc/first_steps/welcome_screen/administration_password/).

Tails utilizza [Onion Grater](https://gitlab.tails.boum.org/tails/tails/-/blob/master/config/chroot_local-includes/usr/local/lib/onion-grater#L3) per sorvegliare l'accesso alla porta di controllo. Abbiamo impacchettato una configurazione oniongrater [`cwtch-tails.yml`](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/linux/cwtch-tails.yml) e uno script di configurazione (`install-tails.sh`) con Cwtch su Linux.

La parte dello script specifica per Tails è riprodotta di seguito:

        # Tails needs to be have been setup up with an Administration account
        # 
        # Make Auth Cookie Readable
        sudo chmod o+r /var/run/tor/control.authcookie
        # Copy Onion Grater Config
        sudo cp cwtch.yml /etc/onion-grater.d/cwtch.yml
        # Restart Onion Grater so the Config Takes effect
        sudo systemctl restart onion-grater.service

Quando si avvia, Cwtch su Tails dovrebbe ricevere la variabile di ambiente `CWTCH_TAILS=true` per configurare automaticamente l'esecuzione in un ambiente simile a Tails:

`exec env CWTCH_TAILS=true LD_LIBRARY_PATH=~/.local/lib/cwtch/:~/.local/lib/cwtch/Tor ~/.local/lib/cwtch/cwtch`

:::info Luogo di installazione

Il comando sopra e la configurazione onion grater sotto presuppongono che Cwtch sia stato installato in `~/.local/lib/cwtch/cwtch` - se Cwtch è stato installato da qualche altra parte (o se lo si sta eseguendo direttamente dalla cartella di download) sarà quindi necessario modificare i comandi in accordo.

:::

## Configurazione Onion Grater

La configurazione onion grater [`cwtch-tails.yml`](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/linux/cwtch-tails.yml) è riprodotta sotto. Come annotato, questa configurazione può probabilmente essere ulteriormente limitata. 

        ---
        # TODO: This can likely be restricted even further, especially in regards to the ADD_ONION pattern
    
        - apparmor-profiles:
            - '/home/amnesia/.local/lib/cwtch/cwtch'
          users:
    
            - 'amnesia'
          commands:
            AUTHCHALLENGE:
    
              - 'SAFECOOKIE .*'
            SETEVENTS:
    
              - 'CIRC WARN ERR'
              - 'CIRC ORCONN INFO NOTICE WARN ERR HS_DESC HS_DESC_CONTENT'
            GETINFO:
    
              - '.*'
            GETCONF:
    
              - 'DisableNetwork'
            SETCONF:
    
              - 'DisableNetwork.*'
            ADD_ONION:
    
              - '.*'
            DEL_ONION:
    
              - '.+'
            HSFETCH:
    
              - '.+'
          events:
            CIRC:
              suppress: true
            ORCONN:
              suppress: true
            INFO:
              suppress: true
            NOTICE:
              suppress: true
            WARN:
              suppress: true
            ERR:
              suppress: true
            HS_DESC:
              response:
    
            - pattern:     '650 HS_DESC CREATED (\S+) (\S+) (\S+) \S+ (.+)'
              replacement: '650 HS_DESC CREATED {} {} {} redacted {}'
    
            - pattern:     '650 HS_DESC UPLOAD (\S+) (\S+) .*'
              replacement: '650 HS_DESC UPLOAD {} {} redacted redacted'
    
            - pattern:     '650 HS_DESC UPLOADED (\S+) (\S+) .+'
              replacement: '650 HS_DESC UPLOADED {} {} redacted'
    
            - pattern:     '650 HS_DESC REQUESTED (\S+) NO_AUTH'
              replacement: '650 HS_DESC REQUESTED {} NO_AUTH'
    
            - pattern:     '650 HS_DESC REQUESTED (\S+) NO_AUTH \S+ \S+'
              replacement: '650 HS_DESC REQUESTED {} NO_AUTH redacted redacted'
    
            - pattern:     '650 HS_DESC RECEIVED (\S+) NO_AUTH \S+ \S+'
              replacement: '650 HS_DESC RECEIVED {} NO_AUTH redacted redacted'
    
            - pattern:     '.*'
              replacement: ''
            HS_DESC_CONTENT:
              suppress: true

## Persistenza

Per impostazione predefinita, Cwtch crea `$HOME/.cwtch` e salva tutti i profili crittografati e i file delle impostazioni. Per salvare eventuali profili/conversazioni in Cwtch su Tails è necessario eseguire il backup di questa cartella in una cartella home non volatile.

Vedere la documentazione di Tails per configurare l'[archivio persistente](https://tails.boum.org/doc/persistent_storage/)
