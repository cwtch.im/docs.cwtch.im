---
sidebar_position: 5
---

# Accedere alle Impostazioni della conversazione

In una finestra di conversazione, fare clic sull'icona Impostazioni nella barra superiore.

<figure>

[![](/img/conversations/settings.png)](/img/conversations/settings.png)

<figcaption></figcaption>
</figure>

Questa azione aprirà una nuova schermata dove è possibile visualizzare e gestire il contatto.

<figure>

[![](/img/conversations/settings-full.png)](/img/conversations/settings-full.png)

<figcaption></figcaption>
</figure>