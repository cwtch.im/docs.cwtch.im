---
sidebar_position: 3
---

# Condividere indirizzi Cwtch

Ci sono molti modi per condividere un indirizzo Cwtch.

## Condividere il tuo indirizzo Cwtch

1. Vai al tuo profilo
2. Fare clic sull'icona della copia indirizzo

Ora è possibile condividere questo indirizzo. Le persone con questo indirizzo saranno in grado di aggiungerti come un contatto Cwtch.

Per informazioni su come bloccare connessioni da persone che non conosci vedi [Impostazioni: Blocca connessioni sconosciute](/docs/settings/behaviour/block-unknown-connections)


# Condividere l'indirizzo Cwtch di un amico

All'interno di Cwtch c'è un altro meccanismo per lo scambio degli indirizzi Cwtch.

:::info

Questa pagina di documentazione è uno stub. Puoi aiutare [espandendola](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::