---
sidebar_position: 1.5
---

# Iniziare una nuova conversazione

1. Seleziona un profilo
2. Clicca sul pulsante "Aggiungi"
3. Scegli "Aggiungi contatto"
4. Incolla un indirizzo Cwtch
5. Il contatto verrà aggiunto alla tua lista contatti

:::info

Questa pagina di documentazione è uno stub. Puoi aiutare [espandendola](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::
