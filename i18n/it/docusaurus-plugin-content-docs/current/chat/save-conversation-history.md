---
sidebar_position: 4
---

# Salvare la cronologia delle conversazioni

Come impostazione predefinita, per la privacy, Cwtch non conserva la cronologia delle conversazioni tra diverse sessioni.

Per abilitare la cronologia per una specifica conversazione:

1. Nella finestra della conversazione vai su "Impostazioni"
2. Vai a "Salva cronologia"
3. Clicca sul menu a tendina
4. Scegli "Salva cronologia"
5. Ora la tua cronologia verrà salvata

La cronologia della conversazione può essere disattivata in qualsiasi momento selezionando "Elimina cronologia" dal menu a discesa.