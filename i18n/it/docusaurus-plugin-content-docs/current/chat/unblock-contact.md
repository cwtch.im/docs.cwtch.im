---
sidebar_position: 8
---

# Sbloccare un contatto

1. Seleziona il contatto nella tua lista conversazioni. I contatti bloccati vengono spostati in fondo alla lista.
2. Vai alle Impostazioni della conversazione
3. Scorri verso il basso fino a "Blocca il contatto"
4. Sposta l'interruttore su "Sblocca il contatto"


:::info

Questa pagina di documentazione è uno stub. Puoi aiutare [espandendola](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::