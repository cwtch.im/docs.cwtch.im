---
sidebar_position: 2
---

# Accettare/Declinare nuove conversazioni

1. Vai al tuo profilo
2. Se qualcuno ti ha aggiunto, un nuovo nome apparirà assieme a due opzioni
   1. Clicca sull'icona del cuore per accettare questo nuovo contatto
   2. Clicca sull'icona del cestino per bloccare il contatto

Vedi anche: [Impostazione: Blocca contatti sconosciuti](https://docs.cwtch.im/docs/settings/behaviour/block-unknown-connections)