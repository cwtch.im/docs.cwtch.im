---
sidebar_position: 5
---

# Rispondere a un messaggio

1. Seleziona un messaggio a cui desideri rispondere
2. Trascinalo a destra
3. Scrivi una risposta al messaggio ora citato
4. Clicca su "Invio"