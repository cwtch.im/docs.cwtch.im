---
sidebar_position: 10
---

# Rimuovere una conversazione

:::warning

Questa funzionalità comporterà l'eliminazione **irreversibile**. L'operazione **non può essere annullata**.

:::

- In una chat con un contatto, vai alle impostazioni di conversazione in alto a destra <span class="icon">![](/img/peer_settings-24px.svg)</span>
- Scorri fino al pulsante **lascia questa conversazione** e premilo.
- Ti verrà chiesto di confermare se vuoi lasciare la conversazione. Questa operazione non può essere annullata.


:::info

Questa pagina di documentazione è uno stub. Puoi aiutare [espandendola](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::
