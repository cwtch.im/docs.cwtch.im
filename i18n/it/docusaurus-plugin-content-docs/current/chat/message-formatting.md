---
sidebar_position: 4.5
---

# Formattazione messaggio

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l'[Esperimento di formattazione messaggio](https://docs.cwtch.im/docs/settings/experiments/message-formatting) attivato.

Come opzione ulteriore, puoi abilitare [Link cliccabili](https://docs.cwtch.im/docs/settings/experiments/clickable-links) per rendere gli URL nei messaggi cliccabili in Cwtch.

:::

Cwtch attualmente supporta la seguente formattazione markdown per i messaggi:

* `**grassetto**` che risulterà in **grassetto**
* `*corsivo*` che risulterà in *corsivo*
* < code > codice < /code > (senza spazi) che risulterà in `codice`
* `^apice^` che risulterà in <sup>apice</sup>
* `^pedice^` che risulterà in <sub>pedice</sub>
* `~~barrato~~` che risulterà in <del>barrato</del>
