---
sidebar_position: 7
---

# Modificare il nome di un gruppo

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l'[Esperimento Gruppi](https://docs.cwtch.im/docs/settings/experiments/group-experiment) attivato.

:::

I nomi dei gruppi sono locali, personali e privati, non saranno condivisi.

1. Nel pannello della chat vai alle impostazioni
2. Cambia il nome del gruppo
3. Clicca su "Salva"
4. Ora il tuo gruppo ha un nuovo nome

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/group_edit.mp4" />
    </video>
</div>