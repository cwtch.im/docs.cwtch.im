---
sidebar_position: 6
---

# Come lasciare un gruppo

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l'[Esperimento Gruppi](https://docs.cwtch.im/docs/settings/experiments/group-experiment) attivato.

:::

:::avviso

Questa funzione comporterà **la cancellazione irreversibile** di materiale chiave. **Non può essere annullata**.

:::

1. Nel pannello della chat vai alle impostazioni
2. Scorri verso il basso fino alla fine del pannello
3. Clicca su "lascia la conversazione"
4. Conferma che vuoi lasciare

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Leave.mp4" />
    </video>
</div>