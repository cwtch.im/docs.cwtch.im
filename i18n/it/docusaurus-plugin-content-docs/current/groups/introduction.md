---
sidebar_position: 1
---

# Un'introduzione ai gruppi di Cwtch

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](/docs/settings/introduction#experiments) e l'[Esperimento Gruppi](/docs/settings/experiments/group-experiment) attivato.

:::

**Nota: la comunicazione di gruppo resistente ai metadati è ancora un'area di ricerca attiva e ciò che è documentato qui probabilmente cambierà in futuro.**

Per impostazione predefinita, Cwtch supporta solo chat peer-to-peer e online. Per supportare le conversazioni con più parti e il recapito offline, è necessaria una terza parte (non fidata). Chiamiamo queste entità terze ["server"](/docs/servers/introduction).

Questi server possono essere creati da chiunque e sono pensati per essere sempre online. Il punto fondamentale è che tutte le comunicazioni con un server sono progettate in modo tale che il server abbia il meno possibile informazioni sui contenuti o metadati.

Per molti aspetti la comunicazione con un server è identica a quella con un normale peer Cwtch, comprende tutti gli stessi passi, tuttavia il server agisce sempre come peer in entrata, e il peer in uscita utilizza sempre una **coppia di chiavi effimera** appena generata - in modo che ogni sessione del server sia disconnessa.

Di conseguenza, le conversazioni peer-server differiscono solo nei *tipi* di messaggi inviati tra le due parti, con il server che memorizza tutti i messaggi che riceve e quindi permette a qualsiasi client di ripescare i messaggi più vecchi.

Il modello di rischio associato ai server è più complicato che la comunicazione peer-to-peer, quindi al momento richiediamo alle persone che vogliono utilizzare i server all'interno di Cwtch di [attivare l'esperimento di Chat di gruppo](/docs/settings/experiments/group-experiment) per aggiungere, gestire e creare gruppi su server non attendibili.

## Come funzionano i gruppi sotto il cappuccio

Quando una persona vuole iniziare una conversazione di gruppo, in primo luogo genera casualmente una `Chiave di gruppo` segreta. Tutte le comunicazioni del gruppo verranno cifrate utilizzando questa chiave.

Insieme alla `Chiave di gruppo`, il creatore del gruppo decide anche il **Server Cwtch** da utilizzare come host del gruppo. Per ulteriori informazioni su come i Server autenticano se stessi si vedano i [pacchetti di chiavi](https://docs.openprivacy.ca/cwtch-security-handbook/key_bundles.html).

Un `Identificatore di gruppo` è generato utilizzando la chiave di gruppo e il server di gruppo e questi tre elementi sono raggruppati in un invito che può essere inviato ai potenziali membri del gruppo (per esempio tramite connessioni peer-to-peer esistenti).

Per inviare un messaggio al gruppo, un profilo si collega al server che ospita il gruppo (vedi sotto), e crittografa il proprio messaggio con la `Chiave di gruppo` e genera una firma crittografica sotto l'`Identificatore di gruppo`, il `Server di gruppo` e il messaggio decriptato (vedi [formati di trasmissione](https://docs.openprivacy.ca/cwtch-security-handbook/message_formats.html) per ulteriori informazioni).

Per ricevere un messaggio dal gruppo, un profilo deve essere collegato al server che ospita il gruppo e scaricare *tutti* i messaggi (dall'ultima connessione precedente). I profili tentano quindi di decifrare ogni messaggio utilizzando la `Chiave di gruppo` e in caso di successo cercano di verificare la firma (vedi [Server di Cwtch](https://docs.openprivacy.ca/cwtch-security-handbook/server.html), [Gruppi di Cwtch](https://docs.openprivacy.ca/cwtch-security-handbook/groups.html) per una panoramica degli attacchi e delle mitigazioni).


