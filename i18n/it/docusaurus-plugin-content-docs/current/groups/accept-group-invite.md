---
sidebar_position: 5
---

# Accettare un Invito a un gruppo

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l'[Esperimento Gruppi](https://docs.cwtch.im/docs/settings/experiments/group-experiment) attivato.

:::

1. Se qualcuno ti invia una richiesta di partecipazione a un gruppo
2. Scegli se vuoi o meno unirti a questo gruppo
3. Ora il gruppo è nella tua lista dei contatti

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_acceptinvite.mp4" />
    </video>
</div>