---
sidebar_position: 3
---

# Creare un nuovo gruppo

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l'[Esperimento Gruppi](https://docs.cwtch.im/docs/settings/experiments/group-experiment) attivato.

:::

1. Nel pannello dei tuoi contatti
2. Clicca sul pulsante di azione "`+`"
3. Clicca su "Crea gruppo"
4. Dai un nome al tuo gruppo
5. Clicca su "Crea"

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Create.mp4" />
    </video>
</div>