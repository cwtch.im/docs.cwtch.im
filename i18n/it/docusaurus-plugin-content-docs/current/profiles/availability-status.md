---
sidebar_position: 14
---

# Impostazione dello stato di disponibilità

:::warning Nuova Funzionalità

Novità in [Cwtch 1.12](/blog/cwtch-nightly-1-12)

Questa funzionalità può essere incompleta e/o pericolosa in caso di uso improprio. Aiutaci a rivederla, e a testarla.
:::

Nel [pannello conversazioni](https://docs.cwtch.im/docs/category/conversations) fare clic sull'icona Stato accanto alla foto del profilo.

<figure>

[![](/img/profiles/status-tooltip.png)](/img/profiles/status-tooltip.png)

<figcaption></figcaption>
</figure>

Apparirà un menu a discesa con varie opzioni, ad esempio Disponibile, Assente e Non disponibile

<figure>

[![](/img/profiles/status-tooltip-busy.png)](/img/profiles/status-tooltip-busy.png)

<figcaption></figcaption>
</figure>

Quando Assente o Non disponibile vengono selezionati come stato, il bordo dell'immagine del profilo cambierà per riflettere lo stato
<figure>

[![](/img/profiles/status-tooltip-busy-set.png)](/img/profiles/status-tooltip-busy-set.png)

<figcaption></figcaption>
</figure>

I contatti vedranno questo cambiamento riflesso nel loro pannello conversazioni.

<figure>

[![](/img/profiles/status-busy.png)](/img/profiles/status-busy.png)

<figcaption></figcaption>
</figure>




