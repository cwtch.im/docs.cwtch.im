---
sidebar_position: 1.5
---

# Creare un nuovo profilo

1. Clicca sul pulsante di azione "`+`" nell'angolo in basso a destra e seleziona "Nuovo profilo"
2. Seleziona un nome da visualizzare
3. Seleziona se vuoi proteggere questo profilo localmente con crittografia avanzata:
    - Password: il tuo account è protetto da altre persone che potrebbero utilizzare questo dispositivo
    - Nessuna password: chiunque abbia accesso a questo dispositivo ha la possibilità di accedere a questo profilo
4. Inserisci nuovamente la tua password
5. Clicca su aggiungi nuovo profilo

## Una nota sui profili protetti da password (crittografati)

I profili sono memorizzati localmente sul disco e crittografati utilizzando una chiave derivata dalla password conosciuta dall'utente (tramite pbkdf2).

Nota che, una volta cifrato e memorizzato su disco, l'unico modo per recuperare un profilo comporta di riderivare la chiave dalla password - non è quindi possibile fornire un elenco completo dei profili a cui l'utente potrebbe accedere fino a quando non viene inserita una password.

Vedi anche: [Manuale sulla sicurezza di Cwtch: Crittografia del profilo & spazio di archiviazione](https://docs.openprivacy.ca/cwtch-security-handbook/profile_encryption_and_storage.html)
