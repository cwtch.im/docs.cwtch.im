---
sidebar_position: 3
---

# Modificare la tua password

1. Clicca sulla matita accanto al profilo che vuoi modificare
2. Vai a "password corrente" e inseriscila
3. Vai a "nuova password" e inserisci la tua nuova password
4. Inserisci la nuova password una seconda volta
5. Clicca su salva il profilo