---
sidebar_position: 6
---

# Eliminare un profilo

:::avviso

Questa funzione comporterà **la cancellazione irreversibile** di materiale chiave. **Non può essere annullata**.

:::

1. Clicca sulla matita accanto al profilo che vuoi modificare
2. Scorri verso il basso fino alla fine del menú
3. Clicca su elimina
4. Clicca su elimina definitivamente il profilo