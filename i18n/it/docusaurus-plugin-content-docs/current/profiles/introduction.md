---
sidebar_position: 1
---

# Un'introduzione ai profili di Cwtch

Su Cwtch puoi creare uno o piú **Profili**. Ogni profilo genera una coppia di chiavi ed25519 casuale compatibile con la rete Tor.

Questo è l'identificatore che puoi fornire ad altre persone e che possono usare per contattarti tramite Cwtch.

Cwtch ti permette di creare e gestire multipli profili separati. Ogni profilo è associato a una diversa coppia di chiavi che lancia un diverso servizio "onion".

## Gestire i profili

All'avvio Cwtch avvierà la schermata Gestione Profili. Da questa schermata è possibile:

- [Creare un nuovo profilo](https://docs.cwtch.im/docs/profiles/create-a-profile)
- [Sbloccare i profili crittografati esistenti](https://docs.cwtch.im/docs/profiles/unlock-profile)
- Gestire i profili caricati
    - [Cambiare il nome visualizzato di un profilo](https://docs.cwtch.im/docs/profiles/change-name/)
    - [Cambiare la password di un profilo](https://docs.cwtch.im/docs/profiles/change-password/)
    - [Eliminare un profilo](https://docs.cwtch.im/docs/profiles/delete-profile)
    - [Modificare l'immagine di un profilo](https://docs.cwtch.im/docs/profiles/change-profile-image/)