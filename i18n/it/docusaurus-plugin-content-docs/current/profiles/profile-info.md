---
sidebar_position: 15
---

# Impostazione degli attributi del profilo

:::warning Nuova Funzionalità

Novità in [Cwtch 1.12](/blog/cwtch-nightly-1-12)

Questa funzionalità può essere incompleta e/o pericolosa in caso di uso improprio. Aiutaci a rivederla, e a testarla.
:::

Nel [pannello di gestione del profilo](/docs/profiles/introduction#manage-profiles) ci sono tre campi a testo libero sotto la tua immagine del profilo.

<figure>

[![](/img/profiles/attributes-empty.png)](/img/profiles/attributes-empty.png)

<figcaption></figcaption>
</figure>

Puoi riempire questi campi con tutte le informazioni che desideri i tuoi potenziali contatti conoscano. **Questa informazione è pubblica** - non inserire qui alcuna informazione che non si desideri condividere con tutti.

<figure>

[![](/img/profiles/attributes-set.png)](/img/profiles/attributes-set.png)

<figcaption></figcaption>
</figure>

I contatti saranno in grado di vedere queste informazioni nelle [impostazioni di conversazione](/docs/chat/conversation-settings)

<figure>

[![](/img/profiles/attributes-contact.png)](/img/profiles/attributes-contact.png)

<figcaption></figcaption>
</figure>




