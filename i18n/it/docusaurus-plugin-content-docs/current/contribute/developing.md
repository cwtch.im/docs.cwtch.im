---
sidebar_position: 1
---

# Sviluppare per Cwtch

Questa sezione documenta alcuni modi per iniziare con lo sviluppo di Cwtch.

## Processo di tracciamento dei problemi di Cwtch

Si tiene traccia di tutti i problemi di Cwtch nel [repository git cwtch-ui](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues), anche se il bug/funzionalità ha origine in una libreria upstream. Questo ci permette di mantenere tutto in un unico posto.

I problemi sono generalmente suddivisi in 4 categorie distinte:

- **Non processati** - Si tratta di nuovi problemi che non sono stati discussi dal team di Cwtch.
- [**Pianificati**](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=open&labels=195&milestone=0&assignee=0&poster=0) - Il trattamento di questi problemi è stato pianificato per una prossima versione. Normalmente sono etichettati con la versione in cui si prevede di risolverli, ad esempio `cwtch-1.11`. Un membro del team di Cwtch sta probabilmente lavorando sui problemi in questa categoria, o prevede di lavorarci nelle prossime settimane.
- [**Auspicati**](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=open&labels=153&milestone=0&assignee=0&poster=0) - Questi sono i problemi che vorremmo risolvere ma per qualche motivo non siamo in grado di pianificare. Ciò potrebbe accadere perché la funzionalità è corposa e richiede parecchio lavoro oppure perché è presente qualche blocco (ad esempio una funzionalità mancante in Flutter o in qualche altra libreria) che ne impedisce il trattamento.
- [**Cercasi aiuto**](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=open&labels=136&milestone=0&assignee=0&poster=0) - In genere si tratta di piccoli problemi che vorremmo risolvere ma a cui è stata assegnata una priorità bassa. Questi sono ideali come primi problemi da assegnare a nuovi volontari.

Se si desidera lavorare su un problema aperto relativo a un bug/funzionalità, basta lasciare un commento nell'issue su git e un membro del team Cwtch risponderà con dei consigli su come procedere. Questo ci aiuta a tenere traccia di chi sta lavorando su quali problemi, e riduce la quantità di lavoro duplicato. Ci proponiamo di rispondere alla maggior parte delle richieste entro 24 ore, sentitevi liberi di "dare un colpo" a un'issue se ci vuole più tempo.

:::note

A causa di un problema con il nostro provider di posta elettronica, al momento non siamo in grado di inviare e-mail in modo coerente dalla nostra istanza gitea. Si prega di controllare regolarmente le issue aperte/richieste pull per aggiornamenti (o di iscriversi ai feed RSS del repository)

:::

## Processo di richiesta pull di Cwtch

Tutte le richieste di pull devono essere esaminate e approvate da uno dei membri principali del team di Cwtch prima del merging. Sarah fa la revisione di tutte le richieste di pull nuove e attive più volte alla settimana.

### Bot per le build

Tutti i progetti Cwtch sono configurati con build e test automatizzati. Ogni pull request dovrebbe essere in grado di passare attraverso queste pipeline prima di poter fare il merging. Se buildbot segnala un fallimento allora Sarah lavorerà con voi per determinare il problema, ed eventuali correzioni necessarie.

Buildbot può fallire per motivi che sfuggono al tuo controllo, ad esempio molti dei nostri test di integrazione si basano sulla creazione di connessioni Tor, e queste possono essere fragili occasionalmente, e causare timeout e fallimenti. Confermare sempre la causa principale del fallimento di un test prima di decidere cosa fare dopo.


## Risorse utili

- [Panoramica dell'ecosistema di Cwtch](/security/components/ecosystem-overview) - un riassunto dei repository Cwtch attivi dal Manuale dello sviluppo sicuro di Cwtch.
- [Contribuire alla documentazione](/docs/contribute/documentation) - consigli su come contribuire alla documentazione Cwtch.
- [Contribuire ai test](/docs/contribute/testing) - consigli su come contribuire ai test di Cwtch.
- [Contribuire alle Traduzioni](/docs/contribute/translate) - consigli per contribuire alle traduzioni di Cwtch.


:::note

Tutti i contributi consentono di [ottenere degli adesivi](/docs/contribute/stickers)

:::