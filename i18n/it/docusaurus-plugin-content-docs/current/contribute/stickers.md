---
sidebar_position: 10
---

# Adesivi

Tutti i contributi consentono di ottenere degli adesivi. Se state contribuendo a bug, funzionalità, test o traduzione, o avete contribuito in modo significativo in passato, scrivete a erinn@openprivacy.ca con i dettagli e un indirizzo a cui possiamo spedire gli adesivi per posta.

![Una foto degli adesivi di Cwtch](/img/stickers-new.jpg)

