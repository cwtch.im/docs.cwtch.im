---
sidebar_position: 2
---

# Traduzioni di Cwtch

Se vuoi contribuire con delle traduzioni all'applicazione di Cwtch o a questo manuale, ecco come fare

## Contribuire alle traduzioni dell'applicazione Cwtch

Ci sono due modi per contribuire alle traduzioni di Cwtch.

### Unisciti al nostro Team Lokalise

Usiamo [Lokalise](https://lokalise.com) per gestire le traduzioni per l'applicazione Cwtch.

1. Registrare un account su Lokalise
2. Mandare una e-mail a [team@cwtch.im](mailto:team@cwtch.im) con la lingua che si e' interessati a tradurre e un'e-mail a cui possiamo mandare l'invito al nostro team di Lokalise

### Direttamente da Git

Per le nuove traduzioni, si può fare una copia di [https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/intl_en.arb](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/intl_en.arb) e iniziare a tradurre - si può quindi o inviare [richieste di pull](/docs/contribute/developing#cwtch-pull-request-process) o inviare aggiornamenti direttamente a noi (team@cwtch.im) e ne faremo il merging.

Per aggiungere alle traduzioni esistenti è possibile effettuare richieste pull direttamente su qualsiasi file in [https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/](https://git.openprivacy.ca/cwtch.im/cwtch-ui/src/branch/trunk/lib/l10n/) e le esamineremo e ne faremo il merging.

## Manuale utente di Cwtch

Questo manuale è tradotto attraverso [Crowdin](https://crowdin.com).

Per unirsi al nostro progetto su Crowdin:

1. Registrare un account su [Crowdin](https://crowdin.com).
2. Unirsi al progetto [cwtch-users-handbook](https://crowdin.com/project/cwtch-users-handbook).

Raggruppiamo le modifiche alla documentazione in batch e le sincronizziamo regolarmente con il progetto Crowdin.

:::note

Tutti i contributi consentono di [ottenere degli adesivi](/docs/contribute/stickers)

:::