---
sidebar_position: 1
---

# Testare Cwtch

Questa sezione documenta alcuni modi per iniziare a testare Cwtch.

### Far girare Fuzzbot

FuzzBot è il nostro bot di test di sviluppo. Puoi aggiungere FuzzBot come contatto: `cwtch:4y2hxlxqzautabituedksnh2ulcgm2coqbure6wvfpg4gi2ci25ta5ad`.

:::Info Aiuto FuzzBot

L'invio a FuzzBot del messaggio `aiuto` lo attiverà ad inviare una risposta con tutti i comandi di test attualmente disponibili.

:::

Per maggiori informazioni su FuzzBot consulta il nostro [blog sullo sviluppo Discreet Log](https://openprivacy.ca/discreet-log/07-fuzzbot/).

### Unisciti al gruppo dei tester delle versioni di Cwtch candidate alla pubblicazione ufficiale

L'invio a Fuzzbot del comando `testgroup-invite` farà sì che FuzzBot ti inviti al **gruppo di tester di Cwtch**! Lì puoi fare domande, inviare segnalazioni di bug e offrire feedback.

### Cwtch Nightlies

Le nightly build di Cwtch sono build di sviluppo che contengono nuove funzionalità che sono pronte per essere testate.

Le versioni di sviluppo più recenti di Cwtch sono disponibili dal nostro [build server](https://build.openprivacy.ca/files/).

**Non** raccomandiamo che i tester si tengano sempre aggiornati con l'ultima nightly build. Invece, pubblicheremo un messaggio sul gruppo Tester di versioni di Cwtch candidate al rilascio quando sarà disponibile una nightly build significativa. Una nightly build è considerata significativa se contiene una nuova funzione o un fix a un bug serio.

:::note

Tutti i contributi consentono di [ottenere degli adesivi](/docs/contribute/stickers)

:::

### Invio di feedback

Ci sono tre modi principali per inviare feedback sui test al team:

* Via Cwtch: tramite il gruppo dei tester delle versioni candidate per il rilascio, o direttamente a un membro del team Cwtch.
* Via Gitea: si prega di aprire un'issue in [https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues) - non occorre preoccuparsi di eventuali issue duplicate, le deduplicheremo come parte del nostro processo di triage.
* Via Email: si mandi una e-mail a `team@cwtch.im` con il bug report e uno dei nostri membri del team lo esaminerà.

:::note

A causa di un problema con il nostro provider di posta elettronica, al momento non siamo in grado di inviare e-mail in modo coerente dalla nostra istanza gitea. Si prega di controllare regolarmente le issue aperte/le richieste pull per aggiornamenti (o di iscriversi ai feed RSS del repository)

:::