---
sidebar_position: 6
---

# Guida allo stile della documentazione

Questa sezione documenta la struttura attesa e la qualità della documentazione Cwtch.

## Screenshot e cast dei personaggi

La maggior parte della documentazione di Cwtch dovrebbe contenere almeno uno screenshot o un'immagine animata. Gli screenshot dell'applicazione Cwtch dovrebbero essere focalizzati sulla funzionalità descritta dalla documentazione.

Per garantire coerenza tra gli screenshot, suggeriamo che ogni profilo coinvolto svolga ruoli particolari e costanti.

- **Alice** - rappresenta il profilo primario
- **Bob** - il contatto principale, utile per dimostrare le funzionalità peer-to-peer
- **Carol** - un contatto secondario, utile per dimostrare le funzionalità di gruppo
- **Mallory** - rappresenta un peer maligno (da usare quando si dimostra la funzionalità di blocco)

## Dialogo e contenuti

Laddove screenshot e dimostrazioni mostrino dialogo, conversazioni e/o immagini, si prega di mantenere le conversazioni brevi, e su argomenti casual. Esempi sono:

- Organizzare un picnic
- Condividere le foto di una vacanza
- Inviare un documento per la revisione

## Esperimenti

Tutte le funzionalità che si basano sul fatto che un esperimento sia abilitato, dovrebbero mostrarlo in maniera ben visibile nella parte superiore della pagina, ad esempio:

:::attenzione Esperimenti necessari

Questa funzione richiede [Experimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e [Esperimento di esempio](https://docs.cwtch.im/docs/settings/experiments/server-hosting) attivato.
:::

## Rischi

Se una funzionalità potrebbe comportare la distruzione del materiale chiave o la cancellazione permanente dello stato, anche questi rischi dovrebbero essere menzionati nella parte superiore della documentazione, ad esempio:


:::warning

Questa funzione comporterà la cancellazione **irreversibile** di materiale chiave. L'operazione **non può essere annullata**.

:::