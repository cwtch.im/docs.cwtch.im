# Piattaforme supportate

La tabella seguente rappresenta la nostra attuale comprensione del supporto di Cwtch su vari sistemi operativi e architetture (a partire da Cwtch 1.10 e gennaio 2023).

In molti casi siamo alla ricerca di tester per confermare che varie funzionalità abbiano il comportamento atteso. Invitiamo chi abbia interesse a testare Cwtch su una piattaforma specifica o voglia offrire lavoro volontario per aiutarci a supportare ufficialmente una piattaforma non elencata qui, a dare un'occhiata a [Contribuire a Cwtch](/docs/category/contribute).

**Legenda:**

- ✅: **Ufficialmente Supportato**. Cwtch dovrebbe funzionare senza problemi su queste piattaforme. Le regressioni sono considerate come prioritarie.
- 🟡: **Supportato per quanto possibile**. Cwtch dovrebbe funzionare su queste piattaforme, ma ci possono essere problemi documentati o sconosciuti. Può essere necessario effettuare dei test. Alcune funzionalità possono richiedere un lavoro aggiuntivo. Lavoro volontario è molto apprezzato.
- ❌: **Non Supportato**. È improbabile che Cwtch funzioni su questi sistemi. Probabilmente non accetteremo segnalazioni di bug per questi sistemi.

| Piattaforma                 | Build ufficiali di Cwtch | Supporto sorgente | Note                                                                                                                                                              |
| --------------------------- | ------------------------ | ----------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Windows 11                  | ✅                        | ✅                 | Solo amd64 a 64-bit.                                                                                                                                              |
| Windows 10                  | ✅                        | ✅                 | Solo amd64 64-bit. Non supportato ufficialmente, ma le build ufficiali possono funzionare.                                                                        |
| Windows 8 and below         | ❌                        | 🟡                 | Non supportato. Build dal sorgente dedicate possono funzionare. Test Necessari.                                                                                   |
| OSX 10 e precedenti         | ❌                        | ❌                 | Solo 64-bit. Le build ufficiali sono state segnalate come funzionanti su Catalina ma non su High Sierra.                                                          |
| OSX 11                      | ✅                        | ✅                 | Solo 64-bit. Le build ufficiali supportano entrambe le architetture arm64 e x86.                                                                                  |
| OSX 12                      | ✅                        | ✅                 | Solo 64-bit. Le build ufficiali supportano entrambe le architetture arm64 e x86.                                                                                  |
| OSX 13                      | ✅                        | ✅                 | Solo 64-bit.  Le build ufficiali supportano entrambe le architetture arm64 e x86.                                                                                 |
| Debian 11                   | ✅                        | ✅                 | Solo amd64 64-bit.                                                                                                                                                |
| Debian 10                   | 🟡                        | ✅                 | Solo amd64 64-bit.                                                                                                                                                |
| Debian 9 e precedenti       | 🟡                        | ✅                 | Solo amd64 64-bit. Le build dal sorgente dovrebbero funzionare, ma le build ufficiali potrebbero essere incompatibili con alcune dipendenze installate.           |
| Ubuntu 22.04                | ✅                        | ✅                 | Solo amd64 64-bit.                                                                                                                                                |
| Altre versioni di Ubuntu    | 🟡                        | ✅                 | Solo 64-bit. Test necessari. Le build dal sorgente dovrebbero funzionare, ma le build ufficiali potrebbero essere incompatibili con alcune dipendenze installate. |
| Tails                       | ✅                        | ✅                 | 64-bit amd64 Only.                                                                                                                                                |
| CentOS                      | 🟡                        | 🟡                 | Test necessari.                                                                                                                                                   |
| Gentoo                      | 🟡                        | 🟡                 | Test necessari.                                                                                                                                                   |
| Arch                        | 🟡                        | 🟡                 | Test necessari.                                                                                                                                                   |
| Whonix                      | 🟡                        | 🟡                 | Help us [test this](https://docs.cwtch.im/docs/platforms/whonix)                                                                                                  |
| Raspian (arm64)             | 🟡                        | ✅                 | Le build dal sorgente funzionano.                                                                                                                                 |
| Altre distribuzioni Linux   | 🟡                        | 🟡                 | Test necessari.                                                                                                                                                   |
| Android 9 e precedenti      | 🟡                        | 🟡                 | Le build ufficiali possono funzionare.                                                                                                                            |
| Android 10                  | ✅                        | ✅                 | L'SDK ufficiale supporta le architetture arm, arm64, e amd64.                                                                                                     |
| Android 11                  | ✅                        | ✅                 | L'SDK ufficiale supporta le architetture arm, arm64, e amd64.                                                                                                     |
| Android 12                  | ✅                        | ✅                 | L'SDK ufficiale supporta le architetture arm, arm64, e amd64.                                                                                                     |
| Android 13                  | ✅                        | ✅                 | L'SDK ufficiale supporta le architetture arm, arm64, e amd64.                                                                                                     |
| LineageOS                   | ✅                        | ✅                 | L'SDK ufficiale supporta le architetture arm, arm64, e amd64.                                                                                                     |
| Altre distribuzioni Android | 🟡                        | 🟡                 | Test necessari.                                                                                                                                                   |
