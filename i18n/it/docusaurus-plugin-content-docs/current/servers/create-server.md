---
sidebar_position: 2
---

# Come creare un server

:::attenzione Esperimenti necessari

Questa funzione richiede [Esperimenti abilitati](https://docs.cwtch.im/docs/settings/introduction#experiments) e l' [Esperimento di server hosting](https://docs.cwtch.im/docs/settings/experiments/server-hosting) attivato.

:::

1. Vai all'icona del server
2. Premi il pulsante di azione "`+`" per creare un nuovo server
3. Scegli un nome per il tuo server
4. Seleziona una password per il tuo server
5. Clicca su “Aggiungi server”

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_New.mp4" />
    </video>
</div>