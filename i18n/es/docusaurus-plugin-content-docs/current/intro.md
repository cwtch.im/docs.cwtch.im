---
sidebar_position: 1
---

# ¿Qué es Cwtch?

Cwtch (/kʊtʃ/ - Una palabra galesa más o menos traducida a “un abrazo que crea un lugar seguro”) es una aplicación de mensajería descentralizada, que preserva la privacidad.

* **Descentralizado y Abierto**: No hay "Servicio de Cwtch" o "Red de Cwtch". Los participantes en Cwtch pueden albergar sus propios espacios seguros o prestar su infraestructura a otros que buscan un espacio seguro. El protocolo Cwtch es abierto, y cualquiera es libre de construir bots, servicios e interfaces de usuario e integrar e interactuar con Cwtch.
* **Preservación de la privacidad**: Toda la comunicación en Cwtch está cifrada de extremo a extremo y tiene lugar en los servicios de Onion Tor v3.
* **Resistente a los metadatos**: Cwtch ha sido diseñada de tal manera que no hay información intercambiada o disponible para nadie sin su consentimiento explícito, incluyendo mensajes on-the-wire y metadatos de protocolo.


# Seguridad y cifrado

Para ver en profundidad la tecnología de seguridad, privacidad y cifrado subyacente utilizada en Cwtch, por favor consulte nuestro [Manual de seguridad](https://docs.openprivacy.ca/cwtch-security-handbook/)

# Primeros pasos

Puedes descargar la última versión de Cwtch desde [https://cwtch.im/download/](https://cwtch.im/download/)

