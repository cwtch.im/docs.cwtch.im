---
sidebar_position: 3
---

# Cómo editar un servidor

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/server-hosting) activado.

:::

1. Ve al icono del servidor
2. Selecciona el servidor que quieres editar
3. Pulsa el icono de lápiz
4. Cambia la descripción/ o activa o desactiva el servidor
5. Haz clic en guardar el servidor

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/server_edit.mp4" />
    </video>
</div>