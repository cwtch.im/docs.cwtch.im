---
sidebar_position: 5
---

# Cómo compartir tu Paquete de Claves del Servidor

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/server-hosting) activado.

:::

El Paquete de Claves del Servidor es el paquete de datos que una aplicación Cwtch necesita para poder usar un servidor. Si sólo quieres hacer que otros usuarios de Cwtch tengan conocimiento de tu servidor, puedes compartir ésto con ellos. Entonces tendrán la habilidad de crear sus propios grupos en el servidor.

1. Ve al icono del servidor
2. Selecciona el servidor que quieras
3. Usa el icono de copiar la dirección para copiar las claves del servidor
4. No compartas las claves con gente en la que no confías

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Keys.mp4" />
    </video>
</div>