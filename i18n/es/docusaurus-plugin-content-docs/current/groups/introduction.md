---
sidebar_position: 1
---

# Una Introducción a los Grupos de Cwtch

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](/docs/settings/introduction#experiments) y el [Experimento de Grupos](/docs/settings/experiments/group-experiment) activado.

:::

**Nota: la Comunicación de Grupos Resistentes a los Metadatos sigue siendo un área de investigación activa y lo que se documenta aquí probablemente cambie en el futuro.**

De forma predeterminada, Cwtch sólo soporta chat de persona a persona. Para soportar conversaciones de varias personas y entrega offline, se requiere un tercero (no confiable). Llamamos a estas entidades ["servidores"](/docs/servers/introduction)

Estos servidores pueden ser configurados por cualquiera y están pensados para estar siempre en línea. Lo más importante es que toda la comunicación con un servidor está diseñada de tal manera que el servidor aprenda la menor información posible sobre los contenidos o metadatos.

En muchos sentidos, la comunicación con un servidor es idéntica a la comunicación con un contacto regular de Cwtch. Se toman todos los mismos pasos, sin embargo el servidor siempre actúa como el par entrante, y el par saliente siempre utiliza **par de claves efímeras** - para que cada sesión del servidor esté desconectada.

Como tal, las conversaciones entre pares y servidores solo difieren en los *tipos* de mensajes que se envían entre las dos partes, con el servidor guardando todos los mensajes que recibe y permitiendo así que cualquier cliente busque mensajes antiguos.

The risk model associated with servers is more complicated that peer-to-peer communication, as such we currently require people who want to use servers within Cwtch to [opt-in to the Group Chat experiment](/docs/settings/experiments/group-experiment) in order to add, manage and create groups on untrusted servers.

## Cómo funcionan los Grupos en profundidad

Cuando una persona quiere iniciar una conversación grupal, primero genera aleatoriamente una `clave grupal` secreta. Todas las comunicaciones del grupo serán cifradas usando esta clave.

Junto con la `clave de grupo`, el creador del grupo también decide el**Servidor de Cwtch** para usar como anfitrión del grupo. Para más información sobre cómo los servidores se autentican ver [paquetes de claves](https://docs.openprivacy.ca/cwtch-security-handbook/key_bundles.html).

Un `Identificador de grupo` se genera usando la clave de grupo y el servidor de grupo. Estos tres elementos se empaquetan en una invitación que puede ser enviada a los miembros potenciales del grupo (i.e a travez de las conexiones peer-to-peer existentes).

Para enviar un mensaje al grupo, un perfil se conecta al servidor que aloja al grupo (ver abajo), y encripta su mensaje usando la `Clave de Grupo` y genera una firma criptográfica a través de la `Identificación de grupo`, `Servidor de Grupo` y el mensaje descifrado (Ve: [Formatos wire](https://docs.openprivacy.ca/cwtch-security-handbook/message_formats.html) para más información).

Para recibir mensajes del grupo, un perfil debe estar conectado al servidor que aloja el grupo y descarga *todos* los mensajes (desde su conexión anterior). Los perfiles intentan descifrar cada mensaje usando la `Clave de Grupo` si logran verificar la firma (ve [servidores de Cwtch](https://docs.openprivacy.ca/cwtch-security-handbook/server.html),  [Grupos de Cwtch](https://docs.openprivacy.ca/cwtch-security-handbook/groups.html) para una visión general de los ataques y mitigaciones).


