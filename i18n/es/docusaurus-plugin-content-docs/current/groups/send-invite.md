---
sidebar_position: 4
---

# Enviar invitaciones a un Grupo

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/group-experiment) activado.

:::

1. Ir a un chat con un amigo
2. Pulsa en el icono de invitación
3. Selecciona el grupo al que deseas invitar a tu amigo
4. Pulsa Invitar
5. Has enviado una invitación

<div>
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Invite.mp4" />
    </video>
</div>