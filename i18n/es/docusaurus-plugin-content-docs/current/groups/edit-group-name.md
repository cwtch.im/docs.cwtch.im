---
sidebar_position: 7
---

# Editar Nombre de un Grupo

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/group-experiment) activado.

:::

Los nombres de grupo son privados para ti, no serán compartidos, es tu nombre local para el grupo.

1. En el panel de chat ir a la configuración
2. Cambia el nombre del grupo
3. Pulsa el botón guardar
4. Ahora tu grupo tiene un nuevo nombre

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/group_edit.mp4" />
    </video>
</div>