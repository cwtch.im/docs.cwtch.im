---
sidebar_position: 3
---

# Crea un grupo nuevo

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/group-experiment) activado.

:::

1. En el panel de contactos
2. Pulse en el botón de acción +
3. Pulsa en Crear Grupo
4. Nombra tu grupo
5. Pulsa en Crear Grupo

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Create.mp4" />
    </video>
</div>