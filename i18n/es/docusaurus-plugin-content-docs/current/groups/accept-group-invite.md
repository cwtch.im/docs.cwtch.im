---
sidebar_position: 5
---

# Aceptar una invitación de grupo

:::caution Experimentos Requeridos

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/group-experiment) activado.

:::

1. Si un amigo te envía una solicitud de grupo
2. Elige si quieres o no unirte a este grupo
3. Ahora el grupo está en tu lista de contactos

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_acceptinvite.mp4" />
    </video>
</div>