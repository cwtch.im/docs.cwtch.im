---
sidebar_position: 1
---

# Una Introducción a los Perfiles de Cwtch

Con Cwtch puedes crear uno o más **Perfiles**. Cada perfil genera un par de claves aleatorias ed25519 compatibles con la Red Tor.

Este es el identificador que puedes dar a tus contactos y que pueden usar para contactarte a través de Cwtch.

Cwtch te permite crear y administrar perfiles múltiples y separados. Cada perfil está asociado con un par de claves diferente que lanza un servicio de Onion diferente.

## Administrar Perfiles

Al iniciarse Cwtch lanzará la pantalla Administrar Perfiles. Desde esta pantalla puedes:

- [Crear un nuevo perfil](https://docs.cwtch.im/docs/profiles/create-a-profile)
- [Desbloquear perfiles cifrados existentes](https://docs.cwtch.im/docs/profiles/unlock-profile)
- Administrar Perfiles Cargados
    - [Cambiar el Nombre Para Mostrar de un Perfil](https://docs.cwtch.im/docs/profiles/change-name/)
    - [Cambiar la Contraseña de un Perfil](https://docs.cwtch.im/docs/profiles/change-password/)
    - [Eliminar un perfil](https://docs.cwtch.im/docs/profiles/delete-profile)
    - [Cambiar tu imagen de perfil](https://docs.cwtch.im/docs/profiles/change-profile-image/)