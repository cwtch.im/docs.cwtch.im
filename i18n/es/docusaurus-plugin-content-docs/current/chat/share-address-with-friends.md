---
sidebar_position: 3
---

# Sharing Cwtch Addresses

There are many ways to share a Cwtch address.

## Sharing Your Cwtch Address

1. Ve a tu perfil
2. Haz clic en el icono de copiar dirección

Ahora puedes compartir esta dirección. Las personas con esta dirección podrán añadirte como un contacto de Cwtch.

Para obtener información sobre cómo bloquear conexiones de personas que no conoces, por favor consulta [Configuración: Bloquear Conexiones Desconocidas](/docs/settings/behaviour/block-unknown-connections)


# Sharing A Friends Cwtch Address

Inside of Cwtch there is another mechanism for exchanging Cwtch addresses.

:::info

This documentation page is a stub. You can help by [expanding it](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::