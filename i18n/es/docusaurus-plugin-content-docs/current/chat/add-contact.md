---
sidebar_position: 1.5
---

# Starting a New Conversation

1. Selecciona un perfil
2. Haz clic en el botón Añadir
3. Selecciona 'Agregar contacto'
4. Pega una dirección de Cwtch
5. El contacto se añadirá a tu lista de contactos

:::info

This documentation page is a stub. You can help by [expanding it](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::
