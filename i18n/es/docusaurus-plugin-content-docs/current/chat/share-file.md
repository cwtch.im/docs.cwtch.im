---
sidebar_position: 6
---

# Compartir un archivo

:::caution Experiments Required

Esta función requiere [Experimentos habilitados](https://docs.cwtch.im/docs/settings/introduction#experiments) y el [Experimento de Grupos](https://docs.cwtch.im/docs/settings/experiments/file-sharing) activado.

Optionally, you can enable [Image Previews and Profile Pictures](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) to see display shared image previews in the conversation window.

:::

En una conversación,

1. Haz clic en el icono de adjuntar archivo
2. Encuentra el archivo que quieres enviar
3. Confirma que quieres enviarlo

## ¿Cómo funciona compartir archivos con grupos? ¿Mis archivos se almacenan en algún servidor?

Los archivos se envían a través de conexiones Cwtch Onion a Onion directamente de la persona que ofrece el archivo a la persona que lo recibe. La oferta inicial de enviar un archivo se publica como un mensaje estándar de Cwtch de conversación/superposición. Para grupos, esto significa que la oferta inicial (que contiene el nombre del archivo, el tamaño, el hash y un nonce) se publica en el servidor de grupos, pero entonces cada destinatario se conecta a usted individualmente para recibir el contenido real del archivo.

## ¿Significa esto que tengo que estar en linea para enviar un archivo?

Sí. Si la persona que ofrece el archivo se desconecta, tendrás que esperar a que se conecte para reanudar la transferencia de archivos. El protocolo subyacente divide los archivos en fragmentos individuales y verificables, para que en una versión futura puedas "rehost" un archivo publicado en un grupo, e incluso descargar desde múltiples hosts a la vez (como bittorrent).

## ¿Por qué aparecen contactos nuevos en mi lista?

Esto se debe a la forma en que Cwtch maneja actualmente conexiones desde direcciones desconocidas. Puesto que publicar un archivo en un grupo resulta en que los miembros del grupo se conecten directamente contigo, algunos de esos miembros pueden no estar en tu lista de contactos y por lo tanto su conexión de descarga contigo aparecerá en tu lista como una solicitud de contacto.

## ¿Qué es "SHA512"?

SHA512 es una [Función hash criptográfica](https://en.wikipedia.org/wiki/Cryptographic_hash_function) que puede utilizarse para verificar que el archivo que descargaste es una copia correcta del archivo que se ofreció. Cwtch realiza esta verificación por ti automáticamente, ¡pero eres bienvenido a probarlo tú mismo! Ten en cuenta que incluimos una nonce aleatoria con invitacions de archivos, así que un contacto no puede pedirte cualquier hash aleatorio que tengas o archivos de conversaciones de las que no formen parte.

## ¿Hay un límite de tamaño de archivo?

El límite actual es de 10 gigabytes por archivo.

## ¿Qué son los archivos .manifest?

Los archivos .manifest se utilizan al descargar el archivo para verificar que los fragmentos individuales se reciben correctamente, y reanudar transferencias interrumpidas. También contienen la información de la oferta original de archivos. Puedes borrarlos sin problemas una vez que la descarga haya finalizado. En Android, los manifiestos se almacenan en la caché de la aplicación, y se pueden borrar a través de la configuración del sistema.

## ¿Qué pasa con los metadatos de archivos?

Enviamos el nombre del archivo como sugerencia y le ayudamos a distinguirlo de otras invitaciones de archivos. El camino completo se despoja antes de enviar el archivo. Debes tener cuidado con los metadatos ocultos que pueden almacenarse en el archivo en sí, que varía dependiendo del formato del archivo. Por ejemplo, las imágenes pueden contener información sobre geolocalización e información sobre la cámara que las tomó. Los archivos PDF son notorios por contener información oculta como el nombre del autor o la máquina en la que fueron creados. En general, sólo deberías enviar y recibir archivos de personas en las que confíes.

## ¿Puedo descargar archivos automáticamente?

If the [Image Previews and Profile Pictures experiment](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) is enabled then Cwtch will automatically download images from accepted conversations
