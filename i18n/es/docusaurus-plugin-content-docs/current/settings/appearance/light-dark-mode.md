---
sidebar_position: 2
---

# Explicación de temas Claros/Oscuros

1. Pulsa el icono de configuración
2. Puedes elegir tema claro u oscuro activando el interruptor de "usar temas claros"
3. Usando el menú desplegable "temas de color", elige un tema que te guste
4.
   1. Cwtch: tonos morados
   2. Fantasma: Tonos grises
   3. Sirena: Tonos turquesa y morados
   4. Medianoche: Tonos negros y grises
   5. Neón 1: Tonos morados y rosados
   6. Neón 2: Tonos morados y turquesa
   7. Calabaza: Tonos morados y naranjas
   8. Bruja: Tonos verdes y rosas
   9. Vampiro: Tonos morados y rojos