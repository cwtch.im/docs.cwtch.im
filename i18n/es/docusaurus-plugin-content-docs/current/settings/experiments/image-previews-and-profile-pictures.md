---
sidebar_position: 4
---

# Vista previa de imágenes y fotos de perfil

:::caution

This experiment requires the  [File Sharing](https://docs.cwtch.im/docs/settings/experiments/file-sharing) experiment enabled.

:::

When enabled, Cwtch will download image files automatically, display image previews in the conversation window, and enable the [Profile Pictures](/docs/profiles/change-profile-image) feature;

On Desktop, enabling this experiment will allow access to an additional setting `"`Download Folder` which can be changed to tell Cwtch where to (automatically) download pictures.