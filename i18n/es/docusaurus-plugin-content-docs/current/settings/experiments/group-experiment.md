---
sidebar_position: 1
---

# Groups Experiment

Enables Cwtch to [connect to untrusted servers](/docs/servers/introduction) and use them to [host private, asynchronous, groups](/docs/groups/introduction).

## To Turn On

1. Ir a Configuración
2. Habilitar Experimentos
3. Habilitar el Experimento de Grupos


