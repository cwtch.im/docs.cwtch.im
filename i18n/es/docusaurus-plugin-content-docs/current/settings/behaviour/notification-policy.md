---
sidebar_position: 2
---

# Política de notificaciones

1. Ir a la configuración
2. Desplazar al comportamiento
3. La política de notificación controla el comportamiento de las notificaciones
4. Haz clic en Predeterminado para cambiar el comportamiento para optar o silenciar todas las notificaciones
5. Elige tu política de notificación preferida