---
sidebar_position: 1
---

# Bloquear Conexiones Desconocidas

By default, Cwtch interprets connections from unknown Cwtch addresses as [Contact Requests](https://docs.cwtch.im/docs/chat/accept-deny-new-conversation). Puede cambiar este comportamiento a través de la configuración de Bloquear Conexiones Desconocidas.

Si está activado, Cwtch cerrará automáticamente todas las conexiones de las direcciones de Cwtch que no has añadido a tu lista de contactos. This will prevent people who have your Cwtch address from contacting you unless you also add them.

Para habilitar:

1. Ve a la Configuración
2. Habilita Bloquear Contactos Desconocidos