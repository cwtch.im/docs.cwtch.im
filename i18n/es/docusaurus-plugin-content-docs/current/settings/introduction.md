---
sidebar_position: 1
---

# Una Introducción a la configuración de Cwtch

## Apariencia

These are settings which effect how Cwtch looks, including themes and localization.

## Comportamiento

These settings impact how Cwtch responds to certain events e.g. notifications for new messages, or requests from unknown public addresses.

## Experimentos

Hay muchas características en Cwtch que a los usuarios les gustaría tener pero cuya implementación requiere metadatos adicionales, o riesgo, más allá del mínimo que Cwtch requiere para operaciones básicas.

Como tal en **Experimentos** encontrarás un número de **ajustes opcionales** que, cuando se activan, proporcionan características adicionales como chat de grupo, intercambio de archivos o formato de mensaje.

Deberías pensar detenidamente al habilitar estas características sobre los nuevos riesgos que podrían implicar, y si estás cómodo optando por esos riesgos. Para muchos, los beneficios de compartir archivos, las previsualizaciones de imágenes y el chat de grupo superan con creces los daños potenciales - pero para otros requerimos que los usuarios opten por usar estas características.

Puede optar por no hacerlo en cualquier momento, todas las características se implementan localmente dentro de la aplicación Cwtch.

