---
sidebar_position: 1
---

# Cwtch Security Handbook

Welcome to the Cwtch Secure Development Handbook! The purpose of this handbook is to provide a guide to the various components of the Cwtch ecosystem, to document the known risks and mitigations, and to enable discussion about improvements and updates to Cwtch secure development processes.

![](https://docs.openprivacy.ca/cwtch-security-handbook/2.png)


## What is Cwtch?

Cwtch (/kʊtʃ/ - a Welsh word roughly translating to “a hug that creates a safe place”) is a decentralized, privacy-preserving, multi-party messaging protocol that can be used to build metadata resistant applications.

* **Decentralized and Open**: There is no “Cwtch service” or “Cwtch network”. Participants in Cwtch can host their own safe spaces, or lend their infrastructure to others seeking a safe space. The Cwtch protocol is open, and anyone is free to build bots, services and user interfaces and integrate and interact with Cwtch.
* **Privacy Preserving**: All communication in Cwtch is end-to-end encrypted and takes place over Tor v3 onion services.
* **Metadata Resistant**: Cwtch has been designed such that no information is exchanged or available to anyone without their explicit consent, including on-the-wire messages and protocol metadata.


## A (Brief) History of Metadata Resistant Chat

In recent years, public awareness of the need and benefits of end-to-end encrypted solutions has increased with applications like [Signal](https://signalapp.org), [Whatsapp](https://whatsapp.com) and [Wire](https://wire.org) now providing users with secure communications.

However, these tools require various levels of metadata exposure to function, and much of this metadata can be used to gain details about how and why a person is using a tool to communicate. [[rottermanner2015privacy]](https://www.researchgate.net/profile/Peter_Kieseberg/publication/299984940_Privacy_and_data_protection_in_smartphone_messengers/links/5a1a9c29a6fdcc50adeb1335/Privacy-and-data-protection-in-smartphone-messengers.pdf).

One tool that did seek to reduce metadata is [Ricochet](https://ricochet.im) first released in 2014. Ricochet used Tor v2 onion services to provide secure end-to-end encrypted communication, and to protect the metadata of communications.

There were no centralized servers that assist in routing Ricochet conversations. No one other than the parties involved in a conversation could know that such a conversation is taking place.

Ricochet wasn't without limitations; there was no multi-device support, nor is there a mechanism for supporting group communication or for a user to send messages while a contact is offline.

This made adoption of Ricochet a difficult proposition; with even those in environments that would be served best by metadata resistance unaware that it exists [[ermoshina2017can]](https://www.academia.edu/download/53192589/ermoshina-12.pdf) [[renaud2014doesn]](https://eprints.gla.ac.uk/116203/1/116203.pdf).

Additionally, any solution to decentralized, metadata resistant communication faces [fundamental problems](https://code.briarproject.org/briar/briar/-/wikis/Fundamental-Problems) when it comes to efficiency, privacy and group security (as defined by [transcript consensus and consistency](https://code.briarproject.org/briar/briar/-/wikis/Fundamental-Problems)).

Modern alternatives to Ricochet include [Briar](https://briarproject.org), [Zbay](https://www.zbay.app/) and [Ricochet Refresh](https://www.ricochetrefresh.net/) - each tool seeks to optimize for a different set of trade-offs e.g. Briar seeks to allow people to communicate [even when underlying network infrastructure is down](https://briarproject.org/how-it-works/) while providing resistant to metadata surveillance.

<hr />

The Cwtch project began in 2017 as an extension protocol for Ricochet providing group conversations via untrusted servers, with an eye to enabling decentralized, metadata resistant applications (like shared lists and bulletin board)

An alpha version of Cwtch was [was launched in February 2019](https://openprivacy.ca/blog/2019/02/14/cwtch-alpha/), and since then the Cwtch team (run by the [Open Privacy Research Society](https://openprivacy.ca)) has conducted research and development into Cwtch and the underlying protocols and libraries and problem spaces.





