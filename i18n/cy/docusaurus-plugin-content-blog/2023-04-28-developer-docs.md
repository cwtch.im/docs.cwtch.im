---
title: Cwtch Developer Documentation, Cwtchbot v0.1.0 and New Nightly.
description: "In this development log we take a look at the new Cwtch developer docs!"
slug: cwtch-developer-documentation
tags:
  - cwtch
  - cwtch-stable
  - developer-documentation
image: /img/devlog9_small.png
hide_table_of_contents: false
toc_max_heading_level: 4
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

One of the larger remaining goals outlined in our [Cwtch Stable roadmap update](/blog/cwtch-stable-roadmap-update) is comprehensive developer documentation. We have recently spent some time writing the foundation for these documents.

In this devlog we will introduce some of them, and outline the next steps. We also have a new nightly Cwtch release available for testing!

We are very interested in getting feedback on these documents, and we encourage anyone who is excited to build a Cwtch Bot, or even an alternative UI, to read them over and reach out to us with comments, questions, and suggestions!

As a reminder, the Open Privacy Research Society have [also announced they are want to raise $60,000 in 2023](https://openprivacy.ca/discreet-log/38-march-2023/) to help move forward projects like Cwtch. Please help support projects like ours with a [one-off donations](https://openprivacy.ca/donate) or [recurring support via Patreon](https://www.patreon.com/openprivacy).

![](/img/devlog9.png)

<!--truncate-->

## Cwtch Development Handbook

We have created a new documentation section, [the developers handbook](/developing/intro). This new section is targeted towards to people working on Cwtch projects (e.g. the official Cwtch library or the Cwtch UI), as well as people who want to build new Cwtch applications (e.g. chat bots or custom clients).

### Release and Packaging Process

The new handbook features a breakdown of [Cwtch release processes](/developing/release) - describing what, and how, build artifacts are created; the difference between nightly and official builds; how the official release process works; and how reproducible build scripts are created.

### Cwtch Application Development and Cwtchbot v0.1.0!

For the first time ever we now have [comprehensive documentation on how to build a Cwtch Application](/developing/category/building-a-cwtch-app). This section of the development handbook covers everything from [choosing a Cwtch library](/developing/building-a-cwtch-app/intro#choosing-a-cwtch-library), to [building your first application](/developing/building-a-cwtch-app/building-an-echobot).

Together with this new documentation we have also [released version 0.1 of the Cwtchbot framework](https://git.openprivacy.ca/sarah/cwtchbot), updating calls to use the [new Cwtch Stable API](/blog/cwtch-stable-api-design).

### New Nightly

There is a [new Nightly build](https://docs.cwtch.im/docs/contribute/testing#cwtch-nightlies) are available from our build server. The latest nightly we recommend testing is [2023-04-26-20-57-v1.11.0-33-gb4371](https://build.openprivacy.ca/files/flwtch-2023-04-26-20-57-v1.11.0-33-gb4371/).

This version has a number of fixes and updates to the file sharing and image previews/profile pictures experiment, and an update to the [in-development Tails support](/docs/platforms/tails).

In addition, this nightly also includes a number of performance improvements that should fix reported rendering issues on less powerful devices.

Please see the contribution documentation for advice on [submitting feedback](/docs/contribute/testing#submitting-feedback)

Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

