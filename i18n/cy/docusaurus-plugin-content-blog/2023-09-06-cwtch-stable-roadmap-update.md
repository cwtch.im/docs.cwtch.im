---
title: September Cwtch Stable Roadmap Update
description: "Back in July we provided an update on several goals that we would have to hit on our way to Cwtch Stable, and the timelines to hit them. In this post we provide a new update on those goals"
slug: cwtch-stable-roadmap-update-sept
tags:
  - cwtch
  - cwtch-stable
  - planning
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

The next large step for the Cwtch project to take is a move from public **Beta** to **Stable** – marking a point at which we consider Cwtch to be secure and usable. We have been working hard towards that goal over the last year.

Today, as we approach the release of Cwtch Stable we would like to provide another update on the ongoing work, and the remaining blockers to certifying a Cwtch Stable release. We also have a new nightly to test out!

![](/img/devlog1.png)
 
<!--truncate-->

## Stable Blocker and Timelines

Back in January we set the ambitious goal of launching a Cwtch Stable in the Summer of 2023. We had planned to finish all of the work prior to the end of August. The vast majority of that work has now been completed - what remains is captured in [Stable Blockers](https://git.openprivacy.ca/cwtch.im/cwtch-ui/projects/15) project which tracks the current state of work that we have marked as being critical to a Cwtch Stable release.

Despite there being a large number of remaining issues, many of the outstanding work is inter-related, relies on common implementations or are tightly coupled together.

 In summary the final few areas of concern are:

 - The ability to delete or purge group conversation history. (For historical reasons storing group history was once considered necessary but this is no longer the case. We plan on enabling this feature in the coming weeks)
 - Appropriate handling of less common system configurations. Cwtch current emits non-fatal exceptions if certain services are not available e.g. dbus. This is related to former 3rd party code for managing networking and notification.
 - A final UI pass. We have designs for better ways to convey certain information and functionality. We would like to implement these prior to a stable release.

Because of this, we have set a goal of labelling a Cwtch Stable Release Candidate by **30th September 2023**.

## A New Nightly

There is a [new nightly version of Cwtch available for testing (2023-09-06-21-25-v1.12.0-33-g05b1)](https://build.openprivacy.ca/files/flwtch-2023-09-06-21-25-v1.12.0-33-g05b1/). This version contains a few bug fixes related to file share management, in addition to a significant improvement in the connection management code.

Additionally, thanks to volunteer testers [and contributors](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/715) the installation instructions and packaged configurations for Whonix have been much improved. See [running Cwtch on Whonix](https://docs.cwtch.im/docs/platforms/whonix) for more information.

## Get Involved

Staff and volunteer shortages have slightly extended our original estimates. In particular we are bottle-necked on review effort for new code. This is why we would like to encourage people to test out the latest nightlies and report any bugs/issues/improvements.

In order to help people get acclimated to our development flow we have created a new section on the main documentation site called [Developing Cwtch](/docs/contribute/developing) - there you will find a collection of useful links and information about how to get started with Cwtch development, what libraries and tools we use, how pull requests are validated and verified, and how to choose an issue to work on.

We also also updated our guides on [Translating Cwtch](/docs/contribute/translate) and [Testing Cwtch](/docs/contribute/testing).

If you are interested in getting started with Cwtch development then please check it out, and feel free to reach out to `team@cwtch.im` (or open an issue) with any questions. All types of contributions [are eligible for stickers](/docs/contribute/stickers).

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)

