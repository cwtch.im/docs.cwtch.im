---
title: Cwtch Call for Contributor Credits
description: "As we journey ever closer to a Cwtch Stable candidate we would like to take this opportunity to ensure that those who have contributed to Cwtch over the years have the optiont to be credited."
slug: cwtch-stable-call-for-credits
tags:
  - cwtch
  - cwtch-stable
  - contributors
  - community
image: /img/devlog1_small.jpg
hide_table_of_contents: false
authors:
  - 
    name: Sarah Jamie Lewis
    title: Executive Director, Open Privacy Research Society
    image_url: /img/sarah.jpg
---

As we journey ever closer to a Cwtch Stable candidate we would like to take this opportunity to ensure that those who have contributed to Cwtch over the years have the option to be credited in some way.

If you have participated in the development process in any way e.g. protocol design, writing code, UI design, writing tests, testing release candidates, reporting issues, translating the application or documentation, promoting metadata resistant applications or any other meaningful contribution to the Cwtch ecosystem we want to offer you the option to have your name or handle credited in both the source code repository and the application itself.

![](/img/devlog1.png)
 
<!--truncate-->

## A History of Cwtch Contibutions and Anonmymity

In the early days of Cwtch we made the explicit decision to not include credits anywhere in the application, and to accept contributions anonymously over a variety of channels, including Cwtch itself.

Due to the nature of the application, and the privacy and metadata resistant space in general, we have always had a policy of evaluating contributions based on merit, and not on identity. This approach means that, while we do have contributors whose identity is known to us in some way, we have many who we know only by writing style, contribution type, or cwtch address.

We understand that many people much prefer it this way, and have no desire to have any identity linked to the Cwtch project. To those people we offer our deep gratitude. Thank you. You have made Cwtch what it is. (And if you ever want Cwtch Stickers - please let us know!)

However, it would not be right of us to release Cwtch Stable without at least one final offer to all contributors. If you want to be credited for your contributions to Cwtch then, please, reach out to us and let us know of a way to appropriately credit you.

## Getting in Touch

You can ask for credit via email (team@cwtch.im), or via Cwtch (either publicly via the [Cwtch Release Candidate Testers groups](https://docs.cwtch.im/docs/contribute/testing#join-the-cwtch-release-candidate-testers-group), or privately in a message to Sarah: `icyt7rvdsdci42h6si2ibtwucdmjrlcb2ezkecuagtquiiflbkxf2cqd`).

You can also [open an issue](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues/new).

When asking, please provide a name or handle, and if desired, a rough description of the contribution  (e.g. development, design, documentation, translating, funding). Anyone who does not provide a description will be grouped under a general thanks section.

This is an open offer. If at any time you change your mind and wish to have credit added (or removed) please let us know.

I want to take another opporunity to say, regardless of whether you wish to be publicly credited for your work on Cwtch, **thank you**.

## Stay up to date!

Subscribe to our [RSS feed](/blog/rss.xml), [Atom feed](/blog/atom.xml), or [JSON feed](/blog/feed.json) to stay up to date, and get the latest on, all aspects of Cwtch development.

## Help us go further!

We couldn't do what we do without all the wonderful community support we get, from [one-off donations](https://openprivacy.ca/donate) to [recurring support via Patreon](https://www.patreon.com/openprivacy).

If you want to see us move faster on some of these goals and are in a position to, please [donate](https://openprivacy.ca/donate). If you happen to be at a company that wants to do more for the community and this aligns, please consider donating or sponsoring a developer.

Donations of **$5 or more** can opt to receive stickers as a thank-you gift!

For more information about donating to Open Privacy and claiming a thank you gift [please visit the Open Privacy Donate page](https://openprivacy.ca/donate/).

![A Photo of Cwtch Stickers](/img/stickers-new.jpg)



