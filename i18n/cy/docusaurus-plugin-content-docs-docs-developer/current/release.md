---
sidebar_position: 1
---

# Release and Packaging Process

Cwtch builds are automatically constructed via Drone. In order to be built the tasks must be approved by a project team member.

## Automated Testing

Drone carries out a suite of automated tests at various stages of the release pipeline.

| Test Suite              | Repository        | Notes                                                                                                         |
| ----------------------- | ----------------- | ------------------------------------------------------------------------------------------------------------- |
| Integration Test        | cwtch.im/cwtch    | A full exercise of peer-to-peer and group messaging                                                           |
| File Sharing Test       | cwtch.im/cwtch    | Tests that file sharing and image downloading work as expected                                                |
| Automated Download Test | cwtch.im/cwtch    | Tests that automated image downloading (e.g. profile pictures) work as expected                               |
| UI Integration Test     | cwtch.im/cwtch-ui | A suite of Gherkin tests to exercise various UI flows like Creating / Deleting profiles and changing settings |

## Cwtch Autobindings

Drone produces the following build artifacts for all Cwtch autobindings builds.

| Build Artifact             | Platform | Notes                                                      |
| -------------------------- | -------- | ---------------------------------------------------------- |
| android/cwtch-sources.jar  | Android  | gomobile derived source code for the Android Cwtch library |
| android/cwtch.aar          | Android  | Android Cwtch library. Supports arm, arm64, and amd64.     |
| linux/libCwtch.h           | Linux    | C header file                                              |
| linux/libCwtch.so          | Linux    | x64 shared library                                         |
| windows/libCwtch.h         | Windows  | C header file                                              |
| windows/libCwtch.dll       | Windows  | x64 bit shared library                                     |
| macos/libCwtch.arm64.dylib | MacOS    | Arm64 shared library                                       |
| macos/libCwtch.x64.dylib   | MacOS    | x64 shared library                                         |

## UI Nightly Builds

We make unreleased versions of Cwtch available for testing as [Cwtch Nightlies](/docs/contribute/testing#cwtch-nightlies).

Each nightly build folder contains a collection  of build artifacts e.g. (APK files for Android, installer executables for Android) in single convenient folder. A full list of build artifacts currently produced is as follows:

| Build Artifact              | Platform | Notes                                                                                  |
| --------------------------- | -------- | -------------------------------------------------------------------------------------- |
| cwtch-VERSION.apk           | Android  | Supports arm, arm64, and amd64. Can be sideloaded.                                     |
| cwtch-VERSION.aab           | Android  | Android App Bundle for publishing to appstores                                         |
| Cwtch-VERSION.dmg           | MacOS    |                                                                                        |
| cwtch-VERSION.tar.gz        | Linux    | Contains the code, libs, and assets in addition to install scripts for various devices |
| cwtch-VERSION.zip           | Windows  |                                                                                        |
| cwtch-installer-VERSION.exe | Windows  | NSIS powered installation wizard                                                       |

Nightly builds are regularly purged from the system

## Official Releases

The Cwtch Team meets on a regular basis and reaches consensus based on nightly testing feedback and project roadmaps.

When the decision is made to cut a release build, a nightly version is built with a new git tag reflecting the release version e.g. `v.1.12.0`. The build artifacts are then copied to the Cwtch release website to a dedicated versioned folder.

### Reproducible Builds

We use [repliqate](https://git.openprivacy.ca/openprivacy/repliqate) to provide [reproducible build scripts for Cwtch](https://git.openprivacy.ca/cwtch.im/repliqate-scripts).

We update the `repliqate-scripts` repository with scripts for all official releases. Currently only Cwtch bindings are reproducible