---
sidebar_position: 2
---

# Light/Dark and themes Breakdown

1. Press the setting icon
2. You can choose light or dark theme by toggling the “use light themes” switch
3. Using the “color theme” drop down menu, pick a theme you like
4.
   1. Cwtch: purple tones
   2. Ghost: Grey tones
   3. Mermaid: Turquoise and purple tones
   4. Midnight: Black and gray tones
   5. Neon 1: purple and pink tones
   6. Neon 2: purple and turquoise tones
   7. Pumpkin: purple and orange tones
   8. Witch: Green and pink tones
   9. Vampire: Purple and red tones