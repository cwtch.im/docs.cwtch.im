---
sidebar_position: 4
---

# Changing Your Profile Image

:::caution

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and both the  [File Sharing](https://docs.cwtch.im/docs/settings/experiments/file-sharing) and [Image Previews and Profile Pictures](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) enabled.

:::

1. Press the pencil next to the profile you want to edit
2. Press on the pink pencil over your profile photo
3. Select an image from your device
4. Scroll down and click on save profile