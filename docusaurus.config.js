// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

const math = require('remark-math');
const katex = require('rehype-katex');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Cwtch',
  tagline: 'Your Guide to setting up, and using, Surveillance Resistant Infrastructure',
  url: 'https://docs.cwtch.im',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.png',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'Open Privacy Research Society', // Usually your GitHub org/user name.
  projectName: 'cwtch.im', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'es', 'de', 'it']
    // Planned future versions to add. removing to start cus each needs a compile on push, no point compiling untranslated dups
    //, 'fr', 'da', 'de', 'it', 'no', 'pl', 'ro', 'ru', 'es-ES', 'cy'],
  },


  
  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://git.openprivacy.ca/cwtch.im/docs.cwtch.im/src/branch/staging/',
                    remarkPlugins: [math],
                    rehypePlugins: [katex],
        },
        blog: {
          feedOptions: {
            type: 'all',
            copyright: 'Copyright © ${new Date().getFullYear()} Open Privacy Research Society',
            title: 'Cwtch Development Log',
            description: 'The latest insights into Cwtch Development and what the Cwtch team are working on',
          },
          blogSidebarCount: 20,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),

        },


      }),
    ],
  ],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      image: 'img/hero1.jpg',
      colorMode: {
	            defaultMode: 'dark',
      },
      navbar: {
        title: 'Cwtch',
        logo: {
          alt: 'Cwtch Logo',
          src: 'img/knott.png'
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Documentation',
          },
          {
            to: '/security/intro',
            position: 'left',
            label: 'Security Handbook',
          },
		{
		  to: '/developing/intro',
		  position: 'left',
		  label: 'Developers Handbook',
		},
          {
            to: 'blog',
            position: 'left',
            label: 'Development Log',
          },
          {
            href: 'https://openprivacy.ca/donate',
            label: 'Donate',
            position: 'right',
          },
          {
            href: 'https://patreon.com/openprivacy',
            label: 'Patreon',
            position: 'right',
          },
                  {
            type: 'localeDropdown',
            position: 'right',
          },
       {type: 'html',
          position: 'right',
          value: '<a href="/download"><button class="actionbutton">Download Cwtch</button></a>'
          },
  
        ],
      },

      footer: {
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Introduction',
                to: '/docs/intro',
              },
				{
				  to: '/security/intro',
				  label: 'Security Handbook',
				},
				{
				  to: '/developing/intro',
				  label: 'Developer Guide',
				},
                	{
				  to: '/changelog',
				  label: 'Changelog',
				},
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Mastodon',
                href: 'https://fosstodon.org/@cwtch',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/cwtch_im',
              },
            ],
          },
          {
            title: 'Contribute',
            items: [
              {
                label: 'Donate',
                href: 'https://openprivacy.ca/donate',
              },
              {
                label: 'Patreon',
                href: 'https://patreon.com/openprivacy',
              },
              {
                label: 'Source Code',
                href: 'https://git.openprivacy.ca/cwtch.im',
              },
              {
                label: 'Download',
                href: 'https://cwtch.im/download',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Open Privacy Research Society.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
    plugins: [
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'docs-security',
        path: 'security',
        routeBasePath: 'security',
        sidebarPath: require.resolve('./sidebars.js'),
		remarkPlugins: [math],
		rehypePlugins: [katex],
      },
    ],
	[
	  '@docusaurus/plugin-content-docs',
	  {
		id: 'docs-developer',
		path: 'developing',
		routeBasePath: 'developing',
		sidebarPath: require.resolve('./sidebars.js'),
		remarkPlugins: [math],
		rehypePlugins: [katex],
	  },
	],
   ],
  stylesheets: [
	{
      href: '/katex/katex.min.css',
      type: 'text/css',
	},
  ],
};

module.exports = config;
