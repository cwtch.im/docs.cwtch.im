---
sidebar_position: 4
---

# Sending Invites to a Group

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and
the [Group Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) turned on.

:::

1. Go to a chat with a friend
2. Press on the invite icon
3. Select the group you want to invite them to
4. Press Invite
5. You have sent an invite

<div>
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Group_Invite.mp4" />
    </video>
</div>