---
sidebar_position: 7
---

# Editing a Group Name

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and
the [Group Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) turned on.

:::

Group names are private to you, it will not be shared, it is your local name for the group.

1. On the chat pane go to settings
2. Change the name of the group
3. Press the save button
4. Now your group has a new name

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/group_edit.mp4" />
    </video>
</div>