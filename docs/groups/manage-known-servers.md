---
sidebar_position: 7
---

# Managing Servers

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and
the [Group Experiment](https://docs.cwtch.im/docs/settings/experiments/group-experiment) turned on.

:::

Cwtch groups are hosted by untrusted servers. If you want to see the servers you know about, their status, and the groups hosted on them:

1. On your contacts pane
2. Got to the manage servers icon

## Import locally hosted server

1. To import a locally hosted server click on select local server
2. Select the server you want

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_Manage.mp4" />
    </video>
</div>