---
sidebar_position: 2
---

# How to create a server

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and
the [Server Hosting Experiment](https://docs.cwtch.im/docs/settings/experiments/server-hosting) turned on.

:::

1. Go to the server icon
2. Press the + action button to make a new server
3. Choose a name for your server
4. Select a password for your server
5. Click on “Add Server”

<div width="400">
    <video playsInline autoPlay muted loop width="400">
        <source src="/video/Server_New.mp4" />
    </video>
</div>