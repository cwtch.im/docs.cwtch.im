---
sidebar_position: 4.5
---

# Message Formatting

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and
the [Message Formatting Experiment](https://docs.cwtch.im/docs/settings/experiments/message-formatting) turned on.

Optionally, you can enable [Clickable Links](https://docs.cwtch.im/docs/settings/experiments/clickable-links) to
make URLs in messages clickable in Cwtch.

:::

Cwtch currently supports the following formatting markdown for messages:

* `**bold**` which will render **bold**
* `*italic*` which will render *italic*
* ``code`` which will render `code`
* `^superscript^` which will render <sup>superscript</sup>
* `_subscript_` which will render <sub>subscript</sub>
* `~~strikthrough~~` which will render <del>strikethrough</del>
