---
sidebar_position: 3
---

# Sharing Cwtch Addresses

There are many ways to share a Cwtch address.

## Sharing Your Cwtch Address

1. Go to your profile
2. Click the copy address icon

You can now share this address. People with this address will be able to add you as a Cwtch contact.

For information on blocking connections from people you don't know please see [Settings: Block Unknown Connections](/docs/settings/behaviour/block-unknown-connections) 


# Sharing A Friends Cwtch Address

Inside of Cwtch there is another mechanism for exchanging Cwtch addresses.

:::info

This documentation page is a stub. You can help
by [expanding it](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::