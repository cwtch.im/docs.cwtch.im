---
sidebar_position: 4
---

# Saving Conversation History

By default, for privacy, Cwtch does not preserve conversation history between sessions. 

To enable history for a specific conversation: 

1. On a conversation window go to Settings
2. Go to Save History
3. Click the dropdown menu
4. Pick Save History
5. Now your history will be saved

Conversation history can be turned off at any point by selecting "Delete History" from the drop down menu.