---
sidebar_position: 5
---

# Accessing Conversation Settings

In a conversation window, click on the Settings icon in the top bar.

<figure>

[![](/img/conversations/settings.png)](/img/conversations/settings.png)

<figcaption></figcaption>
</figure>


This action will open up a new screen where you can view and manage the contact.

<figure>

[![](/img/conversations/settings-full.png)](/img/conversations/settings-full.png)

<figcaption></figcaption>
</figure>