---
sidebar_position: 7
---

# Blocking a Contact

1. On a conversation window
2. Go to Settings
3. Scroll down to Block Contact
4. Move the switch to Block Contact

:::info

This documentation page is a stub. You can help
by [expanding it](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::