---
sidebar_position: 6
---

# Sharing a File

:::caution Experiments Required

This feature requires [Experiments Enabled](https://docs.cwtch.im/docs/settings/introduction#experiments) and
the [File Sharing Experiment](https://docs.cwtch.im/docs/settings/experiments/file-sharing) turned on.

Optionally, you can enable [Image Previews and Profile Pictures](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) to see display shared image previews in the conversation window.

:::

In a Conversation,

1. Click on the attachment icon
2. Find the file you want to send
3. Confirm you want to send it

## How does file sharing with groups work? Are my files stored on a server somewhere?

Files are sent through onion-to-onion Cwtch connections directly from the person offering the file to the person receiving it. The initial offer to send a file is posted as a standard Cwtch conversation/overlay message. For groups, this means that the initial offer (containing the filename, size, hash, and a nonce) is posted to the group server, but then each recipient connects to you individually to receive the actual file contents.

## Does that mean I have to be online to send a file?

Yes. If the person offering the file goes offline, you will have to wait for them to come online to resume the file transfer. The underlying protocol splits the files into individually-requestable, verifiable chunks, so that in a future release you will be able to "rehost" a file posted to a group, and even download from multiple hosts at once (sort of like bittorrent).

## Why are new contacts popping up in my list?

This is due to how Cwtch currently handles connections from unknown addresses. Since posting a file to a group results in group members connecting to you directly, some of those members might not be in your contact list already and so their download connection to you will appear in your list as a contact request.

## What is "SHA512"?

SHA512 is a [cryptographic hash](https://en.wikipedia.org/wiki/Cryptographic_hash_function) that can be used to verify that the file you downloaded is a correct copy of the file that was offered. Cwtch does this verification for you automatically, but you're welcome to try it yourself! Note that we also include a random nonce with file offers, so people can't just ask you for any random hash you might have, or files from conversations they're not part of.

## Is there a file size limit?

The current limit is 10 gigabytes per file.

## What are these .manifest files?

The .manifest files are used while downloading the file to verify that individual chunks are received correctly, and support resuming interrupted transfers. They also contain the info from the original file offer. You can safely delete them once the download is complete. On Android, the manifests are stored in the app's cache, and can be cleared  through your system settings.

## What about file metadata?

We send the file's name as a suggestion and to help distinguish it from other file offers. The full path is stripped before sending the offer. You should be wary of hidden metadata that might be stored in the file itself, which varies depending on the file's format. For example, images might contain geolocation info and information about the camera that took them, and PDF files are notorious for containing hidden information such as the author's name or the machine they were created on. In general, you should only send and receive files with people you trust.

## Can I download files automatically?

If the [Image Previews and Profile Pictures experiment](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) is enabled then Cwtch will automatically download images from accepted conversations
