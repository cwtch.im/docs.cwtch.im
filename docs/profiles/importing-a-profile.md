---
sidebar_position: 11
---

# Importing a Profile

1. Press the `+` action button in the right bottom corner and select "Import Profile"
2. Select an [exported Cwtch profile file](/docs/profiles/exporting-profile) to import
3. Enter the [password](/docs/profiles/create-a-profile#a-note-on-password-protected-encrypted-profiles) associated with the profile
and confirm.

Once confirmed, Cwtch will attempt to decrypt the provided file using a key derived from the given password. If successful
the profile will appear on the Profile Management screen and will be ready to use.

:::note

While a profile can be imported onto multiple devices, currently only one version of a profile can be in-use across all devices at any one time.

Attempts to use the same profile across multiple devices may result in availability issues and messaging failures.

:::