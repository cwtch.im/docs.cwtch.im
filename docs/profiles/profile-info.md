---
sidebar_position: 15
---

# Setting Profile Attributes

:::warning New Feature

New in [Cwtch 1.12](/blog/cwtch-nightly-1-12)

This functionality may be incomplete and/or dangerous if misused. Please help us to review, and test.
:::

On the [profile management pane](/docs/profiles/introduction#manage-profiles) there are three free-form text fields below your profile picture.

<figure>

[![](/img/profiles/attributes-empty.png)](/img/profiles/attributes-empty.png)

<figcaption></figcaption>
</figure>

You can fill these fields with any information your would like potential contacts to know. **This information is public** - do not put any information in here that you do not want to share with everyone.

<figure>

[![](/img/profiles/attributes-set.png)](/img/profiles/attributes-set.png)

<figcaption></figcaption>
</figure>



Contacts will be able to see this information in [conversation settings](/docs/chat/conversation-settings)

<figure>

[![](/img/profiles/attributes-contact.png)](/img/profiles/attributes-contact.png)

<figcaption></figcaption>
</figure>




