---
sidebar_position: 14
---

# Setting Availability Status

:::warning New Feature

New in [Cwtch 1.12](/blog/cwtch-nightly-1-12)

This functionality may be incomplete and/or dangerous if misused. Please help us to review, and test.
:::

On the [conversations pane](https://docs.cwtch.im/docs/category/conversations) click the Status icon next to your profile picture.

<figure>

[![](/img/profiles/status-tooltip.png)](/img/profiles/status-tooltip.png)

<figcaption></figcaption>
</figure>

A drop-down menu will appear with various options e.g. Available, Away, and Busy

<figure>

[![](/img/profiles/status-tooltip-busy.png)](/img/profiles/status-tooltip-busy.png)

<figcaption></figcaption>
</figure>

When you select Away or Busy as a status the border of your profile picture will change to reflect the status
<figure>

[![](/img/profiles/status-tooltip-busy-set.png)](/img/profiles/status-tooltip-busy-set.png)

<figcaption></figcaption>
</figure>


Contacts will see this change reflected in their conversations pane. 

<figure>

[![](/img/profiles/status-busy.png)](/img/profiles/status-busy.png)

<figcaption></figcaption>
</figure>




