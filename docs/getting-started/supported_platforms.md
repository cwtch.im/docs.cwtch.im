# Supported Platforms

The table below represents our current understanding of Cwtch support across various operating systems and architectures (as of Cwtch 1.10 and January 2023).

In many cases we are looking for testers to confirm that various functionality works. If you are interested in testing Cwtch on a specific platform, or want to volunteer to help us official support a platform
not listed here, then check out [Contibuting to Cwtch](/docs/category/contribute).

**Legend:**

- ✅: **Officially Supported**. Cwtch should work on these platforms without issue. Regressions are treated as high priority.
- 🟡: **Best Effort Support**. Cwtch should work on these platforms but there may be documented or unknown issues. Testing may be needed. Some features may require additional work. Volunteer effort is appreciated.
- ❌: **Not Supported**. Cwtch is unlikely to work on these systems. We will probably not accept bug reports for these systems.

| Platform                    | Official Cwtch Builds |  Source Support    | Notes                                                                                                                             |
|-----------------------------|-----------------------|--------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| Windows 11                  | ✅                     |   ✅    | 64-bit amd64 only.                                                                                                                |
| Windows 10                  |✅                   | ✅     | 64-bit amd64 only. Not officially supported, but official builds may work.                                                        |
| Windows 8 and below         | ❌                     | 🟡     | Not supported. Dedicated builds from source may work. Testing Needed.                                                             |
| OSX 10 and below            | ❌                    | 🟡     | 64-bit Only. Official builds have been reported to work on Catalina but not High Sierra                                           |
| OSX 11                      | ✅                     | ✅      | 64-bit Only. Official builds supports both arm64 and x86 architectures.                                                           |
| OSX 12                      | ✅                     | ✅      | 64-bit Only. Official builds supports both arm64 and x86 architectures.                                                           |
| OSX 13                      | ✅                     | ✅      | 64-bit Only.  Official builds supports both arm64 and x86 architectures.                                                          |
| Debian 11                   | ✅                     | ✅     | 64-bit amd64 Only.                                                                                                                |
| Debian 10                   | 🟡                     | ✅      | 64-bit amd64 Only.                                                                                                                |
| Debian 9 and below          | 🟡                    | ✅     | 64-bit amd64 Only. Builds from source should work, but official builds may be incompatible with installed dependencies.           |
| Ubuntu 22.04                | ✅                     | ✅      | 64-bit amd64 Only.                                                                                                                |
| Other Ubuntu                | 🟡                    | ✅     | 64-bit Only. Testing needed. Builds from source should work, but official builds may be incompatible with installed dependencies. | 
| Tails               | ✅                     | ✅      | 64-bit amd64 Only.   
| CentOS                      | 🟡                    | 🟡     | Testing Needed.                                                                                                                   |
| Gentoo                      | 🟡                    | 🟡     | Testing Needed.                                                                                                                   |
| Arch                        | 🟡                    | 🟡     | Testing Needed.                                                                                                                   |
| Whonix                      | 🟡                     | 🟡    | Help us [test this](https://docs.cwtch.im/docs/platforms/whonix)
| Raspian (arm64)             | 🟡                    | ✅  | Builds from source work.                                                                                                          |
| Other Linux Distributions   | 🟡                    | 🟡    | Testing Needed.                                                                                                                   |
| Android 9 and below         | 🟡                    | 🟡     | Official builds may work.                                                                                                         |
| Android 10                  | ✅                     | ✅     | Official SDK supprts arm, arm64, and amd64 architectures.                                                                         |
| Android 11                  | ✅                     | ✅      | Official SDK supprts arm, arm64, and amd64 architectures.                                                                         |
| Android 12                  | ✅                     | ✅      | Official SDK supprts arm, arm64, and amd64 architectures.                                                                         |
| Android 13                  | ✅                     | ✅      | Official SDK supprts arm, arm64, and amd64 architectures.                                                                         |
| LineageOS                   | ✅                    |  ✅    | Official SDK supprts arm, arm64, and amd64 architectures.                                                                         |
| Other Android Distributions | 🟡                    | 🟡      | Testing Needed.                                                                                                                   |
