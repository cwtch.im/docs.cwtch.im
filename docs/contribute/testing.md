---
sidebar_position: 1
---

# Testing Cwtch

This section documents some ways to get started with Cwtch Testing.

### Running Fuzzbot

FuzzBot is our development testing bot. You can add FuzzBot as a contact: `cwtch:4y2hxlxqzautabituedksnh2ulcgm2coqbure6wvfpg4gi2ci25ta5ad`.

:::info FuzzBot Help

Sending FuzzBot a `help` message will trigger it to send a reply with all the currently available testing commands. 

:::

For more information on FuzzBot see our [Discreet Log development blog](https://openprivacy.ca/discreet-log/07-fuzzbot/).

### Join the Cwtch Release Candidate Testers Group

Sending Fuzzbot the command `testgroup-invite` will cause FuzzBot to invite you to the **Cwtch Testers Group**! There
you can ask questions, post bug reports and offer feedback.

### Cwtch Nightlies

Cwtch Nightly builds are development builds that contain new features that are ready for testing.

The most recent few development versions of Cwtch are available from our [build server](https://build.openprivacy.ca/files/).

We **do not** recommend that testers always upgrade to the latest nightly, Instead, we will post a message to the Cwtch Release Candidate Testers group 
when a significant nightly becomes available. A nightly is considered significant if it contains a new feature or a major bug fix.

:::note

All contributions are [eligible for stickers](/docs/contribute/stickers)

:::

### Submitting Feedback

There are three main ways of submitting testing feedback to the team:

* Via Cwtch: Either via the Release Candidate Testers Group or directly to a Cwtch team member.
* Via Gitea: Please open an issue in [https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues) - please do not worry about duplicate issues, we will de-duplicate as part of our triage process.
* Via Email: Email `team@cwtch.im` with the bug report and one of our team will look into it.

:::note

Due to an issue with our email provider, we are currently unable to consistently send email from our gitea instance. Please regularly check open issues / pull-requests for updates (or subscribe to the repository's RSS feeds)

:::