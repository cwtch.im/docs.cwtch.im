---
sidebar_position: 1
---

# Block Unknown Connections

By default, Cwtch interprets connections from unknown Cwtch addresses as [Contact Requests](https://docs.cwtch.im/docs/chat/accept-deny-new-conversation). You can change this behaviour through the Block Unknown Connections
setting.

If enabled, Cwtch will auto close all connections from Cwtch addresses that you have not added to your conversation
list. This will prevent people who have your Cwtch address from contacting you unless you also add them.

To enable:

1. Go to Settings
2. Toggle on Block Unknown Contacts