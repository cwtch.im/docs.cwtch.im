---
sidebar_position: 3
---

# File Sharing

These setting enables Cwtch [filesharing functionality](/docs/chat/share-file). This reveals the "Share File" option in the conversation pane, and allows you to download files from conversations.

Optionally, you can enable [Image Previews and Profile Pictures](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures) to download image files automatically, view image previews in the conversation window, and enable the [Profile Pictures](/docs/profiles/change-profile-image) feature;

:::info

This documentation page is a stub. You can help
by [expanding it](https://git.openprivacy.ca/cwtch.im/docs.cwtch.im).

:::