---
title: Changelog
hide_table_of_contents: false
---

# Changelog

## Cwtch 1.15.5 - February 18th 2025

Small point release with bug fixes and small improvements.

- **Improvements**
	- Search now searches contacts as well as messages
	- Latest Tor (0.4.8.14)
- **Bugs Fixed**
	- Fixed startup crash on some Linux's due to improperly bundled lib in tor (fixed upstream) and changing where we use LD_LIBRARY_DIR in starting Cwtch and Tor
	- Fixed adding profiles in Android
	- Fixed accessing settings for offline contacts
	- Fixed contact list resort on pin/unpin contact

### Reproducible Bindings

Cwtch 1.15.5 is based on libCwtch version `libCwtch-autobindings-v0.1.6`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.5](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.5)
<hr/>
<br/>

## Cwtch 1.15.4 - December 2nd 2024

This is an incremental release that fixes a crash bug on older Android devices, provides a few upgrades to dependencies, and introduces tighter restrictions on unidentified contracts using the newer Enhanced Permissions model.

A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **Improvements:**
    - Fix android gating of new API features which was previously causing Cwtch to crash on older versions of Andorid
    - Ugrades bundles tor to 0.4.8.13
    - Chat history is no longer fetched all at once and is now buffered, greatly improving the scrolling performance when viewing long message threads Thanks [@psyduck](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/944)
    - Search context is now correctly unloaded when the back button is pressed. Thanks [@psyduck](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/947)
    - When downloading shared files, Cwtch now defaults to the users Downloads directory (this can be changed in Settings)
    - New contacts now default to more restricted permissions and no longer have the ability to request name or profile attributes
    - Unaccepted contacts will no longer be automatically considered for reconnection
    - Fixes to Themeing:
        - Fixes for importing custom themes and assets
        - Fix theme coloring of the appbar, panes and other various widgets
        - Profile pictures now show offline status in the Messaging Pane

### Reproducible Bindings

Cwtch 1.15.4 is based on libCwtch version `libCwtch-autobindings-2024-11-29-14-38-v0.1.5`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.5](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.5)
<hr/>
<br/>


## Cwtch 1.15 - October 1st 2024

A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Managed Groups Alpha** - This Cwtch release contains the first iteration of managed groups, allowing a dedicated (and trusted) cwtch peer to host a multi-person chat. To volunteer to test out this new feature please [join the Release Candidate Testers group](https://docs.cwtch.im/docs/contribute/testing#join-the-cwtch-release-candidate-testers-group) and ask for an invite.
  - **Private Profile Labels** - You can now give each of your profiles a local-only descriptive name.
- **Bug Fixes  / Improvements:**
  - Cwtch is now based on Flutter 3.22
  - Cwtch on Linux now uses the Flutter-provided `fl_dart_project_set_aot_library_path` method (contributed by Dan), rather than our own custom solution.
  - File Download verification attempts with incomplete/missing manifests will now allow attempt to redownload the manifest rather than emitting an error.
  - Cwtch will now correctly connect to the Tor Service in Tails OS > 6.0
  - Importing Unencrypted Profiles no longer requires entering [the defecto password](https://docs.cwtch.im/security/components/cwtch/profile-encryption#unencrypted-profiles-and-the-default-password)
  - When the clickable links experiment is enabled, message Formatting now priotizes code formatting over linking. Thanks [@psyduck](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/923)
  - When the image previews experiment is enabled, images are now previewed in their original aspect ratio. Thanks [@psyduck](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/929)
  - Source Tarballs are now also packaged during the release process
  - The Windows installer is now signed
  - New deb package release option (beta, please help us test!)  
  - New *Synergy* Theme, for those who want Cwtch to look like an enterprise product
- **Accessibility / UX:**
  - Core translations for **Brazilian Portuguese**, **Danish** , **Dutch**, **French**, **German**, **Italian**, **Norwegian** , **Romanian** , **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Partial translations for **Korean** (41%), **Japanese** (26%), , **Luxembourgish** (19%), **Greek** (15%), **Uzbek** (9%), and **Portuguese** (5%)
  - **Theme Refresh** - Many small adjustments to existing themes to make them more accessible
  
### Reproducible Bindings

Cwtch 1.15 is based on libCwtch version `libCwtch-autobindings-2024-09-05-11-07-v0.1.3`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.3](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.1.3)
<hr/>
<br/>


## Cwtch 1.14.7 - Februrary 27th 2024

A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

This fix mainly addresses several long standing issues with Android, but also contains an important update to libCwtch.

- New Android 13 Foreground Service Permission (`FOREGROUND_SERVICE_DATA_SYNC`) - to ensure better stability
- Fixes an issue that prevented some contacts from fetching profile images
- Android Notification fix to prevent exceptions when displaying full "Conversation Info" notifications with profile images enabled
- Cwtch Reload functionality fixed on Android to prevent over-zealous deletion of messages for contacts with unsaved history
- A few minor UI fixes for Android and Desktop to better fit small screens (more on the way!)

### Reproducible Bindings

Cwtch 1.14.7 is based on libCwtch version `libCwtch-autobindings-2024-02-26-18-01-v0.0.14`. The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source can be found at https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.14

<hr/>
<br/>

## Cwtch 1.14 - Februrary 14th 2024

A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Custom Themes** - You can now load [custom themes](https://docs.cwtch.im/docs/contribute/themes) into Cwtch.
  - **Message View Backgrounds** - This release contains the first support for (optional) background images in the message view. Future releases will allow per-conversation images.
- **Bug Fixes  / Improvements:**
  - Fixed tor connectivity in newer Tails releases
  - Fixes in the Retry Plugin for better managing of a large number of contacts
  - Several UX improvements for font scaling, and styling
  - Fixed Android File Sharing Bug which prevent downloads of [non-previewed files](https://docs.cwtch.im/docs/settings/experiments/image-previews-and-profile-pictures).
  - Fixed Android File Sharing Bug that resulted in a UI reset triggered by a rate race condition between reconnection and new message arriving
  - Split Settings Pane into multiple tabs for easier navigation of options
  - Fixed contact row date time/localization inconsistency
  - Fixed contact row issue where `LANG` wasn't set on some linux systems
  - libCwtch now support older Mac releases (min 10.12)
  - Updates images and descriptions in the Windows Installer
- **Accessibility / UX:**
  - Core translations for **Brazilian Portuguese**, **Danish** , **Dutch**, **French**, **German**, **Italian**, **Norwegian** , **Romanian** , **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Partial translations for **Korean** (41%), **Japanese** (26%), , **Luxembourgish** (19%), **Greek** (15%), **Uzbek** (9%), and **Portuguese** (5%)
  - **Theme Refresh** - Many small adjustments to existing themes to make them more accessible
  
### Reproducible Bindings

Cwtch 1.14 is based on libCwtch version `libCwtch-autobindings-2024-02-12-11-04-v0.0.12`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.12](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.12)
<hr/>
<br/>

## Cwtch 1.13.2 - November 30th 2023

- Upgrade Packaged Tor to 0.4.8

### Reproducible Bindings

Cwtch 1.13.2 is based on libCwtch version `libCwtch-autobindings-2023-09-26-13-15-v0.0.10`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10)
<hr/>
<br/>

## Cwtch 1.13.1 - October 3rd 2023
- fix emoji font on Windows
- clarify offline mode behaviour

## Reproducible Bindings

Cwtch 1.13.1 is based on libCwtch version `libCwtch-autobindings-2023-09-26-13-15-v0.0.10`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10)
<hr/>
<br/>

## Cwtch 1.13 - September 27th 2023
A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Conversation Search** - Cwtch can now find messages based on their content.
  - **Appear Offline Mode** - in this mode Cwtch does not launch a listening service for inbound contacts, and allows a profile to be more selective in the contacts they connect to.
  - **Whonix Support** - new runtime flags make changes that allow Cwtch to [run on Whonix](https://docs.cwtch.im/docs/platforms/whonix)
  - **Save History Global Setting** - by default Cwtch deletes all messages on shutdown unless a conversation is otherwise configured. This change allows a user to change this default behaviour.
- **Bug Fixes  / Improvements:**
  - Based on Flutter 3.13.4
  - Updated Android Target to 33
  - Profile Status Menu now has many more options, including offline status, edit profile and enabling/disabling profile
  - File Sharing Bug Fixes
    - Manage shared files now supports re-enabling older file shares
  - Improvements towards [UI Reproducible Builds](https://docs.cwtch.im/blog/cwtch-ui-reproducible-builds-linux)
  - Server Info now propagates to the UI consistently
  - Prevent DBus Exceptions on platforms where it is unsupported
  - Packaged Emoji Font
  - Fixes to retry manager which have greater improved (re)connection efficacy
  - Allow deleting server info in Manage Servers
- **Accessibility / UX:**
  - Core translations for **Brazilian Portuguese**, **Danish** , **Dutch**, **French**, **German**, **Italian**, **Norwegian** , **Romanian** , **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Partial translations for **Korean** (37%), **Japanese** (27%), , **Luxembourgish** (20%), **Greek** (15%), **Uzbek** (10%), and **Portuguese** (5%)
  - Font Scaling improvements on several screens
  
### Reproducible Bindings

Cwtch 1.13 is based on libCwtch version `libCwtch-autobindings-2023-09-26-13-15-v0.0.10`. 
The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source 
can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.10)
<hr/>
<br/>

## Cwtch Beta 1.12 - June 16th 2023
A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
  - **Profile Attributes** - profiles can now be augmented with [additional public information](https://docs.cwtch.im/docs/profiles/profile-info)
  - **Availability Status** - you can now notify contacts that you [are **away** or **busy**](https://docs.cwtch.im/docs/profiles/availability-status)
  - **Five New Supported Localizations**: **Japanese**, **Korean**, **Slovak**, **Swahili** and **Swedish**
  - **Support for Tails** - adds an [OnionGrater](https://docs.cwtch.im/docs/platforms/tails) configuration and a new `CWTCH_TAILS` environment variable that enables special Tor behaviour.
- **Bug Fixes  / Improvements:**
  - Based on Flutter 3.10
  - Inter is now the main UI font
  - New Font Scaling setting
  - New Network Management code to better manage Tor on unstable networks
  - File Sharing Experiment Fixes
  	- Fix performance issues for file bubble
  	- Allow restarting of file shares that have timed out
  	- Fix NPE in FileBubble caused by deleting the underlying file
  	- Move from RetVal to UpdateConversationAttributes to minimze UI thread issues
  - Updates to Linux install scripts to support more distributions
  - Add a Retry Peer connection to prioritize connection attempts for certain conversations
  - Updates to `_FlDartProject` to allow custom setting of Flutter asset paths
- **Accessibility / UX:**
  - Full translations for **Brazilian Portuguese**, **Dutch**, **French**, **German**, **Italian**, **Russian**, **Polish**, **Slovak**, **Spanish**, **Swahili**, **Swedish**, **Turkish**, and **Welsh**
  - Core translations for **Danish** (75%), **Norwegian** (76%), and  **Romanian** (75%)
  - Partial translations for **Japanese** (29%), **Korean** (23%), **Luxembourgish** (22%), **Greek** (16%), and **Portuguese** (6%)

### Reproducible Bindings

Cwtch 1.12 is based on libCwtch version `libCwtch-autobindings-2023-06-13-10-50-v0.0.5`. The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.5](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.5)

<hr/>
<br/>

## Cwtch Beta 1.11 - March 29th 2023
A special thanks to the [amazing volunteer translators](https://docs.cwtch.im/docs/contribute/translate) and [testers](https://docs.cwtch.im/docs/contribute/testing) who made this release possible.

- **New Features:**
    - **Based on new Reproducible Cwtch Stable Autobuilds** - this is the first release of cwtch based on [reproducible Cwtch bindings](https://docs.cwtch.im/blog/cwtch-bindings-reproducible) in addition to our new [automatically generated](https://docs.cwtch.im/blog/autobindings)
    - **Two New Supported Localizations**: **Slovak** and **Korean**
- **Bug Fixes  / Improvements:**
    - When preserving a message draft, quoted messages are now also saved
    - Layout issues caused by pathological unicode are now prevented
    - Improved performance of message row rendering
    - Clickable Links: Links in replies are now selectable
    - Clickable Links: Fixed error when highlighting certain URIs
    - File Downloading: Fixes for file downloading and exporting on 32bit Android devices
    - Server Hosting: Fixes for several layout issues
    - Build pipeline now runs automated UI tests
    - Fix issues caused by scrollbar controller overriding
    - Initial support for the Blodeuwedd Assistant (currently compile-time disabled)
    - Cwtch Library:
        - [New Stable Cwtch Peer API](https://docs.cwtch.im/blog/cwtch-stable-api-design/)
        - Ported File Downloading and Image Previews experiments into Cwtch
- **Accessibility / UX:**
    - Full translations for **Brazilian Portuguese**, **Dutch**, **French**, **German**, **Italian**, **Russian**, **Polish**, **Spanish**, **Turkish**, and **Welsh**
    - Core translations for **Danish** (75%), **Norwegian** (76%), and  **Romanian** (75%)
    - Partial translations for **Luxembourgish** (22%), **Greek** (16%), and **Portuguese** (6%)


### Reproducible Bindings

Cwtch 1.11 is based on libCwtch version `2023-03-16-15-07-v0.0.3-1-g50c853a`. The [repliqate scripts](https://docs.cwtch.im/blog/cwtch-bindings-reproducible#introducing-repliqate) to reproduce these bindings from source can be found at [https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.3-1-g50c853a](https://git.openprivacy.ca/cwtch.im/repliqate-scripts/src/branch/main/cwtch-autobindings-v0.0.3-1-g50c853a)

<hr/>
<br/>


## Cwtch Beta 1.10 - December 16th 2022
For a more detailed overview of changes present in the 1.10 release see issues tagged [cwtch-beta-1.10](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=169&milestone=0&assignee=0) in our issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **Fine-grained Profile Autostart** - you can now control which profiles are automatically enabled at start up
    - **New Connection Backend** - we have reworked connection management in Cwtch to minimize contention inside the Tor process, and to prioritize regular contacts. This results in faster peering times, and more stable connections 
* **Bug Fixes  / Improvements:**
    - [Profile Exporting](https://docs.cwtch.im/docs/profiles/exporting-profile) now works on Android devices
    - [Profile Image Shares](https://docs.cwtch.im/docs/profiles/change-profile-image) are now re-initialized every start up to ensure that profile images are available to new contacts. (Previously profile image shares expired after 30-days like all other file shares)
    - Fix a bug that prevented sharing files on Android in certain configurations
    - Many [colorscheme improvements](https://git.openprivacy.ca/cwtch.im/cwtch-ui/pulls/590) to packaged themes
    - A new **Juniper** theme
    - Improved UX for [unlocking profiles](https://docs.cwtch.im/docs/profiles/unlock-profile)
    - Fix a bug that prevented the [deletion of profiles](https://docs.cwtch.im/docs/profiles/delete-profile)
    - [Streamer mode](https://docs.cwtch.im/docs/settings/appearance/streamer-mode) now hides cwtch addresses on configuration screens
    - Message formatting now applies to quoted messages
    - Message formatting can now be disabled when experiments are disabled
    - Fix bug that prevented Cwtch from finding packaged Tor binary in some installations
    - Upgrade to Flutter 3.3.5
    - New Android Splash Screen
* **Accessibility / UX:**
    - Full translations for **Brazilian Portuguese**, **Dutch**, **French**, **German**, **Italian**, **Russian**, **Polish**, **Spanish**, **Turkish**, and **Welsh**
    - Core translations for **Danish** (85%), **Norwegian** (85%), and  **Romanian** (85%)
    - Partial translations for **Luxembourgish** (25%), **Greek** (19%), and **Portuguese** (7%)

<hr/>
<br/>

## Cwtch Beta 1.9 - September 10th 2022
For a more detailed overview of changes present in the 1.9 release see issues tagged [cwtch-beta-1.9](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=160&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **View Replies** - quickly view all replies to a specific message 
    - **Manage Shared Files** - pause or restart sharing of files to specific conversations
    - **Pin Conversations** - important conversations can now be pinned to the top of the conversations list
    - **Experiment: QR Codes** - start of functionality allowing sharing of Cwtch addresses via QR codes
    - **Cwtch Handook** is now available in [Italian](https://docs.cwtch.im/it/docs/intro/), [German](https://docs.cwtch.im/de/docs/intro/) and [Spanish](https://docs.cwtch.im/es/docs/intro/)
* **Bug Fixes  / Improvements:**
    - Group messages are now viewable while the group is actively syncing 
    - Group Anti-spam challenge/completion status is now surfaced in the UX 
    - Fixed duplicate contact add bug (Android)
    - Introduced better error handling for the file sharing experiment
      - Automatic downloads are now not triggered if download directory does not exist or is incorrectly configured
      - Failed file downloads can now be restarted through the UX
    - Tor Version is now surfaced correctly in UI after restarts
    - Upgrade bundled Tor
    - Custom Tor SOCKS port configuration is now used
    - Peering attempts are now paused until Tor is fully bootstrapped
    - Per-conversation messages drafts are now saved while Cwtch is open
* **Accessibility / UX:**
    - Full translations for **French**, **German**, **Italian**, **Russian**, **Polish**, **Spanish**, **Turkish**, and **Welsh**
    - Core translations for **Danish** (85%), **Norwegian** (85%), and  **Romanian** (85%)
    - Partial translations for **Luxembourgish** (25%), **Greek** (19%), **Dutch** (12%), and **Portuguese** (7%)
    - Updates to experiment descriptions to remove outdated references to previous versions

<hr/>
<br/>

## Cwtch Beta 1.8 - June 28th 2022
For a more detailed overview of changes present in the 1.8 release see issues tagged [cwtch-beta-1.8](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=160&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **New Message Formatting Toolbar** 
    - **Apple Silicon Support**
    - **Brand New Documentation Handbook**: [docs.cwtch.im](https://docs.cwtch.im)
* **Bug Fixes  / Improvements:**
    - Clicking on a Quoted Message will not scroll to that message
    - Quoted messages are now clipped to single line to maximize space
    - Clicking a contact in a conversation to initiate a DM will now scroll the conversation list to that contact.  
    - Cwtch is now based on Flutter 3
       - Linux: Holding down a keyboard key will now repeat input
    - Dropdown fields in Settings are now correctly scaled
    - Bug Fix preventing archived messages from displaying their correct timestamp
    - Syncing a new group on a new server will now update the syncing progress bar
    - `Address` has been removed from Group Conversation Settings as it no long has any practical use
* **Accessibility / UX:**
    - Full translations for **French**, **German**, **Italian**, **Russian**, **Polish**, **Spanish**, and **Welsh**
    - Core translations for **Danish** (89%), **Norwegian** (89%), and **Romanian** (89%)
    - Partial translations for **Luxembourgish** (24%), **Greek** (19%), and **Portuguese** (7%)
    - Add Contact UX has been split to prompt for a specific action instead on directly opening the Add Contact pane
    - Several small updates to theming
    - Image / File display overlay now displays timestamp like other messages
    - Snackbar notifications have been added for all copy actions
    - Conversation row now displays the date instead of time for conversations between 1 and 2 days old
    - Conversation row is now split onto multiple lines to better use space
    - When scrolling up a conversation, a button is now displayed to allow quickly scrolling to the most recent messages

<hr/>
<br/>

## Cwtch Beta 1.7.1 - May 2nd 2022
* **Bug Fixes  / Improvements:**
    - File download bubble fixes for when file is moved
    - Fixes for android reply to message
    - Fixes for message cache loading especially on android
* **Accessibility / UX:**
    - Minor improvements to message pane rendering on loading messages
    - Improve "new messages" bubble behaviour

<hr/>
<br/>


## Cwtch Beta 1.7 - April 22nd 2022

For a more detailed overview of changes present in the 1.7 release see issues tagged [cwtch-beta-1.7](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=159&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **Profile Export / Import** - You can now move your profiles across devices by exporting them from one
      client and importing them in another. Note that there is still the limitation that only one instance of a profile
      can run at any one time
    - **New Message Formatting Experiment** - We have introduced a new experiment that allows a subset of markdown
      in messages including **bold**, *italic* and `code` - please see [Disceet Log 28](https://openprivacy.ca/discreet-log/28-message-text-formatting/)
      for more information
    - **Android Stability Improvements** - One of the biggest chunks of work represented by this release is several
  improvements to the Android version of Cwtch. You can find our more in [Disceet Log 27](https://openprivacy.ca/discreet-log/27-android-improvements/)
      for more information
    - **New Setting: Android Battery Exemption Request** - you can now request a power management exemption for the Cwtch application
      on Android directly from the Settings pane. Enabling this may allow Cwtch to run better on Android at the cost
      of increased battery use
* **Bug Fixes  / Improvements:**
    - Android Security Flags - We have enabled several `FLAG_SECURE` on Android
    - Folder Selection Bug Fix
    - UI Profiles Pane now integrates output from the Network Health plugin to display if a profile appears offline
    - Improvements to the UI Message Cache including a new Bulk Fetching/Loading API for improved performance
    - Several small thread and memory inefficiencies and leaks have been fixed in the UI and underlying libraries
      resulting in substantially reduced memory use
    - Notification Policy Setting bug fix
    - Unread messages counts are now saved between client restarts on Android
    - In development mode the Settings pane fetches and displays output from `GetDebugInfo`
    - `CWTCH-PROFILE` environment variable will cause Cwtch to output profiling information when displaying Debug Info
    - Fixes and updates to the (un)installer for Cwtch on Windows 
* **Accessibility / UX:**
    - Full translations for new features for French, German and Italian.
    - Add Contact UX has been split to prompt for a specific action instead on directly opening the Add Contact pane
    - Several small updates to theming
    - Image / File display overlay now displays timestamp like other messages

<hr/>
<br/>

## Cwtch Beta 1.6 - February 11th 2022

For a more detailed overview of changes present in the 1.6 release see issues tagged [cwtch-beta-1.6](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=158&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **Custom Profile Images**
        - This requires enabling the Filesharing and Image Preview experiments
    - **Advanced Tor Configuration**
        - Cwtch can now be configured to cache Tor consensus information, use predefined ports, and connect to an external, system Tor
        - Tor Circuit information is now available for P2P Conversations
    - **Notification Policies**
        - You can now mute specific conversations, turn off notifications entirely,
          or switch all notifications to opt-in
* **Bug Fixes  / Improvements:**
    - Images are now displayed for the sender as well as the recipients
    - Deleting P2P contacts is now supported again
    - *Clickable Links* will no longer prepend `https` to the text of the
      displayed URL
    - Closing via the windows exit button on Linux now triggers Cwtch to
      shut down cleanly
    - Packaged Tor has been upgraded
    - New [integration testing infrastructure](https://openprivacy.ca/discreet-log/23-cucumber-testing/)
    - Fixes to prevent extensive word wrapping on Android Devices (1.6.1)
    - Num-pad Enter now triggers sending a message, instead of inserting a new line (1.6.1)
    - Sending a message via a keyboard or via the send button now invokes identical logic (1.6.1)
    - Android Notification Image Fetching fixes (1.6.2)
    - Android Group Sync Status fixes (1.6.2)
    - Message View Rendering Issues fixes (1.6.2)
    - Improved Android Worker Robustness on Restart/Shutdown (1.6.2)
* **Accessibility / UX:**
    - Notifications are now translatable
    - Translations: French, Italian, German, Spanish, Romanian, Norwegian, Danish, Welsh, Russian, Polish, Luxembourgish and Greek (1.6.2)
    - Settings Pane is now broken down by subheadings
    - Message view now displays an estimated character count/limit for new messages
    - Fix bug that allows multiple file dialog windows to be opened when sharing
      a file
    - Number of unread messages from other loaded profiles are now summarized on the active profile

<hr/>
<br/>

# Cwtch Beta 1.5 - December 21st 2021

For a more detailed overview of changes present in the 1.5 release see issues tagged [cwtch-beta-1.5](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=157&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
    - **Inline Image Preview Experiment**
          - New experiment allows image files to be auto download and displayed
    - **Profile level server management**
        - See what servers your profile is connecting to, label them, and see what groups are hosted on them
        - Import hosted servers easily from the UI now
        - When creating groups, see the target server's description in the dropdown
    - **Clickable Hyperlinks Experiment**
        - Thanks to [Nima Boscarino](https://www.n11o.com/) 
    - Many new themes! Ghost, Mermaid, Midnight, Neon 1 & 2, Pumpkin, Vampire, and Witch all with Dark and Light modes
* **Bug Fixes  / Improvements:**
    - New Storage System
        - On the first load of any profile the Cwtch splash screen will show with a progress spinner while the old profile is migrated to the new storage system. This is a one time process, and may take up to a minute
        - This will enable a lot of improvements and new features going forward, and should be both more scalable and faster
    - Hosted server metrics: We now display "total message count" and "connections" on the server's pane
    - Big improvements to message pane rendering speed on Android thanks to message caching enabled by new storage system
    - Fixed crash on startup if hosting servers that were not marked "autostart"
    - Minor improvement to server connection speed on startup
    - Windows Uninstaller
* **Accessibility / UX:**
    - Full French, Russian and Italian Translations!
        - Splash screen text is now translatable
    - Tor info pane displays bootup progress when starting
    - Adding contact pane now slides up on Android when keyboard is activated on small screens
    - Minor message padding adjustments for Android to be more efficent with limited display width

<hr/>

## Cwtch Beta 1.4 - November 5th 2021

For a more detailed overview of changes present in the 1.4 release see issues tagged [cwtch-beta-1.4](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=156&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the amazing volunteer translators and testers who made this release possible.

* **New Features:**
  - Server Hosting Experiment! Please help us test this.
      - Go to Settings to Enable the Server Hosting Experiment
      - Create your own Servers to host groups on, share these publicly to allow others to privately host
        groups on your infrastructure or keep them private for more efficient groups
      - Only supported on Desktop applications
  - File Sharing Experiment now supports resumption within a 30 day window
      - Note: 1.4 File Sharing is *not* compatible with previous versions
* **Bug Fixes  / Improvements:**
  - Improved handling of errored group messages in the UI
  - Consolidated timeline code for P2P and Group conversations
      - Together these should fix ordering instabilities seen in group chats
  - Introduction of new Zoned Attributes
  - Mac OS builds are now automatically built as part of our release pipeline - which means nightly releases are now available!
* **Accessibility / UX:**
  - Up to date French Translation
  - The start of a Polish Translation
* **Planning / Design**
  - We published [a new roadmap](https://openprivacy.ca/discreet-log/19-cwtch-roadmap/) for the next few versions of Cwtch Beta
  - Storage Engine Redesign (Implementation underway for 1.5)
  - Initial discussions/ draft of the new Hybrid Groups Protocol (planned introduction in 1.6)    
  - Design of Additional UI Themes (to be included in 1.5)
  
<hr/>

## Cwtch Beta 1.3 - October 1st 2021

For a more detailed overview of changes present in the 1.3 release see issues tagged [cwtch-beta-1.3](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=154&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the volunteer translators and testers who made this release possible.

* **New Features:**
  - File Sharing Experiment! Please help us test this.
     - Go to Settings to Enable the File Sharing Experiment
     - Share files with both Contacts and Groups without uploading to a centralized file server
     - For more information see our most recent [Discreet Log #17: Filesharing FAQ](https://openprivacy.ca/discreet-log/17-filesharing-faq/)
  - Streamer/Redaction Mode - Hide sensitive data (addresses etc.) when showing Cwtch to others
      - Enable in Settings
* **Bug Fixes  / Improvements:**
   - Light Mode Fixes
   - Fix Crash that sometimes occurred after Deleting a Group
   - Reply buttons on Android now auto-hide
   - Shutdown button on Mac now works
   - Invitations on Android now persist state
   - Linux / Mac packaging / installer Improvements
   - **(Tapir)** More robust cryptographic checking on Cwtch addresses (these would have
       previously been caught in higher level authentication protocols, but are now rejected when importing
       addresses)
   -  **(Tapir)** Duplicate connections are now explicitly resolved down to a single connection. This should
    improve performance and latency, especially in mobile environments
* **Accessibility / UX:**
    - Completed French Translation
  
<hr/>

## Cwtch Beta 1.2 - August 31st 2021

For a more detailed overview of changes present in the 1.2 release see issues tagged [cwtch-beta-1.2](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&sort=&state=closed&labels=152&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the volunteer translators and testers who made this release possible.

* **New Features:**
  - Mac OS Version! Please help us test this.
  - An icon is now displayed when blocking of unknown contacts is enabled. 
  - Chat now maintains your last read position and provides a quick way of scrolling to most recent messages.
  - The `IME_FLAG_NO_PERSONALIZED_LEARNING` flag on all text fields (private keyboard mode)
  - (Cwtch Server) New Docker Container
  - Archive Conversations
* **Bug Fixes  / Improvements:**
  - Restyling of Message Composition bar buttons
  - Double Pane Scrolling Fixes
  - Fix Keyboard Issues on Android
  - Fix Cursor Issues on Android
  - Message Rows now have draggable/animated feedback for gesture-based actions  
  - Upgrade to Flutter 2.5
  - Memory Use Improvements
  - (Group Experiment) Previously, group signatures assumed that `Group ID` was kept secret from the server in order to preserve
    metadata privacy. This is no longer assumed 1.2 which now only relies on `Group Key` being kept secret. This change is backwards incompatible and as such 1.1 clients will not be able to verify group messages from newer clients. For more
    information please consult the [cwtch secure development handbook](https://docs.openprivacy.ca/cwtch-security-handbook/cwtch-overview.html#multi-party-conversations-groups-and-peer-to-server-communication)
* **Accessibility / UX:**
  - Completed French Translation
  - References to `Peer` replaced with more specific language (e.g. `Contact` or `Conversation`) throughout the UI

<hr/>

## Cwtch Beta 1.1  - July 15th 2021

For a more detailed overview of changes present in the 1.1 release see issues tagged [cwtch-beta-1.1](https://git.openprivacy.ca/cwtch.im/cwtch-ui/issues?q=&type=all&state=closed&labels=151&milestone=0&assignee=0) in our
issue tracker.

A special thanks to the volunteer translators and testers who made this release possible.

* **New Features:**
  - Support for Quoted Messages / Reply-to
  - Support for Multi-line Messages
  - Enable Autofill for Password Fields on Android
  - New Windows Installer
  - Improved Linux Install Scripts
* **Bug Fixes  / Improvements:**
  - Client-side filtering of messages from blocked contacts when in group conversations
  - Improved handling of syncing groups on Android
  - Prevent sending empty messages
  - Prevent sending messages to offline contacts / servers
  - Fix high CPU usage on Windows
  - Fix for unintentional shutdown prompt when clicking on a notification in Android
  - Fix non-truncating contact names on Android
  - Fix regression that preventing unblocking contacts (*v1.1.1*)
* **Accessibility / UX:**
  - Completed French Translation
  - Updated German and Italian Translations (now 85% complete)
  - Improved Window Icon  
  - Improved Group Syncing UI   
  - Contextual explanations for encrypted/unencrypted profiles
  - Tooltips for various UI elements
  
<br/>
