import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './index.module.css';
import HomepageFeatures from '@site/src/components/HomepageFeatures';
import DownloadFeatures from '@site/src/components/DownloadFeature';
import Translate, {translate} from '@docusaurus/Translate';

const items = [
  {id: 1, title: <Translate>CWTCH</Translate>},
  {id: 2, title: <Translate>Privacy Preserving Messaging</Translate>},
  {id: 3, title: <Translate>Get Started With Cwtch</Translate>},
];

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">

        <h1 class={styles.hero__title}><img src="/img/knott.png"/><br/>{items[0].title}</h1>
        <p className="hero__subtitle">{items[1].title}</p>
        <div className={styles.buttons}>
          <Link
            className="button button--secondary button--lg"
            to="/download">
             {items[2].title}
          </Link>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Metadata-Resistant Messaging">
      <HomepageHeader />
      <main>
        <DownloadFeatures />
       
        <div class="container">
            <div class={styles.bodytext}>
             <hr/>
         <div class="row">
        <div class={clsx('col col--6')}><h2><strong>Cwtch /kʊtʃ/</strong></h2></div>
        <div class={clsx('col col--6')}><p>Cwtch (/<a href="/audio/cwtch.mp3">kʊtʃ</a>/ - a Welsh word roughly translating to “a hug that creates a safe place”) is a decentralized, privacy-preserving,
          multi-party messaging protocol that can be used to build metadata resistant applications.</p></div>
      </div>

    <br/><br/>

      <div class="row">
        <div class={clsx('col col--4')}><strong>Decentralized and Open</strong><br/><p>Participants in Cwtch can host their own safe spaces, or lend their infrastructure to others
          seeking a safe space.  There is no “Cwtch service” or “Cwtch network”. The Cwtch protocol is <a href="https://git.openprivacy.ca/cwtch.im/cwtch">open</a>, and anyone is free to <a href="https://git.openprivacy.ca/sarah/cwtchbot">build bots, services</a> and <a href="https://git.openprivacy.ca/cwtch.im/cwtch-ui">user interfaces</a>
          and integrate and interact with Cwtch.</p></div>
        <div class={clsx('col col--4')}><strong>Privacy Preserving</strong><br/><p>All communication in Cwtch is end-to-end encrypted and takes place over Tor v3 onion services.</p></div>
        <div class={clsx('col col--4')}><strong>Metadata Resistant</strong><br/><p>Cwtch has been <a href="https://docs.openprivacy.ca/cwtch-security-handbook/risk.html">designed</a> such that no information is exchanged or available to anyone without their explicit consent, including on-the-wire messages and protocol metadata.</p></div>
      </div>

    <hr id="videos" />

      <h2>Videos</h2>
      <br/>
      <div  class="row">
        <div class={clsx('col col--6')}>
          <div class="col">
            <h3>Cwtch Explainer</h3>
  	    <video width="100%" src="/video/cwtch-explainer.mp4" controls><track default kind="subtitles" srclang="en" src="" /></video>
          </div>
        </div>
        <div class={clsx('col col--6')}>
          <div class="col">
            <h3>Tor State of The Onion 2024 Presentation</h3>
            <video width="100%" src="/video/soto24-cwtch.mp4" controls><track default kind="subtitles" srclang="en" src="" /></video>
          </div>
        </div>
      </div>


    <hr id="screenshots" />

      <h2>Screenshots</h2>
      <br/>
      <div  class="row">
        <div class={clsx('col col--6')}>
            <img src="/img/screenshots/2025/cwtch-desktop-p2p-chat-cwtch.png" class={styles.screenshot} alt="Cwtch peer to peer chat"/>
        </div>

        <div class={clsx('col col--6')}>
          <img src="/img/screenshots/2025/cwtch-desktop-group-chat-midnight.png" class={styles.screenshot}  alt="Cwtch group chat"/>
        </div>
      </div>
      <div class="row">
	<div class={clsx('col col--1')}>&nbsp;</div>
        <div class={clsx('col col--3')}>
          <img src="/img/screenshots/2025/cwtch-mobile-contacts-cwtch.png" class={styles.screenshot}  alt="Cwtch mobile contacts"/>
        </div>

        <div class={clsx('col col--3')}>
          <img src="/img/screenshots/2025/cwtch-mobile-group-chat-midnight.png" class={styles.screenshot}  alt="Cwtch mobile group chat"/>
        </div>
        
        <div class={clsx('col col--3')}>
          <img src="/img/screenshots/2025/cwtch-mobile-settings-light.png" class={styles.screenshot}  alt="Cwtch mobile settings"/>
        </div>
      </div>
	<div class={clsx('col col--1')}>&nbsp;</div>

      </div></div>
      </main>
    </Layout>
  );
}



