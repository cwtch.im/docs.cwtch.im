import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './index.module.css';

import DownloadFeatures from '@site/src/components/DownloadFeature';
import Translate, {translate} from '@docusaurus/Translate';

const items = [
  {id: 1, title: <Translate>Cwtch</Translate>},
  {id: 2, title: <Translate>Privacy Preserving Messaging</Translate>},
  {id: 3, title: <Translate>Get Started With Cwtch</Translate>},
];

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
        <h1 className="hero__title">{items[0].title}</h1>
        <p className="hero__subtitle">{items[1].title}</p>
        <div className={styles.buttons}>
          <Link
            className="button button--secondary button--lg"
            to="/docs/intro">
             {items[2].title}
          </Link>
        </div>
      </div>
    </header>
  );
}



export default function Download() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Metadata-Resistant Messaging">
      <main>
<br/><br/>
        <h1 class={styles.pageheader}>Download Cwtch</h1>
        <DownloadFeatures />
        <hr/>


<div class="container">

<h3 class={styles.downloadlink} id="android"><img class={styles.downloadicon}  src="/img/android.svg"/> Android</h3>

<ul>
<li>Standalone APK (works for arm7 and arm64):  <a href="https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/cwtch-v1.15.5.apk">cwtch-v1.15.5.apk</a>
<ul>
    <li><code>sha512: 43ea78c2642b9e1c15c9bea019b613e2abec892987f7f69f9914998ea62f1c31566ca216629e730fe7c312ffcda7254d1f13ef23646f3900ea5238aef7dbbe90</code></li>
    <li>Note: Syncing experimental Groups on some Android devices can be very slow depending on the underlying hardware and configuration.</li>
    </ul>
    </li>
<li>
   <a class="col-2 text-center align-content-center" href="https://play.google.com/store/apps/details?id=im.cwtch.flwtch">
    <img class={styles.downloadicon2} src="/img/google-play-badge.png"/></a>
   </li>
</ul>

<br/>

<h3 id="windows"><img class={styles.downloadicon}  src="/img/windows.svg"/>  Windows</h3>

<ul>
    <li>
    Signed Installer: <a href="https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/cwtch-installer-v1.15.5.exe">cwtch-installer-v1.15.5.exe</a>
    </li>
    <li>
        <code>sha512: ac7bc60372f21c8a610348dc8b102daad84c51dac877c15f32f6284a3aaf41018fea64c113d4f05cf48769c3a76ba0214ca2ac13b807fd3c5aae7ce236ccc6bf</code>
    </li> 
    <li>
    Unsigned Portable EXE: <a href="https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/cwtch-v1.15.5.zip">cwtch-v1.15.5.zip</a>
    </li>
    <li>
        <code>sha512: 8a9a48a6ac5b912c95185b01c520febd86f8770ec5c21f3d04a0eb62a7b6bbd3644b1cdc6c561a3ffb2f38b9eb60f209412df7b4f617bc3e30e3509b187c63eb</code>
    </li>
    <li>Due to the newness of the Open Privacy certificate, Windows Defender Smart Screen may throw up a warning the first time you try to run the app. To bypass, simply click the "More info" button and then "Run Anyways"</li>
</ul>



<br/>

<h3 id="linux"><img class={styles.downloadicon} src="/img/linux.svg"/>  Linux</h3>

<ul>
    <li>amd64: <a href="https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/cwtch-v1.15.5.tar.gz">cwtch-v1.15.5.tar.gz</a></li>
    <li><code>sha512: a61118adc2a5459f12e18fbd4b8fa67b7990781cb42b25e8a53c68886b36bdca8c3b844ff2c078bc593109c2c95523f01843c027785dfd8f45a629c27f71e2ed</code></li>
    <li>Addtional steps are needed to run Cwtch on more secure operating systems like <a href="/docs/platforms/tails">Tails</a> or <a href="/docs/platforms/whonix">Whonix</a></li>
<br/>
    <li>Debian: <a href="https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/cwtch-1.15.5.deb">cwtch-1.15.5.deb</a></li>
    <li><code>sha512: 7fe6422a01d1e611af1ec3e2ad99d0de539502b3ed19fe79a5052727b33eda7bea85f8e39bad258f8cb3c598742b0a473ff85ae8926d2c9f71c51cd44ad03f74</code></li>
<br/>
    <li>APT Repository<ol>
	<li>Install the Cwtch public GPG software signing key: <pre>wget -O- https://deb.cwtch.im/F6E3CBE475D0929825F9FC363498D4989B3F602B.asc | gpg --dearmor | sudo tee /usr/share/keyrings/deb.cwtch.im-keyring.gpg >/dev/null</pre></li>
	<li>Add the cwtch.im APT repository: <pre>echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/deb.cwtch.im-keyring.gpg] https://deb.cwtch.im/cwtch.im/ stable main' | sudo tee /etc/apt/sources.list.d/cwtch.im.list</pre></li>
	<li>Install Cwtch: <pre>sudo apt update && sudo apt install cwtch</pre></li>
	</ol></li>
    </ul>
<br/>

<h3 id="mac"><img class={styles.downloadicon} src="/img/apple.svg"/>  MacOS</h3>

<ul>
    <li>DMG: <a href="https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/Cwtch-v1.15.5.dmg">Cwtch-v1.15.5.dmg</a> (works for x86_64 and arm64e)</li>
    <li><code>sha512: d11b847eb16972919ba05472b4f98ca33b1ca83927f4d0fe1ccce886d9c1ed20e76a20fb09d7060212e055c58f3845f500be6018cb5094a04cbcd726880c7db1</code></li>
</ul>


<br/>

<h3><img class={styles.downloadicon}  src="/img/apple.svg"/>  iOS</h3>

<p></p> We would love to offer an iOS however there are several limitations of the platform that make this difficult. 
If you would like to support this effort please consider <a href="/docs/category/contribute">volunteering</a> or <a href="https://www.patreon.com/openprivacy">donating</a> to make that possible.

<br/>
<br/>
<h3 id="source"><img class={styles.downloadiconinvert} src="/img/OP_eye.svg"/>  Build from Source</h3>

Everything we build is open source:

<ul>
    <li>
 Cwtch Core Lib: <a href="https://git.openprivacy.ca/cwtch.im/cwtch"> https://git.openprivacy.ca/cwtch.im/cwtch</a>
    </li>
    <li>
libcwtch-go: <a href="https://git.openprivacy.ca/cwtch.im/autobindings">https://git.openprivacy.ca/cwtch.im/autobindings</a>
    </li>
    <li>
        Cwtch UI:  <a href="https://git.openprivacy.ca/cwtch.im/cwtch-ui">https://git.openprivacy.ca/cwtch.im/cwtch-ui</a>
        </li>
</ul>


<br/><br/>

</div>
 </main>

    </Layout>
  );
}

