import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Windows',
    Svg: require('@site/static/img/windows.svg').default,
    link: "https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/cwtch-installer-v1.15.5.exe",
  },
  {
    title: 'Mac',
    Svg: require('@site/static/img/apple.svg').default,
    link: "https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/Cwtch-v1.15.5.dmg",
  },
  {
    title: 'Linux',
    Svg: require('@site/static/img/linux.svg').default,
    link: "https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/cwtch-v1.15.5.tar.gz",
  },
  {
    title: 'Android',
    Svg: require('@site/static/img/android.svg').default,
    link: "https://git.openprivacy.ca/cwtch.im/cwtch-ui/releases/download/v1.15.5/cwtch-v1.15.5.apk",
  }
]

function Feature({Svg, title, link}) {
  return (
   <div className={clsx('col col--3')}>
      <div className="text--center">
      <a href={link}> <Svg className={styles.featureSvg} role="img" /></a>
      </div>
      <div className="text--center padding-horiz--md">
      <a href={link}><h3>{title}</h3></a>
      </div>
    </div>
  );
}

export default function DownloadFeatures() {
  return (
    <section className={styles.features}>
       
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
