---
sidebar_position: 4
---


# Tor Network

Cwtch makes use of Tor Onion Services (v3) and the Tor network for all inter-node communications. As such we inherit the security guarantees and properties of the Tor network. You can familiarize yourself with all the details by reading the [Tor Specification](https://spec.torproject.org/index.html).

Tor is continually working to improve the safety guarantees for users, including onion service hosts (like Cwtch profiles).

## Potential Risks

### Online / Offline metadata leak

If a third party is aware of your Cwtch profile's address, they can query it periodically to monitor when you are online and offline. If your profile online time matches daily patterns it could be used to help locate you with in a range of timezones.

Cwtch does offer a Start Offline mode for Cwtch profiles where it do not run the Onion Service for the profile and only make outbound connections to existing contacts as a mitigation for this style of leak. This means that only your contacts will be privy to on/offline metadata and third parties will not be able to probe for it. It does have the limitation that both parties cannot be operating in this mode or they will be unable to connect.

Tor has been working on an optional extra layer of onion service security with [Onion Client Auth](https://community.torproject.org/onion-services/advanced/client-auth/) (see also [Managing authorized client data](https://spec.torproject.org/rend-spec/restricted-discovery.html#appendix-g-managing-authorized-client-data-restricted-discovery-mgmt)) with work to make sure last hops in the Tor network may be able to protect against leaking on/offline status to non authorized users.

Currently Cwtch does not make use of this, but in the future could have a more restricted profile that was restricted to only preapproved contacts.

### Global Adversaries

When a global adversary leverages all its resources and collaborates with others deanonymization has occurred over time [in a few targeted instances](https://www.heise.de/en/news/Boystown-investigations-Catching-criminals-on-the-darknet-with-a-stopwatch-9904534.html). These attacks combined profiles that publicly shared their contact so that the global adversary could peer with them, start with a online / offline time analysis to find likely timezones and countries the profile was operating in, and then send specific payloads to the onion service / profile and using the global adversary's entire internal internet surveillance to try and observe the payload leaving the entry guard node and going to the target computer via its ISP.

Even with all these resources these attacks take time and a substantial amount of resources (currently). Tor is continuing to work to mitigate these style of attacks with various mechanisms including in investing in entry guard node rotation techniques and other approaches.






