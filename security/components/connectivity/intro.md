---
sidebar_position: 3
---


# Connectivity

Cwtch makes use of Tor Onion Services (v3) for all inter-node communication.

We provide the [openprivacy/connectivity](https://git.openprivacy.ca/openprivacy/connectivity)
package for managing the Tor daemon and setting up and tearing down onion
services through Tor.

## Known Risks

### Private Key Exposure to the Tor Process

**Status:  Partially Mitigated** (Requires Physical Access or Privilege Escalation to
exploit)

We must pass the private key of any onion service we wish to set up to the
connectivity library, through the `Listen` interface (and thus to the Tor
process). This is one of the most critical areas that is outside of our
control. Any binding to a rouge tor process or binary will result in
compromise of the Onion private key.

### Mitigations

Connectivity attempt to bind to the system-provided Tor process as the default,
*only* when it has been provided with an authentication token.

Otherwise connectivity always attempts to deploy its own Tor process
using a known
good binary packaged with the system (outside of the scope of the connectivity
package)

In the long term we hope an integrated library will become available and allow
direct management through an in-process interface to prevent the private key
from leaving the process boundary (or other alternative paths that allow us
to maintain full control over the private key in-memory.)

### Tor Process Management

**Status: Partially Mitigated** (Requires Physical Access or Privilege
Escalation to exploit)

Many issues can arise from the management of a separate process, including the
need to restart, exit and otherwise ensure appropriate management.

The [ACN](https://git.openprivacy.ca/openprivacy/connectivity/src/branch/master/acn.go)
interface provides `Restart`, `Close` and `GetBootstrapStatus` interfaces to
allow applications to manage the underlying Tor process. In addition the `SetStatusCallback`
method can be used to allow an application to be notified when the status of
the Tor process changes.

However, if sufficiently-privileged users wish they can interfere with this
mechanism, and as such the Tor process is a more brittle component
interaction than others.

## Testing Status

Current connectivity has limited unit testing capabilities and none of these
are run during pull requests or merges. There is no integration testing.

It is worth noting that connectivity is used by both Tapir and Cwtch in their
integration tests (and so despite the lack of package level testing, it is
exposed to system-wide test conditions)

