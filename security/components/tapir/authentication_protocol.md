---
sidebar_position: 2
---

# Authentication Protocol

Each peer, given an open connection `$C$`:
```
$$
I = \mathrm{InitializeIdentity()} \\
I_e = \mathrm{InitializeEphemeralIdentity()}  \\
I,I_e \rightarrow C \\
P,P_e \leftarrow C \\
k = \mathrm{KDF}({P_e}^{i} + {P}^{i_e} + {P_e}^{i_e}) \\
c = \mathrm{E}(k, transcript.Commit()) \\
c \rightarrow C \\
c_p \leftarrow C\\
\mathrm{D}(k, c_p) \stackrel{?}{=} transcript.LatestCommit()
$$
```
The above represents a sketch protocol, in reality there are a few
 implementation details worth pointing out:
 
Once derived from the key derivation function `($\mathrm{KDF}$)` the key
 `($k$)` is set *on* the connection, meaning the authentication app doesn't
 do the encryption or decryption explicitly.
 
The concatenation of parts of the 3DH exchange is strictly ordered:

* DH of the Long term identity of the outbound connection by the ephemeral
 key of the inbound connection.
* DH of the Long term identity of the inbound connection by the ephemeral
key of the outbound connection.
* DH of the two ephemeral identities of the inbound and outbound connections.

This strict ordering ensures both sides of the connection derive the *same*
session key. 

### Cryptographic Properties

During an online-session, all messages encrypted with the session key can be authenticated by the peers as having come
from their peer (or at least, someone with possession of their peers secret key as it related to their onion address).

Once the session has ended, a transcript containing the long term and ephemeral public keys, a derived session key and
all encrypted messages in the session cannot be proven to be authentic i.e. this protocol provides message & participant
repudiation (offline deniable) in addition to message unlinkability (offline deniable) in the case where someone is satisfied
that a single message in the transcript must have originated from a peer, there is no way of linking any other message to
the session.

Intuition for the above: the only cryptographic material related to the transcript is the derived session key - if the 
session key is made public it can be used to forge new messages in the transcript - and as such, any standalone
transcript is subject to forgery and thus cannot be used to cryptographically tie a peer to a conversation.

