---
sidebar_position: 1
---

# Cwtch Technical Basics

This page presents a brief technical overview of the Cwtch protocol.

## A Cwtch Profile

Users can create one of more Cwtch Profiles. Each profile generates a random ed25519 keypair compatible with
Tor.

In addition to the cryptographic material, a profile also contains a list of Contacts (other Cwtch profile public keys +
associated data about that profile like nickname and (optionally) historical messages), a list of Groups (containing the group cryptographic material in addition to other associated data like the group nickname and historical messages).

## 2-party conversions: Peer to Peer

![](/img/BASE_3.png)

For 2 parties to engage in a peer-to-peer conversation both must be online, but only one needs to be reachable via
their onion service. For the sake of clarity we often label one party the "inbound peer" (the one who hosts the onion service) and the other party the
"outbound peer" (the one that connects to the onion service).

After connection both parties engage in an authentication protocol which:

* Asserts that each party has access to the private key associated with their public identity.
* Generates an ephemeral session key used to encrypt all further communication during the session.

This exchange (documented in further detail in [authentication protocol](tapir/authentication_protocol.md)) is *offline deniable*
i.e. it is possible for any party to forge transcripts of this protocol exchange after the fact, and as such - after the
fact - it is impossible to definitely prove that the exchange happened at all.

After, the authentication protocol the two parties may exchange messages with each other freely.

## Multi-party conversations: Groups and Peer to Server Communication

**Note: Metadata Resistant Group Communication is still an active research area and what is documented here
will likely change in the future.**

When a person wants to start a group conversation they first randomly generate a secret `Group Key`. All group communication will be encrypted using this key.

Along with the `Group Key`, the group creator also decides on a **Cwtch Server** to use as the host of the group.
For more information on how Servers authenticate themselves see [key bundles](cwtch/key_bundles.md).

A `Group Identifier` is generated using the group key and the group server and these three elements are packaged up
into an invite that can be sent to potential group members (e.g. over existing peer-to-peer connections).

To send a message to the group, a profile connects to the server hosting the group (see below), and encrypts
their message using the `Group Key` and generates a cryptographic signature over the `Group Id`, `Group Server`
and the decrypted message (see: [wire formats](cwtch/message_formats.md) for more information).

To receive message from the group, a profile connected to the server hosting the group and downloads *all* messages (since
their previous connection). Profiles then attempt to decrypt each message using the `Group Key` and if successful attempt
to verify the signature (see [Cwtch Servers](cwtch/server.md)  [Cwtch Groups](./cwtch/groups.md) for an overview of attacks and mitigations).

### Servers are Peers

In many respects communication with a server is identical to communication with a regular Cwtch peer,
all the same steps above are taken however the server always acts as the inbound peer, and the outbound
peer always uses newly generated **ephemeral keypair** as their "longterm identity".

As such peer-server conversations only differ in the *kinds* of messages that are sent between the two parties,
with the server relaying all messages that it receives and also allowing any client to query for older messages.

